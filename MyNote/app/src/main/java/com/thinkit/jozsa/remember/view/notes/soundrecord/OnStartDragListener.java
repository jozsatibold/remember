package com.thinkit.jozsa.remember.view.notes.soundrecord;


import android.support.v7.widget.RecyclerView;

public interface OnStartDragListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}