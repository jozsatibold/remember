package com.thinkit.jozsa.remember.view.notes.smart;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.analyzer.NoteAnalyzer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.GONE;

class SmartChipAdapter extends RecyclerView.Adapter<SmartChipAdapter.SmartHolder> {


    private ColorStateList mSelectedColor;
    private ColorStateList mNormalColor;
    private List<String> mSmartList;
    private List<Integer> mSmartListPos;
    private int mType = -1;
    private SparseBooleanArray mSelected;
    private Drawable mLeftDrawable;
    private Drawable mRightDrawable;
    private Context mContext;
    private SmartLongClickListener mLongClickListener;

    SmartChipAdapter(Context context, List<String> smartList,List<Integer> smartListPos, int type, int primary) {
        mContext = context;
        mSmartListPos = smartListPos;
        mSmartList = smartList;
        mSelectedColor = ColorStateList.valueOf(primary);
        mNormalColor = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.draw_white));
        mType = type;
        mSelected = new SparseBooleanArray();
        switch (type) {
            case 0:
                mLeftDrawable = context.getDrawable(R.drawable.ic_call_black_24dp).mutate();
                mRightDrawable = context.getDrawable(R.drawable.ic_message_black_24dp).mutate();
                break;
            case 1:
                mLeftDrawable = context.getDrawable(R.drawable.ic_email_black_24dp).mutate();
                break;
            case 2:
                mLeftDrawable = context.getDrawable(R.drawable.ic_open_in_browser_black_24dp).mutate();
                break;
            case 4:
                mLeftDrawable = context.getDrawable(R.drawable.ic_place_black_24dp).mutate();
                break;
            case 5:
                mLeftDrawable = context.getDrawable(R.drawable.ic_event_grey_24dp).mutate();
                break;
        }
    }
    public void setSmartLongClickListener(SmartLongClickListener listener){
        mLongClickListener = listener;
    }

    public SmartLongClickListener getSmartLongClickListener() {
        return mLongClickListener;
    }

    @Override
    public SmartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SmartHolder(LayoutInflater.from(parent.getContext()).inflate(mType == 3 ? R.layout.item_chip_large : R.layout.item_chip_small, parent, false));
    }

    @Override
    public void onBindViewHolder(final SmartHolder holder, int position) {
        String text = mSmartList.get(holder.getAdapterPosition());
        if (mType == 3) {
            try {
                JSONObject obj = new JSONObject(text);
                int card = Integer.parseInt(obj.get("card").toString());
                holder.mText.setText(card >= NoteAnalyzer.mCardNames.length ? "Other" : NoteAnalyzer.mCardNames[card] + "\n" + obj.get("number"));
            } catch (JSONException e) {
                Log.w("SmarChip JSON parse", e.toString());
            }
        } else {
            holder.mText.setText(text);
        }
        boolean selected = mSelected.get(holder.getAdapterPosition());
        if (mType == 0) {
            holder.mRightSeparator.setBackgroundColor(!selected ? mSelectedColor.getDefaultColor() : mNormalColor.getDefaultColor());
            holder.mRightImage.setColorFilter(!selected? mSelectedColor.getDefaultColor() : mNormalColor.getDefaultColor());
            holder.mRightImage.setImageDrawable(mRightDrawable.mutate());
        }
        if (mType != 3) {
            holder.mLeftImage.setImageDrawable(mLeftDrawable.mutate());
            holder.mLeftImage.setColorFilter(!selected ? mSelectedColor.getDefaultColor() : mNormalColor.getDefaultColor());
            holder.mLeftSeparator.setBackgroundColor(!selected ? mSelectedColor.getDefaultColor() : mNormalColor.getDefaultColor());
        }
        holder.mContentBackground.setBackgroundTintList(selected ? mSelectedColor : mNormalColor);
        holder.mText.setTextColor(!selected ? mSelectedColor : mNormalColor);
        holder.mChipBackground.setBackgroundTintList(mSelectedColor);

    }


    @Override
    public int getItemCount() {
        return mSmartList.size();
    }

    List<String> getSelectedList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < getItemCount(); ++i) {
            if (mSelected.get(i)) {
                if (mType == 3) {
                    try {
                        JSONObject obj = new JSONObject(mSmartList.get(i));
                        list.add(obj.get("number").toString());
                    } catch (JSONException e) {
                        Log.w("SmartChipAdapterList", e.toString());
                    }
                } else {
                    list.add(mSmartList.get(i));
                }
            }
        }
        return list;
    }

    public int setSelected(int pos) {
        if (pos >= 0 && pos < getItemCount()) {
            mSelected.put(pos, !mSelected.get(pos));
            notifyItemChanged(pos);
            return pos;
        }
        return 0;
    }

    public void setColor(int primary) {
        mSelectedColor = ColorStateList.valueOf(primary);
        notifyDataSetChanged();
    }

    class SmartHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mText;
        View mChipBackground;
        View mContentBackground;
        View mLeftSeparator;
        View mRightSeparator;
        ImageView mLeftImage;
        ImageView mRightImage;

        SmartHolder(View v) {
            super(v);
            mText = (TextViewTypeface) v.findViewById(R.id.smart_chip_text);
            mChipBackground = v.findViewById(R.id.smart_chip_background);
            mContentBackground = v.findViewById(R.id.smart_content_background);
            mLeftImage = (ImageView) v.findViewById(R.id.smart_left_icon);
            mRightImage = (ImageView) v.findViewById(R.id.smart_right_icon);
            mLeftSeparator = v.findViewById(R.id.smart_left_separator);
            mRightSeparator = v.findViewById(R.id.smart_right_separator);
            if (mType != 0) {
                mRightSeparator.setVisibility(GONE);
                mRightImage.setVisibility(GONE);
            }
            if (mType == 3) {
                mLeftSeparator.setVisibility(GONE);
                mLeftImage.setVisibility(GONE);
            }
            mText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelected(getAdapterPosition());
                }
            });
            mRightImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent action = getIntent(false);
                    if (action != null) {
                        mContext.startActivity(action);
                    }
                }
            });
            mLeftImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent action = getIntent(true);
                    if (action != null) {
                        mContext.startActivity(action);
                    }
                }
            });
            mText.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(mLongClickListener != null){
                        if(mType == 3){
                            try {
                                JSONObject obj = new JSONObject(mSmartList.get(getAdapterPosition()));
                                mLongClickListener.onLongClickSmartChip(mSmartListPos.get(getAdapterPosition()), obj.get("number").toString(), v);
                            } catch (JSONException e){
                                Log.w("SmartLongClick JSON", e.toString());
                            }
                        } else {
                            mLongClickListener.onLongClickSmartChip(mSmartListPos.get(getAdapterPosition()), mSmartList.get(getAdapterPosition()), v);
                        }
                    }
                    return false;
                }
            });
        }

        private Intent getIntent(boolean left) {
            String uriText = mSmartList.get(getAdapterPosition());
            switch (mType) {
                case 0:
                    String phoneNumber = "";
                    for (char c : uriText.toCharArray()) {
                        if ((c >= '0' && c <= '9') || c == '+') {
                            phoneNumber += c;
                        }
                    }
                    return new Intent(left ? Intent.ACTION_DIAL : Intent.ACTION_VIEW).setData(Uri.parse((left ? "tel:" : "smsto:") + phoneNumber));
                case 1:
                    return new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto:")).putExtra(Intent.EXTRA_EMAIL, uriText);
                case 2:
                    return new Intent(Intent.ACTION_VIEW).setData(Uri.parse(!uriText.startsWith("http") ? "http://" + uriText : uriText));
                case 4:
                    return new Intent(Intent.ACTION_VIEW).setData(Uri.parse("geo:0,0?q=" + uriText.replaceAll("[ ]", "+")));
                case 5:
                    return new Intent(Intent.ACTION_INSERT).setType("vnd.android.cursor.item/event").putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, getDateFromString(uriText));
            }
            return null;
        }

        private long getDateFromString(String data) {
            Matcher matcher = Pattern.compile("\\d{4}").matcher(data);
            Calendar date = new GregorianCalendar();
            if (matcher.find()) {
                date.set(Calendar.YEAR, Integer.parseInt(matcher.group()));
            }
            matcher = Pattern.compile("[a-zA-Z]{3,12}").matcher(data);
            if (matcher.find()) {
                date.set(Calendar.MONTH, NoteActivity.getMonth(matcher.group()));
                matcher = Pattern.compile("[0-9]{1,2}").matcher(data);
                if (matcher.find()) {
                    date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(matcher.group()));
                }
            } else {
                matcher = Pattern.compile("[0-9]{1,2}").matcher(data);
                int day = -1;
                int month = -1;
                while (matcher.find()) {
                    if (day == -1) {
                        day = Integer.parseInt(matcher.group());
                    } else {
                        month = Integer.parseInt(matcher.group());
                    }
                }
                if (day != -1 && month != -1) {
                    int smaller = day < month ? day : month;
                    if (smaller < 12) {
                        date.set(Calendar.MONTH, smaller - 1);
                        date.set(Calendar.DAY_OF_MONTH, smaller == day ? month : day);
                    }
                }
            }
            return date.getTimeInMillis();
        }
    }
}

