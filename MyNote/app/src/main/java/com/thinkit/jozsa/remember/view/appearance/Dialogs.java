package com.thinkit.jozsa.remember.view.appearance;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;
import com.thinkit.jozsa.remember.view.notes.ImageLabelDrawableAdapter;
import com.thinkit.jozsa.remember.view.notes.imagelist.ImageAdapter;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.notes.webimage.WebImageAdapter;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;
import com.thinkit.jozsa.remember.view.web.WebPageImages;

import java.util.ArrayList;
import java.util.List;

public class Dialogs {

    private static boolean isExist(List<Label> list, String text) {
        for (Label label : list) {
            if (label.getName().equals(text)) {
                return true;
            }
        }
        return false;
    }

    public static void dialocEditLabel(final LabelRecyclerDialogHelper chipAdapter, final Label label, final Context context){
        final Dialog dialogUri = new Dialog(context);
        dialogUri.setContentView(R.layout.dialog_add_label);
        if(label != null) {
            ((TextView) dialogUri.findViewById(R.id.label_dialog_title)).setText(context.getString(R.string.dialog_edit_label));
        }
        dialogUri.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUri.dismiss();
            }
        });
        final TextInputEditText editTextName = (TextInputEditText) dialogUri.findViewById(R.id.dialog_edit_name);
        RecyclerView recyclerImages = (RecyclerView) dialogUri.findViewById(R.id.recycle_label_images);
        recyclerImages.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerImages.setHasFixedSize(true);
        final ImageLabelDrawableAdapter adapter = new ImageLabelDrawableAdapter(context);
        if(label != null) {
            editTextName.setText(label.getName());
            adapter.setSelected(label.getDrawable() - 4);
        }
        adapter.setOnClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                adapter.setSelected(pos);
            }
        });
        recyclerImages.setAdapter(adapter);
        editTextName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (editTextName.getText().length() == 0) {
                        ((TextInputLayout) dialogUri.findViewById(R.id.label_text_input_layout)).setError(context.getString(R.string.error_empty_text));
                    } else if (isExist(chipAdapter.getLabels(), editTextName.getText().toString()) && (label == null || !editTextName.getText().toString().equals(label.getName()))) {
                        ((TextInputLayout) dialogUri.findViewById(R.id.label_text_input_layout)).setError(context.getString(R.string.error_label_exits));
                    } else {
                        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        if(label != null) {
                            chipAdapter.update(new Label(label.getId(), editTextName.getText().toString(), adapter.getSelectedPos() + 4));
                        } else {
                            chipAdapter.addLabel(editTextName.getText().toString(), adapter.getSelectedPos() + 4);
                        }
                        dialogUri.dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        dialogUri.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextName.getText().length() == 0) {
                    ((TextInputLayout) dialogUri.findViewById(R.id.label_text_input_layout)).setError(context.getString(R.string.error_empty_text));
                } else if (isExist(chipAdapter.getLabels(), editTextName.getText().toString()) && (label == null || !editTextName.getText().toString().equals(label.getName()))) {
                    ((TextInputLayout) dialogUri.findViewById(R.id.label_text_input_layout)).setError(context.getString(R.string.error_label_exits));
                } else {
                    ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    if(label != null) {
                        chipAdapter.update(new Label(label.getId(), editTextName.getText().toString(), adapter.getSelectedPos() + 4));
                    } else {
                        chipAdapter.addLabel(editTextName.getText().toString(), adapter.getSelectedPos() + 4);
                    }
                    dialogUri.dismiss();
                }
            }
        });
        dialogUri.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialogUri.show();
    }
    public static void dialogSimpleQuestion(final Context context, String title, String content, String negative, String positive, final DialogSimpleResponse response){
        final AlertDialog simpleDialog = new AlertDialog.Builder(context).create();
        if(!title.isEmpty()){
            simpleDialog.setTitle(title);
        }
        if(!content.isEmpty()) {
            simpleDialog.setMessage(content);
        }
        simpleDialog.setButton(AlertDialog.BUTTON_POSITIVE, positive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        response.dialogAnswer(true);
                        dialog.dismiss();
                    }
                });
        simpleDialog.setButton(AlertDialog.BUTTON_NEGATIVE, negative, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                response.dialogAnswer(false);
                dialog.dismiss();
            }
        });
        simpleDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                simpleDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
                simpleDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));

            }
        });
        simpleDialog.show();
    }
    public static void dialogCheckPassword(final Context context, SettingsPreferences passwordPreference, final DialogSimpleResponse response){

        final String passwordtxt = passwordPreference.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
        final String remindertxt = passwordPreference.getString("Reminder");
        final Dialog dialogCheckPasswd = new Dialog(context);
        dialogCheckPasswd.setCanceledOnTouchOutside(false);
        dialogCheckPasswd.setContentView(R.layout.dialog_check_password);
        final TextInputEditText password = (TextInputEditText) dialogCheckPasswd.findViewById(R.id.dialog_password);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    try {
                        if (!passwordtxt.equals(DataEncrypt.getMd5(password.getText().toString()))) {
                            ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("Password don't match\n" +
                                    "Reminder: " + remindertxt);
                        } else {
                            ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("");
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                           // imm.hideSoftInputFromWindow(((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).getWindowToken(), 0);
                            response.dialogAnswer(true);
                            dialogCheckPasswd.dismiss();
                        }

                    } catch (Exception e) {
                        Log.w("Dialog Password login", e.toString());
                    }
                    handled = true;
                }
                return handled;
            }
        });
        dialogCheckPasswd.findViewById(R.id.dialog_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                   if (!passwordtxt.equals(DataEncrypt.getMd5(((TextInputEditText) dialogCheckPasswd.findViewById(R.id.dialog_password)).getText().toString()))) {
                        ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("Password don't match\n" +
                                "Reminder: " + remindertxt);
                    } else {
                        ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("");
                        response.dialogAnswer(true);
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                      // imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        dialogCheckPasswd.dismiss();
                    }
                } catch (Exception e) {
                    Log.w("Dialog Password2", e.toString());
                }
            }
        });
        dialogCheckPasswd.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialogCheckPasswd.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                response.dialogAnswer(false);
                dialogCheckPasswd.dismiss();
            }
        });
        dialogCheckPasswd.show();
    }

    public static void showUrlDialog(final Activity activity, final Note note,final DatabaseManager db, final ImageAdapter imgGallery) {
        final Dialog dialogUri = new Dialog(activity);
        dialogUri.setContentView(R.layout.dialog_url);

        dialogUri.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUri.dismiss();
            }
        });
        final TextInputEditText editTextUrl = (TextInputEditText) dialogUri.findViewById(R.id.dialog_edit_url);
        final Button save = (Button) dialogUri.findViewById(R.id.dialog_save);
        editTextUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (URLUtil.isValidUrl(editable.toString())) {
                    save.setEnabled(true);
                    TextInputLayout til = (TextInputLayout) dialogUri.findViewById(R.id.url_text_input_layout);
                    til.setError(null);

                } else {
                    save.setEnabled(false);
                    ((TextInputLayout) dialogUri.findViewById(R.id.url_text_input_layout)).setError("Invalid URL Ex. http://www.example.com");
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUri.dismiss();
                showUrlPictures(editTextUrl.getText().toString(), activity, note, db, imgGallery);
            }
        });

        dialogUri.show();
    }

    private static void showUrlPictures(final String Url, final Activity activity,final Note note,final DatabaseManager db,final ImageAdapter gallery) {

        final Dialog mDialogUriPictures = new Dialog(activity);
        mDialogUriPictures.setContentView(R.layout.dialog_web_pictures);
        final RecyclerView mRecyclePictures= (RecyclerView) mDialogUriPictures.findViewById(R.id.dialog_picture_recycler);
        mRecyclePictures.setAdapter(new WebImageAdapter(new ArrayList<String>(), activity));
        mRecyclePictures.setHasFixedSize(true);
        mRecyclePictures.setLayoutManager(new LinearLayoutManager(activity));
        mDialogUriPictures.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogUriPictures.dismiss();
            }
        });
        mDialogUriPictures.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<String> list = ((WebImageAdapter) mRecyclePictures.getAdapter()).getChecked();
                        if(list.size() > 0) {
                            FileManager fm = new FileManager(activity);
                            for (String url : list) {
                                try {
                                    Point point = new Point();
                                    activity.getWindowManager().getDefaultDisplay().getSize(point);
                                    fm.saveToInternalStorage(Glide.
                                            with(activity).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(point.x, point.y).get(), fm.createImageFile());
                                    note.getImagesList().add(new Picture(fm.getPhotoPath(), 0));
                                    db.updateNote(note, 2, 2);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            gallery.update(note.getImagesList());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, "Pictures downloaded", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }).start();
                mDialogUriPictures.dismiss();
            }
        });
        final WebPageImages imagesUrl= new WebPageImages(Url);
        new Thread(imagesUrl).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (imagesUrl.getImageUrlList() == null) {
                    try {
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDialogUriPictures.findViewById(R.id.dialog_picture_progress).setVisibility(View.GONE);
                        if (imagesUrl.getImageUrlList().size() == 0) {
                            mDialogUriPictures.findViewById(R.id.not_found_images_dialog).setVisibility(View.VISIBLE);
                            mRecyclePictures.setVisibility(View.GONE);
                        } else {
                            mDialogUriPictures.findViewById(R.id.not_found_images_dialog).setVisibility(View.GONE);
                            mRecyclePictures.setVisibility(View.VISIBLE);
                            ((WebImageAdapter)mRecyclePictures.getAdapter()).update(imagesUrl.getImageUrlList());
                            mDialogUriPictures.findViewById(R.id.dialog_save).setEnabled(true);
                        }

                    }
                });
            }
        }).start();


        mDialogUriPictures.show();
    }
}
