
package com.thinkit.jozsa.remember.view.search;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;

import java.util.ArrayList;
import java.util.List;

public class TypeRecyclerAdapter extends RecyclerView.Adapter<TypeRecyclerAdapter.ViewHolder> {


    private SparseBooleanArray mSelected;
    private int mNormalColor;
    private int mBackgroundColor;
    private SelectionListener mListener;
    private String[] mNames;
    private List<Drawable> mDrawables;
    private DatabaseManager mDb;
    private List<Integer> mCategory;

    public TypeRecyclerAdapter(Context context, int normalColor, boolean nightMode) {
        mNormalColor = normalColor;
        mDb = DatabaseManager.getInstance(context);
        mCategory = mDb.getCategorys();
        mNames = context.getResources().getStringArray(R.array.search_note_types);
        mSelected = new SparseBooleanArray();
        MyDrawable drawables = new MyDrawable(context);
        mDrawables = new ArrayList<>();
        for (int i = 0; i < mNames.length; ++i) {
            mDrawables.add(drawables.getTypeDrawable(i));
        }

        mBackgroundColor = !nightMode ? context.getResources().getColor(R.color.draw_white) : context.getResources().getColor(R.color.draw_black);
    }

    public boolean refresh() {
        List<Integer> cat = mDb.getCategorys();
        boolean changed = false;
        for (int i : cat) {
            if (!mCategory.contains(i)) {
                mCategory.add(i);
                mSelected.put(i, false);
                if (!changed) {
                    changed = true;
                }
            }
        }
        for (int i = 0; i < mCategory.size(); ++i) {
            if (!cat.contains(mCategory.get(i))) {
                mCategory.remove(i);
                mSelected.delete(i);
                if (!changed) {
                    changed = true;
                }
            }
        }
        if (changed) {
            notifyDataSetChanged();
        }
        return changed;
    }

    public void setOnClickListener(SelectionListener listener) {
        mListener = listener;
    }

    @Override
    public TypeRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_type, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mBackground.setBackgroundTintList(ColorStateList.valueOf(mNormalColor));
        holder.mText.setBackgroundTintList(mSelected.get(position) ? ColorStateList.valueOf(mNormalColor) : ColorStateList.valueOf(mBackgroundColor));
        holder.mText.setTextColor(mSelected.get(position) ? mBackgroundColor : mNormalColor);
        holder.mText.setText(mNames[mCategory.get(position)]);
        holder.mText.setCompoundDrawablesWithIntrinsicBounds(null, mDrawables.get(mCategory.get(position)), null, null);
        holder.mText.getCompoundDrawables()[1].setColorFilter(mSelected.get(position) ? mBackgroundColor : mNormalColor, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public int getItemCount() {
        return mCategory.size();
    }


    public void setSelected(int pos) {
        mSelected.put(pos, !mSelected.get(pos));
        notifyItemChanged(pos);
    }

    public void removeSelection() {
        for (int i = 0; i < getItemCount(); ++i) {
            if (mSelected.get(i)) {
                mSelected.put(i, false);
                notifyItemChanged(i);
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mText;
        LinearLayout mBackground;

        public ViewHolder(View v, final SelectionListener listener) {
            super(v);
            mText = (TextViewTypeface) v.findViewById(R.id.type_text);
            mBackground = (LinearLayout) v.findViewById(R.id.type_background);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.setSelection(mCategory.get(getAdapterPosition()));
                    TypeRecyclerAdapter.this.setSelected(getAdapterPosition());
                }
            });
        }

    }
}

