package com.thinkit.jozsa.remember.controller.filemanager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.controller.encryption.JavaEncryption;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;


public class ImportData extends AsyncTask<Void, Void, Void> {
    private boolean mError = false;
    private boolean mFinished = false;
    private DatabaseManager mDb;
    private ArrayList<Label> mLabels;
    private File mFile;
    private JSONObject mObj;
    private String mPassword;
    private boolean mLocked = false;
    private ArrayList<Integer> mCurrentl;
    private ArrayList<Integer> mNewl;
    private String mEncrypted = "";

    public ImportData(Context context, File file) {
        mDb = DatabaseManager.getInstance(context);
        mFile = file;
        try {
            mEncrypted = loadJSONFromAsset();


            if (!mEncrypted.contains("\"nlabels\"")) {
                mLocked = true;
            } else {
                mObj = new JSONObject(mEncrypted);
                mFinished = true;
            }
        } catch (Exception e) {
            Log.w("Password check", e.toString());
        }
    }

    protected Void doInBackground(Void... s) {

        if (mFinished) {
            mFinished = false;
            try {

                JSONArray nlabels = mObj.getJSONArray("nlabels");
                ArrayList<Note> notes = new ArrayList<>();
                mCurrentl = new ArrayList<>();
                mNewl = new ArrayList<>();
                mNewl.add(0);
                mNewl.add(1);
                mNewl.add(2);
                mLabels = mDb.getLabels();
                if (mPassword != null && !mPassword.isEmpty()) {
                    for (int i = 0; i < nlabels.length(); ++i) {
                        JSONObject label = nlabels.getJSONObject(i);
                        String lb = JavaEncryption.decrypt(label.getString("labelname"), mPassword);
                        int dw = label.getInt("labeldrawable");
                        mCurrentl.add(label.getInt("id"));
                        int d = contain(lb);
                        if(dw >= 38){
                            dw = 4;
                        }
                        mNewl.add(d > -1 ? d : mDb.addLabel(lb, dw));
                    }
                    JSONArray nnotes = mObj.getJSONArray("nnotes");
                    for (int i = 0; i < nnotes.length(); ++i) {
                        JSONObject note = nnotes.getJSONObject(i);
                        notes.add(decryptedNode(note));
                    }

                }else {
                    for (int i = 0; i < nlabels.length(); ++i) {
                        JSONObject label = nlabels.getJSONObject(i);
                        mCurrentl.add(label.getInt("id"));
                        int d = contain(label.getString("labelname"));
                        int dw = label.getInt("labeldrawable");
                        if(dw >= 38){
                            dw = 4;
                        }
                        mNewl.add(d > -1 ? d : mDb.addLabel(label.getString("labelname"), dw));
                    }
                    JSONArray nnotes = mObj.getJSONArray("nnotes");
                    for (int i = 0; i < nnotes.length(); ++i) {
                        JSONObject note = nnotes.getJSONObject(i);
                        notes.add(simple(note));
                    }
                }
                mDb.insertNoteList(notes);
                mFinished = true;
            } catch (Exception e) {
                Log.w("Export JSON", e.toString());
                mError = false;
            }
        }
        return null;
    }

    public boolean isFinshed() {
        return mFinished;
    }

    public boolean getError() {

        return mError;
    }

    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = new FileInputStream(mFile);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    public boolean isLocked(){
        return mLocked;
    }
    public boolean isOk(String password) throws Exception {
        if(JavaEncryption.decrypt(mEncrypted, password).contains("\"nlabels\"")){
            mPassword = password;
            mObj = new JSONObject(JavaEncryption.decrypt(mEncrypted, password));
            mFinished = true;
            return true;
        } else {
            return false;
        }
    }
    private Note simple(JSONObject note) throws Exception{
        Note cnote = new Note();
        cnote.setTitle(note.getString("title"));
        cnote.setTypeId(note.getInt("typeid"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(note.getLong("createDate"));
        cnote.setCreatingDate(calendar);
        cnote.setLastModification(note.getString("lastModification"));
        cnote.setBgColor(note.getInt("bgColor"));
        cnote.setStaticPosition(note.getInt("staticPosition"));
        cnote.setNumberOfView(note.getInt("numberOfView"));
        cnote.setCategory(note.getLong("category"));
        int label = note.getInt("label");
        int t = 0;
        while (t < mCurrentl.size() && mCurrentl.get(t) != label) {
            ++t;
        }
        cnote.setLabel(mNewl.get(t));
        cnote.setSecured(note.getBoolean("secured"));
        if (note.has("noteNote")) {
            JSONObject notenote = note.getJSONObject("noteNote");
            cnote.setText(notenote.getString("notetext"), notenote.getString("actionText"), false);
        }
        if (note.has("noteList")) {
            JSONArray noteList = note.getJSONArray("noteList");
            ArrayList<NoteChecked> checkedList = new ArrayList<>();
            for (int k = 0; k < noteList.length(); ++k) {
                JSONObject checked = noteList.getJSONObject(k);
                checkedList.add(new NoteChecked(checked.getString("label"), checked.getBoolean("checked"), checked.getString("actionText")));
            }
            cnote.setList(checkedList);
        }
        return cnote;
    }

    private Note decryptedNode(JSONObject note) throws Exception{
        Note cnote = new Note();
        cnote.setTitle(JavaEncryption.decrypt(note.getString("title"), mPassword));
        cnote.setTypeId(note.getInt("typeid"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(note.getLong("createDate"));
        cnote.setCreatingDate(calendar);
        cnote.setLastModification(JavaEncryption.decrypt(note.getString("lastModification"), mPassword));
        cnote.setBgColor(note.getInt("bgColor"));
        cnote.setStaticPosition(note.getInt("staticPosition"));
        cnote.setNumberOfView(note.getInt("numberOfView"));
        cnote.setCategory(note.getLong("category"));
        int label = note.getInt("label");
        int t = 0;
        while (t < mCurrentl.size() && mCurrentl.get(t) != label) {
            ++t;
        }
        cnote.setLabel(mNewl.get(t));
        cnote.setSecured(note.getBoolean("secured"));
        if (note.has("noteNote")) {
            JSONObject notenote = note.getJSONObject("noteNote");
            cnote.setText(JavaEncryption.decrypt(notenote.getString("notetext"), mPassword), JavaEncryption.decrypt(notenote.getString("actionText"), mPassword), false);
        }
        if (note.has("noteList")) {
            JSONArray noteList = note.getJSONArray("noteList");
            ArrayList<NoteChecked> checkedList = new ArrayList<>();
            for (int k = 0; k < noteList.length(); ++k) {
                JSONObject checked = noteList.getJSONObject(k);
                checkedList.add(new NoteChecked(JavaEncryption.decrypt(checked.getString("label"), mPassword), checked.getBoolean("checked"), JavaEncryption.decrypt(checked.getString("actionText"), mPassword)));
            }
            cnote.setList(checkedList);
        }
        return cnote;
    }

    private int contain(String name){
        for(Label lb: mLabels){
            if(lb.getName().equals(name)){
                return lb.getId();
            }
        }
        return -1;
    }
}
