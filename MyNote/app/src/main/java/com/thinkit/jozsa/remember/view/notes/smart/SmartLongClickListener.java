package com.thinkit.jozsa.remember.view.notes.smart;


import android.view.View;

public interface SmartLongClickListener {

    void onLongClickSmartChip(int item, String text, View view);
}
