package com.thinkit.jozsa.remember.view.appearance;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TableLayout;

import com.thinkit.jozsa.remember.R;


public class CustomTableLayout extends TableLayout {
    private Animations mAnim;
    private Context mContext;
    public CustomTableLayout(Context context){
        super(context);
        mAnim = new Animations();
        mContext = context;

    }
    public CustomTableLayout(Context context, AttributeSet attrs){
        super(context, attrs);
        mAnim = new Animations();
        mContext = context;
    }
    public void show(){
        if(getVisibility() != VISIBLE) {
            Animation animator = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_animation);
            // animator.setFillAfter(true);
            animator.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation paramAnimation) {
                    CustomTableLayout.this.setVisibility(View.GONE);
                }

                public void onAnimationRepeat(Animation paramAnimation) {
                }

                public void onAnimationEnd(Animation paramAnimation) {
                    CustomTableLayout.this.setVisibility(View.VISIBLE);
                }
            });
            CustomTableLayout.this.startAnimation(animator);
        }
    }
    public void showOrHider(){
        if(this.getVisibility() == VISIBLE){
            hide();
        } else {
            show();
        }
    }
    public void hide(){
        if(getVisibility() == VISIBLE) {
            Animation animator = AnimationUtils.loadAnimation(mContext, R.anim.slide_up_animation);
            // animator.setFillAfter(true);
            animator.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation paramAnimation) {
                    CustomTableLayout.this.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation paramAnimation) {
                }

                public void onAnimationEnd(Animation paramAnimation) {
                    CustomTableLayout.this.setVisibility(View.GONE);
                }
            });
            CustomTableLayout.this.startAnimation(animator);
        }
    }

}
