package com.thinkit.jozsa.remember.view.notes.soundrecord;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Environment;

import com.thinkit.jozsa.remember.controller.filemanager.FileManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SoundRecorder {

    private Context mContext;
    private MediaRecorder myAudioRecorder;
    private String mPath = "";
    private String mPrivatePath;
    public SoundRecorder(Context context){
        mContext = context;
        mPrivatePath = context.getExternalFilesDir(Environment.DIRECTORY_PODCASTS) + "/REC_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".3gp";
    }

    public void refresh(){
        mPrivatePath = mContext.getExternalFilesDir(Environment.DIRECTORY_PODCASTS) + "/REC_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".3gp";
    }

    public void start(){
        if(new File(mPrivatePath).exists()){
            new File(mPrivatePath).delete();
        }
        myAudioRecorder=new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_WB);
        mPath = mPrivatePath;
        myAudioRecorder.setOutputFile(mPath);
        prepare();
        try {
            myAudioRecorder.start();
        }

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void stop(){
        myAudioRecorder.stop();
        release();
        mPath = "";
    }
    private void release(){
        myAudioRecorder.release();
    }
    public void delete(){
        new FileManager(mContext).deleteFile(mPrivatePath);
    }
    public String getPath(){
        return mPath;

    }
    private void prepare(){
        try {
            myAudioRecorder.prepare();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    public String getRealPath(){
        return "file:" + mPrivatePath;
    }
}
