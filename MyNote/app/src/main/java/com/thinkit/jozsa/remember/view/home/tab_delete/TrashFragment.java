package com.thinkit.jozsa.remember.view.home.tab_delete;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.home.adapters.NoteItemTouchHelperCallback;
import com.thinkit.jozsa.remember.view.home.adapters.SwipeListener;
import com.thinkit.jozsa.remember.view.home.card.CardItemDecorationNormal;
import com.thinkit.jozsa.remember.view.home.DataUpdateListener;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;

import java.util.HashMap;
import java.util.List;


public class TrashFragment extends Fragment implements DataUpdateListener {

    private boolean mSelectToDelete = false;
    private RecyclerView mNoteRecycler;
    private NotesAdapter mNoteAdapter;
    private SelectionListener mSelectionListener;
    private boolean mSingle = false;
    private View mPlaceholder;
    private CardItemDecorationNormal mCardDecoration;
    private SwipeListener mSwipeListener;

    public TrashFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNoteAdapter = new NotesAdapter(true, 0, -1, this.getContext(), 2);
        mNoteAdapter.setShowListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                showPlaceholder();
            }
        });
    }

    public void setSwipeListener(SwipeListener listener){
        mSwipeListener = listener;
        if(mNoteAdapter != null){
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity) context).registerDataUpdateListener(this);
    }
    @Override
    public void refreshView(){
        if (mSingle != new SettingsPreferences(getContext(), LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean("SingleColumns")) {
            mSingle = !mSingle;
            mCardDecoration.setMultiColumn(mSingle);
            mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
            mNoteRecycler.setAdapter(this.mNoteAdapter);
        }
    }

    @Override
    public void refresh() {
        mNoteAdapter.refresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) this.getActivity()).unregisterDataUpdateListener(this);
    }

    @Override
    public int getPageNumber() {
        return 2;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trash, container, false);
    }

    @Override
    public void setSelection(SelectionListener selectionListener) {
        mSelectionListener = selectionListener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mSwipeListener != null){
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
        mNoteAdapter.setNoteClickListener(new NoteClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                if (!mSelectToDelete && pos > -1) {
                    Intent intent = new Intent(getActivity(), NoteActivity.class);
                    intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNoteAdapter.getItemAtPosition(pos).getID());
                    getActivity().startActivityForResult(intent, MainActivity.ACTIVITY_RESULT_NOTE_CLICK);
                } else {
                    mNoteAdapter.setSelected(pos);
                    if (mNoteAdapter.getNumberSelected() == 0) {
                        mSelectToDelete = false;
                        mSelectionListener.setSelection(-1);
                    }
                }
            }

            @Override
            public boolean onNoteLongClicked(int position) {
                if (!mSelectToDelete) {
                    mSelectToDelete = true;
                    mSelectionListener.setSelection(2);
                }
                mNoteAdapter.setSelected(position);
                return true;
            }
        });

        mNoteRecycler = (RecyclerView) view.findViewById(R.id.note_recycler);
        mSingle = new SettingsPreferences(getContext(), LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean("SingleColumns");
        mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
        mNoteRecycler.setAdapter(mNoteAdapter);
        NoteItemTouchHelperCallback callback = new NoteItemTouchHelperCallback(mNoteAdapter);
        callback.setLongPress(false);
        callback.setLeftSwipe(true);
        callback.setRightSwipe(false);
        callback.setSwipe(true);
        new ItemTouchHelper(callback).attachToRecyclerView(mNoteRecycler);
        mCardDecoration = new CardItemDecorationNormal(mSingle);
        mNoteRecycler.addItemDecoration(mCardDecoration);
        mNoteRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        mPlaceholder = view.findViewById(R.id.empty_placeholder);
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void removeSelection() {
        mSelectToDelete = false;
        mNoteAdapter.removeSelection();
    }

    @Override
    public void update(int noteID, int to, int from) {
        if (mNoteAdapter != null) {
            if (to == from) {
                mNoteAdapter.updateNote(noteID, getContext());
            } else {
                mNoteAdapter.removeElement(noteID);
                showPlaceholder();
            }
        }
    }

    @Override
    public void makeCopy() {
        mNoteAdapter.makeCopy(getContext());
    }

    @Override
    public void showPlaceholder() {
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        } else {
            mNoteRecycler.setVisibility(View.VISIBLE);
            mPlaceholder.setVisibility(View.GONE);
        }
    }

    public HashMap<Integer, Integer> restore() {
        mSelectToDelete = false;
        return mNoteAdapter.restore();
    }

    @Override
    public List<Integer> getUndoList(){
        return mNoteAdapter.getUndoIds();
    }

    @Override
    public List<Integer> getSelectedList(int type, boolean delete) {
        return null;
    }

    public void deleteForever(Context context) {
        mSelectToDelete = false;
        mNoteAdapter.delete(context);
        showPlaceholder();
    }

    @Override
    public void addNote(int noteID) {
        if (mNoteAdapter != null) {
            mNoteAdapter.addNote(noteID);
            showPlaceholder();
        }
    }

    @Override
    public void remove(int id){
        mNoteAdapter.removeNote(id);
    }
}
