package com.thinkit.jozsa.remember.view.home.card;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thinkit.jozsa.remember.R;


/**
 * Decoration class for {@link RecyclerView}
 */
public class CardItemDecorationNormal extends RecyclerView.ItemDecoration {

    private boolean mSingleColumn;

    public CardItemDecorationNormal(boolean singe) {
        mSingleColumn = singe;
    }

    public void setMultiColumn(boolean multiColumn) {
        mSingleColumn = multiColumn;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int margin = view.getResources().getDimensionPixelSize(R.dimen.half_content_padding);
        // last position
        int position = parent.getChildAdapterPosition(view);

        if(mSingleColumn){
            if (position == 0) {
                outRect.set(0, margin, 0, 0);
            } else if (position == state.getItemCount() - 1) {
                outRect.set(0, 0, 0, margin);
            }
        } else {
            if (position < 2) {
                //first
                outRect.set(0, margin, 0, 0);
            }
        }

    }
}
