package com.thinkit.jozsa.remember.view.home.adapters;

public interface NoteItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    int getItemCount();

    void leftSwipe(int position);

    void rightSwipe(int position);
}