package com.thinkit.jozsa.remember.view.home.card;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;


public class NoteCardView extends CardView {

    public TextViewTypeface mTime;
    public TextViewTypeface mTitle;
    public TextViewTypeface mContent;
    public TextViewTypeface mRecord;
    public TextViewTypeface mDots;
    public ImageView mImage;
    public LinearLayout mList;
    private Context mContext;

    public NoteCardView(Context context) {
        super(context);
        mContext = context;
        initialize();
    }

    public NoteCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initialize();
    }

    public NoteCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initialize();
    }
    //endregion

    private void initialize() {
        inflate(mContext, R.layout.item_note, this);
        mTime = (TextViewTypeface) findViewById(R.id.note_time);
        mTitle = (TextViewTypeface) findViewById(R.id.note_title);
        mContent = (TextViewTypeface) findViewById(R.id.note_content);
        mImage = (ImageView) findViewById(R.id.note_image);
        mDots = (TextViewTypeface) findViewById(R.id.note_dots);
        mList = (LinearLayout) findViewById(R.id.note_list);
        mRecord = (TextViewTypeface) findViewById(R.id.note_record);
        setRadius(getResources().getDimension(R.dimen.half_content_padding));
        setPreventCornerOverlap(true);
        setUseCompatPadding(true);

    }
}
