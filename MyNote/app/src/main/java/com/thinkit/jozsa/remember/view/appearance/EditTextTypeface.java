package com.thinkit.jozsa.remember.view.appearance;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.thinkit.jozsa.remember.R;


public class EditTextTypeface extends AppCompatEditText {
    public EditTextTypeface(Context context) {
        super(context);
        applyCustomFont(context, null);
    }
    public EditTextTypeface(Context context, boolean value) {
        super(context);
    }

    public EditTextTypeface(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextTypeface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.TextViewTypeface);

        String fontName = attributeArray.getString(R.styleable.TextViewTypeface_fontFamily);
        Typeface customFont = TypeFaceCache.selectTypeface(context, fontName);
        if(customFont == null){
            customFont = TypeFaceCache.selectTypeface(context, "Roboto-Black");
        }
        setTypeface(customFont);
        attributeArray.recycle();
    }
}
