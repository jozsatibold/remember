package com.thinkit.jozsa.remember.view.search;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;


import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.home.card.CardItemDecorationNormal;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.analyzer.NoteAnalyzer;

import java.util.ArrayList;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static com.thinkit.jozsa.remember.view.home.MainActivity.ACTIVITY_NOTE_ON_CLICK;
import static com.thinkit.jozsa.remember.view.home.MainActivity.ACTIVITY_RESULT_NOTE_CLICK;

public class SearchActivity extends AppCompatActivity {
    public static final String RESULT_SEARCH_UPDATE = "RESULT_SEARCH_UPDATE_333";
    public static final String RESULT_SEARCH_NOTES = "RESULT_SEARCH_NOTES_555";
    public static final String RESULT_SEARCH_LABEL = "RESULT_SEARCH_LABEL_666";
    public static final String MESSAGE_ARCHIVE = "RESULT_SEARCH_ARCHIVE_444";
    private EditText mSearchBox;
    private String mSearch = "";
    private long mSQLTypes = 0;
    private String mSQLLabel = "";
    private String mSQLBackgroundColor = "";
    private SparseBooleanArray mTypes;
    private ArrayList<String> mNoteResult;
    private View mSearchPanel;
    private boolean mVisible = true;
    private boolean mUpdate = false;
    private boolean mUpdateChip = false;
    private Animations mAnimation;
    private AnimatedVectorDrawable mAnimArrowUp;
    private AnimatedVectorDrawable mAnimArrowDown;
    private SettingsPreferences mSettings;
    private TypeRecyclerAdapter mTypeAdapter;
    private SearchNotesAdapter mNoteAdapter;
    private SearchLabelChipAdapter mLabelAdapter;
    private GridLayoutManager mTypeLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setSupportActionBar((Toolbar) findViewById(R.id.search_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mTypes = new SparseBooleanArray();

        mSettings = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);


        int normal = mSettings.getInt(LoadingActivity.PREFERENCE_TOOLBAR);

        RecyclerView backgroundColorRecycler = (RecyclerView) findViewById(R.id.background_color_recycler);
        backgroundColorRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        final ColorRecyclerAdapter backgroundColor = new ColorRecyclerAdapter(this, new Colors(this).getColorList(3));
        backgroundColorRecycler.setAdapter(backgroundColor);
        backgroundColor.setOnClickListener(new SelectionListener() {
            @Override
            public void setSelection(int pos) {
                if (pos > 0) {
                    mSQLBackgroundColor = " BgColor = " + (pos - 1);
                } else {
                    mSQLBackgroundColor = "";
                }
                backgroundColor.setSelected(pos);
                refreshSearch();
            }
        });

        RecyclerView labelRecycler = (RecyclerView) findViewById(R.id.label_recycler);
        labelRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mLabelAdapter = new SearchLabelChipAdapter(this, normal, false);
        labelRecycler.setAdapter(mLabelAdapter);
        mLabelAdapter.setOnClickListener(new SelectionListener() {
            @Override
            public void setSelection(int pos) {
                if (pos > 0) {
                    mSQLLabel = " NLID = " + mLabelAdapter.getSelectedPos(pos);
                } else {
                    mSQLLabel = "";
                }
                refreshSearch();
            }
        });

        mTypeAdapter = new TypeRecyclerAdapter(this, normal, false);

        RecyclerView typeRecycler = (RecyclerView) findViewById(R.id.types_recycler);
        int orientation = getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE ? 5 : 0;
        mTypeLayoutManager = new GridLayoutManager(this, mTypeAdapter.getItemCount() < 5 + orientation ? 1 : mTypeAdapter.getItemCount() < 8 + orientation ? 2 : 3, LinearLayoutManager.HORIZONTAL, false);
        typeRecycler.setLayoutManager(mTypeLayoutManager);
        typeRecycler.setAdapter(mTypeAdapter);
        View text = findViewById(R.id.search_types_text);
        if (mTypeAdapter.getItemCount() == 0 && text.getVisibility() == View.VISIBLE) {
            text.setVisibility(View.GONE);
        }

        mTypeAdapter.setOnClickListener(new SelectionListener() {
            @Override
            public void setSelection(int pos) {
                mSQLTypes = mSQLTypes + (mTypes.get(pos) ? -NoteAnalyzer.mCategorys[pos] : NoteAnalyzer.mCategorys[pos]);
                mTypes.put(pos, !mTypes.get(pos));
                refreshSearch();
            }
        });
        findViewById(R.id.search_reset).setBackgroundColor(normal);
        findViewById(R.id.search_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSQLBackgroundColor = "";
                mSQLLabel = "";
                mSearch = "";
                mSearchBox.setText("");
                mTypes.clear();
                mSQLTypes = 0;
                mLabelAdapter.setSelected(0);
                mTypeAdapter.removeSelection();
                backgroundColor.setSelected(0);
                refreshSearch();
            }
        });
        mAnimArrowUp = (AnimatedVectorDrawable) getDrawable(R.drawable.animated_arrow_up);
        mAnimArrowDown = (AnimatedVectorDrawable) getDrawable(R.drawable.animated_arrow_down);
    }

    @Override
    public void onEnterAnimationComplete() {
        load();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == ORIENTATION_LANDSCAPE) {
            load();
            int count = mTypeAdapter.getItemCount() < 9 ? 1 : mTypeAdapter.getItemCount() < 14 ? 2 : 3;
            if (mTypeLayoutManager.getSpanCount() != count) {
                mTypeLayoutManager.setSpanCount(count);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            int count = mTypeAdapter.getItemCount() < 5 ? 1 : mTypeAdapter.getItemCount() < 9 ? 2 : 3;
            if (mTypeLayoutManager.getSpanCount() != count) {
                mTypeLayoutManager.setSpanCount(count);
            }
            load();
        }
    }

    private void load() {
        if (mNoteAdapter == null) {
            mNoteAdapter = new SearchNotesAdapter(this);
            RecyclerView noteRecycler = (RecyclerView) findViewById(R.id.search_recycler);
            noteRecycler.setAdapter(mNoteAdapter);
            boolean single = mSettings.getBoolean(LoadingActivity.PREFERENCE_COLUMNS);
            noteRecycler.addItemDecoration(new CardItemDecorationNormal(single));
            noteRecycler.setLayoutManager(single ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
            mNoteAdapter.setNoteClickListener(new SelectionListener() {
                @Override
                public void setSelection(int pos) {
                    if (pos > -1) {
                        Intent intent = new Intent(SearchActivity.this, NoteActivity.class);
                        intent.putExtra(ACTIVITY_NOTE_ON_CLICK, mNoteAdapter.getItemAtPosition(pos).getID());
                        intent.putExtra(MESSAGE_ARCHIVE, mNoteAdapter.getItemAtPosition(pos).getTypeId() == 1 && mSettings.getBoolean("LockArchive"));
                        startActivityForResult(intent, ACTIVITY_RESULT_NOTE_CLICK);
                    }
                }
            });

            mSearchBox = (EditText) findViewById(R.id.search_view);
            mSearchPanel = findViewById(R.id.search_panel);
            mSearchBox.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() >= 0) {
                        mSearch = charSequence.toString();
                        refreshSearch();
                    }
                    if (charSequence.length() >= 0 && mVisible) {
                        showHidePanel();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().length() == 0){
                        mSearch = "";
                        refreshSearch();
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ACTIVITY_RESULT_NOTE_CLICK) {
            mNoteAdapter.updateNote(data.getIntExtra(ACTIVITY_NOTE_ON_CLICK, -1), this);
            mUpdate = true;
            if (mTypeAdapter.refresh()) {
                int count = mTypeAdapter.getItemCount() < 5 ? 1 : mTypeAdapter.getItemCount() < 8 ? 2 : 3;
                if (mTypeLayoutManager.getSpanCount() != count) {
                    mTypeLayoutManager.setSpanCount(count);
                }
                View text = findViewById(R.id.search_types_text);
                if (mTypeAdapter.getItemCount() > 0 && text.getVisibility() != View.VISIBLE) {
                    text.setVisibility(View.VISIBLE);
                } else if (mTypeAdapter.getItemCount() == 0 && text.getVisibility() == View.VISIBLE) {
                    text.setVisibility(View.GONE);
                }
            }
            if (data.getBooleanExtra(NoteActivity.RESULT_LABEL, false)) {
                int count = mLabelAdapter.getItemCount();
                int pos = mLabelAdapter.getSelectedPos(mLabelAdapter.getSelectedPos());
                mLabelAdapter.refresh();
                if (count != mLabelAdapter.getItemCount() && !mUpdateChip) {
                    mUpdateChip = true;
                }
                if (pos != mLabelAdapter.getSelectedPos(mLabelAdapter.getSelectedPos())) {
                    mLabelAdapter.setSelected(0);
                }
            }
            if (mNoteResult == null) {
                mNoteResult = new ArrayList<>();
            }
            int id = data.getIntExtra(ACTIVITY_NOTE_ON_CLICK, -1);
            int copyId = data.getIntExtra(NoteActivity.RESULT_COPY, -1);
            int from = data.getIntExtra(NoteActivity.RESULT_TYPE, -1);
            int to = data.getIntExtra(NoteActivity.RESULT_NOTE, -1);
            if (copyId != -1) {
                mNoteResult.add(from + " " + to + " " + copyId);
            }
            if (id > -1) {
                mNoteResult.add(from + " " + to + " " + id);
            }
            refreshSearch();
        }
    }


    private void refreshSearch() {
        String sqlQuery = "Select NoteId, title, creatingDate, lastModification, reminder, BgColor, secured, numberOfView, NLID, TypeID, staticPosition, category" +
                " FROM Notes ";
        boolean b = false;
        String sqlWhere = "WHERE";
        if (mSQLBackgroundColor.length() > 0) {
            sqlWhere += mSQLBackgroundColor;
            b = true;
        }

        if (mSQLLabel.length() > 0) {
            if (sqlWhere.length() > 5) {
                sqlWhere += " AND";
            }
            sqlWhere += mSQLLabel;
            b = true;
        }
        if (mSQLTypes> 0) {
            sqlWhere += (b ? " AND" : "") +" category >= " + mSQLTypes;
        }
        if (mSearch.replaceAll(" ", "").length() > 0) {
            if (sqlWhere.length() > 5) {
                sqlWhere += " AND";
            }
            sqlWhere += "( NoteID in (Select NoteID from NNotes Where note Like '%" + mSearch + "%') " +
                    "OR " +
                    "NoteID in (Select Distinct NoteID from NListElements Where content Like '%" + mSearch + "%') " +
                    "OR " +
                    "title Like '%" + mSearch + "%'" +
                    ")";
        }
        if (!sqlWhere.equals("WHERE")) {
            sqlQuery += sqlWhere;
        }
        mNoteAdapter.search(sqlQuery, mSQLTypes);

    }

    @Override
    public void onBackPressed() {
        saveAndFinish();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present

        return true;

    }

    public void saveAndFinish() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULT_SEARCH_UPDATE, mUpdate);
        resultIntent.putExtra(RESULT_SEARCH_NOTES, mNoteResult);
        resultIntent.putExtra(RESULT_SEARCH_LABEL, mUpdateChip);
        setResult(RESULT_OK, resultIntent);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            saveAndFinish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void clearSearch(View clear) {
        mSearchBox.setText("");
        refreshSearch();
    }

    private void showHidePanel() {
        AnimatedVectorDrawable drawable = mVisible ? mAnimArrowUp : mAnimArrowDown;
        if (mAnimation == null) {
            mAnimation = new Animations();
        }
        if (mVisible) {
            mAnimation.slideUpInvisibleElevation(mSearchPanel, this);
        } else {
            mAnimation.slideDownVisibleElevation(mSearchPanel, this);
        }
        mVisible = !mVisible;

        ((ImageView) findViewById(R.id.search_panel_show)).setImageDrawable(drawable);
        drawable.start();
    }

    public void onClickHide(View view) {
        showHidePanel();
    }
}
