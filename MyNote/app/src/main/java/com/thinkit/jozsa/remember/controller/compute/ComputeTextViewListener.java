package com.thinkit.jozsa.remember.controller.compute;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by jozsatibold on 13.05.2017.
 */

public interface ComputeTextViewListener {
    void returnTextView(EditText textView);
}
