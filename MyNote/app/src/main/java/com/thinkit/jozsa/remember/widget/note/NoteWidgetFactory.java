package com.thinkit.jozsa.remember.widget.note;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.widget.list.ListWidget;

import java.util.List;


class NoteWidgetFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext = null;
    private int mNoteID;

    NoteWidgetFactory(Context context, Intent intent) {
        this.mContext = context;
        mNoteID = intent.getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1);
    }

    @Override
    public void onCreate() {
        // no-op
    }

    @Override
    public void onDestroy() {
        // no-op
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews card = new RemoteViews(mContext.getPackageName(), R.layout.item_note_widget_type);
        card.removeAllViews(R.layout.item_note_widget_type);
        List<String> noteContent = DatabaseManager.getInstance(mContext).getCardNote(mNoteID).getCardData();
        if (position < 1 && noteContent != null) {
            boolean secured = noteContent.get(noteContent.size() - 1).equals("S:1");
            for (int i = 0; i < noteContent.size() - 1; ++i) {
                String content = noteContent.get(i);
                int begin = Integer.parseInt(content.charAt(0) + "");
                String source = content.substring(2);
                RemoteViews type;
                switch (begin) {
                    case 1:
                        type = new RemoteViews(mContext.getPackageName(), R.layout.item_note_widget_picture);
                        if (!secured) {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 4;
                            Bitmap bt = BitmapFactory.decodeFile(source.split(":")[1], options);
                            if (bt != null) {
                                type.setImageViewBitmap(R.id.item_note_widget_picture, bt);
                            }
                        } else {
                            type.setImageViewResource(R.id.item_note_widget_picture, R.drawable.ic_lock_black_96dp);
                        }
                        break;

                    case 2:
                        type = new RemoteViews(mContext.getPackageName(), R.layout.item_note_widget_record);
                        String recDate = source.split("REC_")[1];
                        recDate = recDate.substring(0, 4) + "." + recDate.substring(4, 6) + "." + recDate.substring(6, 8);
                        type.setTextViewText(R.id.item_note_widget_record, !secured ? recDate : recDate.replaceAll(".", "*"));
                        break;

                    case 3:
                        type = new RemoteViews(mContext.getPackageName(), R.layout.item_note_widget_text);
                        type.setTextViewText(R.id.item_note_widget_text, !secured ? source : source.replaceAll(".", "*"));
                        break;

                    default:
                        String lb = source.substring(0, source.length() - 2);
                        type = new RemoteViews(mContext.getPackageName(), source.endsWith(":1") ? R.layout.item_note_widget_list_checked : R.layout.item_note_widget_list_unchecked);
                        type.setTextViewText(R.id.item_note_widget_list, !secured ? lb : lb.replaceAll(".", "*"));
                }

                card.addView(R.id.item_note_widget_type, type);
            }
            Intent i = new Intent();
            i.putExtra(ListWidget.EXTRA_WORD, mNoteID);
            card.setOnClickFillInIntent(R.id.item_note_widget_type, i);
        }
        return (card);
    }

    @Override
    public RemoteViews getLoadingView() {
        return (null);
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {
    }
}
