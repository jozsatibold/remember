package com.thinkit.jozsa.remember.view.notes.soundrecord;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;

import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;

import java.util.Locale;

public class Timer {
    private TextViewTypeface mTimeText;
    private long mStartTime = 0L;
    private Handler mCustomHandler;
    private long mTimeInMilliseconds = 0L;
    private long mTimeSwapBuff = 0L;
    private int mMin = 0;
    private String mMaxTime = "";
    private SoundRecorder mSoundRecord;
    private boolean mRunning;
    public Timer(Context context, TextViewTypeface timeText, int min){
        mTimeText = timeText;
        mMin = min;
        mCustomHandler = new Handler();
        mMaxTime = "/" + min / 60 + ":" + String.format(Locale.getDefault(), "%02d", min % 60) + ":00";
        mSoundRecord = new SoundRecorder(context);
    }
    public void refresh(TextViewTypeface timeText){
        mTimeText = timeText;
        mSoundRecord.refresh();
        mStartTime = 0L;
        mTimeInMilliseconds = 0L;
        mTimeSwapBuff = 0L;
    }

    public void setTime(int min){
        mMin = min;
        mMaxTime = "/" + min / 3600 + ":" + String.format(Locale.getDefault(), "%02d", min % 60) + ":00";
    }
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            if((mTimeSwapBuff + mTimeInMilliseconds) / 60000 < mMin) {
                mTimeInMilliseconds = SystemClock.uptimeMillis() - mStartTime;
                int secs = (int) ((mTimeSwapBuff + mTimeInMilliseconds) / 1000);
                mTimeText.setText((secs / 3600) + ":" + String.format(Locale.getDefault(), "%02d",secs / 60 % 60)+ ":" + String.format(Locale.getDefault(), "%02d", secs % 60) + mMaxTime);
                mCustomHandler.postDelayed(this, 0);
            } else {
                mSoundRecord.stop();
            }
        }
    };
    public void startTime(){
        if(!mRunning) {
            mRunning = true;
            mSoundRecord.start();
            mTimeSwapBuff = 0;
            mStartTime = SystemClock.uptimeMillis();
            mCustomHandler.postDelayed(updateTimerThread, 0);
        }
    }

    public void stopTime(){
        if(mRunning) {
            mRunning = false;
            mTimeSwapBuff += mTimeInMilliseconds;
            mCustomHandler.removeCallbacks(updateTimerThread);
            mSoundRecord.stop();
        }
    }
    public long getTime(){
        return mTimeSwapBuff;
    }
    public String getPath(){
        return mSoundRecord.getPath();
    }

    public String getRealPath(){
        return mSoundRecord.getRealPath();
    }



}
