package com.thinkit.jozsa.remember.view.home.tab_notes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.home.adapters.NoteItemTouchHelperCallback;
import com.thinkit.jozsa.remember.view.home.adapters.SwipeListener;
import com.thinkit.jozsa.remember.view.home.card.CardItemDecoration;
import com.thinkit.jozsa.remember.view.home.DataUpdateListener;
import com.thinkit.jozsa.remember.view.home.adapters.LabelChipAdapter;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.notes.DialogLabelAdapter;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.notes.list.ListItemTouchHelperCallback;
import com.thinkit.jozsa.remember.view.notes.soundrecord.OnStartDragListener;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.List;


public class NotesFragment extends Fragment implements DataUpdateListener, NoteClickListener {

    private boolean mSelectToDelete = false;
    private RecyclerView mNoteRecycler;
    private NotesAdapter mNoteAdapter;
    private View mPlaceholder;
    private SelectionListener mSelectionListener;
    private SwipeListener mSwipeListener;
    private SettingsPreferences mPreference;
    private boolean mSingle = false;
    private boolean mLongClick = false;
    private FabListener mFabListener;
    private CardItemDecoration mCardDecoration;
    private int mScroll = 0;
    private LabelChipAdapter mChipAdapter;
    private int mToolbarColor;
    private DatabaseManager mDb;
    private int mWaiting = 0;
    private long mLastPassword = 0;

    public NotesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreference = new SettingsPreferences(getActivity(), LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        mWaiting = mPreference.getInt(LoadingActivity.PREFERENCE_SECURE_WAITING) + 1;
        int label = mPreference.getInt(LoadingActivity.PREFERENCE_LABEL);
        mToolbarColor = mPreference.getInt(LoadingActivity.PREFERENCE_TOOLBAR);
        mNoteAdapter = new NotesAdapter(true, 4, label, this.getContext(), 0);
        mDb = DatabaseManager.getInstance(getContext());
        mChipAdapter = new LabelChipAdapter(getContext(), mDb, mToolbarColor, label, true);
        mNoteAdapter.setShowListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                showPlaceholder();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notes, container, false);
    }

    @Override
    public void setSelection(SelectionListener selectionListener) {
        mSelectionListener = selectionListener;
    }

    public void setSwipeListener(SwipeListener listener) {
        mSwipeListener = listener;
        if (mNoteAdapter != null) {
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
    }

    @Override
    public int getPageNumber() {
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity) context).registerDataUpdateListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) this.getActivity()).unregisterDataUpdateListener(this);
    }

    @Override
    public void refreshView() {
        if (mSingle != mPreference.getBoolean(LoadingActivity.PREFERENCE_COLUMNS)) {
            mSingle = !mSingle;
            mCardDecoration.setMultiColumn(mSingle);
            mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
            mNoteRecycler.setAdapter(this.mNoteAdapter);
        }
        int toolbarColor = mPreference.getInt(LoadingActivity.PREFERENCE_TOOLBAR);
        if (toolbarColor != mToolbarColor) {
            mToolbarColor = toolbarColor;
            mChipAdapter.setColor(mToolbarColor);
        }
    }


    public void setFabListener(FabListener listener) {
        mFabListener = listener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mSwipeListener != null) {
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
        mNoteAdapter.setNoteClickListener(this);
        final RelativeLayout sortLinear = (RelativeLayout) view.findViewById(R.id.main_sort_label);
        mNoteRecycler = (RecyclerView) view.findViewById(R.id.note_recycler);
        mSingle = mPreference.getBoolean(LoadingActivity.PREFERENCE_COLUMNS);
        mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
        mNoteRecycler.setAdapter(mNoteAdapter);
        NoteItemTouchHelperCallback callback = new NoteItemTouchHelperCallback(mNoteAdapter);
        callback.setLongPress(true);
        callback.setLeftSwipe(true);
        callback.setRightSwipe(true);
        callback.setSwipe(true);
        new ItemTouchHelper(callback).attachToRecyclerView(mNoteRecycler);
        mCardDecoration = new CardItemDecoration(mSingle);
        mNoteRecycler.addItemDecoration(mCardDecoration);
        mNoteRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy != 0 && mFabListener != null) {
                    mFabListener.scollTo(dy);
                    if (dy > 0) {
                        if (sortLinear.getVisibility() == View.VISIBLE && mScroll > -1) {
                            mScroll = -1;
                            new Animations().slideUpInvisible(sortLinear, NotesFragment.this.getContext());
                        }
                    } else if (dy < 0) {
                        if (sortLinear.getVisibility() == View.GONE && mScroll < 1) {
                            mScroll = 1;
                            new Animations().slideDownVisible(sortLinear, NotesFragment.this.getContext());
                        }
                    }
                }

            }
        });

        mPlaceholder = view.findViewById(R.id.empty_placeholder);
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        }
        sortLayoutInit(view);
    }


    @Override
    public void makeCopy() {
        mNoteAdapter.makeCopy(getContext());
    }

    @Override
    public void removeSelection() {
        mSelectToDelete = false;
        mNoteAdapter.removeSelection();
    }

    @Override
    public void update(int noteID, int to, int from) {
        if (mNoteAdapter != null) {
            if (to == from) {
                mNoteAdapter.updateNote(noteID, getContext());
            } else {
                mNoteAdapter.removeElement(noteID);
                showPlaceholder();
            }
            showPlaceholder();
        }

    }

    @Override
    public void showPlaceholder() {
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        } else {
            mNoteRecycler.setVisibility(View.VISIBLE);
            mPlaceholder.setVisibility(View.GONE);
        }
    }

    public void setLabel(int label) {
        if (mNoteAdapter != null) {
            mNoteAdapter.setLabel(label);
            showPlaceholder();
        }
    }

    @Override
    public ArrayList<Integer> getSelectedList(int id, boolean delete) {
        mSelectToDelete = false;
        return mNoteAdapter.getSelectedList(id, delete);
    }

    public ArrayList<NoteLite> getSelectedNotes() {
        return mNoteAdapter.getSelectedNotes();
    }

    public void removeNote(NoteLite note) {
        mNoteAdapter.remove(note);
    }

    @Override
    public void addNote(int noteID) {
        if (mNoteAdapter != null) {
            mNoteAdapter.addNote(noteID);
            showPlaceholder();
        }
    }

    private void sortLayoutInit(View view) {
        RecyclerView labelRecycler = (RecyclerView) view.findViewById(R.id.main_label_recycle);
        labelRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        labelRecycler.setAdapter(mChipAdapter);

        ListItemTouchHelperCallback callback = new ListItemTouchHelperCallback(mChipAdapter);
        callback.setSaveLastItems(2);
        callback.setSaveFirstItems(1);
        callback.setOrientation(true);
        final ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(labelRecycler);
        mChipAdapter.setOnStartDragListener(new OnStartDragListener() {
            @Override
            public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                touchHelper.startDrag(viewHolder);
            }
        });
        mChipAdapter.setOnClickListener(new NoteClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                if(!mSelectToDelete) {
                    if (pos == -2) {
                        mSelectionListener.setSelection(-2);
                    } else {
                        int size = mChipAdapter.getItemCount();
                        if (pos < size - 2) {
                            if (!mLongClick) {
                                if (!mSelectToDelete) {
                                    removeSelection();
                                }
                                mChipAdapter.setSelected(pos);
                                int label = mChipAdapter.getSelectedPos(pos);
                                mPreference.setInt(LoadingActivity.PREFERENCE_LABEL, label);
                                setLabel(label);
                            }
                            mLongClick = false;
                        } else if (pos == size - 2) {
                            if (mPreference.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                                Dialogs.dialogCheckPassword(NotesFragment.this.getContext(), new SettingsPreferences(NotesFragment.this.getContext(), SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                                    @Override
                                    public void dialogAnswer(boolean ok) {
                                        if (ok) {
                                            mLastPassword = System.currentTimeMillis();
                                            Dialogs.dialocEditLabel(mChipAdapter, null, getContext());
                                        }
                                    }
                                });
                            } else {
                                mLastPassword = System.currentTimeMillis();
                                Dialogs.dialocEditLabel(mChipAdapter, null, getContext());
                            }
                        } else {
                            if (mChipAdapter.getItemCount() > 6) {
                                if (mPreference.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                                    Dialogs.dialogCheckPassword(NotesFragment.this.getContext(), new SettingsPreferences(NotesFragment.this.getContext(), SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                                        @Override
                                        public void dialogAnswer(boolean ok) {
                                            if (ok) {
                                                mLastPassword = System.currentTimeMillis();
                                                removeDialog();
                                            }
                                        }
                                    });
                                } else {
                                    mLastPassword = System.currentTimeMillis();
                                    removeDialog();
                                }

                            } else {
                                Toast.makeText(getContext(), R.string.label_not_found, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean onNoteLongClicked(final int position) {
                mLongClick = true;
                if (mSelectToDelete) {
                    if (position > 0 && position < mChipAdapter.getItemCount() - 2) {
                        if (mPreference.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            Dialogs.dialogCheckPassword(NotesFragment.this.getContext(), new SettingsPreferences(NotesFragment.this.getContext(), SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                                @Override
                                public void dialogAnswer(boolean ok) {
                                    if (ok) {
                                        mLastPassword = System.currentTimeMillis();
                                        copyDialog(position);
                                    }
                                }
                            });
                        } else {
                            mLastPassword = System.currentTimeMillis();
                            copyDialog(position);
                        }
                    }
                } else if (mChipAdapter.getPosLabel(position).getId() > 3 && position < mChipAdapter.getItemCount() - 2) {
                    if (mPreference.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                        Dialogs.dialogCheckPassword(NotesFragment.this.getContext(), new SettingsPreferences(NotesFragment.this.getContext(), SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                            @Override
                            public void dialogAnswer(boolean ok) {
                                if (ok) {
                                    mLastPassword = System.currentTimeMillis();
                                    Dialogs.dialocEditLabel(mChipAdapter, mChipAdapter.getPosLabel(position), getContext());
                                }
                            }
                        });
                    } else {
                        mLastPassword = System.currentTimeMillis();
                        Dialogs.dialocEditLabel(mChipAdapter, mChipAdapter.getPosLabel(position), getContext());
                    }
                }
                return false;
            }
        });

//        view.findViewById(R.id.main_show_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //you have to ask for the permission in runtime.
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext())) {
//                    //If the draw over permission is not available open the settings screen
//                    //to grant the permission.
//                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                            Uri.parse("package:" + NotesFragment.this.getContext().getPackageName()));
//                    startActivityForResult(intent, MainActivity.CODE_DRAW_OVER_OTHER_APP_PERMISSION);
//                } else {
//                    getContext().startService(new Intent(getContext(), FloatingViewService.class));
//                }
//            }
//        });
    }

    private void copyDialog(int position) {
        final int from = mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos());
        final int to = mChipAdapter.getSelectedPos(position);
        if (from != to) {
            Dialogs.dialogSimpleQuestion(getContext(), getString(R.string.action_copy), getString(R.string.label_copy_dialog, mChipAdapter.getPosLabel(from).getName(), mChipAdapter.getPosLabel(to).getName()),
                    getString(R.string.dialog_button_cancel), getString(R.string.dialog_button_ok), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                List<NoteLite> selected = getSelectedNotes();
                                for (NoteLite note : selected) {
                                    note.setLabel(to);
                                    mDb.updateNoteLite(note);
                                    if (from != 0) {
                                        removeNote(note);
                                    }
                                }
                                removeSelection();
                            }
                        }
                    });

        }
    }

    private void removeDialog() {
        final Dialog dialogRemoveLabel = new Dialog(getContext());
        dialogRemoveLabel.setContentView(R.layout.dialog_remove_label);
        final RecyclerView recyclerLabel = (RecyclerView) dialogRemoveLabel.findViewById(R.id.dialog_label_recycler);
        recyclerLabel.setAdapter(new DialogLabelAdapter(mChipAdapter.getDatabaseLabels()));
        recyclerLabel.setHasFixedSize(true);
        recyclerLabel.setLayoutManager(new LinearLayoutManager(getContext()));
        dialogRemoveLabel.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRemoveLabel.dismiss();
            }
        });
        dialogRemoveLabel.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Label> list = ((DialogLabelAdapter) recyclerLabel.getAdapter()).getChecked();
                if (list.size() > 0) {
                    int pos = mChipAdapter.getSelectedPos();
                    for (Label lb : list) {
                        mNoteAdapter.removedLabel(lb.getId());
                        mChipAdapter.removeLabel(lb);
                    }
                    if (pos != mChipAdapter.getSelectedPos()) {
                        int otherPos = mChipAdapter.getOtherLocations();
                        mPreference.setInt(LoadingActivity.PREFERENCE_LABEL, otherPos);
                        setLabel(otherPos);
                    }
                    for (Label label : list) {
                        if (label.getId() > 2) {
                            mDb.removeLabel(label.getName());
                        }
                    }
                    Toast.makeText(getContext(), R.string.label_moved, Toast.LENGTH_LONG).show();
                }
                dialogRemoveLabel.dismiss();
            }
        });
        dialogRemoveLabel.show();
    }

    public void refreshChipAdapter() {
        int pos = mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos());
        mChipAdapter.refresh();
        if (pos != mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos())) {
            mPreference.setInt(LoadingActivity.PREFERENCE_LABEL, mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos())));
            setLabel(mChipAdapter.getSelectedPos(mChipAdapter.getSelectedPos()));
        }
    }

    @Override
    public void onNoteClicked(int pos) {
        if (!mSelectToDelete && pos > -1) {
            Intent intent = new Intent(getActivity(), NoteActivity.class);
            intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNoteAdapter.getItemAtPosition(pos).getID());

            getActivity().startActivityForResult(intent, MainActivity.ACTIVITY_RESULT_NOTE_CLICK);
        } else {
            mNoteAdapter.setSelected(pos);
            if (mNoteAdapter.getNumberSelected() == 0) {
                mSelectToDelete = false;
                mSelectionListener.setSelection(-1);
            }
        }
    }

    @Override
    public List<Integer> getUndoList() {
        return mNoteAdapter.getUndoIds();
    }

    @Override
    public void refresh() {
        mNoteAdapter.refresh();
    }

    @Override
    public boolean onNoteLongClicked(int position) {
        if (!mSelectToDelete) {
            mSelectToDelete = true;
            if (mSelectionListener != null) {
                mSelectionListener.setSelection(1);
            }
        }
        mNoteAdapter.setSelected(position);
        return true;
    }

    @Override
    public void remove(int id) {
        mNoteAdapter.removeNote(id);
    }
}
