package com.thinkit.jozsa.remember.view.notes.list;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.InputType;
import android.text.Layout;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.compute.Compute;
import com.thinkit.jozsa.remember.controller.compute.ComputeTextViewListener;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.other.Result;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.TooltipStringResponse;
import com.thinkit.jozsa.remember.view.appearance.TooltipWindow;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.notes.soundrecord.OnStartDragListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import static android.view.View.GONE;
import static android.view.inputmethod.EditorInfo.IME_ACTION_NEXT;

public class ListRecyclerViewOperator implements OnStartDragListener {
    private ListAdapter mAdapterUnChecked, mAdapterChecked;
    private Note mNote;
    private List<NoteChecked> mListAll;
    private ArrayList<Integer> mListChecked, mListUnChecked;
    private int mGreyColor;
    private DatabaseManager mDb;
    private TextView mShowListButton;
    private Activity mActivity;
    private int mID;
    private ToolbarImageClickListener mLastEdited;
    private ItemTouchHelper mTouchHelper;
    private ViewGroup mViewGroup;
    private boolean mLoaded = false;
    private Compute mCompute;
    private TooltipWindow mTooltip;
    private TooltipStringResponse mResponse;

    public ListRecyclerViewOperator(ViewGroup viewGroup, final RecyclerView recyclerViewUnChecked, final RecyclerView recyclerViewChecked, final Activity context, Note note, int greyColor, TextView showListButton, ToolbarImageClickListener lastEdited, DatabaseManager db) {
        mNote = note;
        mDb = db;
        mTooltip = new TooltipWindow(context);
        mGreyColor = greyColor;
        mViewGroup = viewGroup;
        mActivity = context;
        mLastEdited = lastEdited;
        mShowListButton = showListButton;
        mListAll = mNote.getList();
        mListUnChecked = new ArrayList<>();
        mListChecked = new ArrayList<>();
        mCompute = new Compute(new ComputeTextViewListener() {
            @Override
            public void returnTextView(final EditText textView) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Result res = mCompute.getResult();
                        String s = " ";
                        if (res.ok.isEmpty()) {
                            s += res.result;
                        } else {
                            s += res.ok;
                        }
                        int pos = textView.getSelectionStart();
                        Layout layout = textView.getLayout();
                        int line = layout.getLineForOffset(pos);
                        int baseline = layout.getLineBaseline(line);
                        int ascent = layout.getLineAscent(line);
                        float x = layout.getPrimaryHorizontal(pos);
                        float y = baseline + ascent;
                        mTooltip.show(x, y, textView, s, mResponse);
                    }
                });
            }
        });
        mResponse = new TooltipStringResponse() {
            @Override
            public void onClickInsert(String result, EditText textView) {
                String text = textView.getText().toString();
                int cursorPos = textView.getSelectionStart() - 1;
                if (result.length() < 10 && text.charAt(cursorPos) == '=') {
                    textView.setText(text.substring(0, cursorPos + 1) + result + text.substring(cursorPos + 1, text.length()));
                    textView.setSelection(cursorPos + result.length() + 1);
                }
            }
        };
        mID = 0;
        for (NoteChecked element : mNote.getList()) {
            if (element.getChecked()) {
                mListChecked.add(mID);
            } else {
                mListUnChecked.add(mID);
            }
            ++mID;
        }
        mListUnChecked.add(mID++);
        mAdapterChecked = new ListAdapter(true);
        mAdapterUnChecked = new ListAdapter(false);
        recyclerViewChecked.setAdapter(mAdapterChecked);
        ListItemTouchHelperCallback callback = new ListItemTouchHelperCallback(mAdapterUnChecked);
        callback.setSaveLastItems(1);
        mTouchHelper = new ItemTouchHelper(callback);
        mTouchHelper.attachToRecyclerView(recyclerViewUnChecked);
        mAdapterUnChecked.setDragListener(this);

        recyclerViewUnChecked.setAdapter(mAdapterUnChecked);
        recyclerViewChecked.setLayoutManager(new LinearLayoutManager(context));
        recyclerViewUnChecked.setLayoutManager(new LinearLayoutManager(context));

        if (mListChecked.isEmpty()) {
            mShowListButton.setVisibility(View.INVISIBLE);
        } else if (mListChecked.size() > 10) {
            recyclerViewChecked.setVisibility(GONE);
            mShowListButton.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_keyboard_arrow_down_grey_24dp).mutate(), null, null, null);
        }
        mShowListButton.setText(context.getString(R.string.checked_elements, mListChecked.size()));
        mShowListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerViewChecked.getVisibility() == View.VISIBLE) {
                    new Animations().animateViews(recyclerViewChecked.getWidth(), 0, recyclerViewChecked.getWidth() / 2, 0, GONE, recyclerViewChecked);
                    mShowListButton.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_keyboard_arrow_down_grey_24dp).mutate(), null, null, null);
                } else {
                    new Animations().animateViews(recyclerViewChecked.getWidth() / 2, 0, 0, recyclerViewChecked.getHeight(), View.VISIBLE, recyclerViewChecked);
                    mShowListButton.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_keyboard_arrow_up_grey_24dp).mutate(), null, null, null);
                }
            }
        });
    }

    public void setLoaded(boolean loaded) {
        if (loaded && !mLoaded) {
            int cbbefore = mAdapterChecked.getItemCount();
            int ucbefore = mAdapterUnChecked.getItemCount();
            mLoaded = true;
            int cafter = mAdapterChecked.getItemCount();
            int ucafter = mAdapterUnChecked.getItemCount();
            mAdapterChecked.notifyItemRangeInserted(cbbefore, cafter - cbbefore);
            mAdapterUnChecked.notifyItemRangeInserted(ucbefore, ucafter - ucbefore);
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mTouchHelper.startDrag(viewHolder);
    }

    class ListAdapter extends RecyclerView.Adapter<ListRecyclerViewOperator.ListAdapter.ViewHolder> implements ItemTouchHelperAdapter {

        private boolean mChecked;
        private boolean mFocus = false;
        private OnStartDragListener mDragStartListener;

        ListAdapter(boolean checked) {
            mChecked = checked;
        }

        private void setDragListener(OnStartDragListener dragStartListener) {
            mDragStartListener = dragStartListener;
        }

        @Override
        public ListRecyclerViewOperator.ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_elements, parent, false));
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            if (mFocus) {
                mFocus = false;
                holder.mTextView.requestFocus();

            }
            holder.mBind = false;
            if (mChecked) {
                holder.mClick.setVisibility(GONE);
                if (holder.mTextView.getCurrentTextColor() != mGreyColor) {
                    holder.mTextView.setTextColor(mGreyColor);
                    holder.mTextView.setLinkTextColor(mGreyColor);
                    holder.mTextView.setPaintFlags(holder.mTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
                int pos = mListChecked.get(holder.getAdapterPosition());
                if (!holder.mTextView.getText().toString().equals(mListAll.get(pos).getLabel())) {
                    String label = mListAll.get(pos).getLabel();
                    holder.mTextView.setText(label);
                    holder.mTextView.post(new Runnable() {
                        @Override
                        public void run() {
                            Linkify.addLinks(holder.mTextView, Linkify.ALL);
                        }
                    });
                }
            } else {
                int pos = mListUnChecked.get(holder.getAdapterPosition());
                if (pos >= mListAll.size() && pos < 500) {
                    holder.mClick.setVisibility(GONE);
                    holder.mTextView.setText("");
                } else if (!holder.mTextView.getText().toString().equals(mListAll.get(pos).getLabel())) {
                    String label = mListAll.get(pos).getLabel();
                    holder.mTextView.setText(label);
                    holder.mTextView.post(new Runnable() {
                        @Override
                        public void run() {
                            Linkify.addLinks(holder.mTextView, Linkify.ALL);
                        }
                    });
                    holder.mClick.setVisibility(View.VISIBLE);
                }
            }

            holder.mCheckBox.setChecked(mChecked);

            holder.mBind = true;
            holder.mTextView.setRawInputType(InputType.TYPE_CLASS_TEXT);
            holder.mTextView.setImeOptions(IME_ACTION_NEXT);

            holder.mTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (event == null && actionId == EditorInfo.IME_ACTION_NEXT && holder.getAdapterPosition() < getItemCount() - 1) {
                        if (holder.mTextView.getText().length() > 0) {
                            holder.mTextView.focusSearch(View.FOCUS_DOWN);
                            mFocus = true;
                            ListAdapter.this.notifyItemChanged(holder.getAdapterPosition() + 1);
                        }

                        return true;
                    } else {
                        return true;
                    }
                }
            });
            holder.mTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        holder.mImage.setVisibility(View.VISIBLE);
                    } else {
                        holder.mImage.setVisibility(View.INVISIBLE);
                        if (holder.mTextView.getText().length() <= 0 && holder.getAdapterPosition() >= 0) {

                            if (!mChecked) {
                                if (holder.getAdapterPosition() < getItemCount() - 1 || mListAll.size() == 150) {
                                    removeElement(holder.getAdapterPosition(), mChecked);
                                }
                            } else {
                                removeElement(holder.getAdapterPosition(), mChecked);
                            }
                        }
                    }
                }
            });
            holder.mTextView.addTextChangedListener(new TextWatcher() {
                int prevLength = 0;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    prevLength = s.length();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    int holderPos = holder.getAdapterPosition();
                    if (holder.mBind) {
                        if (!mChecked && holderPos == mListUnChecked.size() - 1 && mListAll.size() < 500) {
                            mListUnChecked.add(mID++);
                            holder.mClick.setVisibility(View.VISIBLE);
                            mAdapterUnChecked.notifyItemInserted(mAdapterUnChecked.getItemCount() - 1);
                        } else if (mListAll.size() >= 500) {
                            Toast.makeText(mActivity, R.string.maximum_list, Toast.LENGTH_LONG).show();
                        }
                        if (holder.mCheckBox.getVisibility() != View.VISIBLE) {
                            holder.mCheckBox.setVisibility(View.VISIBLE);
                        }
                        final String label = editable.toString();
                        if (holderPos > -1 && label.length() >= 0 && holderPos < (mChecked ? mListChecked.size() : mListUnChecked.size() - 1)) {
                            if (!label.equals(!(mListAll.size() == 0 || mListUnChecked.get(holderPos) >= mListAll.size()) ?
                                    mListAll.get(mChecked ? mListChecked.get(holderPos) : mListUnChecked.get(holderPos)).getLabel()
                                    : "")) {
                                setElement(holderPos, mChecked, label);
                            }
                        }
                        int pos = holder.mTextView.getSelectionStart();
                        if (pos > 0 && label.charAt(pos - 1) == '=' && prevLength < label.length()) {
                            mCompute.getResult(label.substring(0, pos - 1), holder.mTextView);
                        }
                    }
                }

            });
            holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (holder.mBind) {
                        switchElement(holder.getAdapterPosition(), mChecked);
                    }
                }
            });
            holder.mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final NoteChecked checked;
                    if (mChecked) {
                        checked = mListAll.get(mListChecked.get(holder.getAdapterPosition()));
                        removeElement(holder.getAdapterPosition(), true);
                    } else {
                        checked = mListAll.get(mListUnChecked.get(holder.getAdapterPosition()));
                        if (holder.getAdapterPosition() < ListAdapter.this.getItemCount() - 1) {
                            removeElement(holder.getAdapterPosition(), false);
                        }
                    }
                    Snackbar snackbar = Snackbar
                            .make(mViewGroup, mActivity.getString(R.string.note_list_item_snack_bar_text), Snackbar.LENGTH_LONG)
                            .setAction(mActivity.getString(R.string.action_undo_big), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    addElement(checked.getChecked(), checked.getLabel());
                                }
                            });

                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();

                }
            });
            if (!mChecked && position >= mListUnChecked.size() - 1) {
                holder.mCheckBox.setVisibility(View.INVISIBLE);
            } else if (holder.mCheckBox.getVisibility() != View.VISIBLE) {
                holder.mCheckBox.setVisibility(View.VISIBLE);
            }


        }

        @Override
        public void onItemMove(final int fromPosition, final int toPosition) {

            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    switchElementID(i, i + 1);
                    Collections.swap(mListAll, mListUnChecked.get(i), mListUnChecked.get(i + 1));
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    switchElementID(i, i - 1);
                    Collections.swap(mListAll, mListUnChecked.get(i), mListUnChecked.get(i - 1));
                }
            }
            updateModification();
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public int getItemCount() {
            if (!mLoaded) {
                if (!mChecked) {
                    return mListUnChecked.size() > 10 ? 10 : mListUnChecked.size();
                } else {
                    return mListUnChecked.size() > 10 ? 0 : 10 - mListUnChecked.size() > mListChecked.size() ? mListChecked.size() : 10 - mListUnChecked.size();
                }
            } else {
                return mChecked ? mListChecked.size() : mListUnChecked.size();
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            EditText mTextView;
            CheckBox mCheckBox;
            ImageButton mImage;
            View mClick;
            boolean mBind = false;

            ViewHolder(View v) {
                super(v);
                mTextView = (EditText) v.findViewById(R.id.list_element_edit_text);
                mCheckBox = (CheckBox) v.findViewById(R.id.list_element_checkbox);
                mImage = (ImageButton) v.findViewById(R.id.list_element_delete);
                mClick = v.findViewById(R.id.list_element_click);
                mClick.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (MotionEventCompat.getActionMasked(motionEvent) ==
                                MotionEvent.ACTION_DOWN) {
                            mDragStartListener.onStartDrag(ViewHolder.this);
                        }
                        return false;
                    }
                });
            }


        }

    }

    private void setElement(int position, boolean checked, String label) {
        mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
        mLastEdited.onNoteClicked(0);
        int pos = checked ? mListChecked.get(position) : mListUnChecked.get(position);
        if (pos >= mListAll.size()) {
            NoteChecked noteChecked = new NoteChecked(label);
            noteChecked.setId(mDb.insertListElement(noteChecked, mNote));
            mListAll.add(noteChecked);
            if (checked) {
                mListChecked.set(position, mListAll.size() - 1);
            } else {
                mListUnChecked.set(position, mListAll.size() - 1);
            }
        } else {
            mListAll.get(pos).setLabel(label);
            mDb.updateListElement(mListAll.get(pos), mNote);

        }
    }

    private void addElement(boolean checked, String label) {
        mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
        mLastEdited.onNoteClicked(0);
        ;

        NoteChecked noteChecked = new NoteChecked();
        noteChecked.setId(mDb.insertListElement(noteChecked, mNote));
        noteChecked.setLabel(label);
        noteChecked.setChecked(checked);
        mListAll.add(noteChecked);
        mDb.updateListElement(mListAll.get(mListAll.size() - 1), mNote);
        if (checked) {
            mListChecked.add(mListAll.size() - 1);
            mAdapterChecked.notifyItemInserted(mListAll.size() - 1);
            mShowListButton.setText(mActivity.getString(R.string.checked_elements, mListChecked.size()));
        } else {
            mListUnChecked.add(mListUnChecked.size() - 2, mListAll.size() - 1);
            mAdapterUnChecked.notifyItemInserted(mListAll.size() - 2);
        }
    }

    private void switchElementID(final int from, final int to) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mListAll.get(mListUnChecked.get(from)).setId(mListAll.get(mListUnChecked.get(to)).swithcID(mListAll.get(mListUnChecked.get(from)).getId()));
                mDb.updateListElement(mListAll.get(from), mNote);
                mDb.updateListElement(mListAll.get(to), mNote);
            }
        }).start();

    }

    private void updateModification() {
        mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
        mLastEdited.onNoteClicked(0);
    }

    private void switchElement(int position, boolean checked) {
        if (position > -1) {
            final int allPos = checked ? mListChecked.get(position) : mListUnChecked.get(position);
            mListAll.get(allPos).setChecked(!checked);
            mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
            mLastEdited.onNoteClicked(0);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mDb.updateListElement(mListAll.get(allPos), mNote);

                }
            }).start();

            if (!checked) {
                mListChecked.add(allPos);
                mAdapterChecked.notifyItemInserted(mListChecked.size() - 1);

                mListUnChecked.remove(position);
                mAdapterUnChecked.notifyItemRemoved(position);

            } else {
                int i = 0;
                while (i < mListUnChecked.size() - 1 && allPos > mListUnChecked.get(i)) {
                    ++i;
                }
                if (i < mListUnChecked.size() - 1) {
                    mListUnChecked.add(i, allPos);
                    mAdapterUnChecked.notifyItemInserted(i);
                } else {
                    mListUnChecked.add(mListUnChecked.size() - 1, allPos);
                    mAdapterUnChecked.notifyItemInserted(mListUnChecked.size() - 2);
                }
                mListChecked.remove(position);
                mAdapterChecked.notifyItemRemoved(position);

            }
            if (mListChecked.isEmpty() && mShowListButton.getVisibility() == View.VISIBLE) {
                mShowListButton.setVisibility(View.INVISIBLE);
            } else if (mShowListButton.getVisibility() == View.INVISIBLE && !mListChecked.isEmpty()) {
                mShowListButton.setVisibility(View.VISIBLE);
            } else if (!mListChecked.isEmpty()) {
                mShowListButton.setText(mActivity.getString(R.string.checked_elements, mListChecked.size()));
            }
        }
    }

    private void removeElement(final int position, final boolean checked) {
        mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
        mLastEdited.onNoteClicked(0);
        ;
        final int noteChecked = checked ? mListChecked.get(position) : mListUnChecked.get(position);
        if (checked) {
            mListChecked.remove(position);
            mAdapterChecked.notifyItemRemoved(position);
            mAdapterChecked.notifyItemRangeChanged(position, mListChecked.size());
            mShowListButton.setText(mActivity.getString(R.string.checked_elements, mListChecked.size()));
        } else {
            mListUnChecked.remove(position);
            mAdapterUnChecked.notifyItemRemoved(position);
            mAdapterUnChecked.notifyItemRangeChanged(position, mListUnChecked.size());
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (noteChecked < mListAll.size()) {
                    mDb.removeListElement(mListAll.get(noteChecked), mNote);
                    mListAll.remove(noteChecked);
                    for (int i = 0; i < mListChecked.size(); ++i) {
                        if (mListChecked.get(i) > noteChecked) {
                            mListChecked.set(i, mListChecked.get(i) - 1);
                        }
                    }
                    for (int i = 0; i < mListUnChecked.size(); ++i) {
                        if (mListUnChecked.get(i) > noteChecked) {
                            mListUnChecked.set(i, mListUnChecked.get(i) - 1);
                        }
                    }
                }
            }
        }).start();


    }
}
