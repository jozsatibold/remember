package com.thinkit.jozsa.remember.widget;


import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;


import com.thinkit.jozsa.remember.widget.list.ListWidget;
import com.thinkit.jozsa.remember.widget.note.NoteWidget;
import com.thinkit.jozsa.remember.widget.note.NoteWidgetConfigureActivity;

import java.util.ArrayList;

public class UpdateWidgets  extends AsyncTask<String, Integer, Boolean> {
        private Context mContext;
        public UpdateWidgets(Context context){
            mContext = context;
        }
        protected Boolean doInBackground(String... ids) {
            String widgets = ids[0];
            String label = ids[1];
            if(widgets != null && label != null) {
                int id = Integer.parseInt(ids[2]);
                if (widgets.contains("|:")) {
                    Intent intent = new Intent(mContext, NoteWidget.class);
                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    String[] cID = widgets.split(":");
                    ArrayList<Integer> wids = new ArrayList<>();
                    if (cID.length > 0) {
                        for (int i = 1; i < cID.length - 1; ++i) {
                            if (!cID[i].equals("") && !cID[i].equals("|") && cID[i].length() > 1) {
                                int cid = Integer.parseInt(cID[i].substring(0, cID[i].length() - 1));
                                wids.add(cid);
                                NoteWidgetConfigureActivity.saveTitlePref(mContext, cid, id);
                            }

                        }
                        int cid = Integer.parseInt(cID[cID.length - 1]);
                        wids.add(cid);
                        NoteWidgetConfigureActivity.saveTitlePref(mContext, cid, id);
                    }
                    int list[] = new int[wids.size()];
                    int j = 0;
                    for (int i : wids) {
                        list[j] = i;
                        ++j;
                    }
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, list);
                    mContext.sendBroadcast(intent);

                }
                String s = mContext.getSharedPreferences(ListWidget.LIST_WIDGET_FILE_NAME, 0).getString(ListWidget.LIST_WIDGET_WIDGETS, "");
                if (s.length() > 0) {
                    String[] wdgts = s.split(" ");
                    s = "";
                    for (String widgetInfo : wdgts) {
                        String[] info = widgetInfo.split(">", 3);
                        if (widgetInfo.contains(">")) {
                            String[] lb = label.split("-");
                            if (info.length == 2 && (info[1].equals(lb[1]) || info[1].equals(lb[0]) || info[1].equals("-1"))) {
                                s += info[0] + " ";
                            }
                        }

                    }
                    if (!s.isEmpty()) {
                        String st[] = s.split(" ");
                        if (st.length > 0) {
                            int widgetIDs[] = new int[st.length];
                            for (int i = 0; i < st.length; ++i) {
                                widgetIDs[i] = Integer.parseInt(st[i]);

                            }
                            Intent intent = new Intent(mContext, ListWidget.class);
                            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIDs);
                            mContext.sendBroadcast(intent);
                        }
                    }
                }
            } else {
                return false;
            }
            return true;
        }
}
