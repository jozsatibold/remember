package com.thinkit.jozsa.remember.view.appearance;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.thinkit.jozsa.remember.R;

import java.util.ArrayList;
import java.util.List;


public class MyDrawable {
    public Context mContext;

    public MyDrawable(Context context) {
        this.mContext = context;
    }

    public String getDrawableName(int id) {
        switch (id) {
            case 0:
                return mContext.getString(R.string.note_sort);
            case 1:
                return mContext.getString(R.string.list_sort);
            case 2:
                return mContext.getString(R.string.draw_sort);
            case 3:
                return mContext.getString(R.string.picture_sort);
            case 4:
                return mContext.getString(R.string.shop_sort);
            case 5:
                return mContext.getString(R.string.event_sort);
            case 6:
                return mContext.getString(R.string.location_sort);
            case 7:
                return mContext.getString(R.string.contact_sort);
            case 8:
                return mContext.getString(R.string.checked);
            case 9:
                return mContext.getString(R.string.unchecked);
            case 10:
                return mContext.getString(R.string.personal_label);
            case 11:
                return mContext.getString(R.string.work_label);
            case 12:
                return mContext.getString(R.string.other_label);
            case 13:
                return mContext.getString(R.string.take_picture);
            case 14:
                return mContext.getString(R.string.add_gallery);
            case 15:
                return mContext.getString(R.string.add_web);
            case 16:
                return mContext.getString(R.string.add_draw);
            case 17:
                return mContext.getString(R.string.add_record);
            case 18:
                return mContext.getString(R.string.label_label);
            case 19:
                return mContext.getString(R.string.make_copy);
            case 20:
                return mContext.getString(R.string.action_share);
            case 21:
                return mContext.getString(R.string.add_to_calendar);
            default:
                return "";
        }
    }

    public Drawable getDrawable(int id) {
        switch (id) {
            case 0:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_note_grey_24dp).mutate();
            case 1:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_list_black_24dp).mutate();
            case 2:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_draw_grey_24dp).mutate();
            case 3:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_picture_grey_24dp).mutate();
            case 4:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_shop_grey_24dp).mutate();
            case 5:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_event_grey_24dp).mutate();
            case 6:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_location_grey_24dp).mutate();
            case 7:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_contact_grey_24dp).mutate();
            case 8:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_check_box_grey_16dp).mutate();
            case 9:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_check_box_unchecked_grey_16dp).mutate();
            case 10:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_personal_black_24dp).mutate();
            case 11:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_work_black_24dp).mutate();
            case 12:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_more_black_24dp).mutate();
            case 13:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_add_a_photo_black_24dp).mutate();
            case 14:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_add_gallery_black_24dp).mutate();
            case 15:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_add_web).mutate();
            case 16:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_add_draw_black_24dp).mutate();
            case 17:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_add_record_black_24dp).mutate();
            case 18:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_label_black_24dp).mutate();
            case 19:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_archive_selector).mutate();
            case 20:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_notes_selector).mutate();
            case 21:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_delete_selector).mutate();
            case 22:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_copy_white_24dp).mutate();
            case 23:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_share_black_24dp).mutate();
            case 24:
                return ContextCompat.getDrawable(mContext, R.drawable.ic_event_note_black_24dp).mutate();
            default:
                return null;
        }
    }

    public int getDrawableId(int id) {
        switch (id) {
            case 0:
                return R.drawable.ic_note_grey_24dp;
            case 1:
                return R.drawable.ic_list_black_24dp;
            case 2:
                return R.drawable.ic_draw_grey_24dp;
            case 3:
                return R.drawable.ic_picture_grey_24dp;
            case 4:
                return R.drawable.ic_shop_grey_24dp;
            case 5:
                return R.drawable.ic_event_grey_24dp;
            case 6:
                return R.drawable.ic_location_grey_24dp;
            case 7:
                return R.drawable.ic_contact_grey_24dp;
            case 8:
                return R.drawable.ic_check_box_grey_16dp;
            case 9:
                return R.drawable.ic_check_box_unchecked_grey_16dp;
            case 10:
                return R.drawable.ic_personal_black_24dp;
            case 11:
                return R.drawable.ic_work_black_24dp;
            case 12:
                return R.drawable.ic_more_black_24dp;
            case 13:
                return R.drawable.ic_add_a_photo_black_24dp;
            case 14:
                return R.drawable.ic_add_gallery_black_24dp;
            case 15:
                return R.drawable.ic_add_web;
            case 16:
                return R.drawable.ic_add_draw_black_24dp;
            case 17:
                return R.drawable.ic_add_record_black_24dp;
            case 18:
                return R.drawable.ic_label_black_24dp;
            default:
                return -1;
        }
    }

    public int getLabelDrawable(int id) {
        switch (id) {
            case 0:
                return R.drawable.ic_all_labels_24dp;
            case 1:
                return R.drawable.ic_personal_black_24dp;
            case 2:
                return R.drawable.ic_work_black_24dp;
            case 3:
                return R.drawable.ic_more_black_24dp;
            case 4:
                return R.drawable.ic_label_black_24dp;
            case 5:
                return R.drawable.ic_account_balance_wallet_black_24dp;
            case 6:
                return R.drawable.ic_assignment_black_24dp;
            case 7:
                return R.drawable.ic_bookmark_black_24dp;
            case 8:
                return R.drawable.ic_book_black_24dp;
            case 9:
                return R.drawable.ic_card_travel_black_24dp;
            case 10:
                return R.drawable.ic_favorite_black_24dp;
            case 11:
                return R.drawable.ic_home_black_24dp;
            case 12:
                return R.drawable.ic_receipt_black_24dp;
            case 13:
                return R.drawable.ic_place_black_24dp;
            case 14:
                return R.drawable.ic_shopping_cart_black_24dp;
            case 15:
                return R.drawable.ic_album_black_24dp;
            case 16:
                return R.drawable.ic_movie_black_24dp;
            case 17:
                return R.drawable.ic_library_music_black_24dp;
            case 18:
                return R.drawable.ic_call_black_24dp;
            case 19:
                return R.drawable.ic_email_black_24dp;
            case 20:
                return R.drawable.ic_weekend_black_24dp;
            case 21:
                return R.drawable.ic_functions_black_24dp;
            case 22:
                return R.drawable.ic_cloud_black_24dp;
            case 23:
                return R.drawable.ic_laptop_black_24dp;
            case 24:
                return R.drawable.ic_tv_black_24dp;
            case 25:
                return R.drawable.ic_brush_black_24dp;
            case 26:
                return R.drawable.ic_tag_faces_black_24dp;
            case 27:
                return R.drawable.ic_map_black_24dp;
            case 28:
                return R.drawable.ic_local_cafe_black_24dp;
            case 29:
                return R.drawable.ic_cake_black_24dp;
            case 30:
                return R.drawable.ic_public_black_24dp;
            case 31:
                return R.drawable.ic_poll_black_24dp;
            case 32:
                return R.drawable.ic_school_black_24dp;
            case 33:
                return R.drawable.ic_location_city_black_24dp;
            case 34:
                return R.drawable.ic_star_black_24dp;
            case 35:
                return R.drawable.ic_directions_bus_black_24dp;
            case 36:
                return R.drawable.ic_directions_run_black_24dp;
            default:
                return R.drawable.ic_directions_car_black_24dp;

        }
    }
    public int[] getAllLabelDrawable(){
        int [] labels = new int[38];
        for(int i = 0; i < 38; ++i){
            labels[i] = getLabelDrawable(i);
        }
        return labels;
    }

    public Drawable getTypeDrawable(int pos){
        switch(pos){
            case 0: return mContext.getDrawable(R.drawable.ic_note_grey_24dp).mutate();
            case 1: return mContext.getDrawable(R.drawable.ic_list_black_24dp).mutate();
            case 2: return mContext.getDrawable(R.drawable.ic_picture_grey_24dp).mutate();
            case 3: return mContext.getDrawable(R.drawable.ic_draw_grey_24dp).mutate();
            case 4: return mContext.getDrawable(R.drawable.ic_mic_black2_24dp).mutate();
            case 5: return mContext.getDrawable(R.drawable.ic_notifications_black_24dp).mutate();
            case 6: return mContext.getDrawable(R.drawable.ic_call_black_24dp).mutate();
            case 7: return mContext.getDrawable(R.drawable.ic_mail_black_24dp).mutate();
            case 8: return mContext.getDrawable(R.drawable.ic_link_black_24dp).mutate();
            case 9: return mContext.getDrawable(R.drawable.ic_credit_card_black_24dp).mutate();
            case 10: return mContext.getDrawable(R.drawable.ic_place_black_24dp).mutate();
            case 11: return mContext.getDrawable(R.drawable.ic_functions_black_24dp).mutate();
            case 12: return mContext.getDrawable(R.drawable.ic_event_grey_24dp).mutate();
        }
        return null;
    }
    public List getAllLabelDrawableList(){
        List<Integer> labels = new ArrayList<>();
        for(int i = 0; i < 38; ++i){
            labels.add(getLabelDrawable(i));
        }
        return labels;
    }
}
