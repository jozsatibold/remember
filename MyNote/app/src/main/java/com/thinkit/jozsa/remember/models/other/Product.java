package com.thinkit.jozsa.remember.models.other;

import android.os.Parcel;
import android.os.Parcelable;


public class Product implements Parcelable {
    private int mID;
    private double mPrice;
    private String mName;

    public Product() {
        this.mID = 0;
        this.mPrice = 0.0;
        this.mName = "";
    }

    public Product(int id, double price, String name) {
        this.mID = id;
        this.mPrice = price;
        this.mName = name;
    }

    public Product(Parcel in) {
        mID = in.readInt();
        mName = in.readString();
        mPrice = in.readDouble();
    }

    public void setPrice(int price) {
        mPrice = price;
    }

    public double getPrie() {
        return mPrice;
    }

    public int getID() {
        return mID;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mID);
        dest.writeString(mName);
        dest.writeDouble(mPrice);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

}
