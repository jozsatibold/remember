package com.thinkit.jozsa.remember.view.home.card;


public interface NoteClickListener {
    void onNoteClicked(int pos);
    boolean onNoteLongClicked(int position);
}
