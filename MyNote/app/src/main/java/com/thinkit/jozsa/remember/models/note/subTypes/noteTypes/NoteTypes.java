package com.thinkit.jozsa.remember.models.note.subTypes.noteTypes;

import java.util.ArrayList;

public interface NoteTypes {

    void setText(String text);

    String getText();

    void setList(ArrayList list);

    ArrayList getList();
}
