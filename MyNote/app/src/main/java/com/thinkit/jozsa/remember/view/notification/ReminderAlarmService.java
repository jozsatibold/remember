package com.thinkit.jozsa.remember.view.notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.thinkit.jozsa.remember.view.home.MainActivity;


public class ReminderAlarmService extends Service {

        @Override
        public IBinder onBind(Intent arg0)
        {
            return null;
        }

        @Override
        public void onCreate()
        {
            super.onCreate();
        }

        @Override
        public void onStart(Intent intent, int startId)
        {
            super.onStart(intent, startId);
            try{
                ReminderManager.generateNotification(this, intent.getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1));
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
        }

}
