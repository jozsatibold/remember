package com.thinkit.jozsa.remember.widget.note;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;


import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.widget.WidgetClickActivity;
import com.thinkit.jozsa.remember.widget.list.ListWidget;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter.TIME_FORMAT;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link NoteWidgetConfigureActivity NoteWidgetConfigureActivity}
 */
public class NoteWidget extends AppWidgetProvider {
    private static String ACTION_WIDGET_ONCLICK = "NOTEWIDGETONCLICK_";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_note);
        int id = NoteWidgetConfigureActivity.loadTitlePref(context, appWidgetId);

        if (id > -1) {
            Note note = DatabaseManager.getInstance(context).getWidgetNote(id, 20);
            if (!note.getWidgets().contains("|:" + appWidgetId)) {
                note.setWidgets(note.getWidgets() + "|:" + appWidgetId);
                DatabaseManager.getInstance(context).updateNote(note, 1, 0);
            }
            int backgroundID;
            switch (note.getBgColor()) {

                case 0:
                    backgroundID = R.drawable.radius_corner_white;
                    break;

                case 1:
                    backgroundID = R.drawable.radius_corner_grey;
                    break;

                case 2:
                    backgroundID = R.drawable.radius_corner_yellow;
                    break;

                case 3:
                    backgroundID = R.drawable.radius_corner_orange;
                    break;

                case 4:
                    backgroundID = R.drawable.radius_corner_red;
                    break;

                case 5:
                    backgroundID = R.drawable.radius_corner_purple;
                    break;

                case 6:
                    backgroundID = R.drawable.radius_corner_blue;
                    break;

                default:
                    backgroundID = R.drawable.radius_corner_green;

            }

            views.setImageViewResource(R.id.widget_card, backgroundID);

            if (!note.getTitle().isEmpty()) {
                views.setViewVisibility(R.id.widget_title, VISIBLE);
                views.setTextViewText(R.id.widget_title, note.getTitle());
            } else {
                views.setViewVisibility(R.id.widget_title, GONE);
            }
            if (note.getReminder() != null && !note.getReminder().isEmpty() && note.getReminder().startsWith("T")) {

                try {
                    views.setViewVisibility(R.id.widget_note_reminder, VISIBLE);
                    views.setTextViewText(R.id.widget_note_reminder, new SimpleDateFormat(TIME_FORMAT, Locale.getDefault()).format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));
                } catch (ParseException e) {
                    Log.e("NoteWidget reminder", e.toString());
                }
            } else {
                views.setViewVisibility(R.id.widget_note_reminder, GONE);
            }
            Intent svcIntent = new Intent(context, NoteWidgetService.class);
            svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            svcIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, note.getID());
            svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
            views.setRemoteAdapter(R.id.widget_note_list, svcIntent);

            views.setOnClickPendingIntent(R.id.widget_card, PendingIntent.getBroadcast(context, 0,
                    new Intent(context, NoteWidget.class)
                            .setAction(ACTION_WIDGET_ONCLICK + id),
                    PendingIntent.FLAG_UPDATE_CURRENT));

            views.setPendingIntentTemplate(R.id.widget_note_list, PendingIntent.getActivity(context, 0,
                    new Intent(context, WidgetClickActivity.class)
                            .setAction(ACTION_WIDGET_ONCLICK + id),
                    PendingIntent.FLAG_UPDATE_CURRENT));

        } else {
            views.setViewVisibility(R.id.widget_title, VISIBLE);
            views.setTextViewText(R.id.widget_title, "Note not found! :(");
        }
        ComponentName component = new ComponentName(context, NoteWidgetFactory.class);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widget_note_list);
        appWidgetManager.updateAppWidget(component, views);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            DatabaseManager db = DatabaseManager.getInstance(context);
            Note note = db.getNote(NoteWidgetConfigureActivity.loadTitlePref(context, appWidgetId));
            if (note.getID() > -1) {
                note.setWidgets(note.getWidgets().replace("|:" + appWidgetId, ""));
                db.updateNote(note, 1, 0);
                NoteWidgetConfigureActivity.deleteTitlePref(context, appWidgetId);
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.startsWith(ACTION_WIDGET_ONCLICK)) {
            Intent resultIntent = new Intent(context, WidgetClickActivity.class);
            resultIntent.putExtra(ListWidget.EXTRA_WORD, Integer.parseInt(action.substring(ACTION_WIDGET_ONCLICK.length())));
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_FORWARD_RESULT);
//
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//            stackBuilder.addParentStack(NoteActivity.class);
//            stackBuilder.addNextIntent(resultIntent);
//            context.startActivities(stackBuilder.getIntents());
            context.startActivity(resultIntent);

        }
        super.onReceive(context, intent);

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

