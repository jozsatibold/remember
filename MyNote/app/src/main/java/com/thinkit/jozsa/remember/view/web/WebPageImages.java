package com.thinkit.jozsa.remember.view.web;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WebPageImages implements Runnable {
    private OkHttpClient mClient;
    private String mURL;
    private ArrayList<String> mUrlImageList;

    public WebPageImages(String url) {
        mURL = url;
        mClient = new OkHttpClient();
    }
    @Override
    public void run() {
        try {


            Request request = new Request.Builder()
                    .url(mURL)
                    .build();
            try (Response response = mClient.newCall(request).execute()) {
                String htmlCode = response.body().string();
                final String src[] = htmlCode.split("src=\"");
                for(String i:src){
                    final String source = i.split("\"")[0];
                    if(source.endsWith("jpg") || source.endsWith("png") || source.endsWith("jpeg")) {
                        if(source.startsWith("http")){

                            if(mUrlImageList == null){
                                mUrlImageList = new ArrayList<>();
                            }
                            int k = 0;
                            while(k < mUrlImageList.size() && !mUrlImageList.get(k).equals(source)){
                                ++k;
                            }
                            if(k >= mUrlImageList.size()) {
                                mUrlImageList.add(source);
                            }
                        }
                    }

                }
                if(mUrlImageList == null){
                    mUrlImageList = new ArrayList<>();
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public ArrayList<String> getImageUrlList(){
        return mUrlImageList;
    }
}
