package com.thinkit.jozsa.remember.controller.storage.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteCheckedLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "note.db";
    public static SimpleDateFormat mFormat;
    private static DatabaseManager INSTANCE;

    public static synchronized DatabaseManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseManager(context);
        }
        return INSTANCE;
    }

    private DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, 1);
        mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        // Database Note part
        db.execSQL(
                "CREATE TABLE NLabels (" +
                        "NLID integer primary key autoincrement, " +
                        "name varchar(50)," +
                        "drawable integer," +
                        "position integer" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE Notes (" +
                        "NoteId integer primary key autoincrement," +
                        "TypeID tinyint," +
                        "title varchar(180), " +
                        "creatingDate datetime, " +
                        "lastModification datetime, " +
                        "reminder varchar(20), " +
                        "BgColor tinyint, " +
                        "Category UNSIGNED BIG INT," +
                        "secured tinyint, " +
                        "staticPosition float, " +
                        "numberOfView float," +
                        "widgets string," +
                        "NLID integer, " +
                        "FOREIGN KEY(NLID) REFERENCES NLabels(NLID)" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE NNotes (" +
                        "NNID integer primary key autoincrement, " +
                        "note text, " +
                        "NoteID integer, " +
                        "actionText text, " +
                        "scanned tinyint, " +
                        "FOREIGN KEY(NoteID) REFERENCES Notes(NoteID)" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE NPictures (" +
                        "NPID integer primary key autoincrement, " +
                        "NoteID integer, " +
                        "src varchar(255), " +
                        "pictureType tinyint, " +
                        "FOREIGN KEY(NoteID) REFERENCES Notes(NoteID)" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE NListElements (" +
                        "NLEID integer primary key autoincrement, " +
                        "content text, " +
                        "checked boolean, " +
                        "NoteID integer, " +
                        "scanned tinyint, " +
                        "actionText text, " +
                        "FOREIGN KEY(NoteID) REFERENCES Notes(NoteID)" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE NRecords (" +
                        "NRID integer primary key autoincrement, " +
                        "src varchar(255), " +
                        "NoteID integer, " +
                        "FOREIGN KEY(NoteID) REFERENCES Notes(NoteID)" +
                        ");"
        );


        db.execSQL(
                "CREATE TABLE Products (" +
                        "PID integer primary key autoincrement, " +
                        "pidCategory integer, " +
                        "price double, " +
                        "name varchar(180), " +
                        "info text," +
                        "lastUse date," +
                        "numberOfUse integer" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE Money (" +
                        "MID integer primary key autoincrement, " +
                        "price double, " +
                        "name varchar(5)" +
                        ");"
        );
        String[] labels = {"Personal", "Work", "Other"};
        for (int i = 0; i < 3; ++i) {
            ContentValues labelValue = new ContentValues();
            labelValue.put("name", labels[i]);
            labelValue.put("drawable", i + 1);
            labelValue.put("position", i + 1);
            db.insert("NLabels", null, labelValue);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS Notes");
        db.execSQL("DROP TABLE IF EXISTS NNotes");
        db.execSQL("DROP TABLE IF EXISTS NListElements");
        db.execSQL("DROP TABLE IF EXISTS NPictures");
        db.execSQL("DROP TABLE IF EXISTS NRecords");
        db.execSQL("DROP TABLE IF EXISTS NLabels");
        db.execSQL("DROP TABLE IF EXISTS Products");
        db.execSQL("DROP TABLE IF EXISTS Money");
        onCreate(db);
    }

    public synchronized int insertNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues noteValues = new ContentValues();
        noteValues.put("title", note.getTitle());
        noteValues.put("TypeID", note.getTypeId());
        noteValues.put("creatingDate", mFormat.format(note.getCreateDate().getTime()));
        noteValues.put("BgColor", note.getBgColor());
        noteValues.put("staticPosition", note.getStaticPosition());
        noteValues.put("numberOfView", note.getNumberOfView());
        noteValues.put("NLID", note.getLabel());
        noteValues.put("TypeID", note.getTypeId());
        noteValues.put("lastModification", note.getLastModification());
        noteValues.put("Category", note.getCategory());
        noteValues.put("secured", note.getSecured());
        noteValues.put("reminder", note.getReminder() != null ? note.getReminder() : "0");

        long lastId = db.insert("Notes", null, noteValues);
        if (lastId != -1) {
            if (note.getText() != null) {

                ContentValues nnote = new ContentValues();
                nnote.put("note", note.getText().getText());
                nnote.put("actionText", note.getText().getActionText());
                nnote.put("NoteId", lastId);
                nnote.put("scanned", note.getText().getScanned() ? 1 : 0);
                db.insert("NNotes", null, nnote);
            }
            if (note.getList() != null && note.getList().size() > 0) {
                List<NoteChecked> clist = note.getList();
                for (NoteChecked ch : clist) {
                    ContentValues listElement = new ContentValues();
                    listElement.put("content", ch.getLabel());
                    listElement.put("checked", ch.getChecked());
                    listElement.put("actionText", ch.getActionText());
                    listElement.put("NoteID", lastId);
                    listElement.put("scanned", ch.getScanned() ? 1 : 0);
                    db.insert("NListElements", null, listElement);
                }
            }

            if (note.getImagesList().size() > 0) {
                List<Picture> clist = note.getImagesList();
                for (Picture picture : clist) {

                    ContentValues pictureElement = new ContentValues();
                    pictureElement.put("NoteID", lastId);
                    pictureElement.put("src", picture.getSrc());
                    pictureElement.put("pictureType", picture.getPictureType());
                    db.insert("NPictures", null, pictureElement);
                }
            }
            if (!note.getRecord().isEmpty()) {
                ContentValues record = new ContentValues();
                record.put("src", note.getRecord().get(0));
                db.insert("NRecords", null, record);
            }
        }
        return (int) lastId;
    }

    public synchronized NoteLite getCardNote(int id, int count) {
        if (id >= 0) {
            SQLiteDatabase db = this.getReadableDatabase();
            NoteLite note = new NoteLite();

            Cursor res = db.query("Notes", new String[]{"staticPosition", "BgColor", "title", "numberOfView", "NLID", "creatingDate", "reminder", "TypeID", "secured", "lastModification", "category"}, "NoteID = ?", new String[]{Integer.toString(id)}, null, null, null);

            if (res != null && res.moveToFirst()) {

                try {
                    note = new NoteLite(id, res.getFloat(0), res.getInt(1), res.getString(2), res.getFloat(3), res.getInt(4),
                            DateToCalendar(mFormat.parse(res.getString(5))), res.getString(6), res.getInt(7),
                            res.getString(8).equals("true"), res.getString(9), res.getLong(10));

                } catch (Exception e) {
                    e.getMessage();
                }
                res.close();
                res = db.query("NListElements", new String[]{"content", "checked"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "checked, NLEID ASC", Integer.toString(count));
                if (res != null && res.moveToFirst()) {
                    ArrayList<NoteCheckedLite> listElements = new ArrayList<>();
                    while (!res.isAfterLast()) {
                        listElements.add(new NoteCheckedLite(res.getString(0), (res.getInt(1) == 1)));
                        res.moveToNext();
                    }
                    note.setList(listElements);
                    res.close();
                }

                res = db.query("NNotes", new String[]{"note"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    note.setText(res.getString(0));
                    res.close();
                }

                res = db.query("NPictures", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "1");
                if (res != null && res.moveToFirst()) {
                    note.setImage(res.getString(0));
                    res.close();
                }
                res = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    note.setRecord(res.getString(0));
                    res.close();
                }
            }
            return note;
        } else {
            return null;
        }
    }

    public synchronized Note getWidgetNote(int id, int count) {
        if (id >= 0) {
            SQLiteDatabase db = this.getReadableDatabase();
            Note note = new Note();

            Cursor res = db.query("Notes", new String[]{"staticPosition", "BgColor", "title", "numberOfView", "NLID", "creatingDate", "reminder", "TypeID", "secured", "lastModification", "category", "widgets"}, "NoteID = ?", new String[]{Integer.toString(id)}, null, null, null);

            if (res != null && res.moveToFirst()) {

                try {
                    note = new Note(id, res.getFloat(0), res.getInt(1), res.getString(2), res.getFloat(3), res.getInt(4),
                            DateToCalendar(mFormat.parse(res.getString(5))), res.getString(6), res.getInt(7),
                            res.getString(8).equals("true"), res.getString(9), res.getLong(10), res.getString(11));

                } catch (Exception e) {
                    e.getMessage();
                    return null;
                }
                res.close();
                res = db.query("NListElements", new String[]{"content", "checked"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "checked, NLEID ASC", Integer.toString(count));
                if (res != null && res.moveToFirst()) {
                    ArrayList<NoteChecked> listElements = new ArrayList<>();
                    while (!res.isAfterLast()) {
                        listElements.add(new NoteChecked(res.getString(0), (res.getInt(1) == 1)));
                        res.moveToNext();
                    }
                    note.setList(listElements);
                    res.close();
                }

                res = db.query("NNotes", new String[]{"note"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    note.setText(res.getString(0));
                    res.close();
                }

                res = db.query("NPictures", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "1");
                if (res != null && res.moveToFirst()) {
                    ArrayList<Picture> pictures = new ArrayList<>();
                    pictures.add(new Picture(res.getString(0)));
                    note.setImagesList(pictures);
                    res.close();
                }
                res = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    ArrayList<String> records = new ArrayList<>();
                    records.add(res.getString(0));
                    note.setRecord(records);
                    res.close();
                }
            }
            return note;
        } else {
            return null;
        }
    }

    public synchronized Note getNote(int id) {
        if (id >= 0) {
            SQLiteDatabase db = this.getReadableDatabase();
            Note note = new Note();

            Cursor res = db.query("Notes", new String[]{"staticPosition", "BgColor", "title", "numberOfView", "NLID", "creatingDate", "reminder", "TypeID", "secured", "lastModification", "category", "widgets"}, "NoteID = ?", new String[]{Integer.toString(id)}, null, null, null);

            if (res != null && res.moveToFirst()) {

                try {
                    note = new Note(id, res.getFloat(0), res.getInt(1), res.getString(2), res.getFloat(3), res.getInt(4),
                            DateToCalendar(mFormat.parse(res.getString(5))), res.getString(6), res.getInt(7),
                            res.getString(8).equals("true"), res.getString(9), res.getLong(10), res.getString(11));

                } catch (Exception e) {
                    e.getMessage();
                }
                res.close();
                res = db.query("NListElements", new String[]{"content", "checked", "actionText", "scanned", "NLEID"}, "NListElements.NoteID=?", new String[]{Integer.toString(id)}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    List<NoteChecked> listElements = note.getList();
                    while (!res.isAfterLast()) {
                        boolean checked = false;
                        if (res.getInt(1) == 1) {
                            checked = true;
                        }
                        listElements.add(new NoteChecked(res.getLong(4), res.getString(0), checked, res.getString(2), (res.getInt(3) == 1)));
                        res.moveToNext();
                    }
                    res.close();
                }
                res = db.query("NNotes", new String[]{"note", "actionText", "scanned"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    note.setText(res.getString(0), res.getString(1), (res.getInt(2) == 1));
                    res.close();

                }

                res = db.query("NPictures", new String[]{"src", "pictureType"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    List<Picture> picturesList = note.getImagesList();
                    while (!res.isAfterLast()) {
                        picturesList.add(new Picture(res.getString(0), res.getInt(1)));
                        res.moveToNext();
                    }
                    res.close();
                }
                res = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    while (!res.isAfterLast()) {
                        note.getRecord().add(res.getString(0));
                        res.moveToNext();
                    }
                    res.close();
                }
            }
            return note;
        } else {
            return null;
        }
    }

    public synchronized Note getCardNote(int id) {
        if (id >= 0) {
            SQLiteDatabase db = this.getReadableDatabase();
            Note note = new Note();

            Cursor res = db.query("Notes", new String[]{"staticPosition", "BgColor", "title", "numberOfView", "NLID", "creatingDate", "reminder", "TypeID", "secured", "lastModification", "category", "widgets"}, "NoteID = ?", new String[]{Integer.toString(id)}, null, null, null);

            if (res != null && res.moveToFirst()) {

                try {
                    note = new Note(id, res.getFloat(0), res.getInt(1), res.getString(2), res.getFloat(3), res.getInt(4),
                            DateToCalendar(mFormat.parse(res.getString(5))), res.getString(6), res.getInt(7),
                            res.getString(8).equals("true"), res.getString(9), res.getLong(10), res.getString(11));

                } catch (Exception e) {
                    e.getMessage();
                }
                res.close();
                res = db.query("NListElements", new String[]{"content", "checked", "actionText", "scanned", "NLEID"}, "NoteID=?", new String[]{Integer.toString(id)}, null, null, "checked, NLEID ASC", null);
                if (res != null && res.moveToFirst()) {
                    List<NoteChecked> listElements = note.getList();
                    while (!res.isAfterLast()) {
                        boolean checked = false;
                        if (res.getInt(1) == 1) {
                            checked = true;
                        }
                        listElements.add(new NoteChecked(res.getLong(4), res.getString(0), checked, res.getString(2), (res.getInt(3) == 1)));
                        res.moveToNext();
                    }
                    res.close();
                }
                res = db.query("NNotes", new String[]{"note", "actionText", "scanned"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    note.setText(res.getString(0), res.getString(1), (res.getInt(2) == 1));
                    res.close();

                }

                res = db.query("NPictures", new String[]{"src", "pictureType"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    List<Picture> picturesList = note.getImagesList();
                    while (!res.isAfterLast()) {
                        picturesList.add(new Picture(res.getString(0), res.getInt(1)));
                        res.moveToNext();
                    }
                    res.close();
                }
                res = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (res != null && res.moveToFirst()) {
                    String record = res.getString(0);
                    if (!record.isEmpty()) {
                        note.getRecord().add(record);
                    }
                    res.close();
                }
            }
            return note;
        } else {
            return null;
        }
    }

    public synchronized ArrayList<NoteLite> getQueryNotes(String query, long category) {
        ArrayList<NoteLite> notes = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        Cursor subRes;
        if (res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                NoteLite note;
                boolean ok = true;
                try {
                    note = new NoteLite(res.getInt(0), res.getString(1), DateToCalendar(mFormat.parse(res.getString(2))), res.getString(3), res.getString(4), res.getInt(5), res.getString(6).equals("true"), res.getFloat(7), res.getInt(8), res.getInt(9), res.getFloat(10), res.getLong(11));
                    long ccategory = note.getCategory();
                    for (int i = 0; i < 64; ++i) {
                        if (((category >> i) & 1) == 1 && ((ccategory >> i) & 1) != 1) {
                            ok = false;
                            break;
                        }
                    }
                } catch (Exception e) {
                    ok = false;
                    note = new NoteLite();
                    e.getMessage();
                }
                if (ok) {
                    Cursor lelements = db.query("NListElements", new String[]{"content", "checked"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null, "11");
                    if (lelements != null && lelements.moveToFirst()) {
                        ArrayList<NoteCheckedLite> listElements = new ArrayList<>();
                        while (!lelements.isAfterLast()) {
                            listElements.add(new NoteCheckedLite(lelements.getString(0), (lelements.getInt(1) == 1)));
                            lelements.moveToNext();
                        }
                        note.setList(listElements);
                        lelements.close();
                    }

                    Cursor nnote = db.query("NNotes", new String[]{"note"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                    if (nnote != null && nnote.moveToFirst()) {
                        note.setText(nnote.getString(0));
                        nnote.close();
                    }

                    Cursor pictures = db.query("NPictures", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                    if (pictures != null && pictures.moveToFirst()) {
                        note.setImage(pictures.getString(0));
                        pictures.close();

                    }
                    subRes = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                    if (subRes != null && subRes.moveToFirst()) {

                        note.setRecord(subRes.getString(0));
                        subRes.close();
                    }
                    notes.add(note);
                }
                res.moveToNext();
            }
            res.close();
        }
        return getSortedAuto(notes);
    }

    public synchronized ArrayList<Note> getSortedNotes(int typeID, int label) {
        ArrayList<Note> notes = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String type = typeID < 0 ? "" : (typeID < 2 ? "TypeID=" + typeID : "TypeID >= 2");
        if (label != -1) {
            type += " AND NLID=" + label;
        }
        Cursor res, subRes;
        res = db.query("Notes", new String[]{"NoteId", "title", "creatingDate", "lastModification", "reminder", "BgColor", "secured", "numberOfView", "NLID", "TypeID", "staticPosition", "category", "widgets"},
                type, null,
                null, null, "creatingDate DESC");

        if (res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                Note note;
                try {
                    note = new Note(res.getInt(0), res.getString(1), DateToCalendar(mFormat.parse(res.getString(2))), res.getString(3), res.getString(4), res.getInt(5), res.getString(6).equals("true"), res.getFloat(7), res.getInt(8), res.getInt(9), res.getFloat(10), res.getLong(11), res.getString(12));
                } catch (Exception e) {
                    note = new Note();
                    e.getMessage();
                }
                Cursor lElements = db.query("NListElements", new String[]{"content", "checked"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "checked, NLEID ASC", "11");
                if (lElements != null && lElements.moveToFirst()) {
                    List<NoteChecked> listElements = note.getList();
                    while (!lElements.isAfterLast()) {
                        listElements.add(new NoteChecked(lElements.getString(0), (lElements.getInt(1) == 1)));
                        lElements.moveToNext();
                    }
                    lElements.close();
                }

                Cursor nnote = db.query("NNotes", new String[]{"note"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (nnote != null && nnote.moveToFirst()) {
                    note.setText(nnote.getString(0));
                    nnote.close();
                }

                Cursor pictures = db.query("NPictures", new String[]{"src", "pictureType"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "1");
                if (pictures != null && pictures.moveToFirst()) {
                    List<Picture> picturesList = note.getImagesList();
                    while (!pictures.isAfterLast()) {
                        picturesList.add(new Picture(pictures.getString(0), pictures.getInt(1)));
                        pictures.moveToNext();
                    }
                    pictures.close();
                }
                subRes = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (subRes != null && subRes.moveToFirst()) {

                    note.getRecord().add(subRes.getString(0));
                    subRes.close();
                }
                notes.add(note);
                res.moveToNext();
            }
            res.close();
        }
        return notes;
    }


    public synchronized ArrayList<NoteLite> getSortedCardNotes(int sorttype, int typeID, int label) {
        ArrayList<NoteLite> notes = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String type = typeID < 0 ? "" : (typeID < 2 ? "TypeID=" + typeID : "TypeID >= 2");
        if (label != -1) {
            type += " AND NLID=" + label;
        }
        Cursor res, subRes;
        int sortType = sorttype == 4 ? 0 : sorttype;

        switch (sortType) {
            case 0:
                res = db.query("Notes", new String[]{"NoteId", "substr(title, 0, 100)", "creatingDate", "lastModification", "reminder", "BgColor", "secured", "numberOfView", "NLID", "TypeID", "staticPosition", "category"},
                        type, null,
                        null, null, "creatingDate DESC");
                break;
            case 1:
                res = db.query("Notes", new String[]{"NoteId", "title", "creatingDate", "lastModification", "reminder", "BgColor", "secured", "numberOfView", "NLID", "TypeID", "staticPosition", "category"},
                        type, null,
                        null, null, "lastModification DESC");
                break;
            case 2:
                res = db.query("Notes", new String[]{"NoteId", "title", "creatingDate", "lastModification", "reminder", "BgColor", "secured", "numberOfView", "NLID", "TypeID", "staticPosition", "category"},
                        type, null,
                        null, null, "title COLLATE NOCASE ASC, creatingDate DESC");
                break;
            default:
                res = db.query("Notes", new String[]{"NoteId", "title", "creatingDate", "lastModification", "reminder", "BgColor", "secured", "numberOfView", "NLID", "TypeID", "staticPosition", "category"},
                        type, null,
                        null, null, "reminder DESC, creatingDate DESC");
                break;
        }
        if (res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                NoteLite note;
                try {
                    note = new NoteLite(res.getInt(0), res.getString(1), DateToCalendar(mFormat.parse(res.getString(2))), res.getString(3), res.getString(4), res.getInt(5), res.getString(6).equals("true"), res.getFloat(7), res.getInt(8), res.getInt(9), res.getFloat(10), res.getLong(11));
                } catch (Exception e) {
                    note = new NoteLite();
                    e.getMessage();
                }

                subRes = db.query("NListElements", new String[]{"substr(content, 0, 51)", "checked"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "checked, NLEID ASC", "11");
                if (subRes != null && subRes.moveToFirst()) {
                    ArrayList<NoteCheckedLite> listElements = new ArrayList<>();
                    while (!subRes.isAfterLast()) {
                        listElements.add(new NoteCheckedLite(subRes.getString(0), (subRes.getInt(1) == 1)));
                        subRes.moveToNext();
                    }
                    note.setList(listElements);
                    subRes.close();
                }

                subRes = db.query("NNotes", new String[]{"substr(note, 0 , 201)"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (subRes != null && subRes.moveToFirst()) {
                    note.setText(subRes.getString(0));
                    subRes.close();
                }

                subRes = db.query("NPictures", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, "1");
                if (subRes != null && subRes.moveToFirst()) {
                    note.setImage(subRes.getString(0));
                    subRes.close();
                }
                subRes = db.query("NRecords", new String[]{"src"}, "NoteID=?", new String[]{Integer.toString(note.getID())}, null, null, null);
                if (subRes != null && subRes.moveToFirst()) {

                    note.setRecord(subRes.getString(0));
                    subRes.close();
                }
                notes.add(note);
                res.moveToNext();
            }
            res.close();
        }
        if (sorttype == 4) {
            return notes.size() < 2 ? notes : getSortedAuto(notes);
        }
        return notes;
    }

    private synchronized ArrayList<NoteLite> getSortedAuto(List<NoteLite> notes) {
        ArrayList<Double> noteValues = new ArrayList<>();
        int n = notes.size();
        for (NoteLite nt : notes) {
            noteValues.add(getValue(nt));
        }
        for (int j = 1; j < n; ++j) {
            NoteLite assistant = notes.get(j);
            double assistantValue = noteValues.get(j);
            int i = j - 1;
            while ((i >= 0) && (noteValues.get(i)) < assistantValue) {
                notes.set(i + 1, notes.get(i));
                noteValues.set(i + 1, noteValues.get(i));
                --i;
            }

            notes.set(i + 1, assistant);
            noteValues.set(i + 1, assistantValue);
        }

        return (ArrayList<NoteLite>) notes;
    }

    public synchronized static Calendar DateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
    public synchronized void deletedLabel(int label){
        SQLiteDatabase db = this.getWritableDatabase();
        String labelSQL = "UPDATE Notes SET NLID = 3 WHERE NLID = " + label + ";";
        db.execSQL(labelSQL);
    }
    public synchronized boolean updateNoteLite(NoteLite note) {
        // if part == 0 - all, part == 1 just-Note, part = 2 - Content
        SQLiteDatabase db = this.getWritableDatabase();
        int id = note.getID();

        ContentValues noteValues = new ContentValues();
        noteValues.put("title", note.getTitle());
        noteValues.put("creatingDate", mFormat.format(note.getCreateDate().getTime()));
        noteValues.put("reminder", note.getReminder() != null ? note.getReminder() : "0");


        noteValues.put("BgColor", note.getBgColor());
        noteValues.put("staticPosition", note.getStaticPosition());
        noteValues.put("numberOfView", note.getNumberOfView());
        noteValues.put("lastModification", note.getLastModification());
        noteValues.put("NLID", note.getLabel());
        noteValues.put("TypeID", note.getTypeId());
        noteValues.put("Category", note.getCategory());
        noteValues.put("secured", note.getSecured());

        db.update("Notes", noteValues, "NoteID = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public synchronized boolean updateNote(Note note, int part, int type) {
        // if part == 0 - all, part == 1 just-Note, part = 2 - Content
        SQLiteDatabase db = this.getWritableDatabase();
        int id = note.getID();
        if (part == 0 || part == 1) {
            ContentValues noteValues = new ContentValues();
            noteValues.put("title", note.getTitle());
            noteValues.put("creatingDate", mFormat.format(note.getCreateDate().getTime()));
            noteValues.put("reminder", note.getReminder() != null ? note.getReminder() : "0");


            noteValues.put("BgColor", note.getBgColor());
            noteValues.put("staticPosition", note.getStaticPosition());
            noteValues.put("numberOfView", note.getNumberOfView());
            noteValues.put("lastModification", note.getLastModification());
            noteValues.put("NLID", note.getLabel());
            noteValues.put("TypeID", note.getTypeId());
            noteValues.put("Category", note.getCategory());
            noteValues.put("secured", note.getSecured());
            noteValues.put("widgets", note.getWidgets());
            db.update("Notes", noteValues, "NoteID = ? ", new String[]{Integer.toString(id)});
        }
        if (part == 0 || part == 2) {
            if (type == 0) {
                Cursor res = db.query("NNotes", new String[]{"note"}, "NoteID=?", new String[]{Integer.toString(id)}, null, null, null);
                if (!note.getText().isEmpty()) {
                    ContentValues nnote = new ContentValues();
                    nnote.put("note", note.getText().getText());
                    nnote.put("actionText", note.getText().getActionText());

                    nnote.put("scanned", note.getText().getScanned() ? 1 : 0);

                    if (res != null && res.moveToFirst()) {
                        db.update("NNotes", nnote, "NoteID= ? ", new String[]{Integer.toString(id)});
                        res.close();
                    } else {
                        nnote.put("NoteId", id);
                        db.insert("NNotes", null, nnote);
                    }
                } else {
                    db.delete("NNotes", "NoteID = ?", new String[]{Integer.toString(id)});
                    note.setText(null);
                }
            } else if (type == 1) {
                db.delete("NListElements", "NoteId=?", new String[]{Integer.toString(id)});
                List<NoteChecked> clist = note.getList();
                for (NoteChecked ch : clist) {
                    ContentValues listElement = new ContentValues();
                    listElement.put("NoteID", id);
                    listElement.put("content", ch.getLabel());
                    listElement.put("checked", ch.getChecked());
                    listElement.put("actionText", ch.getActionText());

                    listElement.put("scanned", ch.getScanned() ? 1 : 0);

                    db.insert("NListElements", null, listElement);
                }

            } else if (type == 2) {
                db.delete("NPictures", "NoteID=?", new String[]{Integer.toString(id)});
                List<Picture> clist = note.getImagesList();
                for (Picture ch : clist) {
                    ContentValues listElement = new ContentValues();
                    listElement.put("NoteID", id);
                    listElement.put("src", ch.getSrc());
                    listElement.put("pictureType", ch.getPictureType());
                    db.insert("NPictures", null, listElement);
                }
            } else if (type == 3) {
                db.delete("NRecords", "NoteID=?", new String[]{Integer.toString(id)});
                for (String src : note.getRecord()) {
                    if (src.length() > 10) {
                        ContentValues recordElement = new ContentValues();
                        recordElement.put("src", src);
                        recordElement.put("NoteID", id);
                        db.insert("NRecords", null, recordElement);
                    }
                }
            }
        }
        return true;
    }

    public synchronized Integer deleteNote(Note note, Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("NRecords", "NoteID = ?", new String[]{Integer.toString(note.getID())});
        db.delete("NNotes", "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        FileManager fm = new FileManager(context);
        for (Picture pc : note.getImagesList()) {
            fm.deleteFile(pc.getSrc().split(":")[1]);
        }
        db.delete("NListElements", "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        db.delete("NPictures", "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        if (!note.getRecord().isEmpty()) {
            for (String src : note.getRecord()) {
                if (!src.isEmpty() && src.length() > 10) {
                    fm.deleteFile(src.split(":")[1]);
                }
            }
            db.delete("NRecords", "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        }
        db.delete("Notes", "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        return 0;
    }

    public synchronized int insertNewNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues noteValues = new ContentValues();
        noteValues.put("title", note.getTitle());
        noteValues.put("TypeID", note.getTypeId());
        noteValues.put("creatingDate", mFormat.format(note.getCreateDate().getTime()));
        noteValues.put("BgColor", note.getBgColor());
        noteValues.put("staticPosition", note.getStaticPosition());
        noteValues.put("numberOfView", note.getNumberOfView());
        noteValues.put("lastModification", note.getLastModification());
        noteValues.put("NLID", note.getLabel());
        noteValues.put("reminder", "0");
        noteValues.put("Category", note.getCategory());
        noteValues.put("secured", note.getSecured());
        noteValues.put("widgets", note.getWidgets());
        return (int) db.insert("Notes", null, noteValues);
    }

    public synchronized void removeLabel(String labelName) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor res = db.rawQuery("Select Notes.NoteID From Notes, NLabels Where Notes.NLID = NLabels.NLID and NLabels.name ='" + labelName + "'", new String[]{});
        if (res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                ContentValues label = new ContentValues();
                label.put("NLID", 3);
                db.update("Notes", label, "NoteID=?", new String[]{Integer.toString(res.getInt(0))});
                res.moveToNext();
            }

            res.close();
        }
        db.delete("NLabels", "name=?", new String[]{labelName});
    }

    public synchronized int addLabel(String labelName, int drawable) {
        int pos = getLargestPostitionLabel();
        if (pos == -1) {
            return -1;

        }
        ContentValues label = new ContentValues();
        label.put("name", labelName);
        label.put("drawable", drawable);
        label.put("position", pos + 1);
        return (int) this.getWritableDatabase().insert("NLabels", null, label);
    }

    public synchronized ArrayList<Label> getLabels() {
        ArrayList<Label> labels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("NLabels", new String[]{"NLID", "name", "drawable", "position"}, null, null, null, null, "position");
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                labels.add(new Label(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return labels;
    }

    public List<Integer> getCategorys() {
        List<Integer> categorys = new ArrayList<>();
        boolean[] category = new boolean[64];
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(true, "Notes", new String[]{"category"}, null, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                long cat = cursor.getLong(0);
                if (cat > 0) {
                    for (int i = 0; i < 64; ++i) {
                        if (!category[i]) {
                            category[i] = ((cat >> i) & 1) == 1;
                        }
                    }
                }
                cursor.moveToNext();
            }
            cursor.close();
        }
        for (int i = 0; i < 64; ++i) {
            if (category[i]) {
                categorys.add(i);
            }
        }
        return categorys;
    }

    public synchronized boolean updateLabel(Label label) {
        SQLiteDatabase db = this.getReadableDatabase();
        if (label.getId() >= 0) {
            ContentValues lbel = new ContentValues();
            lbel.put("name", label.getName());
            lbel.put("drawable", label.getDrawable());
            lbel.put("position", label.getPosition());
            db.update("NLabels", lbel, "NLID=?", new String[]{Integer.toString(label.getId())});
        }
        return true;
    }

    public synchronized Label getLabel(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select NLID, name , drawable From NLabels Where NLID = " + id + " LIMIT 1", null);
        if (cursor != null && cursor.moveToFirst() && !cursor.isAfterLast()) {
            Label labels = new Label(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));
            cursor.close();
            return labels;
        }
        return null;
    }

    public synchronized Label getLabel(String definition) {
        Label labels;
        String max = "";
        if (definition.contains("<")) {
            max = " ORDER BY NLID DESC";
            if (definition.contains("-1")) {
                definition = " > -1";
            }
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select NLID, name , drawable From NLabels Where NLID " + definition + max + " LIMIT 1", null);
        if (cursor != null && cursor.moveToFirst() && !cursor.isAfterLast()) {
            labels = new Label(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));
            cursor.close();
            return labels;
        }
        return null;
    }

    private synchronized int getLargestPostitionLabel() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("NLabels", new String[]{"position"}, null, null, null, null, "position DESC", "1");
        if (cursor != null && cursor.moveToFirst()) {
            int pos = cursor.getInt(0);
            cursor.close();
            return pos;
        }
        return -1;
    }

    public synchronized void insertNoteList(List<Note> notes) {
        for (Note note : notes) {
            insertNote(note);
        }
    }

    public synchronized void removeListElement(NoteChecked listElement, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues noteValues = new ContentValues();

        noteValues.put("lastModification", note.getLastModification());
        db.update("Notes", noteValues, "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        db.delete("NListElements", "NLEID=? and NoteID=?", new String[]{Long.toString(listElement.getId()), Integer.toString(note.getID())});
    }

    public synchronized long insertListElement(NoteChecked listElement, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues noteValues = new ContentValues();

        noteValues.put("lastModification", note.getLastModification());
        db.update("Notes", noteValues, "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        ContentValues element = new ContentValues();
        element.put("NoteID", note.getID());
        element.put("content", listElement.getLabel());
        element.put("checked", listElement.getChecked());
        element.put("actionText", listElement.getActionText());

        element.put("scanned", listElement.getScanned() ? 1 : 0);
        return db.insert("NListElements", null, element);
    }

    public synchronized void updateListElement(NoteChecked listElement, Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues noteValues = new ContentValues();

        noteValues.put("lastModification", note.getLastModification());
        db.update("Notes", noteValues, "NoteID = ? ", new String[]{Integer.toString(note.getID())});
        ContentValues element = new ContentValues();
        element.put("content", listElement.getLabel());
        element.put("checked", listElement.getChecked());
        element.put("actionText", listElement.getActionText());
        element.put("scanned", listElement.getScanned() ? 1 : 0);

        db.update("NListElements", element, "NLEID=? and NoteID=?", new String[]{Long.toString(listElement.getId()), Integer.toString(note.getID())});
    }
    public synchronized void updateListElement(NoteChecked listElement, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues element = new ContentValues();
        element.put("content", listElement.getLabel());
        element.put("checked", listElement.getChecked());
        element.put("actionText", listElement.getActionText());
        element.put("scanned", listElement.getScanned() ? 1 : 0);

        db.update("NListElements", element, "NLEID=? and NoteID=?", new String[]{Long.toString(listElement.getId()), Integer.toString(id)});
    }


    public double getValue(NoteLite value) {
        double sum = 0.0;
        Calendar cal = new GregorianCalendar();
        double age = (cal.getTimeInMillis() - value.getCreateDate().getTimeInMillis()) / 86400000.0;
        sum += (value.getNumberOfView() + value.getStaticPosition() + (!value.getTitle().isEmpty() ? 0.2 : 0.0) + (age < 1.0 ? 10 : 10 / age));
        try {
            String reminder = value.getReminder();
            if (reminder.startsWith("T")) {
                Calendar calendar = DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(reminder.substring(1, reminder.length())));
                double millis = ((calendar.getTimeInMillis() - cal.getTimeInMillis()) / 86400000.0);
                if (millis < 10.0 && millis > 0) {
                    sum += ((10.0 - millis) / 3.0);
                }

            }
        } catch (Exception e) {
            Log.e("Date Format Exception", e.toString());
        }

        if (value.getText() != null && !value.getText().isEmpty()) {
            sum += 0.30;
        }
        if (value.getList() != null && !value.getList().isEmpty()) {
            sum += (0.06 * value.getList().size());
        }
        if (value.getImage() != null && !value.getImage().isEmpty()) {
            sum += 0.20;
        }
        if (value.getRecord() != null && !value.getRecord().isEmpty()) {
            sum += 0.20;
        }
        if (value.getBgColor() != 0) {
            sum += 0.50;
        }
        return sum;
    }

}
