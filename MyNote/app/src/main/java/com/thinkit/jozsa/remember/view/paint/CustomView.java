package com.thinkit.jozsa.remember.view.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.support.v7.widget.AppCompatImageView;
import com.thinkit.jozsa.remember.R;

import java.util.ArrayList;


public class CustomView extends AppCompatImageView{

    //drawing path
    private Path drawPath;


    //defines what to draw

    //defines how to draw
    private Paint mDrawPaint;

    //initial color
    private int mPaintColor = 0xFF000000;


    //canvas - holding pen, holds your drawings
    //and transfers them to the view

    //canvas bitmap

    //brush size
    private float mLineSize;
    private float mSize;
    private boolean mErase;
    private int mUndoRedo;
    private int mNumberOfLines = 0;
    private ArrayList<Path> mPaths;
    private ArrayList<Path> mUndonePaths;
    private ArrayList<Integer> mPathsColor;
    private ArrayList<Integer> mUndonePathsColor;
    private ArrayList<Float> mPathsLine;

    private ArrayList<Float> mUndonePathsLine;
    private int mEraseColor = 0XFFEEEEEE;

    private void init(){
        mLineSize = getResources().getInteger(R.integer.small_size);
        mSize = mLineSize;
        mErase = false;
        mPaths = new ArrayList<>();
        mUndonePaths = new ArrayList<>();
        mPathsColor = new ArrayList<>();
        mUndonePathsColor = new ArrayList<>();
        mPathsLine = new ArrayList<>();
        mUndonePathsLine = new ArrayList<>();
        drawPath = new Path();
        mDrawPaint = new Paint();
        mDrawPaint.setColor(mPaintColor);
        mDrawPaint.setAntiAlias(true);
        mDrawPaint.setStrokeWidth(mLineSize);
        mDrawPaint.setStyle(Paint.Style.STROKE);
        mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
        mDrawPaint.setStrokeCap(Paint.Cap.ROUND);
        setDrawingCacheEnabled(true);

    }


    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //  canvas.drawBitmap(mCanvasBitmap, 0, 0, mCanvasPaint);
        for (int i = 0; i < mPaths.size(); ++i){
            mDrawPaint.setColor(mPathsColor.get(i));
            mDrawPaint.setStrokeWidth(mPathsLine.get(i));
            canvas.drawPath(mPaths.get(i), mDrawPaint);
        }
        mDrawPaint.setColor(mErase ? mEraseColor : mPaintColor);
        mDrawPaint.setStrokeWidth(mSize);
        canvas.drawPath(drawPath, mDrawPaint);
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        //respond to down, move and up events
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mUndonePaths.clear();
                mUndoRedo = 0;
                mUndonePathsLine.clear();
                mUndonePathsColor.clear();
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                drawPath.lineTo(touchX, touchY);
                mPaths.add(drawPath);
                mPathsColor.add(!mErase ? mPaintColor : mEraseColor);
                mPathsLine.add(mSize);
                ++mNumberOfLines;
                drawPath = new Path();
                break;
            default:
                return false;
        }
        //redraw
        invalidate();
        return true;
    }
    public void setPaintColor(int color){
        mPaintColor = color;
    }

    public void setLineSize(float size){
        mSize = mLineSize * size;
    }
    public boolean undo(){
        int length = mPaths.size();
        if (length > 0 && mUndoRedo <= 10)
        {
            ++mUndoRedo;
            --mNumberOfLines;
            mUndonePaths.add(mPaths.remove(length - 1));
            mUndonePathsColor.add(mPathsColor.remove(length - 1));
            mUndonePathsLine.add(mPathsLine.remove(length - 1));
            invalidate();
            return true;
        }
        return false;
    }
    public boolean redo(){
        int length = mUndonePaths.size();
        if (length > 0)
        {
            if(mUndoRedo > 0) {
                --mUndoRedo;
            }
            ++mNumberOfLines;
            mPaths.add(mUndonePaths.remove(length - 1));
            mPathsColor.add(mUndonePathsColor.remove(length - 1));
            mPathsLine.add(mUndonePathsLine.remove(length - 1));
            invalidate();
            return true;
        }
        return false;
    }


    public void setErase(boolean erase) {
        mErase = erase;
    }
    public boolean save(){
        return (mNumberOfLines > 0);
    }

}
