package com.thinkit.jozsa.remember.view.paint;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.view.images.FullScreenImageActivity;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

import java.io.File;
import java.io.IOException;


public class PaintActivity extends AppCompatActivity {
    public static String DRAW_IMAGE = "PATH";
    public static String DRAW_IS_DRAWING = "DRAWING";
    public static String DRAW_REMOVE = "REMOVE";
    private String mSaved;
    private String mSavedShare;
    private CustomView mPaintPanel;
    private ImageView mBackgroundPanel;
    private ColorRecyclerAdapter mAdapter;
    private Animations mAnimation;
    private int mAction = 0;
    private boolean erase = false;
    private FileManager mFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_top));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mPaintPanel = (CustomView) findViewById(R.id.custom_view);
        mBackgroundPanel = (ImageView) findViewById(R.id.image_view);
        mPaintPanel.setDrawingCacheEnabled(true);
        mBackgroundPanel.setDrawingCacheEnabled(true);
        mFile = new FileManager(this);
        mSaved = getIntent().getStringExtra(FullScreenImageActivity.FULLSCREEN_POSITION_IMAGE);
        if (mSaved != null) {
            mAction = 1;
            Glide.with(this).load(mSaved).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(mBackgroundPanel);
        }
        mAnimation = new Animations();
        mAdapter = new ColorRecyclerAdapter(this);
        final ImageView colorButton = (ImageView) findViewById(R.id.draw_color);
        colorButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(PaintActivity.this, "Color", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        mAdapter.setOnClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                mPaintPanel.setPaintColor(mAdapter.getPosColor(pos));
                colorButton.setColorFilter(mAdapter.getPosColor(pos));
            }
        });
        final RecyclerView horizontal = (RecyclerView) findViewById(R.id.draw_color_recycler);
        horizontal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        horizontal.setAdapter(mAdapter);
        horizontal.setHasFixedSize(true);
        findViewById(R.id.draw_add_color).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ColorPicker cp = new ColorPicker(PaintActivity.this, 0, 0, 0);
                cp.show();
                Button okColor = (Button) cp.findViewById(R.id.okColorButton);
                okColor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int color = cp.getColor();
                        mPaintPanel.setPaintColor(color);
                        mAdapter.addColor(color);
                        colorButton.setColorFilter(color);
                        cp.dismiss();
                    }
                });
            }
        });
        final TextViewTypeface lineSize = (TextViewTypeface) findViewById(R.id.draw_line_size);
        lineSize.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(PaintActivity.this, "Line size", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        lineSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View relative = findViewById(R.id.draw_size_selector);
                if (relative.getVisibility() == View.VISIBLE) {
                    mAnimation.slideDown(relative, PaintActivity.this);
                } else {
                    hidePickers(true);
                    mAnimation.slideUp(relative, PaintActivity.this);
                }
            }
        });

        ((SeekBar) findViewById(R.id.draw_size_seek_bar)).setProgress(1);
        ((SeekBar) findViewById(R.id.draw_size_seek_bar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                ((TextViewTypeface) findViewById(R.id.draw_line_size_text)).setText(progresValue + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                lineSize.setText(progress + "");
                mPaintPanel.setLineSize((float) progress);
            }
        });
        final ImageView tools = (ImageView) findViewById(R.id.draw_tools);
        tools.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (erase) {
                    Toast.makeText(PaintActivity.this, "Erase", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PaintActivity.this, "Pen", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        tools.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (erase) {
                    tools.setImageDrawable(getDrawable(R.drawable.ic_create_black_24dp));
                } else {
                    tools.setImageDrawable(getDrawable(R.drawable.ic_erase_black_24dp));
                }
                erase = !erase;
                mPaintPanel.setErase(erase);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paint, menu);
        tintMenuIcon(menu.findItem(R.id.action_delete), ContextCompat.getColor(this, R.color.tintedDrawIcon));
        tintMenuIcon(menu.findItem(R.id.action_share), ContextCompat.getColor(this, R.color.tintedDrawIcon));
        return true;
    }

    @Override
    public void onBackPressed() {
        saveAndFinish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_undo:
                if (!mPaintPanel.undo()) {
                    Toast.makeText(this, "No more undo", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_redo:
                if (!mPaintPanel.redo()) {
                    Toast.makeText(this, "No more redo", Toast.LENGTH_SHORT).show();
                }
                break;
            case android.R.id.home:
                saveAndFinish();
                break;
            case R.id.action_delete:
                if (mSaved != null) {
                    if( 1 > new FileManager(this).deleteFile(mSaved.split(":")[1])){
                        Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
                    }
                }
                Intent intent = new Intent();
                intent.putExtra(DRAW_REMOVE, 1);
                setResult(RESULT_OK, intent);
                if (mSavedShare != null) {
                    mFile.deleteFile(mSavedShare);
                }
                mPaintPanel.destroyDrawingCache();
                mBackgroundPanel.destroyDrawingCache();
                finish();
                break;
            case R.id.action_share:
                if (new SettingsPreferences(PaintActivity.this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean("ActionLock")) {
                    Dialogs.dialogCheckPassword(PaintActivity.this, new SettingsPreferences(PaintActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                try {
                                    final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                    shareIntent.setType("image/jpg");
                                    File imgFile = mFile.createImageFile();
                                    if (mSavedShare != null) {
                                        mFile.deleteFile(mSavedShare);
                                    }
                                    mFile.saveToInternalStorage(combineImages(mBackgroundPanel.getDrawingCache(), mPaintPanel.getDrawingCache(true)), imgFile);
                                    mPaintPanel.setDrawingCacheEnabled(false);
                                    mPaintPanel.setDrawingCacheEnabled(true);

                                    mSavedShare = mFile.getPhotoPath().split(":")[1];
                                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imgFile));
                                    startActivity(Intent.createChooser(shareIntent, "Share image using"));

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    try {
                        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("image/jpg");
                        File imgFile = mFile.createImageFile();
                        if (mSavedShare != null) {
                            mFile.deleteFile(mSavedShare);
                        }
                        mFile.saveToInternalStorage(combineImages(mBackgroundPanel.getDrawingCache(), mPaintPanel.getDrawingCache(true)), imgFile);
                        mPaintPanel.setDrawingCacheEnabled(false);
                        mPaintPanel.setDrawingCacheEnabled(true);

                        mSavedShare = mFile.getPhotoPath().split(":")[1];
                        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imgFile));
                        startActivity(Intent.createChooser(shareIntent, "Share image using"));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showHideColorPicker(View view) {
        View relative = findViewById(R.id.draw_color_selector);
        if (relative.getVisibility() == View.VISIBLE) {
            mAnimation.slideDown(relative, this);
        } else {
            hidePickers(false);
            mAnimation.slideUp(relative, this);
        }
    }

    public void hidePickers(boolean color) {
        View view;
        if (color) {
            view = findViewById(R.id.draw_color_selector);
        } else {
            view = findViewById(R.id.draw_size_selector);
        }
        if (view.getVisibility() == View.VISIBLE) {
            mAnimation.slideDown(view, this);
        }
    }

    public static void tintMenuIcon(MenuItem item, int color) {
        Drawable normalDrawable = item.getIcon();
        Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
        DrawableCompat.setTint(wrapDrawable, color);

        item.setIcon(wrapDrawable);
    }

    public void saveAndFinish() {
        if (mPaintPanel.save() || mAction == 1) {
            try {
                Intent intent = new Intent();
                if (mSaved == null) {
                    mFile.saveToInternalStorage(combineImages(mBackgroundPanel.getDrawingCache(), mPaintPanel.getDrawingCache()), mFile.createImageFile());
                    intent.putExtra(DRAW_IMAGE, mFile.getPhotoPath());
                } else {
                    if (mPaintPanel.save()) {
                        mFile.saveToInternalStorage(combineImages(mBackgroundPanel.getDrawingCache(), mPaintPanel.getDrawingCache()), new File(mSaved.split(":")[1]));
                    }
                    intent.putExtra(DRAW_IMAGE, mSaved);
                }
                if (mPaintPanel.save()) {
                    intent.putExtra(DRAW_IS_DRAWING, 1);
                }
                setResult(RESULT_OK, intent);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mSavedShare != null) {
            mFile.deleteFile(mSavedShare);
        }
        mPaintPanel.destroyDrawingCache();
        mBackgroundPanel.destroyDrawingCache();
        finish();
    }

    public Bitmap combineImages(Bitmap c, Bitmap s) {

        Bitmap cs;

        cs = Bitmap.createBitmap(s.getWidth(), s.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(cs);

        comboImage.drawBitmap(c, 0, 0, null);
        comboImage.drawBitmap(s, 0, 0, null);

        return cs;
    }
}
