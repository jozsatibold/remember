package com.thinkit.jozsa.remember.models.note.subTypes.otherTypes;


import java.io.Serializable;

public class NoteChecked implements Serializable {
    private String mLabel;
    private Boolean mChecked;
    private Boolean mScanned;
    private String mActionText;
    private long mId;

    public NoteChecked() {
        this.mLabel = "";
        this.mChecked = false;
        this.mActionText = "";
        this.mScanned = false;
    }
    public NoteChecked(String label) {
        this.mLabel = label;
        this.mChecked = false;
        this.mActionText = "";
        this.mScanned = false;
    }

    public NoteChecked(int id) {
        this.mId = id;
        this.mLabel = "";
        this.mChecked = false;
        this.mActionText = "";
        this.mScanned = false;
    }

    public NoteChecked(String label, boolean checked) {
        this.mId = -1;
        this.mLabel = label;
        this.mChecked = checked;
        this.mActionText = "";
        this.mScanned = false;
    }

    public NoteChecked(long id, String label, boolean checked, String actionText, boolean scanned) {
        this.mId = id;
        this.mLabel = label;
        this.mChecked = checked;
        this.mActionText = actionText;
        this.mScanned = scanned;
    }

    public NoteChecked(String label, boolean checked, String actionText) {
        this.mId = 0;
        this.mLabel = label;
        this.mChecked = checked;
        this.mActionText = actionText;
        this.mScanned = false;
    }

    public NoteChecked(int id, String label, boolean checked) {
        this.mId = id;
        this.mLabel = label;
        this.mChecked = checked;
        this.mActionText = "";
        this.mScanned = false;
    }

    public NoteChecked(NoteChecked note) {
        this.mId = note.getId();
        this.mLabel = note.getLabel();
        this.mChecked = note.getChecked();
        this.mActionText = note.getActionText();
        this.mScanned = note.getScanned();
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public Boolean getChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    public long swithcID(long id) {
        long cid = this.mId;
        this.mId = id;
        return cid;
    }

    public String getActionText() {
        return mActionText;
    }

    public void setActionText(String actionText) {
        this.mActionText = actionText;
    }

    public Boolean getScanned() {
        return mScanned;
    }

    public void setScanned(Boolean scanned) {
        this.mScanned = scanned;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }
}
