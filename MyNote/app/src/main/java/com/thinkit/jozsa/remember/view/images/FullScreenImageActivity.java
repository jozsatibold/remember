package com.thinkit.jozsa.remember.view.images;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.paint.PaintActivity;

import java.io.File;


public class FullScreenImageActivity extends AppCompatActivity {
    public static String FULLSCREEN_RESULT_IMAGE = "EVENT";
    public static String FULLSCREEN_POSITION_IMAGE = "NOTE_GALLERY_POSITION";
    private ExtendedViewPager mViewPager;
    private FullScreenImageAdapter mTabsAdapter;
    private Note mNote;
    private int EVENT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);
        setSupportActionBar((Toolbar) findViewById(R.id.fullscreen_toolbar));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        mViewPager = (ExtendedViewPager) findViewById(R.id.fullscreen_pager);
        mNote = DatabaseManager.getInstance(this).getNote(getIntent().getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1));
        mTabsAdapter = new FullScreenImageAdapter(this, mNote.getImagesList(), this);
        mViewPager.setAdapter(mTabsAdapter);
        mViewPager.setCurrentItem(getIntent().getIntExtra(NoteActivity.NOTE_GALLERY_POSITION, 0));
        mViewPager.setOffscreenPageLimit(19);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mTabsAdapter.refreshItem(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fullscreen_image, menu);
        return true;

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(FULLSCREEN_RESULT_IMAGE, EVENT);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == NoteActivity.RESULT_PAINT) {
            int response = data.getIntExtra(PaintActivity.DRAW_REMOVE, 0);
            if (response == 1) {
                EVENT = 1;
                int i = mViewPager.getCurrentItem();
                int n = mTabsAdapter.getCount();
                mNote.getImagesList().remove(mViewPager.getCurrentItem());
                DatabaseManager.getInstance(this).updateNote(mNote, 2, 2);
                if (n <= 1) {
                    Intent intent2 = new Intent();
                    intent2.putExtra(FULLSCREEN_RESULT_IMAGE, EVENT);
                    setResult(RESULT_OK, intent2);
                    finish();
                }
                mTabsAdapter.removeImage(mViewPager, i);
                //mViewPager.removeViewAt(i);
            } else if (data.getIntExtra(PaintActivity.DRAW_IS_DRAWING, 0) == 1) {
                EVENT = 1;
                DatabaseManager.getInstance(this).updateNote(mNote, 2, 2);

                mTabsAdapter.refreshItem(mViewPager.getCurrentItem());

            }
            //mTabsAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra(FULLSCREEN_RESULT_IMAGE, EVENT);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case R.id.action_delete:
                EVENT = 1;
                int i = mViewPager.getCurrentItem();
                int n = mTabsAdapter.getCount();
                if (1 > new FileManager(this).deleteFile(mNote.getImagesList().get(i).getSrc().split(":")[1])) {
                    Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
                } else {
                    int pos = mViewPager.getCurrentItem();
                    mTabsAdapter.removeImage(mViewPager, i);
                    mNote.getImagesList().remove(pos);

                    DatabaseManager.getInstance(this).updateNote(mNote, 2, 2);
                    if (n <= 1) {
                        Intent intent2 = new Intent();
                        intent2.putExtra(FULLSCREEN_RESULT_IMAGE, EVENT);
                        setResult(RESULT_OK, intent2);
                        finish();
                    }
                }
                return true;
            case R.id.action_share:
                final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/jpg");
                final File photoFile = new File(mTabsAdapter.getList().get(mViewPager.getCurrentItem()).getSrc().split(":")[1]);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(photoFile));
                startActivity(Intent.createChooser(shareIntent, "Share image using"));
                return true;
            case R.id.action_edit:
                Intent intentPaint = new Intent(FullScreenImageActivity.this, PaintActivity.class);
                intentPaint.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
                intentPaint.putExtra(FullScreenImageActivity.FULLSCREEN_POSITION_IMAGE, mNote.getImagesList().get(mViewPager.getCurrentItem()).getSrc());
                startActivityForResult(intentPaint, NoteActivity.RESULT_PAINT);
                return true;
        }
        return false;
    }

}
