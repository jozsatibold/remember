package com.thinkit.jozsa.remember.view.home.adapters;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteCheckedLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.home.card.NoteViewHolder;
import com.thinkit.jozsa.remember.view.home.card.NoteCardView;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.widget.note.NoteWidget;
import com.thinkit.jozsa.remember.widget.note.NoteWidgetConfigureActivity;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class NotesAdapter extends RecyclerView.Adapter<NoteViewHolder> implements NoteItemTouchHelperAdapter {

    public static final String TIME_FORMAT = "MMM. d, HH:mm";
    private SimpleDateFormat mDateFormat;
    private LayoutInflater mInflater;
    private List<NoteLite> mDisplayedNotes;
    private NoteClickListener mListener;
    private SparseBooleanArray mSelectedNotes;
    private DatabaseManager mDb;
    private boolean mDirection;
    private int mSortType;
    private int mLabel;
    private int mType;
    private Context mContext;
    private int[] mColors;
    private SwipeListener mSwipeListener;
    private ToolbarImageClickListener mShowListener;
    private List<Integer> mUndoNotes;

    public NotesAdapter(boolean direction, int sortType, int label, Context context, int type) {
        mDb = DatabaseManager.getInstance(context);
        mDirection = direction;
        mSortType = sortType;
        mLabel = label;
        mType = type;
        mContext = context;
        mDateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        mColors = new Colors(context).getColorArray(3);
        mDisplayedNotes = mDb.getSortedCardNotes(mSortType, mType, mLabel);
        mInflater = LayoutInflater.from(context);
        mUndoNotes = new ArrayList<>();
        init();
    }

    public void setShowListener(ToolbarImageClickListener listener) {
        mShowListener = listener;
    }

    public void setSwipeListener(SwipeListener listener) {
        mSwipeListener = listener;
    }

    private void init() {
        if (mSelectedNotes == null) {
            mSelectedNotes = new SparseBooleanArray();
        } else {
            mSelectedNotes.clear();
        }
    }

    public void delete(Context context) {
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                Note note = mDb.getNote(mDisplayedNotes.get(i).getID());
                mDb.deleteNote(note, context);

                if (note.getWidgets().contains("|:")) {
                    Intent intent = new Intent(mContext, NoteWidget.class);
                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    String[] ids = note.getWidgets().split(":");
                    ArrayList<Integer> wids = new ArrayList<>();
                    if (ids.length > 0) {
                        for (int j = 1; j < ids.length - 1; ++j) {
                            if (!ids[j].equals("") && !ids[j].equals("|") && ids[j].length() > 1) {
                                int id = Integer.parseInt(ids[j].substring(0, ids[j].length() - 1));
                                wids.add(id);
                                NoteWidgetConfigureActivity.deleteTitlePref(mContext, id);
                            }

                        }
                        int id = Integer.parseInt(ids[ids.length - 1]);
                        wids.add(id);
                        NoteWidgetConfigureActivity.deleteTitlePref(mContext, id);
                    }
                    int list[] = new int[wids.size()];
                    int j = 0;
                    for (int k : wids) {
                        list[j] = k;
                        ++j;
                    }
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, list);
                    mContext.sendBroadcast(intent);
                }
                remove(i);
                --i;
            }
        }
    }

    public void remove(int position) {
        for (int i = position; i < mDisplayedNotes.size() - 1; ++i) {
            //mSelected.put(i, mSelected.get(i + 1));
            mSelectedNotes.put(i, mSelectedNotes.get(i + 1));
        }
        //mSelected.remove(mDisplayedNotes.size() - 1);
        mSelectedNotes.delete(mDisplayedNotes.size() - 1);
        mDisplayedNotes.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(NoteLite note) {
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mDisplayedNotes.get(i).getID() == note.getID()) {
                mDisplayedNotes.remove(i);
                notifyItemRemoved(i);
                // mSelected.remove(i);
                mSelectedNotes.delete(i);
                break;
            }
        }
    }

    public HashMap<Integer, Integer> restore() {
        HashMap<Integer, Integer> list = new HashMap<>();
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                NoteLite note = mDisplayedNotes.get(i);
                note.setTypeId(note.getTypeId() - 10);
                mDb.updateNoteLite(note);
                list.put(note.getID(), note.getTypeId());
                remove(i);
                --i;
            }
        }

        return list;
    }

    public List<Integer> getUndoIds() {
        return mUndoNotes;
    }

    public void removeNote(int id) {
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mDisplayedNotes.get(i).getID() == id) {
                mDisplayedNotes.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public ArrayList<NoteLite> getSelectedNotes() {
        ArrayList<NoteLite> list = new ArrayList<>();
        mUndoNotes.clear();
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                list.add(new NoteLite(mDisplayedNotes.get(i)));
                mUndoNotes.add(mDisplayedNotes.get(i).getID());
            }
        }
        return list;
    }

    public ArrayList<Integer> getSelectedList(int type, boolean delete) {
        ArrayList<Integer> list = new ArrayList<>();
        if(mUndoNotes == null){
            mUndoNotes = new ArrayList<>();
        } else {
            mUndoNotes.clear();
        }
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                NoteLite note = mDisplayedNotes.get(i);
                note.setTypeId(!delete ? type : type + 10);
                mDb.updateNoteLite(note);
                list.add(note.getID());
                mUndoNotes.add(note.getID());
                remove(i);
                --i;
            }
        }
        return list;
    }

    public int getNumberSelected() {
        int db = 0;
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                db++;
            }
        }
        return db;
    }

    public void removeElement(int ID) {
        int position1 = 0;
        while (position1 < mDisplayedNotes.size() && mDisplayedNotes.get(position1).getID() != ID) {
            ++position1;
        }
        if (position1 < mDisplayedNotes.size()) {
            remove(position1);
        }
    }

    public void removeSelection() {
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                mSelectedNotes.put(i, false);
            }
            notifyItemChanged(i);

        }
        init();
    }


    // Setters method

    public void setLabel(int label) {
        mLabel = label;
        mDisplayedNotes.clear();
        mDisplayedNotes.addAll(mDb.getSortedCardNotes(mSortType, mType, mLabel));
        if (!mDirection) {
            Collections.reverse(mDisplayedNotes);
        }
        init();
        notifyDataSetChanged();
    }

    public void refresh() {
        setLabel(mLabel);
    }

    public void setSelected(int num) {
        if (mSelectedNotes == null) {
            init();
        }
        if (!mSelectedNotes.get(num)) {
            mSelectedNotes.put(num, true);
            this.notifyItemChanged(num);
        } else {
            mSelectedNotes.put(num, false);
            this.notifyItemChanged(num);
        }
    }

    public void setNoteClickListener(NoteClickListener listener) {
        mListener = listener;
    }


    //Adapter methods
    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoteViewHolder(new NoteCardView(parent.getContext()), mListener);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            NoteCardView current = (NoteCardView) holder.itemView;
            NoteLite note = mDisplayedNotes.get(holder.getAdapterPosition());
            current.setCardBackgroundColor(mColors[note.getBgColor()]);
            float textLength = 0;
            boolean secured = note.getSecured();

            if (note.getTitle().isEmpty()) {
                current.mTitle.setVisibility(GONE);

            } else {
                current.mTitle.setVisibility(VISIBLE);
                current.mTitle.setText(note.getTitle());
            }

            if (note.getText() != null && !note.getText().isEmpty()) {
                current.mContent.setVisibility(VISIBLE);
                String text = note.getText();
                textLength += (20.0f - text.length() / 10.0f) / 2;
                current.mContent.setText(secured ? text.replaceAll(".", "*") : text);
            } else {
                current.mContent.setVisibility(GONE);
            }

            if (note.getList() != null && !note.getList().isEmpty()) {

                current.mList.setVisibility(VISIBLE);
                current.mList.removeAllViews();
                ArrayList<NoteCheckedLite> list = note.getList();
                int listSize = list.size();
                if (!list.isEmpty()) {
                    if (textLength != 0) {
                        textLength = (textLength + (10.0f - listSize) / 2 + 1) / 2;
                    } else {
                        textLength += (10.0f - listSize) / 2 + 1;
                    }
                    current.mDots.setVisibility(GONE);
                } else {
                    textLength *= 1.3;
                    current.mDots.setVisibility(VISIBLE);
                    current.mDots.setTextSize(14.0f + textLength);

                }
                int n = 0;
                float listFontSize = 14.0f + textLength;
                for (int i = 0; i < listSize && n < 10; ++i) {
                    NoteCheckedLite check = list.get(i);
                    TextView checkText;
                    if (!check.getChecked()) {
                        checkText = (TextView) mInflater.inflate(R.layout.item_card_list_element_unchecked, current.mList, false);
                    } else {
                        checkText = (TextView) mInflater.inflate(R.layout.item_card_list_element_checked, current.mList, false);
                        checkText.setPaintFlags(checkText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    }
                    checkText.setText(secured ? check.getLabel().replaceAll(".", "*") : check.getLabel());
                    checkText.setTextSize(listFontSize);
                    current.mList.addView(checkText);
                    ++n;
                }
            } else {
                current.mDots.setVisibility(GONE);
                current.mList.setVisibility(GONE);
            }
            current.mContent.setTextSize(14.0f + textLength);
            current.mTitle.setTextSize(16.0f + textLength);
            if (!note.getImage().isEmpty()) {
                current.mImage.setVisibility(VISIBLE);
                Glide.with(mContext).load(secured ? "" : note.getImage()).placeholder(R.drawable.ic_lock_black_96dp).error(R.drawable.ic_not_found).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(current.mImage);
            } else {
                current.mImage.setVisibility(GONE);
            }

            if (!note.getRecord().isEmpty() && note.getRecord().contains("R")) {
                current.mRecord.setVisibility(VISIBLE);
                String recDate = note.getRecord().split("REC_")[1];
                current.mRecord.setText(recDate.substring(0, 4) + "." + recDate.substring(4, 6) + "." + recDate.substring(6, 8));
            } else {
                current.mRecord.setVisibility(GONE);
            }
            char c = note.getReminder().charAt(0);
            if (c == '0') {
                current.mTime.setVisibility(GONE);
            } else if (c == 'T' || c == 't') {
                current.mTime.setVisibility(VISIBLE);
                try {
                    String reminder = note.getReminder();
                    Calendar calendar = DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(reminder.substring(1, reminder.length())));
                    Calendar cal = new GregorianCalendar();
                    double millis = ((calendar.getTimeInMillis() - cal.getTimeInMillis()) / 86400000.0);
                    if (millis >= 2) {
                        current.mTime.setText(mDateFormat.format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));
                    } else if (millis >= 1 || (cal.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH) && millis < 1 && millis >= 0)) {
                        current.mTime.setText(mContext.getString(R.string.card_tomorrow, reminder.substring(reminder.length() - 5)));
                    } else if (millis >= 0.00000) {
                        current.mTime.setText(mContext.getString(R.string.card_today, reminder.substring(reminder.length() - 5)));
                    } else {
                        if (millis > -1 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis > -1 && millis < 0)) {
                            current.mTime.setText(mContext.getString(R.string.card_today, reminder.substring(reminder.length() - 5)));
                        } else {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() - 86400000);
                            if (millis >= -2 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis < -1 && millis >= -2)) {
                                current.mTime.setText(mContext.getString(R.string.card_yesterday, reminder.substring(reminder.length() - 5)));
                            } else {
                                current.mTime.setText(mDateFormat.format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));
                            }
                        }
                        current.mTime.setPaintFlags(current.mTime.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    }


                } catch (Exception e) {
                    Log.e("Card time", e.toString());
                }
            }
            current.findViewById(R.id.note_gradient).setVisibility(mSelectedNotes.get(holder.getAdapterPosition()) ? VISIBLE : INVISIBLE);
        }

    }

    public static boolean notEmpty(NoteLite note) {
        if (!note.getTitle().replaceAll("[ \n]", "").isEmpty()) {
            return true;
        } else if (note.getText() != null && !note.getText().replaceAll("[ \n]", "").isEmpty()) {
            return true;
        } else if (note.getImage() != null && !note.getImage().isEmpty()) {
            return true;
        } else if (note.getList() != null && !note.getList().isEmpty()) {
            return true;
        } else if (note.getRecord() != null && !note.getRecord().isEmpty()) {
            return true;
        } else if (note.getReminder().startsWith("T")) {
            return true;
        }
        return false;


    }

    //updateNote
    public void updateNote(int id, Context context) {
        int i = 0;
        int size = mDisplayedNotes.size();
        while (size > i && mDisplayedNotes.get(i).getID() != id) {
            ++i;
        }
        boolean delete = new SettingsPreferences(mContext, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE);
        NoteLite note = mDb.getCardNote(id, 11);
        if (note.getLabel() != mLabel && mLabel != -1) {
            if (i < size) {
                mDisplayedNotes.remove(i);
                init();
                notifyItemRemoved(i);
            }
        } else if (i < size) {
            mDisplayedNotes.remove(i);
            if (!delete || notEmpty(note)) {
                int pos = getPosition(note);
                if (pos == i) {
                    mDisplayedNotes.add(i, note);
                    init();
                    notifyItemChanged(i);
                } else {
                    notifyItemRemoved(i);
                    if (pos >= mDisplayedNotes.size()) {
                        mDisplayedNotes.add(note);
                        init();
                        notifyItemInserted(size - 1);
                    } else {
                        mDisplayedNotes.add(pos, note);
                        init();
                        notifyItemInserted(pos);
                    }
                }
            } else {
                mDb.deleteNote(mDb.getNote(note.getID()), context);
                notifyItemRemoved(i);
            }
        } else {
            if (!delete || notEmpty(note)) {
                int pos = getPosition(note);
                if (pos < size) {
                    mDisplayedNotes.add(pos, note);
                    init();
                    notifyItemInserted(pos);
                } else {
                    mDisplayedNotes.add(note);
                    init();
                    notifyItemInserted(size);

                }
            } else {
                mDb.deleteNote(mDb.getNote(note.getID()), context);
            }
        }
    }
    public void removedLabel(final int label){
        if(mLabel == label) {
            mDisplayedNotes.clear();
            init();
            notifyDataSetChanged();

        }
        mDb.deletedLabel(label);
    }
    private int getPosition(NoteLite note) {
        int i;
        for (i = 0; i < mDisplayedNotes.size(); ++i) {
            int greater = 0;
            switch (mSortType) {
                case 0:
                    greater = -mDisplayedNotes.get(i).getCreateDate().compareTo(note.getCreateDate());
                    break;
                case 1:
                    try {
                        greater = (-1) * DatabaseManager.mFormat.parse(mDisplayedNotes.get(i).getLastModification()).compareTo(DatabaseManager.mFormat.parse(note.getLastModification()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    greater = mDisplayedNotes.get(i).getTitle().toLowerCase().compareTo(note.getTitle().toLowerCase());
                    break;
                case 3:
                    try {
                        if (note.getReminder().length() > 2 && mDisplayedNotes.get(i).getReminder().length() <= 2) {
                            greater = 1;
                        } else if (note.getReminder().length() <= 2 && mDisplayedNotes.get(i).getReminder().length() > 2) {
                            greater = -1;
                        } else if (note.getReminder().length() <= 1 && mDisplayedNotes.get(i).getReminder().length() <= 1) {
                            greater = -1;
                        } else {
                            greater = DatabaseManager.mFormat.parse(mDisplayedNotes.get(i).getReminder().substring(1)).compareTo(DatabaseManager.mFormat.parse(note.getReminder().substring(1)));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    double a = mDb.getValue(mDisplayedNotes.get(i));
                    double b = mDb.getValue(note);
                    if (a > b) {
                        greater = -1;
                    } else if (a < b) {
                        greater = 1;
                    } else {
                        greater = 0;
                    }
            }
            if (mDirection && greater >= 0) {
                break;
            }
            if (!mDirection && greater <= 0) {
                break;
            }
        }
        return i;
    }

    public void addNote(int id) {
        NoteLite note = mDb.getCardNote(id, 11);
        if (note.getLabel() == mLabel || mLabel == -1 || mType != 0) {
            int i = getPosition(note);
            mDisplayedNotes.add(i, note);
            init();
            notifyItemInserted(i);
        }

    }

    public void makeCopy(Context context) {
        FileManager mFile = new FileManager(context);
        boolean ok = true;
        int db = 0;
        ArrayList<NoteLite> cnotes = new ArrayList<>();
        ArrayList<Integer> cpos = new ArrayList<>();
        for (int i = 0; i < mDisplayedNotes.size(); ++i) {
            if (mSelectedNotes.get(i)) {
                mSelectedNotes.put(i, false);
                notifyItemChanged(i);
                ++db;
                try {

                    Note note = new Note(mDb.getNote(mDisplayedNotes.get(i).getID()));
                    note.setNumberOfView(0);
                    for (Picture pc : note.getImagesList()) {
                        mFile.copyFile(new File(pc.getSrc().split(":")[1]), mFile.creteFileCopy(".jpg"));
                        pc.setSrc(mFile.getPhotoPath());
                    }
                    if (!note.getRecord().isEmpty()) {
                        mFile.copyFile(new File(note.getRecord().get(0).split(":")[1]), mFile.creteFileCopy(".jpg"));
                        note.getRecord().add(mFile.getPhotoPath());
                    }

                    int copyID = mDb.insertNote(note);
                    if (copyID == -1) {
                        ok = false;
                        Toast.makeText(context, R.string.copy_error, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    NoteLite nt = new NoteLite(note);
                    cpos.add(getPosition(nt));
                    cnotes.add(nt);
                } catch (IOException e) {
                    ok = false;
                    Log.w("Note file copy error", e.toString());
                    Toast.makeText(context, R.string.copy_file_error, Toast.LENGTH_SHORT).show();
                    break;
                }

            }
        }

        if (ok && db > 0) {
            for (int i = 0; i < cnotes.size(); ++i) {
                mDisplayedNotes.add(cpos.get(i), cnotes.get(i));
                init();
                notifyItemInserted(cpos.get(i));
            }
            Toast.makeText(context, db > 1 ? R.string.copys_made : R.string.copy_made, Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < cnotes.size(); ++i) {
                mDb.deleteNote(mDb.getNote(cnotes.get(i).getID()), context);
            }
        }
    }

    //Getter methods
    @Override
    public int getItemCount() {
        return mDisplayedNotes.size();
    }

    public NoteLite getItemAtPosition(int pos) {
        return mDisplayedNotes.get(pos);
    }

    @Override
    public void leftSwipe(int position) {
        if (mSwipeListener != null) {
            mUndoNotes.clear();
            NoteLite note = mDisplayedNotes.get(position);
            mUndoNotes.add(note.getID());
            int type = mType;
            switch (mType) {
                case 0:
                    type = 1;
                    note.setTypeId(1);
                    break;
                case 1:
                    type = 2;
                    note.setTypeId(note.getTypeId() + 10);
                    break;
                case 2:
                    note.setTypeId(note.getTypeId() - 10);
                    type = note.getTypeId();
                    break;
            }
            mDb.updateNoteLite(note);
            mDisplayedNotes.remove(position);
            notifyItemRemoved(position);
            mSwipeListener.swiped(type, mType, note.getID());
            if (mShowListener != null) {
                mShowListener.onNoteClicked(0);
            }
        }
    }

    @Override
    public void rightSwipe(int position) {
        if (mSwipeListener != null) {
            mUndoNotes.clear();
            NoteLite note = mDisplayedNotes.get(position);
            mUndoNotes.add(note.getID());
            int type = mType;
            switch (mType) {
                case 0:
                    type = 2;
                    note.setTypeId(note.getTypeId() + 10);
                    break;
                case 1:
                    type = 0;
                    note.setTypeId(0);
                    break;
                case 2:
                    type = 2;
                    break;
            }
            mDb.updateNoteLite(note);
            mDisplayedNotes.remove(position);
            notifyItemRemoved(position);
            mSwipeListener.swiped(type, mType, note.getID());
            if (mShowListener != null) {
                mShowListener.onNoteClicked(0);
            }
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        NoteLite fromNote = mDisplayedNotes.get(fromPosition);
        double staticPosition = mDb.getValue(mDisplayedNotes.get(toPosition));

        if (fromPosition < toPosition) {
            if (toPosition + 1 < getItemCount()) {
                staticPosition = (staticPosition + mDb.getValue(mDisplayedNotes.get(toPosition + 1))) / 2;
            }
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mDisplayedNotes, i, i + 1);
                mSelectedNotes.put(i, false);
            }
        } else {
            if (toPosition - 1 > 0) {
                staticPosition = (staticPosition + mDb.getValue(mDisplayedNotes.get(toPosition - 1))) / 2;
            }
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mDisplayedNotes, i, i - 1);
                mSelectedNotes.put(i, false);
            }
        }
        fromNote.setStaticPosition(0.0f);
        staticPosition -= mDb.getValue(fromNote);
        if (staticPosition < 0) {
            staticPosition *= -1;
        }
        fromNote.setStaticPosition((float) staticPosition);
        mDb.updateNoteLite(fromNote);

        mSelectedNotes.put(fromPosition, false);
        mSelectedNotes.put(toPosition, false);
        notifyItemMoved(fromPosition, toPosition);
    }
}