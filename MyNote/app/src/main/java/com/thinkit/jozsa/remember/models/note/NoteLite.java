package com.thinkit.jozsa.remember.models.note;

import android.os.Parcel;
import android.os.Parcelable;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteCheckedLite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


public class NoteLite implements Parcelable {
    private int mId;
    private int mTypeId;
    private String mTitle;
    private String mText;
    private ArrayList<NoteCheckedLite> mList;
    private String mImages;
    private String mRecord;
    private Calendar mCreateDate;
    private String mLastModification;
    private String mRemainder = "0";
    private long mCategory;
    private int mBgColor;
    private float mStaticPosition;
    private float mNumberOfView;
    private int mLabel;
    private boolean mSecured;


    public NoteLite() {
        mId = 0;
        mTypeId = 0;
        mTitle = "";
        mCreateDate = new GregorianCalendar();
        mLastModification = DatabaseManager.mFormat.format(new GregorianCalendar().getTime());
        mRemainder = "0";
        mCategory = 0;
        mRecord = "";
        mImages = "";
        mBgColor = 0;
        mStaticPosition = -1;
        mNumberOfView = 0;
        mLabel = 1;
        mSecured = false;
    }

    public NoteLite(Note note) {
        mId = note.getID();
        mTitle = note.getTitle();
        mCreateDate = note.getCreateDate();
        mRemainder = note.getReminder();
        mCategory = note.getCategory();
        mText = note.getText().getText();
        if (note.getList() != null) {
            mList = new ArrayList<>();
            for (NoteChecked ch : note.getList()) {
                mList.add(new NoteCheckedLite(ch));
            }
        }
        mImages = !note.getImagesList().isEmpty() ? note.getImagesList().get(0).getSrc() : "";
        mRecord = !note.getRecord().isEmpty() ? note.getRecord().get(0) : "";
        mBgColor = note.getBgColor();
        mStaticPosition = note.getStaticPosition();
        mNumberOfView = note.getNumberOfView();
        mLabel = note.getLabel();
        mTypeId = getTypeId();
        mSecured = note.getSecured();
        mLastModification = note.getLastModification();

    }

    public NoteLite(NoteLite note) {
        mId = note.getID();
        mTitle = note.getTitle();
        mCreateDate = note.getCreateDate();
        mRemainder = note.getReminder();
        mCategory = 0;
        mText = note.getText();
        if (note.getList() != null) {
            mList = new ArrayList<>(note.getList());
        }
        mImages = note.getImage();
        mRecord = note.getRecord();
        mBgColor = note.getBgColor();
        mStaticPosition = note.getStaticPosition();
        mNumberOfView = note.getNumberOfView();
        mLabel = note.getLabel();
        mTypeId = getTypeId();
        mSecured = note.getSecured();
        mLastModification = note.getLastModification();

    }

    public NoteLite(int noteID, String title, Calendar creatingDate, String lastModification, String reminder, int bgColor, boolean secured, float numberOfView, int nlid, int typeId, float staticPosition, long category) {
        this.mId = noteID;
        this.mTitle = title;
        this.mCreateDate = creatingDate;
        this.mLastModification = lastModification;
        this.mRemainder = reminder;
        this.mBgColor = bgColor;
        this.mSecured = secured;
        this.mNumberOfView = numberOfView;
        mStaticPosition = staticPosition;
        this.mLabel = nlid;
        this.mTypeId = typeId;
        this.mText = "";
        this.mImages = "";
        this.mRecord = "";
        this.mCategory = category;
    }

    public NoteLite(int id, float staticPosition, int bgColor, String title, float numberOfView, int label, Calendar creatingDate, String reminder, int type, boolean secured, String lastModification, long category) {
        this.mId = id;
        this.mTitle = title;
        this.mCreateDate = creatingDate;
        this.mLastModification = lastModification;
        this.mRemainder = reminder;
        this.mBgColor = bgColor;
        this.mSecured = secured;
        this.mNumberOfView = numberOfView;
        this.mStaticPosition = staticPosition;
        this.mLabel = label;
        this.mTypeId = type;
        this.mText = "";
        this.mImages = "";
        this.mRecord = "";
        this.mCategory = category;
    }

    public NoteLite(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        mText = in.readString();
        mList = (ArrayList<NoteCheckedLite>) in.readArrayList(NoteCheckedLite.class.getClassLoader());
        mCreateDate = (Calendar) in.readValue(Calendar.class.getClassLoader());
        mRemainder = in.readString();
        mCategory = in.readLong();
        mBgColor = in.readInt();
        mStaticPosition = in.readFloat();
        mNumberOfView = in.readFloat();
        mImages = in.readString();
        mLabel = in.readInt();
        mTypeId = in.readInt();
        mSecured = in.readByte() == 1;
        mRecord = in.readString();
        mLastModification = in.readString();
    }

    public int getID() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title.length() > 50 ? title.substring(0, 47) + "..." : title;

    }

    public String getText() {
        return mText.replaceAll("[\n]{2,}", "\n").replaceAll("[ ]{3,}", " ");
    }

    public void setText(String text) {
        mText = text.length() > 200 ? text.substring(0, 197) + "..." : text;
        if (mText.length() - mText.replaceAll("\n", "").length() > 10) {
            String s[] = mText.split("\n");
            mText = "";
            for (int i = 0; i < 10; ++i) {
                mText += s[i] + "\n";
            }
        }
    }

    public ArrayList<NoteCheckedLite> getList() {
        return mList;
    }

    public void setList(ArrayList<NoteCheckedLite> list) {
        if (mList == null) {
            mList = new ArrayList<>(list);
        } else {
            mList.clear();
            this.mList.addAll(list);
        }
    }

    public String getImage() {
        return mImages;
    }

    public void setImage(String src) {
        mImages = src;
    }

    public String getRecord() {
        return mRecord;
    }

    public void setRecord(String record) {
        mRecord = record;
    }

    public Calendar getCreateDate() {
        return mCreateDate;
    }


    public String getReminder() {
        return mRemainder;
    }

    public void setReminder(String mRemainderDate) {
        this.mRemainder = mRemainderDate;
    }

    public long getCategory() {
        return mCategory;
    }


    public int getBgColor() {
        return mBgColor;
    }


    public float getStaticPosition() {
        return mStaticPosition;
    }

    public void setStaticPosition(float position) {
        this.mStaticPosition = position;
    }

    public int getLabel() {
        return mLabel;
    }

    public void setLabel(int label) {
        this.mLabel = label;
    }

    public int getTypeId() {
        return mTypeId;
    }

    public void setTypeId(int typeId) {
        this.mTypeId = typeId;
    }

    public boolean getSecured() {
        return mSecured;
    }

    public void setSecured(boolean secured) {
        this.mSecured = secured;
    }

    public float getNumberOfView() {
        return mNumberOfView;
    }


    public String getLastModification() {
        return mLastModification;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeValue(mText);
        dest.writeValue(mList);
        dest.writeValue(mCreateDate);
        dest.writeString(mRemainder);
        dest.writeLong(mCategory);
        dest.writeInt(mBgColor);
        dest.writeFloat(mStaticPosition);
        dest.writeFloat(mNumberOfView);
        dest.writeString(mImages);
        dest.writeInt(mLabel);
        dest.writeInt(mTypeId);
        dest.writeByte(mSecured ? 1 : (byte) 0);
        dest.writeString(mRecord);
        dest.writeString(mLastModification);

    }

    @Override
    public String toString() {
        return "Note{" +
                "mId=" + mId +
                ", mTypeId=" + mTypeId +
                ", mTitle='" + mTitle + '\'' +
                ", mText=" + mText +
                ", mList=" + mList +
                ", mImages=" + mImages +
                ", mRecord=" + mRecord +
                ", mCreateDate=" + mCreateDate +
                ", mLastModification='" + mLastModification + '\'' +
                ", mRemainder='" + mRemainder + '\'' +
                ", mCategory=" + mCategory +
                ", mBgColor=" + mBgColor +
                ", mStaticPosition=" + mStaticPosition +
                ", mNumberOfView=" + mNumberOfView +
                ", mRadio=" + mLabel +
                ", mSecured='" + mSecured + '\'' +
                '}';
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
