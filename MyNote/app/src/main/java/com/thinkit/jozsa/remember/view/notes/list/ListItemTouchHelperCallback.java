package com.thinkit.jozsa.remember.view.notes.list;


import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class ListItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ItemTouchHelperAdapter mAdapter;
    private boolean mHorizontal = false;
    private int mSaveLastItems = 0;
    private int mSaveFirstItems = -1;

    public ListItemTouchHelperCallback(ItemTouchHelperAdapter adapter) {
        mAdapter = adapter;
    }

    public void setOrientation(boolean horizontal){
        mHorizontal = horizontal;
    }

    public void setSaveFirstItems(int count){
        mSaveFirstItems = count - 1;
    }
    public void setSaveLastItems(int count){
        mSaveLastItems = count;
    }
    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(!mHorizontal ? ItemTouchHelper.UP | ItemTouchHelper.DOWN : ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, 0);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder userViewHolder, RecyclerView.ViewHolder target) {
        if (target.getAdapterPosition() < mAdapter.getItemCount() - mSaveLastItems && target.getAdapterPosition() > mSaveFirstItems) {
            mAdapter.onItemMove(userViewHolder.getAdapterPosition(), target.getAdapterPosition());
        }
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
    }

}
