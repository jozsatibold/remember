package com.thinkit.jozsa.remember.view.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.thinkit.jozsa.remember.view.home.MainActivity;

public class ReminderReceiver  extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, ReminderAlarmService.class);
        notificationIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, intent.getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1));
        context.startService(notificationIntent);
    }

}
