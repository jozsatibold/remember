package com.thinkit.jozsa.remember.view.notification;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.view.home.MainActivity;

import java.util.Calendar;
import java.util.Date;

public class NotificationActivity extends Activity {
    public static final String NOTIFICATION_TIME = "notification_time";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int time = getIntent().getIntExtra(NOTIFICATION_TIME, -1);
        int id = getIntent().getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1);
        if (time > -1 && id > -1) {
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(id * 100);
            Intent myIntent = new Intent(this, ReminderReceiver.class);
            myIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
            ((AlarmManager) getSystemService(ALARM_SERVICE)).set(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis() + time * 60000, PendingIntent.getBroadcast(this, id, myIntent, 0));
            DatabaseManager db = DatabaseManager.getInstance(this);
            Note note = db.getNote(id);
            if(note != null){
                if(note.getReminder().equals("0") || note.getReminder().isEmpty()){
                    Date d = new Date();
                    d.setTime(Calendar.getInstance().getTimeInMillis() + time * 60000);
                    note.setReminder("D" + DatabaseManager.mFormat.format(d));
                    db.updateNote(note, 1, 1);
                }
            }
        }
        finish();
    }
}
