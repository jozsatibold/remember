package com.thinkit.jozsa.remember.view.notes.imagelist;


public interface ToolbarImageClickListener {
    void onNoteClicked(int pos);
}
