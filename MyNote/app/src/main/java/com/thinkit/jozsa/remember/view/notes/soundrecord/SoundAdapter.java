package com.thinkit.jozsa.remember.view.notes.soundrecord;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SoundAdapter extends RecyclerView.Adapter<SoundAdapter.RecordHolder> {

    private int mColor;
    private int mPlayPosition = -1;
    private int mRecording = -1;
    private boolean mColorChanged;
    private boolean mFinalize = false;
    private long mWaiting = 0;
    private long mLastPassword = 0;
    private Activity mContext;
    private SettingsPreferences mSettings;
    private ModifyListener mModifyListener;
    private List<String> mRecords;
    private ToolbarImageClickListener mListener;
    private Timer mSoundTimer;


    public SoundAdapter(List<String> list, Activity context, int color, SettingsPreferences settings) {
        mContext = context;
        mRecords = list;
        if (mRecords.size() < 8) {
            mRecords.add("");
        }
        mSettings = settings;
        mColor = color;
    }

    @Override
    public RecordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_record, parent, false));
    }

    public void setColor(int color) {
        mColor = color;
        mColorChanged = true;
        notifyDataSetChanged();
        mColorChanged = false;
    }

    public void setModifyListener(ModifyListener listener) {
        mModifyListener = listener;
    }

    public void setWaiting(long waiting) {
        mWaiting = waiting;
    }

    public void setLastPassword(long lastPassword) {
        mLastPassword = lastPassword;
    }

    public void addRecord() {
        if (mRecords.size() == 8 && !mRecords.get(7).isEmpty()) {
            Toast.makeText(mContext, R.string.note_record_maximum, Toast.LENGTH_SHORT).show();
        } else {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long availableKb = stat.getBlockSizeLong() * stat.getAvailableBlocksLong() * 1024;
            if (availableKb < 300) {
                Toast.makeText(mContext, R.string.not_enough_space, Toast.LENGTH_SHORT).show();
            } else {
                if (mRecording != -1 && mSoundTimer != null) {
                    mSoundTimer.stopTime();
                    int pos = mRecording;
                    mRecording = -1;
                    mRecords.set(pos, mSoundTimer.getRealPath());
                    mListener.onNoteClicked(pos);
                    mModifyListener.refresh(1);
                    notifyItemChanged(pos);
                }

                if (mPlayPosition != -1) {
                    int pos = mPlayPosition;
                    mPlayPosition = -1;
                    notifyItemChanged(pos);
                }

                if (mRecords.size() < 8) {
                    mRecords.add("");
                    notifyItemInserted(mRecording + 1);
                    mRecording = getItemCount() - 2;
                } else {
                    mRecording = getItemCount() - 1;
                }
                notifyItemChanged(mRecording);
            }
        }
    }

    public boolean stopRecording() {
        if (mRecording == -1) {
            return true;
        } else if (mSoundTimer != null && mRecording > -1) {
            mSoundTimer.stopTime();
            int pos = mRecording;
            mRecording = -1;
            mRecords.set(pos, mSoundTimer.getRealPath());
            mListener.onNoteClicked(-pos);
            mModifyListener.refresh(1);
            notifyItemChanged(pos);
        }
        return false;
    }

    public void stopPlaying() {
        if (mPlayPosition != -1) {
            int pos = mPlayPosition;
            mPlayPosition = -1;
            notifyItemChanged(pos);
        }
    }

    public void release() {
        mFinalize = true;
        notifyDataSetChanged();
    }

    private void endRecording(int pos) {
        if (mSoundTimer != null && mRecording > -1) {
            mSoundTimer.stopTime();
            int pos2 = mRecording;
            mRecording = -1;
            mRecords.set(pos2, mSoundTimer.getRealPath());
            mListener.onNoteClicked(pos);
            mModifyListener.refresh(1);
            notifyItemChanged(pos2);
        }
    }

    @Override
    public void onBindViewHolder(final RecordHolder holder, int position) {
        if ((mRecords.get(holder.getAdapterPosition()).isEmpty() && holder.getAdapterPosition() != mRecording)) {
            holder.mSeekBar.setVisibility(GONE);
            holder.mCreationDate.setText(R.string.note_tap_to_record);
            holder.mLengthTime.setVisibility(GONE);
            holder.mPlayStop.setVisibility(GONE);
            holder.mCurrentTime.setVisibility(GONE);
            holder.mCreationDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRecording == -1) {
                        mModifyListener.refresh(2);
                    } else {
                        Toast.makeText(mContext, R.string.note_finalize_recording, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (mFinalize) {
                if (holder.mMediaPlayer != null) {
                    if (holder.mMediaPlayer.isPlaying()) {
                        holder.mMediaPlayer.stop();
                    }
                    holder.mMediaPlayer.reset();
                    holder.mMediaPlayer.release();
                }
            } else {
                holder.mCreationDate.setOnClickListener(null);
                holder.mPlayStop.setVisibility(VISIBLE);
                holder.mLengthTime.setVisibility(VISIBLE);
                if (mRecording == holder.getAdapterPosition()) {
                    holder.mSeekBar.setVisibility(GONE);
                    if (mSoundTimer == null) {
                        mSoundTimer = new Timer(mContext, holder.mLengthTime, 180);
                    } else {
                        mSoundTimer.refresh(holder.mLengthTime);
                    }
                    mSoundTimer.startTime();
                    holder.mCreationDate.setText(R.string.note_recording);
                    holder.mCurrentTime.setVisibility(GONE);
                    holder.mMore.setVisibility(GONE);
                    holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_stop_black_24dp));
                    holder.mPlayStop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                            endRecording(holder.getAdapterPosition());
                        }
                    });
                } else {
                    if (holder.mSeekBar.getVisibility() == GONE) {
                        holder.mSeekBar.setVisibility(VISIBLE);
                        holder.mCurrentTime.setVisibility(VISIBLE);
                        holder.mMore.setVisibility(VISIBLE);
                        holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                    }
                    if (holder.mPlay && mPlayPosition != holder.getAdapterPosition()) {
                        holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                        holder.mPlay = false;
                        holder.mMediaPlayer.stop();
                        holder.mTimeElapsed = holder.mMediaPlayer.getCurrentPosition();
                    }
                    if (!mColorChanged) {

                        String recDate = mRecords.get(holder.getAdapterPosition()).split("REC_")[1];
                        recDate = recDate.substring(0, 4) + "." + recDate.substring(4, 6) + "." + recDate.substring(6, 8);
                        holder.mCreationDate.setText(recDate);
                        holder.mPlayStop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mRecording != -1) {
                                    endRecording(mRecording);
                                }
                                if (holder.mPlay) {
                                    holder.mPlay = false;
                                    mPlayPosition = -1;
                                    holder.mTimeElapsed = holder.mMediaPlayer.getCurrentPosition();
                                    holder.mMediaPlayer.pause();
                                    holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                                } else {
                                    if (mPlayPosition != -1) {
                                        int pos = mPlayPosition;
                                        mPlayPosition = -1;
                                        if (pos < getItemCount()) {
                                            notifyItemChanged(pos);
                                        }
                                    }
                                    holder.mMediaPlayer.start();
                                    holder.mPlay = true;
                                    mPlayPosition = holder.getAdapterPosition();
                                    holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_pause_black_24dp));
                                    holder.mHandler.postDelayed(holder.mUpdateSeekBar, 100);
                                }
                            }
                        });
                        if (holder.mMediaPlayer == null) {
                            holder.mMediaPlayer = MediaPlayer.create(mContext, Uri.fromFile(new File(mRecords.get(holder.getAdapterPosition()).split(":")[1])));
                            holder.mFinalTime = holder.mMediaPlayer.getDuration();
                            holder.mSeekBar.setMax(holder.mFinalTime);
                            holder.mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    holder.mMediaPlayer.seekTo(0);
                                    holder.mSeekBar.setProgress(0);
                                    holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                                    holder.mPlay = false;
                                    mPlayPosition = -1;
                                }
                            });
                        }
                        String text;
                        if (holder.mFinalTime / 1000 == 0) {
                            text = "00:00";
                        } else {
                            int time = holder.mFinalTime / 1000;
                            text = String.format(Locale.getDefault(), "%02d", time / 60 % 60) + ":" + String.format(Locale.getDefault(), "%02d", time % 60);
                            time /= 3600;
                            if (time > 0) {
                                text += time + ":" + text;
                            }
                        }
                        holder.mLengthTime.setText(text);
                    }
                }
                holder.mPlayStop.setColorFilter(mColor, PorterDuff.Mode.SRC_ATOP);
                holder.mMore.setColorFilter(mColor, PorterDuff.Mode.SRC_ATOP);
                holder.mSeekBar.setThumbTintList(ColorStateList.valueOf(mColor));
                holder.mSeekBar.setProgressTintList(ColorStateList.valueOf(mColor));
                holder.mSeekBar.setProgressBackgroundTintList(ColorStateList.valueOf(mColor));
                if (mPlayPosition != holder.getAdapterPosition() && holder.mPlay) {
                    holder.mPlay = false;
                    holder.mTimeElapsed = holder.mMediaPlayer.getCurrentPosition();
                    holder.mMediaPlayer.pause();
                    holder.mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                }
            }
        }
    }

    public void setRecordClickListener(ToolbarImageClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mRecords.size();
    }

    class RecordHolder extends RecyclerView.ViewHolder {
        boolean mPlay;
        ImageView mPlayStop;
        ImageView mMore;
        SeekBar mSeekBar;
        TextViewTypeface mCurrentTime;
        TextViewTypeface mLengthTime;
        TextViewTypeface mCreationDate;
        int mFinalTime = 0;
        int mTimeElapsed = 0;
        Handler mHandler;
        MediaPlayer mMediaPlayer;
        Runnable mUpdateSeekBar = new Runnable() {
            public void run() {
                if (mPlay) {
                    mTimeElapsed = mMediaPlayer.getCurrentPosition();
                    mSeekBar.setProgress(mTimeElapsed);
                    int sec = (mFinalTime - mTimeElapsed) / 1000;
                    String text;
                    if (sec == 0) {
                        text = "00:00";
                    } else {
                        text = String.format(Locale.getDefault(), "%02d", sec / 60 % 60) + ":" + String.format(Locale.getDefault(), "%02d", sec % 60);
                        sec /= 3600;
                        if (sec > 0) {
                            text += sec + ":" + text;
                        }
                    }
                    mCurrentTime.setText(text);
                    mHandler.postDelayed(this, 100);
                    if (mFinalTime - mTimeElapsed < 200 && mPlay) {
                        mPlayStop.setImageDrawable(mContext.getDrawable(R.drawable.ic_play_arrow_black_24dp));
                        mPlay = false;
                        mPlayPosition = -1;
                    }
                }
            }
        };

        RecordHolder(View v) {
            super(v);
            mPlayStop = (ImageView) v.findViewById(R.id.record_play_stop);
            mMore = (ImageView) v.findViewById(R.id.record_more);
            mSeekBar = (SeekBar) v.findViewById(R.id.record_seek);
            mCurrentTime = (TextViewTypeface) v.findViewById(R.id.record_current_time);
            mLengthTime = (TextViewTypeface) v.findViewById(R.id.record_time);
            mCreationDate = (TextViewTypeface) v.findViewById(R.id.record_date);
            mHandler = new Handler();
            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    String text;
                    if (i / 1000 == 0) {
                        text = "00:00";
                    } else {
                        int time = i / 1000;

                        text = String.format(Locale.getDefault(), "%02d", time / 60 % 60) + ":" + String.format(Locale.getDefault(), "%02d", time % 60);
                        time /= 3600;
                        if (time > 0) {
                            text += time + ":" + text;
                        }
                    }
                    mTimeElapsed = i;
                    if (!mMediaPlayer.isPlaying()) {
                        mMediaPlayer.seekTo(i < mFinalTime ? i : mFinalTime - 1);
                    }
                    mCurrentTime.setText(text);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    if (mPlay) {
                        mMediaPlayer.pause();
                    }
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mPlay) {
                        mMediaPlayer.start();
                    }
                }
            });
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context wrapper = new ContextThemeWrapper(mContext, R.style.MyPopupMenu);
                    PopupMenu dropDownMenu = new PopupMenu(wrapper, mMore);
                    dropDownMenu.getMenuInflater().inflate(R.menu.menu_record, dropDownMenu.getMenu());
                    dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(final MenuItem menuItem) {
                            if (mSettings.getBoolean("ActionLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                                Dialogs.dialogCheckPassword(mContext, new SettingsPreferences(mContext, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                                    @Override
                                    public void dialogAnswer(boolean ok) {
                                        if (ok) {
                                            switch (menuItem.getItemId()) {
                                                case R.id.action_delete:
                                                    if (mPlay) {
                                                        Toast.makeText(mContext, R.string.stop_playing, Toast.LENGTH_LONG).show();
                                                    } else {
                                                        mMediaPlayer.stop();
                                                        mMediaPlayer.reset();
                                                        mMediaPlayer.release();
                                                        if (mPlayPosition == getAdapterPosition()) {
                                                            mPlayPosition = -1;
                                                        }
                                                        FileManager fm = new FileManager(mContext);
                                                        if (fm.deleteFile(mRecords.get(getAdapterPosition()).split(":")[1]) > 0) {
                                                            mRecords.remove(getAdapterPosition());
                                                            notifyItemRemoved(getAdapterPosition());
                                                            mListener.onNoteClicked(getAdapterPosition());
                                                        } else {
                                                            Toast.makeText(mContext, R.string.record_not_found, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                    break;
                                                case R.id.action_share:
                                                    if (mPlay) {
                                                        Toast.makeText(mContext, R.string.stop_playing, Toast.LENGTH_LONG).show();
                                                    } else {
                                                        mMediaPlayer.stop();
                                                        mPlay = false;
                                                        mLastPassword = System.currentTimeMillis();
                                                        String sharePath = mRecords.get(getAdapterPosition());
                                                        Uri uri = Uri.parse(sharePath);
                                                        Intent share = new Intent(Intent.ACTION_SEND);
                                                        share.setType("audio/*");
                                                        share.putExtra(Intent.EXTRA_STREAM, uri);
                                                        mModifyListener.refresh(1);
                                                        mContext.startActivity(Intent.createChooser(share, "Share Record File"));
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                });
                            } else {
                                switch (menuItem.getItemId()) {
                                    case R.id.action_delete:
                                        if (mPlay || mRecording > -1) {
                                            Toast.makeText(mContext, mPlay ? R.string.stop_playing : R.string.stop_recording, Toast.LENGTH_LONG).show();
                                        } else {
                                            mMediaPlayer.stop();
                                            mMediaPlayer.reset();
                                            mMediaPlayer.release();
                                            if (mPlayPosition == getAdapterPosition()) {
                                                mPlayPosition = -1;
                                            }
                                            mRecords.remove(getAdapterPosition());
                                            notifyItemRemoved(getAdapterPosition());
                                            mListener.onNoteClicked(getAdapterPosition());
                                            if (mRecords.size() < 8 && !mRecords.get(mRecords.size() - 1).isEmpty()) {
                                                mRecords.add("");
                                                notifyItemInserted(mRecords.size() - 1);
                                            }
                                        }
                                        break;
                                    case R.id.action_share:
                                        if (mPlay) {
                                            Toast.makeText(mContext, R.string.stop_playing, Toast.LENGTH_LONG).show();
                                        } else {
                                            mMediaPlayer.stop();
                                            mPlay = false;
                                            mLastPassword = System.currentTimeMillis();
                                            String sharePath = mRecords.get(getAdapterPosition());
                                            Uri uri = Uri.parse(sharePath);
                                            Intent share = new Intent(Intent.ACTION_SEND);
                                            share.setType("audio/*");
                                            share.putExtra(Intent.EXTRA_STREAM, uri);
                                            mModifyListener.refresh(1);
                                            mContext.startActivity(Intent.createChooser(share, "Share Record File"));
                                        }
                                        break;
                                }
                            }
                            return true;
                        }
                    });
                    dropDownMenu.show();
                }
            });
        }
    }

    public void update(ArrayList<String> list) {
        mRecords.clear();
        mRecords.addAll(list);
        this.notifyDataSetChanged();
    }
}
