package com.thinkit.jozsa.remember.view.home.adapters;


import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class NoteItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final NoteItemTouchHelperAdapter mAdapter;
    private boolean mLongPress;
    private boolean mSwipe;
    private boolean mLeftSwipe;
    private boolean mRightSwipe;

    public NoteItemTouchHelperCallback(NoteItemTouchHelperAdapter adapter) {
        mAdapter = adapter;
    }

    public void setLongPress(boolean enabled){
        if(mLongPress != enabled){
            mLongPress = enabled;
        }
    }
    public void setSwipe(boolean enabled){
        if(mSwipe!= enabled){
            mSwipe= enabled;
        }
    }

    public void setLeftSwipe(boolean enabled){
        if(mLeftSwipe != enabled){
            mLeftSwipe = enabled;
        }
    }

    public void setRightSwipe(boolean enabled){
        if(mRightSwipe != enabled){
            mRightSwipe = enabled;
        }
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return mLongPress;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return mSwipe;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, (mLeftSwipe ? ItemTouchHelper.LEFT : 0) | (mRightSwipe ? ItemTouchHelper.RIGHT : 0));
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
        if (source.getItemViewType() != target.getItemViewType()) {
            return false;
        }
        mAdapter.onItemMove(source.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if(direction == ItemTouchHelper.RIGHT){
            mAdapter.rightSwipe(viewHolder.getAdapterPosition());
        } else {
            mAdapter.leftSwipe(viewHolder.getAdapterPosition());
        }
    }

}
