package com.thinkit.jozsa.remember.controller.filemanager;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.noteTypes.NoteNote;
import com.thinkit.jozsa.remember.models.note.subTypes.noteTypes.NoteTypes;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.controller.encryption.JavaEncryption;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ExportData extends AsyncTask<Void, Void, Void> {
    private int mState = 0;
    private List<Note> mArchive;
    private List<Note> mNotes;
    private boolean mError = false;
    private List<Label> mLabels;
    private String mPassword;
    public ExportData(Context context, String password){
        mPassword = password;
        DatabaseManager db = DatabaseManager.getInstance(context);
        mArchive = db.getSortedNotes(0,-1);
        mNotes = db.getSortedNotes(1,-1);
        mLabels = db.getLabels();
    }

    protected Void doInBackground(Void... s) {
        JSONObject database = new JSONObject();
        JSONArray labels = new JSONArray();
        JSONArray notes = new JSONArray();
        try {
            if (!mPassword.isEmpty()) {
                database.put("password", DataEncrypt.getMd5(mPassword));
                for (Label lb : mLabels) {
                    JSONObject label = new JSONObject();
                    label.put("labelname", JavaEncryption.encrypt(lb.getName(), mPassword));
                    label.put("labeldrawable", lb.getDrawable());
                    label.put("id", lb.getId());
                    labels.put(label);
                }
                for (Note note : mNotes) {
                    ++mState;
                    notes.put(encrypted(note));
                }
                for (Note note : mArchive) {

                    ++mState;
                    notes.put(encrypted(note));
                }
            } else {
                for (Label lb : mLabels) {
                    JSONObject label = new JSONObject();
                    label.put("labelname", lb.getName());
                    label.put("labeldrawable", lb.getDrawable());
                    label.put("id", lb.getId());
                    labels.put(label);
                }
                for (Note note : mNotes) {
                    ++mState;
                    notes.put(normal(note));
                }
                for (Note note : mArchive) {

                    ++mState;
                    notes.put(normal(note));
                }
            }
            database.put("nlabels",labels);
            database.put("nnotes", notes);

            File f = new File(Environment.getExternalStorageDirectory(), "Remember_backup");
            if (!f.exists()) {
                f.mkdirs();
            }
            File file = new File( f, "Backup_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".json");
            Writer output = new BufferedWriter(new FileWriter(file));
            output.write(JavaEncryption.encrypt(database.toString(), mPassword));
            output.close();

        } catch (Exception e){
            Log.w("JSON export", e.toString());
            mError = true;
        }
        return  null;
    }
    public int getState(){
        return mState;
    }

    public int getAll(){

        int n = 0;
        if(mArchive != null) {
            n += mArchive.size();
        }
        if(mNotes != null) {
            n += mNotes.size();
        }
        return n;
    }

    public boolean getError(){

        return mError;
    }

    private JSONObject encrypted(Note note) throws Exception{
        JSONObject cnote = new JSONObject();
        cnote.put("id", note.getID());
        cnote.put("title", JavaEncryption.encrypt(note.getTitle(), mPassword));
        cnote.put("typeid", note.getTypeId());
        cnote.put("createDate", note.getCreateDate().getTimeInMillis());
        cnote.put("lastModification", JavaEncryption.encrypt(note.getLastModification(), mPassword));
        cnote.put("bgColor", note.getBgColor());
        cnote.put("staticPosition", note.getStaticPosition());
        cnote.put("numberOfView", note.getNumberOfView());
        cnote.put("label", note.getLabel());
        cnote.put("secured", note.getSecured());
        cnote.put("category", note.getCategory());
        NoteTypes notetype = note.getText();
        if(notetype != null && !notetype.getText().isEmpty()){
            JSONObject text = new JSONObject();
            text.put("notetext", JavaEncryption.encrypt(notetype.getText(), mPassword));
            text.put("actionText", JavaEncryption.encrypt(((NoteNote)notetype).getActionText(), mPassword));
            cnote.put("noteNote", text);
        }
        List<NoteChecked> checked = note.getList();
        if(notetype != null && !(checked.isEmpty())) {
            JSONArray list = new JSONArray();
            for (NoteChecked cn : checked) {
                JSONObject nc = new JSONObject();
                nc.put("label", JavaEncryption.encrypt(cn.getLabel(), mPassword));
                nc.put("actionText", JavaEncryption.encrypt(cn.getActionText(), mPassword));
                nc.put("checked", cn.getChecked());
                list.put(nc);
            }
            cnote.put("noteList", list);
        }
        return  cnote;

    }
    private JSONObject normal(Note note) throws  Exception{
        JSONObject cnote = new JSONObject();
        cnote.put("id", note.getID());
        cnote.put("title", note.getTitle());
        cnote.put("typeid", note.getTypeId());
        cnote.put("createDate", note.getCreateDate().getTimeInMillis());
        cnote.put("lastModification", note.getLastModification());
        cnote.put("bgColor", note.getBgColor());
        cnote.put("staticPosition", note.getStaticPosition());
        cnote.put("numberOfView", note.getNumberOfView());
        cnote.put("label", note.getLabel());
        cnote.put("category", note.getCategory());
        cnote.put("secured", note.getSecured());
        NoteTypes notetype = note.getText();
        if(notetype != null && !notetype.getText().isEmpty()){
            JSONObject text = new JSONObject();
            text.put("notetext", notetype.getText());
            text.put("actionText", ((NoteNote)notetype).getActionText());
            cnote.put("noteNote", text);
        }
        List<NoteChecked> checked = note.getList();
        if(notetype != null && !checked.isEmpty()) {
            JSONArray list = new JSONArray();
            for (NoteChecked cn : checked) {
                JSONObject nc = new JSONObject();
                nc.put("label", cn.getLabel());
                nc.put("actionText", cn.getActionText());
                nc.put("checked", cn.getChecked());
                list.put(nc);
            }
            cnote.put("noteList", list);
        }
        return  cnote;
    }
}
