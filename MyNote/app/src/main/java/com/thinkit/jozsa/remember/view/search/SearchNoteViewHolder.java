package com.thinkit.jozsa.remember.view.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thinkit.jozsa.remember.view.home.card.NoteCardView;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;


public class SearchNoteViewHolder extends RecyclerView.ViewHolder {



    public SearchNoteViewHolder(final NoteCardView cardView, final SelectionListener noteListener) {
        super(cardView);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (noteListener != null) {
                    noteListener.setSelection(getAdapterPosition());
                }
            }
        });

    }

    public SearchNoteViewHolder(NoteCardView cardView) {
        super(cardView);
    }

}
