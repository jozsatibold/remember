package com.thinkit.jozsa.remember.view.notes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;

public class ImageLabelDrawableAdapter extends RecyclerView.Adapter<ImageLabelDrawableAdapter.ViewHolder> {


    private int mSelected;
    private int mSelectedColor;
    private int mNormalColor;
    private ToolbarImageClickListener mListener;
    private int[] mLabelDrawables;
    private Context mContext;

    public ImageLabelDrawableAdapter(Context context) {
        int[] t = new MyDrawable(context).getAllLabelDrawable();
        mLabelDrawables = new int[t.length - 4];
        System.arraycopy(t, 4, mLabelDrawables, 0,t.length - 4);
        mContext = context;
        mSelectedColor = ContextCompat.getColor(context, android.R.color.holo_blue_light);
        mNormalColor = ContextCompat.getColor(context, R.color.secondary_text_grey);
        mSelected = 0;
    }

    public void setOnClickListener(ToolbarImageClickListener listener) {
        mListener = listener;
    }

    @Override
    public ImageLabelDrawableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (mSelected == position) {
            holder.mImage.setColorFilter(mSelectedColor);
        } else {
            holder.mImage.setColorFilter(mNormalColor);
        }
        holder.mImage.setImageDrawable(getDrawable(holder.getAdapterPosition()));
    }


    @Override
    public int getItemCount() {
        return mLabelDrawables.length;
    }


    public int setSelected(int pos) {
        if (pos >= 0 && pos != mSelected) {
            int last = mSelected;
            mSelected = pos;
            notifyItemChanged(last);
            notifyItemChanged(mSelected);
            return pos;
        }
        return 0;
    }

    public int getSelectedPos() {
        return mSelected;
    }

    public Drawable getDrawable(int pos) {
        return mContext.getDrawable(mLabelDrawables[pos]);

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;

        public ViewHolder(View v, final ToolbarImageClickListener listener) {
            super(v);
            mImage = (ImageView) v.findViewById(R.id.label_icon);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onNoteClicked(getAdapterPosition());
                    }
                }
            });
        }

    }

}

