package com.thinkit.jozsa.remember.view.appearance;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;


public class TypeFaceCache {

    private static HashMap<String, Typeface> fontHash;

    public static Typeface selectTypeface(Context context, String fontName) {
        if (fontHash == null) {
            fontHash = new HashMap<>();
        }
        if (fontHash.get(fontName) == null) {
            Typeface typeface =  Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s.ttf", fontName));
            fontHash.put(fontName, typeface);
            return fontHash.get(fontName);

        } else {
            return fontHash.get(fontName);
        }
    }
}
