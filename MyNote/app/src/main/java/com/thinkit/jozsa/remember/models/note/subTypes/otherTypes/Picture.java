package com.thinkit.jozsa.remember.models.note.subTypes.otherTypes;

public class Picture {

    private String mSrc;
    private int mPictureType;

    public Picture(){
        mSrc = "";
        mPictureType = -1;
    }
    public Picture(String src, int pictureType){
        mSrc = src;
        mPictureType = pictureType;
    }
    public Picture(String src){
        mSrc = src;
        mPictureType = -1;
    }

    public int getPictureType() {
        return mPictureType;
    }

    public void setPictureType(int pictureType) {
        this.mPictureType = pictureType;
    }

    public String getSrc() {
        return mSrc;
    }

    public void setSrc(String src) {
        this.mSrc = src;
    }

}
