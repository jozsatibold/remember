package com.thinkit.jozsa.remember.view.appearance;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.thinkit.jozsa.remember.R;

public class TooltipWindow {
    private static final int MSG_DISMISS_TOOLTIP = 100;
    private PopupWindow mTipWindow;
    private View mContentView;
    private int mBackgroundColor = 0xFF616161;
    private Context mContext;
    private View mSquareView;

    public TooltipWindow(Context context) {
        mTipWindow = new PopupWindow(context);
        mContext = context;
        mContentView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tooltip_layout, null);
        mContentView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mSquareView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tooltip_layout_square, null);
        mSquareView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1000);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);

        AnimationSet animation = new AnimationSet(false);
        animation.addAnimation(fadeIn);
        animation.addAnimation(fadeOut);
        mContentView.setAnimation(animation);
    }

    public void show(View view, String text) {

        mTipWindow.setHeight(LayoutParams.WRAP_CONTENT);
        mTipWindow.setWidth(LayoutParams.WRAP_CONTENT);

        mTipWindow.setOutsideTouchable(true);
        mTipWindow.setTouchable(true);
        mTipWindow.setFocusable(true);
        mTipWindow.setBackgroundDrawable(new BitmapDrawable());

        Point screenSize = new Point();
        ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(screenSize);
        TextViewTypeface tooltipText = (TextViewTypeface) mContentView.findViewById(R.id.tooltip_text);
        tooltipText.setText(text);
        mContentView.setElevation(1f);
        tooltipText.setTextColor(ContextCompat.getColor(mContext, R.color.draw_white));
        tooltipText.setBackgroundTintList(ColorStateList.valueOf(mBackgroundColor));
        mTipWindow.setContentView(mContentView);
        int contentViewHeight = mContentView.getMeasuredHeight();
        int contentViewWidth = mContentView.getMeasuredWidth();
        int screen_pos[] = new int[2];
        view.getLocationOnScreen(screen_pos);

        int position_x = (screen_pos[0] + view.getWidth() / 2) - contentViewWidth / 2;
        position_x = screenSize.x < position_x + contentViewWidth / 2 ? (position_x - (screenSize.x - (position_x + contentViewWidth) + (int) mContext.getResources().getDimension(R.dimen.content_padding))) : position_x;
        position_x = 0 > position_x - contentViewWidth / 2 ? (position_x - (position_x - contentViewWidth / 2) + (int) mContext.getResources().getDimension(R.dimen.content_padding)) : position_x;


        int position_y = screen_pos[1] - 6 * contentViewHeight / 5;
        mTipWindow.showAtLocation(view, Gravity.NO_GRAVITY, position_x, position_y);
        mHandler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 1500);
    }

    public void show(View view, Spanned text) {

        mTipWindow.setHeight(LayoutParams.WRAP_CONTENT);
        mTipWindow.setWidth(LayoutParams.WRAP_CONTENT);
        mTipWindow.setOutsideTouchable(true);
        mTipWindow.setTouchable(true);
        mTipWindow.setFocusable(true);
        mTipWindow.setBackgroundDrawable(new BitmapDrawable());

        Point screenSize = new Point();
        ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(screenSize);
        TextViewTypeface tooltipText = (TextViewTypeface) mContentView.findViewById(R.id.tooltip_text);
        tooltipText.setText(text);
        mContentView.setElevation(3f);
        tooltipText.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_text_grey));
        tooltipText.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.background_white)));
        mTipWindow.setContentView(mContentView);
        int screen_pos[] = new int[2];
        view.getLocationOnScreen(screen_pos);

        mTipWindow.showAtLocation(view, Gravity.NO_GRAVITY, screenSize.x / 3 - view.getWidth() / 2, screen_pos[1] - 6 * mContentView.getMeasuredHeight() / 5);
        mHandler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 1500);
    }

    public void show(final float x,final float y, final EditText view, final String text, final TooltipStringResponse listener) {
        mTipWindow.setHeight(LayoutParams.WRAP_CONTENT);
        mTipWindow.setWidth(LayoutParams.WRAP_CONTENT);
        mTipWindow.setOutsideTouchable(true);
        mTipWindow.setTouchable(true);
        mTipWindow.setFocusable(true);
        mTipWindow.setBackgroundDrawable(new BitmapDrawable());
        Point screenSize = new Point();
        ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(screenSize);
        TextViewTypeface tooltipText = (TextViewTypeface) mSquareView.findViewById(R.id.tooltip_text);
        tooltipText.setText(text);
        tooltipText.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_text_grey));
        tooltipText.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.background_white)));
        mSquareView.findViewById(R.id.tooltip_insert_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickInsert(text, view);
                mTipWindow.dismiss();
            }
        });
        mTipWindow.setContentView(mSquareView);
        int t[] = new int[2];
        view.getLocationOnScreen(t);
        int screen_pos[] = new int[2];
        view.getLocationOnScreen(screen_pos);
        int height = (int)mContext.getResources().getDimension(R.dimen.quad_content_padding_plus);
        int xpos = (int) x - (mSquareView.getWidth() / (text.length() <= 10 && mSquareView.getWidth() > 500 ? 4 : 2));
        int ypos = (int) y - (t[1] + y < screenSize.y / 3 ? -height : height) + t[1];
        mTipWindow.showAtLocation(view, Gravity.NO_GRAVITY, xpos, ypos);
        mHandler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 3000);
    }

    public boolean isShown() {
        return mTipWindow != null && mTipWindow.isShowing();
    }

    public void dismiss() {
        if (mTipWindow != null && mTipWindow.isShowing())
            mTipWindow.dismiss();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == MSG_DISMISS_TOOLTIP && mTipWindow != null && mTipWindow.isShowing()) {
                mTipWindow.dismiss();
            }
        }

        ;
    };
}
