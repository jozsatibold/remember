package com.thinkit.jozsa.remember.view.home.card;


public interface SelectionListener {
    void setSelection(int pos);
}
