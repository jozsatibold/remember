package com.thinkit.jozsa.remember.widget.list;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.widget.WidgetClickActivity;
import com.thinkit.jozsa.remember.widget.floating.FloatingViewService;

public class ListWidget extends AppWidgetProvider {

    public static String ACTION_WIDGET_NEXT = "ActionReceiverNext";
    public static String ACTION_WIDGET_PREVIOUS = "ActionReceiverPrevious";
    public static String ACTION_WIDGET_NEW = "ActionReceiverNew";
    public static String ACTION_WIDGET_OPEN = "ActionReceiverOpen";
    public static String ACTION_WIDGET_FLOATING = "ActionReceiverOpen_1234";

    public static final String EXTRA_WORD = "com.thinkit.jozsa.remember.widget.WORd";
    public static final String LIST_WIDGET_FILE_NAME = "ListWidget";
    public static final String LIST_WIDGET_LABEL_NAME = "ListWidgetLabelName";
    public static final String LIST_WIDGET_LABEL_ID = "ListWidgetLabelID";
    public static final String LIST_WIDGET_WIDGET_ID = "ListWidgetID";
    public static final String LIST_WIDGET_WIDGETS = "ListWidgets";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        int label = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).getInt(LIST_WIDGET_LABEL_ID + appWidgetId, -1);
        String widgets = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).getString(LIST_WIDGET_WIDGETS, "");
        if (!widgets.contains(appWidgetId + "")) {
            widgets += " " + appWidgetId + ">" + label;
            context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).edit().putString(LIST_WIDGET_WIDGETS, widgets).apply();
        }

        RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.widget_list);
        view.setTextViewText(R.id.widget_label_current, context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0)
                .getString(LIST_WIDGET_LABEL_NAME + appWidgetId, "All"));

        Intent svcIntent = new Intent(context, ListWidgetService.class);
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        svcIntent.putExtra(LIST_WIDGET_LABEL_ID, label);

        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

        view.setRemoteAdapter(R.id.widget_note_list, svcIntent);

        view.setPendingIntentTemplate(R.id.widget_note_list, PendingIntent.getActivity(context, 0,
                new Intent(context, WidgetClickActivity.class)
                        .setAction(ACTION_WIDGET_OPEN),
                PendingIntent.FLAG_UPDATE_CURRENT));

        view.setOnClickPendingIntent(R.id.widget_add, PendingIntent.getBroadcast(context, 0,
                new Intent(context, WidgetClickActivity.class)
                        .setAction(ACTION_WIDGET_NEW), 0));

        view.setOnClickPendingIntent(R.id.widget_next_label, PendingIntent.getBroadcast(context, appWidgetId,
                new Intent(context, ListWidget.class)
                        .putExtra(LIST_WIDGET_WIDGET_ID, appWidgetId)
                        .setAction(ACTION_WIDGET_NEXT), 0));

//        view.setOnClickPendingIntent(R.id.widget_floating, PendingIntent.getBroadcast(context, appWidgetId,
//                new Intent(context, ListWidget.class)
//                        .setAction(ACTION_WIDGET_FLOATING), 0));

        view.setOnClickPendingIntent(R.id.widget_previous_label, PendingIntent.getBroadcast(context, appWidgetId,
                new Intent(context, ListWidget.class)
                        .putExtra(LIST_WIDGET_WIDGET_ID, appWidgetId)
                        .setAction(ACTION_WIDGET_PREVIOUS), 0));

        ComponentName component = new ComponentName(context, ListWidgetFactory.class);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widget_note_list);
        appWidgetManager.updateAppWidget(component, view);
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, view);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);

        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
//        if(action.equals(ACTION_WIDGET_FLOATING)){
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)) {
//                Toast.makeText(context, R.string.overlay_enable, Toast.LENGTH_LONG).show();
//            } else {
//                context.startService(new Intent(context, FloatingViewService.class));
//            }
//        } else
            if (action.equals(ACTION_WIDGET_NEW)) {
            Intent resultIntent = new Intent(context, WidgetClickActivity.class);
            resultIntent.setAction(ACTION_WIDGET_NEW);
            resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1);
            context.startActivity(resultIntent);

        } else if (action.equals(ACTION_WIDGET_OPEN)) {
            int id = intent.getIntExtra(ListWidget.EXTRA_WORD, -1);
            if (id != -1) {
                Intent resultIntent = new Intent(context, WidgetClickActivity.class);
                resultIntent.setAction(ACTION_WIDGET_NEW);
                resultIntent.putExtra(EXTRA_WORD, id);
                context.startActivity(resultIntent);
            }

        } else if (action.equals(ACTION_WIDGET_NEXT)) {
            SharedPreferences.Editor prefs = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).edit();
            int widgetID = intent.getExtras().getInt(LIST_WIDGET_WIDGET_ID, -1);
            int labelID = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).getInt(LIST_WIDGET_LABEL_ID + widgetID, -1);
            Label nextLabel = DatabaseManager.getInstance(context).getLabel(" > " + labelID);
            if (nextLabel == null) {
                nextLabel = new Label(-1, "All", 0);
            }
            prefs.putInt(LIST_WIDGET_LABEL_ID + widgetID, nextLabel.getId());
            prefs.putString(LIST_WIDGET_LABEL_NAME + widgetID, nextLabel.getName());
            prefs.apply();
            this.onUpdate(context, AppWidgetManager.getInstance(context), new int[]{widgetID});

        } else if (action.equals(ACTION_WIDGET_PREVIOUS)) {
            SharedPreferences.Editor prefs = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).edit();
            int widgetID = intent.getIntExtra(LIST_WIDGET_WIDGET_ID, -1);
            int labelID = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).getInt(LIST_WIDGET_LABEL_ID + widgetID, -1);
            Label previousLabel = DatabaseManager.getInstance(context).getLabel(" < " + labelID);
            if (previousLabel == null) {
                previousLabel = new Label(-1, "All", 0);
            }
            prefs.putInt(LIST_WIDGET_LABEL_ID + widgetID, previousLabel.getId());
            prefs.putString(LIST_WIDGET_LABEL_NAME + widgetID, previousLabel.getName());
            prefs.apply();
            this.onUpdate(context, AppWidgetManager.getInstance(context), new int[]{widgetID});
        } else {
            super.onReceive(context, intent);

        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        String listWidgets = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).getString(LIST_WIDGET_WIDGETS, "");
        String widgets[] = listWidgets.split(" ");
        SharedPreferences.Editor prefs = context.getSharedPreferences(LIST_WIDGET_FILE_NAME, 0).edit();
        for (int appWidgetId : appWidgetIds) {
            for (int i = 0; i < widgets.length; ++i) {
                if (widgets[i].contains("" + appWidgetId)) {
                    prefs.putInt(LIST_WIDGET_LABEL_ID + appWidgetId, -1);
                    prefs.putString(LIST_WIDGET_LABEL_NAME + appWidgetId, "All");
                    widgets[i] = "";

                }
            }
        }
        listWidgets = "";
        for (String s : widgets) {
            if (!s.isEmpty()) {
                listWidgets += s + " ";
            }
        }
        if (!listWidgets.isEmpty()) {
            listWidgets = listWidgets.substring(0, listWidgets.length() - 1);
        }
        prefs.putString(LIST_WIDGET_WIDGETS, listWidgets);
        prefs.apply();
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

