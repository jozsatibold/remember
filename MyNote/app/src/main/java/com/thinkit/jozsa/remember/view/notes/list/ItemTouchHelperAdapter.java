package com.thinkit.jozsa.remember.view.notes.list;

public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);
    int getItemCount();
}