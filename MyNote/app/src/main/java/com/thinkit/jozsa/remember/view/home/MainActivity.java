package com.thinkit.jozsa.remember.view.home;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.home.adapters.SwipeListener;
import com.thinkit.jozsa.remember.view.home.adapters.TabPagerAdapter;
import com.thinkit.jozsa.remember.view.home.tab_archive.ArchiveFragment;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.home.tab_delete.TrashFragment;
import com.thinkit.jozsa.remember.view.home.tab_notes.FabListener;
import com.thinkit.jozsa.remember.view.home.tab_notes.NotesFragment;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.search.SearchActivity;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;
import com.thinkit.jozsa.remember.widget.floating.FloatingViewService;
import com.thinkit.jozsa.remember.widget.list.ListWidget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.security.AccessController.getContext;


public class MainActivity extends AppCompatActivity {
    public static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    public static final int ACTIVITY_RESULT_NOTE_CLICK = 11;
    public static final int ACTIVITY_RESULT_SETTINGS = 22;
    public static final int ACTIVITY_RESULT_SEARCH = 33;
    public static final String ACTIVITY_NOTE_ON_CLICK = "NoteID";

    private FloatingActionButton mFab;
    private TabPagerAdapter mTabPager;
    private SettingsPreferences mPreference;
    private ActionBar mActionBar;
    private int mSelection = -1;
    private List<DataUpdateListener> mListeners;
    private int mStatusColor;
    private int mToolbarColor;
    private int mPageNumber = 1;
    private boolean mWaiting;
    private int mUndo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        mPreference = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.main_toolbar));
        checkListenerList();
        mFab = (FloatingActionButton) findViewById(R.id.main_fab);
        //COLORS
        mStatusColor = mPreference.getInt(LoadingActivity.PREFERENCE_STATUS_BAR);
        mToolbarColor = mPreference.getInt(LoadingActivity.PREFERENCE_TOOLBAR);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(mStatusColor);

        findViewById(R.id.main_appbar).setBackgroundColor(mToolbarColor);
        findViewById(R.id.main_content).setBackgroundColor(mPreference.getInt(LoadingActivity.PREFERENCE_BACKGROUND));
        mFab.setBackgroundTintList(ColorStateList.valueOf(mToolbarColor));

        //PAGER
        mTabPager = new TabPagerAdapter(getSupportFragmentManager(), this);
        mTabPager.setSwipeListener(new SwipeListener() {
            @Override
            public void swiped(final int to, final int from, final int id) {
                if (mPreference.getBoolean("ActionLock")) {
                    Dialogs.dialogCheckPassword(MainActivity.this, new SettingsPreferences(MainActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                for (DataUpdateListener listener : mListeners) {
                                    if (listener.getPageNumber() == to) {
                                        listener.addNote(id);
                                    }
                                }

                            } else {
                                for (DataUpdateListener listener : mListeners) {
                                    if (listener.getPageNumber() == from) {
                                        listener.addNote(id);
                                    }
                                }
                            }
                        }
                    });
                } else {
                    for (DataUpdateListener listener : mListeners) {
                        if (listener.getPageNumber() == to) {
                            listener.addNote(id);
                        }
                    }
                }
            }
        });
        ViewPager pager = (ViewPager) findViewById(R.id.main_pager);
        pager.setAdapter(mTabPager);
        pager.setOffscreenPageLimit(3);
        TabLayout tabs = (TabLayout) findViewById(R.id.main_tabs);
        tabs.setupWithViewPager(pager);
        setupTabIcons(tabs);
        pager.setCurrentItem(1);
        mActionBar = getSupportActionBar();
        mActionBar.setTitle(mTabPager.getTitle(1));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mPageNumber = 1;
                        break;
                    case 1:
                        mPageNumber = 0;
                        break;
                    default:
                        mPageNumber = 2;
                }
                mActionBar.setTitle(mTabPager.getTitle(position));
                if (position == 0) {
                    for (DataUpdateListener listener : mListeners) {
                        if (listener.getPageNumber() == 1) {
                            ((ArchiveFragment) listener).setVisible();
                        }
                    }
                }
                if (position == 1) {
                    mFab.show();

                } else {
                    mFab.hide();

                }
                if (mSelection != -1) {
                    removeSelection(mSelection);
                    mSelection = -1;
                    MainActivity.this.invalidateOptionsMenu();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        mTabPager.setSelectionListener(new SelectionListener() {
            @Override
            public void setSelection(int pos) {
                if (pos == -2) {
                    mSelection = -1;
                    MainActivity.this.invalidateOptionsMenu();
                } else {
                    if (mSelection == -1) {
                        mSelection = pos;
                        if (mSelection == 1) {
                            mFab.hide();
                        }
                        MainActivity.this.invalidateOptionsMenu();
                    } else {
                        mSelection = -1;
                        MainActivity.this.invalidateOptionsMenu();
                    }
                }
            }
        });
        mTabPager.setFabListener(new FabListener() {
            @Override
            public void scollTo(int dy) {
                if (dy > 0) {
                    mFab.hide();
                } else if (dy < 0) {
                    mFab.show();

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mSelection != -1) {
            removeSelection(mSelection);
            if (mSelection == 1) {
                mFab.show();
            }
            mSelection = -1;
            invalidateOptionsMenu();
        } else {
            this.finish();
        }
    }

    //region MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if (mSelection == -1) {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_archive).setVisible(false);
            menu.findItem(R.id.action_unarchive).setVisible(false);
            menu.findItem(R.id.action_restore).setVisible(false);
            menu.findItem(R.id.action_delete_forever).setVisible(false);
            menu.findItem(R.id.action_copy).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            menu.findItem(R.id.action_search).setVisible(true);
            menu.findItem(R.id.action_floating).setVisible(true);
        } else {
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.action_search).setVisible(false);
            menu.findItem(R.id.action_floating).setVisible(false);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            menu.findItem(R.id.action_copy).setVisible(true);
            switch (mSelection) {
                case 0:
                    menu.findItem(R.id.action_unarchive).setVisible(true);
                    menu.findItem(R.id.action_delete).setVisible(true);
                    menu.findItem(R.id.action_archive).setVisible(false);
                    menu.findItem(R.id.action_restore).setVisible(false);
                    menu.findItem(R.id.action_delete_forever).setVisible(false);
                    break;
                case 1:
                    menu.findItem(R.id.action_archive).setVisible(true);
                    menu.findItem(R.id.action_delete).setVisible(true);
                    menu.findItem(R.id.action_unarchive).setVisible(false);
                    menu.findItem(R.id.action_restore).setVisible(false);
                    menu.findItem(R.id.action_delete_forever).setVisible(false);
                    break;
                default:
                    menu.findItem(R.id.action_restore).setVisible(true);
                    menu.findItem(R.id.action_delete_forever).setVisible(true);
                    menu.findItem(R.id.action_archive).setVisible(false);
                    menu.findItem(R.id.action_unarchive).setVisible(false);
                    menu.findItem(R.id.action_delete).setVisible(false);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if (!mWaiting) {
            if (id == R.id.action_floating) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
                    //If the draw over permission is not available open the settings screen
                    //to grant the permission.
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + this.getPackageName()));
                    startActivityForResult(intent, MainActivity.CODE_DRAW_OVER_OTHER_APP_PERMISSION);
                } else {
                    startService(new Intent(this, FloatingViewService.class));
                }

            } else if (id == R.id.action_search) {
                Intent intentSearch = new Intent(this, SearchActivity.class);
                startActivityForResult(intentSearch, ACTIVITY_RESULT_SEARCH);
                return true;
            } else if (id == android.R.id.home) {
                removeSelection(mSelection);
                mSelection = -1;
                invalidateOptionsMenu();
                return true;
            } else if (id == R.id.action_settings) {
                startActivityForResult(new Intent(this, SettingsActivity.class), ACTIVITY_RESULT_SETTINGS);
                return true;
            } else if (id == R.id.action_delete_forever) {
                if (mPreference.getBoolean("DeleteLock") || mPreference.getBoolean("ActionLock")) {
                    Dialogs.dialogCheckPassword(this, new SettingsPreferences(this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                Dialogs.dialogSimpleQuestion(MainActivity.this, getString(R.string.dialog_delete_note_title), getString(R.string.dialog_delete_note_content), getString(R.string.dialog_button_remove), getString(R.string.dialog_button_cancel), new DialogSimpleResponse() {
                                    @Override
                                    public void dialogAnswer(boolean ok) {
                                        if (ok) {
                                            setLoadingView(true);
                                            for (DataUpdateListener listener : mListeners) {
                                                if (listener.getPageNumber() == 2) {
                                                    ((TrashFragment) listener).deleteForever(MainActivity.this);
                                                    listener.showPlaceholder();
                                                }
                                            }
                                            setLoadingView(false);
                                        }
                                    }
                                });
                                mSelection = -1;
                                invalidateOptionsMenu();
                            }
                        }
                    });
                } else {
                    Dialogs.dialogSimpleQuestion(MainActivity.this, getString(R.string.dialog_delete_note_title), getString(R.string.dialog_delete_note_content), getString(R.string.dialog_button_cancel), getString(R.string.dialog_button_remove), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                setLoadingView(true);
                                for (DataUpdateListener listener : mListeners) {
                                    if (listener.getPageNumber() == 2) {
                                        ((TrashFragment) listener).deleteForever(MainActivity.this);
                                        listener.showPlaceholder();
                                    }
                                }
                                setLoadingView(false);
                            }
                        }
                    });
                    mSelection = -1;
                    invalidateOptionsMenu();
                }
                return true;
            } else {
                if (mPreference.getBoolean("ActionLock")) {
                    Dialogs.dialogCheckPassword(this, new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                setLoadingView(true);
                                mUndo = mSelection == 0 ? 1 : (mSelection == 1 ? 0 : 2);

                                actionMenu(id);
                                setLoadingView(false);
                            }
                        }
                    });
                } else {
                    setLoadingView(true);
                    mUndo = mSelection == 0 ? 1 : (mSelection == 1 ? 0 : 2);
                    actionMenu(id);
                    setLoadingView(false);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setLoadingView(boolean visible) {
        mWaiting = visible;
        findViewById(R.id.main_loading).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private boolean actionMenu(int id) {
        if (id != R.id.action_copy && id != R.id.action_delete_forever && id != android.R.id.home) {
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.main_content), getString(R.string.main_snack_bar_text), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.action_undo_big), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            undo();
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
        }
        switch (id) {
            case R.id.action_copy: {
                if (mSelection == 1) {
                    mSelection = 0;
                } else if (mSelection == 0) {
                    mSelection = 1;
                }
                for (DataUpdateListener listener : mListeners) {
                    if (listener.getPageNumber() == mSelection) {
                        listener.makeCopy();
                        mSelection = -1;
                        if (mPageNumber == 1) {
                            mFab.show();
                        }
                        listener.removeSelection();
                        invalidateOptionsMenu();
                        break;
                    }
                }
                return true;
            }
            case R.id.action_archive: {
                move(1, 0);
                mSelection = -1;
                mFab.show();
                invalidateOptionsMenu();
                return true;
            }
            case R.id.action_unarchive: {
                move(0, 1);
                mSelection = -1;
                invalidateOptionsMenu();
                return true;
            }
            case R.id.action_delete: {
                move(mSelection, 2);
                mSelection = -1;
                mFab.show();
                String s = this.getSharedPreferences(ListWidget.LIST_WIDGET_FILE_NAME, 0).getString(ListWidget.LIST_WIDGET_WIDGETS, "");
                if (s.length() > 0) {
                    String[] widgets = s.split(" ");
                    s = "";
                    for (String widgetInfo : widgets) {
                        String[] info = widgetInfo.split(">", 3);
                        if (widgetInfo.contains(">"))
                            if (info.length == 2) {
                                s += info[0] + " ";
                            }
                    }
                    if (!s.isEmpty()) {
                        String st[] = s.split(" ");
                        if (st.length > 0) {
                            int widgetIDs[] = new int[st.length];
                            for (int i = 0; i < st.length; ++i) {
                                widgetIDs[i] = Integer.parseInt(st[i]);

                            }
                            Intent intent = new Intent(this, ListWidget.class);
                            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIDs);
                            sendBroadcast(intent);
                        }
                    }
                }

                invalidateOptionsMenu();
                return true;
            }
            case R.id.action_restore: {
                for (DataUpdateListener listener : mListeners) {
                    if (listener.getPageNumber() == 2) {

                        HashMap<Integer, Integer> list = ((TrashFragment) listener).restore();
                        Iterator it = list.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            int to = (int) pair.getValue();
                            replace(to, (int) pair.getKey());
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                        listener.showPlaceholder();
                    }
                }
                mSelection = -1;
                invalidateOptionsMenu();
                return true;
            }
        }
        return false;
    }

    public void undo() {
        setLoadingView(true);
        for (DataUpdateListener listener : mListeners) {
            if (mUndo == listener.getPageNumber()) {

                List<Integer> list = listener.getUndoList();
                DatabaseManager db = DatabaseManager.getInstance(this);
                int type;
                for (Integer id : list) {

                    NoteLite note = db.getCardNote(id, 1);
                    type = note.getTypeId() > 2 ? 2 : note.getTypeId();
                    switch (mUndo) {
                        case 0:
                            note.setTypeId(0);
                            break;
                        case 1:
                            note.setTypeId(mUndo);
                            break;
                        case 2:
                            note.setTypeId(note.getTypeId() * 10);
                            break;
                    }
                    db.updateNoteLite(note);
                    for (DataUpdateListener listener2 : mListeners) {
                        if (listener2.getPageNumber() == type) {
                            listener2.remove(note.getID());
                            break;
                        }
                    }
                    listener.addNote(note.getID());
                }
                break;
            }
        }
        setLoadingView(false);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ACTIVITY_RESULT_NOTE_CLICK:
                    if (data.getBooleanExtra(NoteActivity.RESULT_LABEL, false)) {
                        for (DataUpdateListener listener : mListeners) {
                            if (listener.getPageNumber() == 0) {
                                ((NotesFragment) listener).refreshChipAdapter();
                            }
                        }
                    }
                    dataUpdated(data.getIntExtra(NoteActivity.RESULT_TYPE, -1), data.getIntExtra(NoteActivity.RESULT_NOTE, -1), data.getIntExtra(ACTIVITY_NOTE_ON_CLICK, -1));
                    int id = data.getIntExtra(NoteActivity.RESULT_COPY, -1);
                    if (id > -1) {
                        for (DataUpdateListener listener : mListeners) {
                            if (listener.getPageNumber() == 0) {
                                listener.addNote(id);
                            }
                        }
                    }
                    break;
                case ACTIVITY_RESULT_SETTINGS:
                    if (data.getBooleanExtra(SettingsActivity.SETTINGS_CHANGED, false)) {
                        int statusColor = mPreference.getInt(LoadingActivity.PREFERENCE_STATUS_BAR);
                        if (statusColor != this.mStatusColor) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(statusColor);
                            this.mStatusColor = statusColor;
                            this.mToolbarColor = mPreference.getInt(LoadingActivity.PREFERENCE_TOOLBAR);
                            findViewById(R.id.main_appbar).setBackgroundColor(mToolbarColor);
                            mFab.setBackgroundTintList(ColorStateList.valueOf(mToolbarColor));
                        }
                        findViewById(R.id.main_content).setBackgroundColor(mPreference.getInt(LoadingActivity.PREFERENCE_BACKGROUND));
                        for (DataUpdateListener listener : mListeners) {
                            listener.refreshView();
                        }
                    }
                    break;
                case ACTIVITY_RESULT_SEARCH:
                    if (data.getBooleanExtra(SearchActivity.RESULT_SEARCH_UPDATE, false)) {
                        for (DataUpdateListener listener : mListeners) {
                            listener.refresh();
                        }
                    }
                    ArrayList<String> updateNotes = data.getStringArrayListExtra(SearchActivity.RESULT_SEARCH_NOTES);
                    if (updateNotes != null && !updateNotes.isEmpty()) {
                        for (String s : updateNotes) {
                            String t[] = s.split(" ");
                            dataUpdated(Integer.parseInt(t[0]), Integer.parseInt(t[1]), Integer.parseInt(t[2]));
                        }
                    }
                    if (data.getBooleanExtra(SearchActivity.RESULT_SEARCH_LABEL, false)) {
                        for (DataUpdateListener listener : mListeners) {
                            if (listener.getPageNumber() == 0) {
                                ((NotesFragment) listener).refreshChipAdapter();
                            }
                        }
                    }
                    break;
                case CODE_DRAW_OVER_OTHER_APP_PERMISSION:
                    startService(new Intent(MainActivity.this, FloatingViewService.class));
                    break;
            }

        }
    }

    private void setupTabIcons(TabLayout tab) {

        MyDrawable drawable = new MyDrawable(this);
        for (int i = 0; i < 3; ++i) {
            tab.getTabAt(i).setIcon(drawable.getDrawable(19 + i));
        }
    }

    public synchronized void registerDataUpdateListener(DataUpdateListener listener) {
        checkListenerList();
        mListeners.add(listener);
    }

    private synchronized void checkListenerList() {
        if (mListeners == null) {
            mListeners = new ArrayList<>();
        }
    }

    public synchronized void unregisterDataUpdateListener(DataUpdateListener listener) {
        mListeners.remove(listener);
    }

    public synchronized void dataUpdated(int from, int to, int noteID) {

        for (DataUpdateListener listener : mListeners) {
            if (from == listener.getPageNumber()) {
                listener.update(noteID, to, from);
                if (to != from) {
                    replace(to, noteID);
                }

            }

        }
    }

    private void move(int from, int to) {
        int cat;
        if (from == 0) {
            ++from;
        } else if (from == 1) {
            --from;
        }
        boolean delete = false;
        switch (to) {
            case 0:
                cat = 1;
                break;
            case 1:
                cat = 0;
                break;
            default:
                delete = true;
                cat = 2;
                break;
        }
        for (DataUpdateListener listener : mListeners) {
            if (from == listener.getPageNumber()) {
                replace(cat, listener.getSelectedList(delete ? from : cat, delete));
                listener.showPlaceholder();
                break;
            }
        }
    }

    private void replace(int to, int noteID) {
        for (DataUpdateListener listener2 : mListeners) {
            if (to == listener2.getPageNumber()) {
                listener2.addNote(noteID);
                break;
            }
        }
    }

    private void replace(int to, List<Integer> ids) {
        for (DataUpdateListener listener2 : mListeners) {
            if (to == listener2.getPageNumber()) {
                for (int id : ids) {
                    listener2.addNote(id);
                }
                break;
            }
        }
    }

    private void removeSelection(int pos) {
        if (pos == 0) {
            ++pos;
        } else if (pos == 1) {
            --pos;
        }
        for (DataUpdateListener listener : mListeners) {
            if (listener.getPageNumber() == pos) {
                listener.removeSelection();
            }
        }
    }

    public void onClickFab(View view) {
        Note note = new Note();
        int label = mPreference.getInt("SortLabel");
        note.setLabel(label > 0 ? label : 3);
        int id = DatabaseManager.getInstance(this).insertNewNote(note);
        for (DataUpdateListener listener : mListeners) {
            if (listener.getPageNumber() == mPageNumber) {
                listener.addNote(id);
            }
        }
        Intent intent = new Intent(MainActivity.this, NoteActivity.class);
        intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
        MainActivity.this.startActivityForResult(intent, MainActivity.ACTIVITY_RESULT_NOTE_CLICK);
    }
}
