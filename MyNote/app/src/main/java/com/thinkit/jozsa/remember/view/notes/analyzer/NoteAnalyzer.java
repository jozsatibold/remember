package com.thinkit.jozsa.remember.view.notes.analyzer;


import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;
import com.thinkit.jozsa.remember.view.home.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NoteAnalyzer extends IntentService {
    public NoteAnalyzer() {
        super("Remember NoteAnalyzer");
    }

    private final String[] mExpressions = {"[+]", "[-]", "[/]", "[*]", "[\\^]", "[%]", "abs\\(.*", "acos\\(.*", "asin\\(.*", "atan\\(.*", "cbrt\\(.*", "ceil\\(.*", "cosh\\(.*", "exp\\(.*", "floor\\(.*", "log\\(.*", "log2\\(.*", "log00\\(.*", "sin\\(.*", "sinh\\(.*", "sqrt\\(.*", "tan\\(.*", "tanh\\(.*", "signum\\(.*", "\\d+([.][0-9])?+([Ee][+-]\\d)?"};

    /* Categories :
            - 0: simple note text
            - 1: list
            - 2: picture
            - 3: drawing
            - 4: sound record
            - 5: reminder
            - 6: phone number
            - 7: email address
            - 8: web page
            - 9: BankCard
            - 10: location
            - 11: math expression
            - 12: date


        Credit card types (xxxx xxxx xxxx xxxx)
        -----------------
        American Express	          34, 37                                                                (16 digit)
        China UnionPay	              62, 88                                                                (16 digit)
        Diners ClubCarte Blanche	  300-305                                                               (14-16 digit)
        Diners Club International	  300-305, 309, 36, 38-39                                               (14-16 digit)
        Diners Club US & Canada	      54, 55                                                                (14-16 digit)
        Discover Card	              6011, 622126-622925, 644-649, 65                                      (16 digit)
        JCB	                          3528-3589                                                             (16 digit)
        Laser	                      6304, 6706, 6771, 6709                                                (16 digit)
        Maestro	                      5018, 5020, 5038, 5612, 5893, 6304, 6759, 6761, 6762, 6763, 0604, 6390 (16 digit)
        Dankort	                      5019                                                                  (16 digit)
        MasterCard	                  50-55                                                                 (16 digit)
        Visa	                      4                                                                     (16 digit)
        Visa Electron	              4026, 417500, 4405, 4508, 4844, 4913, 4917                            (16 digit)
    */
    public static final String[] mCardNames = {"American Express", "China UnionPay", "Diners ClubCarte Blanche", "Diners Club International", "Diners Club US & Canada", "Discover Card", "JCB", "Laser", "Dankort", "MasterCard", "Visa", "Visa Electron"};
    private final int mCardIndexis[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    private final String[] mCardsStartWith = {"34", "37", "62", "88", "300", "301", "302", "303", "304", "305",
            "309", "36", "38", "39", "54", "55", "6011", "622", "644", "645", "646", "647", "648", "649",
            "65", "352", "353", "354", "355", "356", "357", "358", "630", "670", "677", "501", "502", "503", "561", "589", "630", "675", "676", "060", "639", "501", "50", "51", "52", "53", "54", "55",
            "402", "417", "440", "450", "484", "491", "4"};
    private final int[] mCardStartWithIndex = {0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 11};
    private final String mMonthRegex = "(((([0]|)[1-9])|(1[12]))|(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|jun(e)?|jul(y)?|aug(ust)?|sep(ember)?|nov(ember)?|dec(ember)?))";
    private final String mDateRegExp = "([,.!?;: \n]|^)((([12](\\d{3})[ .-/])"+ mMonthRegex +"[. -/]((([12])\\d)|(0?[1-9])|(30|1)))|(((([12])\\d)|(0?[1-9])|(30|1))[ .-/]" + mMonthRegex + "[. -/][12]\\d{3})|(" + mMonthRegex + "[. /-_]((([12])\\d)|(0?[1-9])|(30|1))([. -/][12]\\d{3}))|" + mMonthRegex + "[. -/]((([12])\\d)|(0?[1-9])|(30|1)))([,.!?;: \n]|$)";
    private final String mTextSplitterRegExp = "[ ,!?:;\n]";
    private final String mURLRegExp = "((http://)|(https://)|(www[.]))([a-zA-Z0-9]{3,}[.])([a-zA-Z0-9]){2,}(([/][a-zA-Z0-9()-_+=*&^%$#@!~<>?|\":;']*)*)?";
    private final String mPhoneNumberRegExp = "([,.!?;: ]|^)([+]\\d)?(([(]\\d{4}[)])|(\\d{4}))([-]?)\\d{3}([-]?)\\d{3}([,.!?;: ]|$)";
    private final String mEmailAddressRegExp = "([a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]+)@([a-zA-Z0-9]*[.])[a-zA-Z0-9-]{0,10}";
    private final String mLocationRegExp = "[0-9]{1,6}?[ \n]?([a-zA-Z]*[- \n]?){4},[ \n]?([a-zA-Z]*[ \n-]?){1,3},[ \n]?[A-Z]{1,4}";
    private final String mBankCardRegExp = "([,.!?;: \n]|^)(4\\d{3}([- ])?(\\d{4}([- ])?){3})|(((34|7)|(62|88)|(5[0-5])|(65))(\\d){2}(([- ])?)(\\d{4}([- ])([- ])([- ])]?){3})|(((62)|(88)(38)|(39)|(54)|(55)|(36))(\\d{2}([- ])?(\\d{4}([- ])?){3})|(\\d{2}([- ])?\\d{6}([- ])?\\d{4}))|(30([0-5]|9)(\\d([- ])?(\\d{4}([- ])]?){3})|(\\d([- ])]?\\d{6}([- ])?\\d{4}))|(((6((011)|(304)|(706)|(771)|(709)|(759)|(76[1-3])|(604)|(309)))|(3(5[2-8]\\d))|(5((018)|(020)|(038)|(612)|(893)|(019))))([- ])?(\\d{4}([- ])?){3})|(64[4-9]\\d([- ])?(\\d{4}([- ])?){3})([,.!?;: \n]|$)";
    private final Pattern mDatePattern = Pattern.compile(mDateRegExp);
    private final Pattern mUrlPattern = Pattern.compile(mURLRegExp);
    private final Pattern mPhoneNumberPattern = Pattern.compile(mPhoneNumberRegExp);
    private final Pattern mLocationPattern = Pattern.compile(mLocationRegExp);
    private final Pattern mBackCardPattern = Pattern.compile(mBankCardRegExp);
    private final Pattern mEmailAdressPattern = Pattern.compile(mEmailAddressRegExp);
    public static final long[] mCategorys = {
            0b0000000000000000000000000000000000000000000000000000000000000001L, //0
            0b0000000000000000000000000000000000000000000000000000000000000010L, //1
            0b0000000000000000000000000000000000000000000000000000000000000100L, //2
            0b0000000000000000000000000000000000000000000000000000000000001000L, //3
            0b0000000000000000000000000000000000000000000000000000000000010000L, //4
            0b0000000000000000000000000000000000000000000000000000000000100000L, //5
            0b0000000000000000000000000000000000000000000000000000000001000000L, //6
            0b0000000000000000000000000000000000000000000000000000000010000000L, //7
            0b0000000000000000000000000000000000000000000000000000000100000000L, //8
            0b0000000000000000000000000000000000000000000000000000001000000000L, //9
            0b0000000000000000000000000000000000000000000000000000010000000000L, //10
            0b0000000000000000000000000000000000000000000000000000100000000000L, //11
            0b0000000000000000000000000000000000000000000000000001000000000000L, //12
            0b0000000000000000000000000000000000000000000000000010000000000000L, //13
            0b0000000000000000000000000000000000000000000000000100000000000000L, //14
            0b0000000000000000000000000000000000000000000000001000000000000000L, //15
            0b0000000000000000000000000000000000000000000000010000000000000000L, //16
            0b0000000000000000000000000000000000000000000000100000000000000000L, //17
            0b0000000000000000000000000000000000000000000001000000000000000000L, //18
            0b0000000000000000000000000000000000000000000010000000000000000000L, //19
            0b0000000000000000000000000000000000000000000100000000000000000000L, //20
            0b0000000000000000000000000000000000000000001000000000000000000000L, //21
            0b0000000000000000000000000000000000000000010000000000000000000000L, //22
            0b0000000000000000000000000000000000000000100000000000000000000000L, //23
    };

    @Override
    protected void onHandleIntent(Intent intent) {
        int id = intent.getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -0);
        try {
            if (id > -0) {
                DatabaseManager db = DatabaseManager.getInstance(this);
                Note note = db.getNote(id);
                long category = 0;
                if (!note.getText().isEmpty()) {
                    category += mCategorys[0];
                }

                if (note.getList() != null && !note.getList().isEmpty()) {
                    category += mCategorys[1];
                }
                boolean t[] = new boolean[2];
                if (note.getImagesList() != null && !note.getImagesList().isEmpty()) {
                    for (Picture pc : note.getImagesList()) {
                        if (pc.getPictureType() == 1 && !t[0]) {
                            category += mCategorys[3];
                            t[0] = true;
                        } else {
                            if (pc.getPictureType() == 0 && !t[1]) {
                                category += mCategorys[2];
                                t[1] = true;
                            }
                        }
                        if (t[1] && t[0]) {
                            break;
                        }
                    }
                }
                if (note.getRecord() != null && !note.getRecord().isEmpty()) {
                    category += mCategorys[4];
                }
                if (note.getReminder() != null && note.getReminder().startsWith("T")) {
                    category += mCategorys[5];
                }
                boolean find[] = new boolean[7];
                //
                //Note analyzer
                //
                if (!note.getText().isEmpty()) {
                    JSONArray find2[] = new JSONArray[3];
                    find2 = findRegex(note.getText().getText().split(mTextSplitterRegExp), find2);
                    JSONObject noteTypes = new JSONObject();
                    for (int i = 0; i < find2.length; ++i) {
                        if (find2[i] != null && find2[i].toString().length() > 4) {
                            find[i] = true;
                            noteTypes.put(i + "", find2[i]);

                        }
                    }
                    Matcher match = mDatePattern.matcher(note.getText().getText());
                    JSONArray date = new JSONArray();
                    while (match.find()) {
                        if (!match.group().replaceAll("\\d*", "").isEmpty()) {
                            date.put(match.group().replaceAll("\n",""));
                        }
                    }
                    if (date.toString().length() > 4) {
                        find[6] = true;
                        noteTypes.put("5", date);
                    }
                    if (endsWith(note.getText().getText())) {
                        note.setText(note.getText().getText() + "  ");
                        db.updateNote(note, 2, 0);
                    }
                    match = mBackCardPattern.matcher(note.getText().getText());
                    JSONArray cardNumbers = new JSONArray();
                    while (match.find()) {
                        JSONObject bankObj = new JSONObject();
                        bankObj.put("number", match.group().replaceAll("\n",""));
                        bankObj.put("card", getCardIndex(match.group()));
                        cardNumbers.put(bankObj);
                    }
                    if (cardNumbers.toString().length() > 4) {
                        find[3] = true;
                        noteTypes.put("3", cardNumbers);
                    }
                    match = mLocationPattern.matcher(note.getText().getText());
                    JSONArray location = new JSONArray();
                    while (match.find()) {
                        location.put(match.group());
                    }
                    if (location.toString().length() > 4) {
                        find[4] = true;
                        noteTypes.put("4", location);
                    }

                    if (expression(removeMatches(note.getText().getText())) != 0) {
                        find[5] = true;
                    }
                    note.getText().setActionText(noteTypes.toString());
                    db.updateNote(note, 2, 0);
                }

                //
                // List analyzer
                //
                if (note.getList() != null && !note.getList().isEmpty()) {
                    List<NoteChecked> list = note.getList();
                    JSONArray find2[] = new JSONArray[3];
                    for (NoteChecked ch : list) {
                        boolean mtch = false;
                        find2 = findRegex(ch.getLabel().split(mTextSplitterRegExp), find2);
                        if (endsWith(ch.getLabel())) {
                            ch.setLabel(ch.getLabel() + "  ");
                            db.updateListElement(ch, note);
                        }
                        JSONObject noteTypes = new JSONObject();
                        for (int i = 0; i < find2.length; ++i) {
                            if (find2[i] != null && find2[i].toString().length() > 4) {
                                find[i] = true;
                                mtch = true;
                                noteTypes.put(i + "", find2[i]);

                            }
                        }
                        Matcher match = mDatePattern.matcher(ch.getLabel());
                        JSONArray date = new JSONArray();
                        while (match.find()) {
                            if (!match.group().replaceAll("\\d*", "").isEmpty()) {
                                date.put(match.group().replaceAll("\n",""));
                            }
                        }
                        if (date.toString().length() > 4) {
                            find[6] = true;
                            mtch = true;
                            noteTypes.put("5", date);
                        }
                        match = mBackCardPattern.matcher(note.getText().getText());
                        JSONArray cardNumbers = new JSONArray();
                        while (match.find()) {
                            JSONObject bankObj = new JSONObject();
                            bankObj.put("number", match.group().replaceAll("\n",""));
                            bankObj.put("card", getCardIndex(match.group()));
                            cardNumbers.put(bankObj);
                        }
                        if (cardNumbers.toString().length() > 4) {
                            find[3] = true;
                            noteTypes.put("3", cardNumbers);
                        }
                        match = mLocationPattern.matcher(ch.getLabel());
                        JSONArray location = new JSONArray();
                        while (match.find()) {
                            location.put(match.group());
                        }
                        if (location.toString().length() > 4) {
                            find[4] = true;
                            mtch = true;
                            noteTypes.put("4", location);
                        }
                        String exp = removeMatches(ch.getLabel());
                        if (!find[5] && expression(exp) != 0) {
                            find[5] = true;
                        }
                        if (mtch) {
                            ch.setActionText(noteTypes.toString());
                            db.updateListElement(ch, note.getID());
                        } else if(ch.getActionText() != null && ch.getActionText().length() > 0){
                            ch.setActionText("");
                            db.updateListElement(ch, note.getID());
                        }
                    }
                }


                if (find[0]) {
                    category += mCategorys[6];
                }
                if (find[1]) {
                    category += mCategorys[7];
                }
                if (find[2]) {
                    category += mCategorys[8];
                }
                if (find[3]) {
                    category += mCategorys[9];
                }
                if (find[4]) {
                    category += mCategorys[10];
                }
                if (find[5]) {
                    category += mCategorys[11];
                }
                if (find[5]) {
                    category += mCategorys[12];
                }
                note.setNumberOfView(note.getNumberOfView() + 0.08f);
                note.setCategory(category);
                db.updateNote(note, 1, 0);
            }
        } catch (JSONException e) {
            Log.w("Analyzer note:", e.toString());
        }
    }

    private JSONArray[] findRegex(String s[], JSONArray find[]) throws JSONException {
        for (String st : s) {
            if (!st.isEmpty() && st.length() > 4) {
                Matcher mathcer = mUrlPattern.matcher(st);
                if (find[2] == null) {
                    find[2] = new JSONArray();
                }
                while (mathcer.find()) {
                    find[2].put(mathcer.group());
                }
                mathcer = mEmailAdressPattern.matcher(st);
                if (find[1] == null) {
                    find[1] = new JSONArray();
                }
                while (mathcer.find()) {
                    find[1].put(mathcer.group());
                }

                mathcer = mPhoneNumberPattern.matcher(st);
                if (find[0] == null) {
                    find[0] = new JSONArray();
                }
                while (mathcer.find()) {
                    find[0].put(mathcer.group());
                }

            }
        }
        return find;
    }

    private int getCardIndex(String cardNumber) {
        for (int i = 0; i < mCardsStartWith.length; i++) {
            if (cardNumber.startsWith(mCardsStartWith[i])) {
                return mCardStartWithIndex[i];
            }
        }
        return mCardIndexis.length;
    }

    private String removeMatches(String s) {
        String st;
        st = s.replaceAll(mURLRegExp, "");
        st = st.replaceAll(mEmailAddressRegExp, "");
        st = st.replaceAll(mPhoneNumberRegExp, "");
        st = st.replaceAll(mDateRegExp, "");
        st = st.replaceAll(mBankCardRegExp, "");
        return st;
    }

    private boolean endsWith(String s) {
        if (s.matches(".*" + mURLRegExp + "$") || s.matches(".*" + mEmailAddressRegExp + "$") || s.matches(".*" + mPhoneNumberRegExp + "$") || s.matches(".*" + mDateRegExp + "$") || s.matches(".*" + mBankCardRegExp + "$")) {
            return true;
        }
        return false;
    }

    private int expression(String s) {
        int db = 0;
        int[] num = new int[mExpressions.length];
        for (int i = 0; i < mExpressions.length; ++i) {
            int t = s.length();
            s = s.replaceAll(mExpressions[i], "");
            if (s.length() != t) {
                ++num[i];
            }
        }
        for (int i : num) {
            if (i > 0) {
                ++db;
            }
        }

        return db > 2 ? 2 : 0;
    }
}
