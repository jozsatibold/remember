package com.thinkit.jozsa.remember.view.home.adapters;


public interface SwipeListener {
    void swiped(int to, int from, int id);
}
