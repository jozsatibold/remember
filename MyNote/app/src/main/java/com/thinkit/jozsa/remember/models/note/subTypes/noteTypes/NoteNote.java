package com.thinkit.jozsa.remember.models.note.subTypes.noteTypes;


import java.io.Serializable;
import java.util.ArrayList;

public class NoteNote implements NoteTypes, Serializable {
    private String mNote;
    private String mActionText;
    private boolean mScanned;

    public NoteNote(String text){
        mNote = text;
        mActionText = "";
    }
    public NoteNote(String text, String actionText, boolean scanned){
        mNote = text;
        mActionText = actionText;
        mScanned = scanned;
    }

    public NoteNote(){
        mNote = "";
        mActionText = "";
        mScanned = false;
    }

    public void setText(String text){
        mNote = text;
    }

    public String getText(){
        return  mNote;
    }
    public boolean isEmpty(){
        return mNote.isEmpty();
    }
    public String getActionText() { return mActionText; }

    public void setActionText(String actionText) { mActionText = actionText; }

    public void setList(ArrayList list){}

    public ArrayList getList(){ return null; }

    public Boolean getScanned() {
        return mScanned;
    }

    public void setScanned(Boolean scanned) {
        this.mScanned = mScanned;
    }

    @Override
    public String toString() {
        return "NoteNote{" +
                "mNote='" + mNote + '\'' +
                ", mActionText='" + mActionText + '\'' +
                ", mScanned=" + mScanned +
                '}';
    }
}
