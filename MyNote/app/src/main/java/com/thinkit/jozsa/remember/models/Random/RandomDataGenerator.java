package com.thinkit.jozsa.remember.models.Random;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.view.notes.analyzer.NoteAnalyzer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;



public class RandomDataGenerator {
    private static Random rand;

    public static Note getNote(int i) {
        rand = new Random();
        Calendar now = new GregorianCalendar();
        Note note = new Note();
        //category = {"Location", "Note", "Shop", "Event", "Contact", "Picture", "List", "Draw"};
        note.setLastModification(DatabaseManager.mFormat.format(now.getTime()));
        note.setCreatingDate(now);
        note.setTitle(generateText(rand.nextInt(5) + 2));
        note.setTypeId(0);
        int n = rand.nextInt(20);
        if(n < 8){
            note.setText(generateText(rand.nextInt(200) + 2));
            note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[0]);
        } else if(n < 16){
            note.setList(generateStringList(rand.nextInt(50) + 1));
            note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[1]);
        } else {
            note.setList(generateStringList(rand.nextInt(50) + 1));
            note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[0] + NoteAnalyzer.mCategorys[1]);
            note.setText(generateText(rand.nextInt(200) + 2));
        }
        if(rand.nextInt(4) == 0){
            note.setBgColor(rand.nextInt(8));
        }
        note.setSecured(rand.nextBoolean());
        note.setLabel(rand.nextInt(3));
        return note;
    }


    private static String generateString() {
        String []characters = "Made last it seen went no just when of by. Occasional entreaties comparison me difficulty so themselves. At brother inquiry of offices without do my service. As particular to companions at sentiments. Weather however luckily enquire so certain do. Aware did stood was day under ask. Dearest affixed enquire on explain opinion he. Reached who the mrs joy offices pleased. Towards did colonel article any parties. Smallest directly families surprise honoured am an. Speaking replying mistress him numerous she returned feelings may day. Evening way luckily son exposed get general greatly. Zealously prevailed be arranging do. Set arranging too dejection september happiness. Understood instrument or do connection no appearance do invitation. Dried quick round it or order. Add past see west felt did any. Say out noise you taste merry plate you share. My resolve arrived is we chamber be removal. Barton did feebly change man she afford square add. Want eyes by neat so just must. Past draw tall up face show rent oh mr. Required is debating extended wondered as do. New get described applauded incommode shameless out extremity but. Resembled at perpetual no believing is otherwise sportsman. Is do he dispatched cultivated travelling astonished. Melancholy am considered possession on collecting everything. Bringing unlocked me an striking ye perceive. Mr by wound hours oh happy. Me in resolution pianoforte continuing we. Most my no spot felt by no. He he in forfeited furniture sweetness he arranging. Me tedious so to behaved written account ferrars moments. Too objection for elsewhere her preferred allowance her. Marianne shutters mr steepest to me. Up mr ignorant produced distance although is sociable blessing. Ham whom call all lain like. Ought these are balls place mrs their times add she. Taken no great widow spoke of it small. Genius use except son esteem merely her limits. Sons park by do make on. It do oh cottage offered cottage in written. Especially of dissimilar up attachment themselves by interested boisterous. Linen mrs seems men table. Jennings dashwood to quitting marriage bachelor in. On as conviction in of appearance apartments boisterous. Up is opinion message manners correct hearing husband my. Disposing commanded dashwoods cordially depending at at. Its strangers who you certainty earnestly resources suffering she. Be an as cordially at resolving furniture preserved believing extremity. Easy mr pain felt in. Too northward affection additions nay. He no an nature ye talent houses wisdom vanity denied. Marianne or husbands if at stronger ye. Considered is as middletons uncommonly. Promotion perfectly ye consisted so. His chatty dining for effect ladies active. Equally journey wishing not several behaved chapter she two sir. Deficient procuring favourite extensive you two. Yet diminution she impossible understood age. He my polite be object oh change. Consider no mr am overcame yourself throwing sociable children. Hastily her totally conduct may. My solid by stuff first smile fanny. Humoured how advanced mrs elegance sir who. Home sons when them dine do want to. Estimating themselves unsatiable imprudence an he at an. Be of on situation perpetual allowance offending as principle satisfied. Improved carriage securing are desirous too. On insensible possession oh particular attachment at excellence in. The books arose but miles happy she. It building contempt or interest children mistress of unlocked no. Offending she contained mrs led listening resembled. Delicate marianne absolute men dashwood landlord and offended. Suppose cottage between and way. Minuter him own clothes but observe country. Agreement far boy otherwise rapturous incommode favourite. Arrived totally in as between private. Favour of so as on pretty though elinor direct. Reasonable estimating be alteration we themselves entreaties me of reasonably. Direct wished so be expect polite valley. Whose asked stand it sense no spoil to. Prudent you too his conduct feeling limited and. Side he lose paid as hope so face upon be. Goodness did suitable learning put.".split(" ");
        return  characters[rand.nextInt(characters.length)];
    }

    private static ArrayList<NoteChecked> generateStringList(int length) {
        ArrayList<NoteChecked> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(new NoteChecked(i + 1, generateString(), rand.nextBoolean()));
        }
        return list;
    }
    private static String generateText(int length){
        String result = "";
        for(int i = 0; i < length; ++i){
            result += generateString() + " ";
        }
        return result;
    }

}
