package com.thinkit.jozsa.remember.models.note.subTypes.otherTypes;

public class Label {
    private int mId;
    private String mName;
    private int mDrawable;
    private int mPosition;


    public Label() {
        mId = -1;
        mName = "";
        mDrawable = 0;
    }


    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }

    public Label(int id, String name, int drawable) {
        mId = id;
        mName = name;
        mDrawable = drawable;
    }
    public Label(int id, String name, int drawable, int position) {
        mId = id;
        mName = name;
        mDrawable = drawable;
        mPosition = position;
    }

    public int getId() {
        return mId;
    }

    public void setId(int Id) {
        this.mId = Id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getDrawable() {
        return mDrawable;
    }

    public void setDrawable(int drawable) {
        this.mDrawable = drawable;
    }

    @Override
    public String toString() {
        return "Label{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mDrawable=" + mDrawable +
                ", mPosition=" + mPosition +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Label label = (Label) o;

        if (mId != label.mId) return false;
        if (mDrawable != label.mDrawable) return false;
        if (mPosition != label.mPosition) return false;
        return mName.equals(label.mName);

    }

    @Override
    public int hashCode() {
        int result = mId;
        result = 31 * result + mName.hashCode();
        result = 31 * result + mDrawable;
        result = 31 * result + mPosition;
        return result;
    }
}
