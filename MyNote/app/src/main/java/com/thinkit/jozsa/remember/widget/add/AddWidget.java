package com.thinkit.jozsa.remember.widget.add;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.widget.WidgetClickActivity;

/**
 * Implementation of App Widget functionality.
 */
public class AddWidget extends AppWidgetProvider {
    private static String ACTION_WIDGET_ADD_ONCLICK = "ADDWIDGETONCLICK";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_add);
        int colors[] = new Colors(context).getColorArray(5);
        int scolor = new SettingsPreferences(context, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getInt("StatusBarColor");
        int i;
        for(i = 0; i < colors.length; ++i){
            if(scolor == colors[i]){
                break;
            }
        }
        switch (i) {
            case 0:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_white);
                break;
            case 1:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_grey);
                break;
            case 2:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_yellow);
                break;
            case 3:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_orange);
                break;
            case 4:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_red);
                break;
            case 5:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_purple);
                break;
            case 6:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_blue);
                break;
            default:
                views.setImageViewResource(R.id.widget_add_background, R.drawable.ic_cycle_widget_green);
                break;
        }
        views.setOnClickPendingIntent(R.id.widget_add, PendingIntent.getBroadcast(context, 0,
                new Intent(context, WidgetClickActivity.class)
                        .setAction(ACTION_WIDGET_ADD_ONCLICK), 0));
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_WIDGET_ADD_ONCLICK)) {
            Intent resultIntent = new Intent(context, NoteActivity.class);
            int id = DatabaseManager.getInstance(context).insertNewNote(new Note());
            if (id != -1) {
                resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addParentStack(NoteActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                context.startActivities(stackBuilder.getIntents());
            }

        }
        super.onReceive(context, intent);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

