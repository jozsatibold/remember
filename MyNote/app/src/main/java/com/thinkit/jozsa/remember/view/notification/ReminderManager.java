package com.thinkit.jozsa.remember.view.notification;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v7.app.NotificationCompat;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.widget.WidgetClickActivity;
import com.thinkit.jozsa.remember.widget.list.ListWidget;


class ReminderManager {

    static void generateNotification(Context context, int id) {

        NoteLite note = DatabaseManager.getInstance(context).getCardNote(id, 6);
        if (note != null) {
            note.setReminder("t" + note.getReminder().substring(1, note.getReminder().length()));
            DatabaseManager.getInstance(context).updateNoteLite(note);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent resultIntent = new Intent(context, WidgetClickActivity.class);
            resultIntent.putExtra(ListWidget.EXTRA_WORD, id);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
          //  stackBuilder.addParentStack(NoteActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setAutoCancel(true);
            builder.setContentTitle(note.getTitle().isEmpty() ? "Remember" : note.getTitle());
            if (!note.getText().isEmpty()) {
                builder.setContentText(note.getText());
            } else if (note.getList() != null && !note.getList().isEmpty()) {
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                inboxStyle.setBigContentTitle(note.getTitle());
                int n = note.getList().size() > 6 ? 6 : note.getList().size();
                for (int i = 0; i < n; ++i) {
                    inboxStyle.addLine(note.getList().get(i).getLabel());
                }
                builder.setStyle(inboxStyle);
            } else {
                builder.setContentText(context.getString(R.string.notification_reminder));
            }
            builder.setOngoing(false);
            builder.setSmallIcon(R.drawable.ic_launcher);
            Colors color = new Colors(context);
            int cColor = note.getBgColor() == 0 ? color.getStatusBarColor(0) : color.getToolbarColor(note.getBgColor());
            builder.setColor(cColor);
            Intent hour1 = new Intent(context, NotificationActivity.class);
            hour1.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
            hour1.putExtra(NotificationActivity.NOTIFICATION_TIME, 15);
            Intent hour2 = new Intent(context, NotificationActivity.class);
            hour2.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
            hour2.putExtra(NotificationActivity.NOTIFICATION_TIME, 30);
            Intent hour3 = new Intent(context, NotificationActivity.class);
            hour3.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id);
            hour3.putExtra(NotificationActivity.NOTIFICATION_TIME, 60);
            PendingIntent pIntent1 = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), hour1, 0);
            PendingIntent pIntent2 = PendingIntent.getActivity(context, (int) System.currentTimeMillis() + 2, hour2, 0);
            PendingIntent pIntent3 = PendingIntent.getActivity(context, (int) System.currentTimeMillis() + 3, hour3, 0);
            builder.addAction(R.drawable.ic_notification_builder_black_24dp, context.getString(R.string.notification_hour1), pIntent1);
            builder.addAction(R.drawable.ic_notification_builder_black_24dp, context.getString(R.string.notification_hour2), pIntent2);
            builder.addAction(R.drawable.ic_notification_builder_black_24dp, context.getString(R.string.notification_hour3), pIntent3);
            builder.setOnlyAlertOnce(true);
            builder.setContentIntent(pendingIntent);
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            builder.setVibrate(new long[]{500, 500, 500});
            builder.setLights(Color.YELLOW, 3000, 3000);
            notificationManager.notify(id * 100, builder.build());
        }

    }
}
