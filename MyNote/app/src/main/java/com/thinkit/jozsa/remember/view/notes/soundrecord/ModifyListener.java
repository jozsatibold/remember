package com.thinkit.jozsa.remember.view.notes.soundrecord;


public interface ModifyListener {
    void refresh(int type);
}
