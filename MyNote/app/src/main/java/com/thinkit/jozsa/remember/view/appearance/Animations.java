package com.thinkit.jozsa.remember.view.appearance;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.thinkit.jozsa.remember.R;

import static android.view.View.VISIBLE;


public class Animations {
    public void showHidePanel(final View view, final int visible) {
        if (view.getVisibility() != visible) {
            Animator animator = visible != VISIBLE ?
                    ViewAnimationUtils.createCircularReveal(
                            view, view.getWidth() / 15, view.getHeight() / 3 * 2, view.getWidth(), view.getHeight() / 2)
                    :
                    ViewAnimationUtils.createCircularReveal(
                            view, view.getWidth() / 15, view.getHeight() / 3 * 2, -view.getWidth(), view.getHeight() / 2);

            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    view.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(visible == VISIBLE ? visible : View.GONE);
                }
            });
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setStartDelay(50);
            animator.setDuration(250);
            animator.start();
        }
    }

    public void animateAppAndStatusBar(final View revealView, View revealBackgroundView, int fromColor, final int toColor) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                revealView,
                revealView.getWidth() / 2,
                revealView.getHeight() / 2, 0,
                revealView.getWidth() / 2);

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                revealView.setBackgroundColor(toColor);
            }

        });

        revealBackgroundView.setBackgroundColor(fromColor);
        animator.setStartDelay(200);
        animator.setDuration(125);
        animator.start();
        revealView.setVisibility(VISIBLE);
    }

    public void animateAppAndStatusBar(int fromColor, final int toColor, final CollapsingToolbarLayout collapsedToolbar) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                collapsedToolbar,
                collapsedToolbar.getWidth() / 2,
                collapsedToolbar.getHeight() / 2, 0,
                collapsedToolbar.getWidth() / 2);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                collapsedToolbar.setContentScrimColor(toColor);
                collapsedToolbar.setStatusBarScrimColor(toColor);
                collapsedToolbar.setBackgroundColor(toColor);
            }
        });

        animator.setStartDelay(200);
        collapsedToolbar.setContentScrimColor(fromColor);
        collapsedToolbar.setBackgroundColor(fromColor);
        animator.setDuration(125);
        animator.start();
    }

    public void animateViewColor(int fromColor, final int toColor, final View view) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view,
                view.getWidth() / 2,
                view.getHeight() / 2, 0,
                view.getWidth() / 2);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setBackgroundColor(toColor);
            }
        });

        animator.setStartDelay(200);
        view.setBackgroundColor(fromColor);
        animator.setDuration(125);
        animator.start();
    }

    public void animateViews(int fromWidth, final int fromHeight, int toWidth, final int toHeight, final int visibility, final View view) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view,
                fromWidth,
                fromHeight, toWidth,
                toHeight);

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(visibility);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(VISIBLE);
            }
        });

        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(250);
        animator.start();
    }

    public void slideUp(final View view, Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }
        });
        view.startAnimation(animator);
    }

    public void slideUpFast(final View view, Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_up_fast);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }
        });
        view.startAnimation(animator);
    }

    public void slideDown(final View view, Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(View.GONE);
            }
        });
        view.startAnimation(animator);
    }

    public void fadeOutSlow(final View view, final Context context, final boolean hide) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.fade_out);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                if (hide) {
                    view.setVisibility(View.GONE);
                }
                view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorFullTransparent));
            }
        });
        view.startAnimation(animator);
    }

    public void slideUpInvisible(final View view, Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_up_animation);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(View.GONE);
            }
        });
        view.startAnimation(animator);
    }


    public void slideUpInvisibleElevation(final View view, final Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_up_animation);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
                view.setElevation(context.getResources().getDimension(R.dimen.card_elevation));
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(View.GONE);
                view.setElevation(context.getResources().getDimension(R.dimen.sort_elevation));
            }
        });
        view.startAnimation(animator);
    }

    public void slideDownVisible(final View view, Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_down2);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
            }
        });
        view.startAnimation(animator);
    }

    public void slideDownVisibleElevation(final View view, final Context context) {
        Animation animator = AnimationUtils.loadAnimation(context, R.anim.slide_down2);
        // animator.setFillAfter(true);
        animator.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) {
                view.setVisibility(View.GONE);
                view.setElevation(context.getResources().getDimension(R.dimen.card_elevation));
            }

            public void onAnimationRepeat(Animation paramAnimation) {
            }

            public void onAnimationEnd(Animation paramAnimation) {
                view.setVisibility(VISIBLE);
                view.setElevation(context.getResources().getDimension(R.dimen.sort_elevation));
            }
        });
        view.startAnimation(animator);
    }

}

