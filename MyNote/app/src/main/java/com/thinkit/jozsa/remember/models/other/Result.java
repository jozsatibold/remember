package com.thinkit.jozsa.remember.models.other;

public class Result {
    public String ok ="";
    public double result = 0;

    @Override
    public String toString() {
        return "Result{" +
                "ok=" + ok +
                ", result=" + result +
                '}';
    }
}
