package com.thinkit.jozsa.remember.widget.list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteCheckedLite;
import com.thinkit.jozsa.remember.view.appearance.Colors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter.TIME_FORMAT;


class ListWidgetFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext = null;
    private SimpleDateFormat mDateFormat;
    private Colors mColor;
    private ArrayList<NoteLite> mNotes;
    private int labelID;

    ListWidgetFactory(Context context, Intent intent) {
        this.mContext = context;
        //int mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        labelID = intent.getIntExtra(ListWidget.LIST_WIDGET_LABEL_ID, -1);
        mColor = new Colors(context);
        mDateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        mNotes = DatabaseManager.getInstance(mContext).getSortedCardNotes(0, 0, labelID);
    }

    @Override
    public void onCreate() {
        // no-op
    }

    @Override
    public void onDestroy() {
        // no-op
    }

    @Override
    public int getCount() {
        return mNotes.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews card = new RemoteViews(mContext.getPackageName(), R.layout.item_widget_note);
        if (mNotes != null && position < mNotes.size()) {

            NoteLite note = mNotes.get(position);
            boolean secured = note.getSecured();
            card.setInt(R.id.widget_card, "setBackgroundColor", mColor.getCardBackgroundColor(note.getBgColor()));

            if (!note.getTitle().isEmpty()) {
                card.setViewVisibility(R.id.widget_title, VISIBLE);
                card.setTextViewText(R.id.widget_title, note.getTitle());

            } else {
                card.setViewVisibility(R.id.widget_title, GONE);
            }

            if (!note.getText().isEmpty()) {
                card.setViewVisibility(R.id.widget_content, VISIBLE);
                card.setTextViewText(R.id.widget_content, !secured ? note.getText() : note.getText().replaceAll(".", "*"));

            } else {
                card.setViewVisibility(R.id.widget_content, GONE);
            }
            if (!note.getImage().isEmpty()) {
                card.setViewVisibility(R.id.widget_image, VISIBLE);
                if (!secured) {
                    // card.setViewVisibility(R.id.widget_image2, GONE);

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 4;
                    Bitmap bt = BitmapFactory.decodeFile(note.getImage().split(":")[1], options);
                    if (bt != null) {
                        card.setImageViewBitmap(R.id.widget_image, bt);
                    }
                } else {
                    card.setImageViewResource(R.id.widget_image, R.drawable.ic_lock_black_96dp);
                }
            } else {
                card.setViewVisibility(R.id.widget_image, GONE);
            }

            if (!note.getRecord().isEmpty()) {
                card.setViewVisibility(R.id.widget_record, VISIBLE);
                String recDate = note.getRecord().split("REC_")[1];
                recDate = recDate.substring(0, 4) + "." + recDate.substring(4, 6) + "." + recDate.substring(6, 8);
                card.setTextViewText(R.id.widget_record, !secured ? recDate : recDate.replaceAll(".", "*"));

            } else {
                card.setViewVisibility(R.id.widget_record, GONE);
            }


            if (note.getList() != null && !note.getList().isEmpty()) {
                card.setViewVisibility(R.id.widget_note_list, VISIBLE);
                card.removeAllViews(R.id.widget_note_list);
                int db = 0;
                ArrayList<NoteCheckedLite> chList = note.getList();

                for (int i = 0; i < chList.size() && db < 10; ++i) {
                    RemoteViews listElement = new RemoteViews(mContext.getPackageName(), R.layout.list_group);
                    listElement.setTextViewCompoundDrawablesRelative(R.id.list_title, !chList.get(i).getChecked() ? R.drawable.ic_check_box_unchecked_grey_16dp : R.drawable.ic_check_box_grey_16dp, 0, 0, 0);
                    listElement.setTextViewText(R.id.list_title, !secured ? chList.get(i).getLabel() : chList.get(i).getLabel().replaceAll(".", "*"));
                    card.addView(R.id.widget_note_list, listElement);
                    ++db;
                }

            } else {
                card.setViewVisibility(R.id.widget_note_list, GONE);
            }
            card.setViewVisibility(R.id.widget_image, !note.getImage().isEmpty() ? VISIBLE : GONE);

            try {
                if (note.getReminder().charAt(0) == 'T') {
                    card.setViewVisibility(R.id.widget_time, VISIBLE);
                    card.setTextViewText(R.id.widget_time, mDateFormat.format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));

                } else {
                    card.setViewVisibility(R.id.widget_time, GONE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Intent i = new Intent();
            i.putExtra(ListWidget.EXTRA_WORD, mNotes.get(position).getID());
            card.setOnClickFillInIntent(R.id.widget_card, i);
        }
        return (card);
    }

    @Override
    public RemoteViews getLoadingView() {
        return (null);
    }

    @Override
    public int getViewTypeCount() {
        return (1);
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }

    @Override
    public boolean hasStableIds() {
        return (true);
    }

    @Override
    public void onDataSetChanged() {
        mNotes = DatabaseManager.getInstance(mContext).getSortedCardNotes(0, 0, labelID);
    }
}
