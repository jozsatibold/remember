package com.thinkit.jozsa.remember.controller.storage.preference;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingsPreferences {
    private SharedPreferences mPreferences;

    public SettingsPreferences(Context context, String fileName) {
        mPreferences = context.getApplicationContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    public String getString(String key) {
        return mPreferences.getString(key, null);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        return mPreferences.getInt(key, -1);
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public long getLong(String key) {
        return mPreferences.getLong(key, 0);
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return mPreferences.getBoolean(key, false);
    }

    public void setBoolean(String key, Boolean value) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

}
