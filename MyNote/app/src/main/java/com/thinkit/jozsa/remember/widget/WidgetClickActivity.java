package com.thinkit.jozsa.remember.widget;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.SplashActivity;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.widget.list.ListWidget;


public class WidgetClickActivity extends Activity {
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        boolean secure = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION);
        int id = getIntent().getIntExtra(ListWidget.EXTRA_WORD, -1);
        Intent resultIntent = new Intent(this, secure ? SplashActivity.class : NoteActivity.class);
        resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id != -1 ? id : secure ? -2 : DatabaseManager.getInstance(this).insertNewNote(new Note()));
        if(secure){
            startActivity(resultIntent);
        } else {
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(NoteActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            startActivities(stackBuilder.getIntents());
        }
    }

}
