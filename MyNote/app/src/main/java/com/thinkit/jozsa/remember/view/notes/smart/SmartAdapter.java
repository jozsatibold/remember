package com.thinkit.jozsa.remember.view.notes.smart;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;

import java.util.ArrayList;
import java.util.List;

public class SmartAdapter extends RecyclerView.Adapter<SmartAdapter.SmartHolder> {

    private boolean mColorChanged = false;
    private List<String>[] mSmartThings;
    private List<Integer>[] mSmartThingsPos;
    private Context mContext;
    private String[] mNames;
    private int mSize = 0;
    private int mColor;
    private SmartLongClickListener mSmartClickListener;
    private List<SmartChipAdapter> mAdapters;
    public SmartAdapter(List<String>[] list,List<Integer>[] listPos, Context context, int color) {
        mSmartThings = list;
        mSmartThingsPos = listPos;
        mContext = context;
        for(List<String> ls : list){
            if(ls != null && !ls.isEmpty()){
                ++mSize;
            }
        }
        mColor = color;
        mNames = mContext.getResources().getStringArray(R.array.smart_things);
        mAdapters = new ArrayList<>();
    }

    public void setSmartLongClickListener(SmartLongClickListener listener){
        mSmartClickListener = listener;
    }
    private int getSizeList(int pos){
        int j = -1;
        for(int i = 0; i < mSmartThings.length; ++i){
            if(mSmartThings[i] != null && !mSmartThings[i].isEmpty()){
                ++j;
            }
            if(j == pos){
                return i;
            }
        }
        return -1;
    }
    public void setColor(int color){
        mColor = color;
        mColorChanged = true;
        notifyDataSetChanged();
    }
    @Override
    public SmartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SmartHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_smart_things, parent, false));
    }

    @Override
    public void onBindViewHolder(final SmartHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        if(!mColorChanged) {
            int i = getSizeList(pos);
            if (i >= 0 && i < mSmartThings.length) {
                holder.mSmartRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                if (pos >= mAdapters.size() || mAdapters.get(pos) == null) {
                    SmartChipAdapter adapter = new SmartChipAdapter(mContext, mSmartThings[i], mSmartThingsPos[i], i, mColor);
                    adapter.setSmartLongClickListener(mSmartClickListener);
                    mAdapters.add(adapter);

                }
                holder.mSmartRecycler.setAdapter(mAdapters.get(pos));
                holder.mText.setText(mNames[i]);
            }
        } else {
            if(pos == getItemCount() - 1){
                mColorChanged = false;
            }
            SmartChipAdapter adapter = mAdapters.get(pos);
            if(adapter != null){
                if(adapter.getSmartLongClickListener() == null){
                    adapter.setSmartLongClickListener(mSmartClickListener);
                }
                adapter.setColor(mColor);
            }
        }
    }
    public List<String> getSelectedList(){
        List<String> list = new ArrayList<>();
        for(int i = 0; i < mAdapters.size(); ++i){

            List<String> selectedData = mAdapters.get(i).getSelectedList();
            if(selectedData != null && selectedData.size() > 0){
                String s = getSizeList(i) + "";
                for(String st : selectedData){
                    s += ".." + st;
                }
                list.add(s);
            }
        }
        return list;
    }
    @Override
    public int getItemCount() {
        return mSize;
    }
    class SmartHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mText;
        RecyclerView mSmartRecycler;
        SmartHolder(View v) {
            super(v);
            mText = (TextViewTypeface) v.findViewById(R.id.smart_name);
            mSmartRecycler = (RecyclerView) v.findViewById(R.id.smart_recycler);
        }
    }

}
