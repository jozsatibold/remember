
package com.thinkit.jozsa.remember.view.home.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.home.tab_archive.ArchiveFragment;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.home.tab_delete.TrashFragment;
import com.thinkit.jozsa.remember.view.home.tab_notes.FabListener;
import com.thinkit.jozsa.remember.view.home.tab_notes.NotesFragment;


public class TabPagerAdapter extends FragmentPagerAdapter {

    private String[] mTitles;
    private NotesFragment mNotes;
    private SelectionListener mSelectionListener;
    private FabListener mFabListener;
    private SwipeListener mSwipeListener;

    public TabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mTitles = new String[]{
                context.getString(R.string.pager_archive),
                context.getString(R.string.pager_notes),
                context.getString(R.string.pager_trash)
        };
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    public void setLabel(int label) {
        if (getItem(1) != null) {
            mNotes.setLabel(label);
        }
    }

    public void setSelectionListener(SelectionListener selectionListener) {
        mSelectionListener = selectionListener;
    }

    public void setSwipeListener(SwipeListener swipeListener) {
        mSwipeListener = swipeListener;
    }

    public void setFabListener(FabListener fabListener) {
        mFabListener = fabListener;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ArchiveFragment archive = new ArchiveFragment();
                archive.setSelection(mSelectionListener);
                archive.setSwipeListener(mSwipeListener);
                return archive;
            case 1:
                if (mNotes == null) {
                    mNotes = new NotesFragment();
                }
                mNotes.setFabListener(mFabListener);
                mNotes.setSelection(mSelectionListener);
                mNotes.setSwipeListener(mSwipeListener);
                return mNotes;
            default:
                TrashFragment trash = new TrashFragment();
                trash.setSelection(mSelectionListener);
                trash.setSwipeListener(mSwipeListener);
                return trash;
        }
    }

    public CharSequence getTitle(int position) {
        return mTitles[position];
    }

}
