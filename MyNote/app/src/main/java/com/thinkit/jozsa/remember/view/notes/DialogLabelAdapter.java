package com.thinkit.jozsa.remember.view.notes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;

import java.util.ArrayList;
import java.util.List;


public class DialogLabelAdapter extends RecyclerView.Adapter<DialogLabelAdapter.ViewHolder> {

    private List<Label> mLabels;
    private ArrayList<Boolean> mChecked;

    public DialogLabelAdapter(List<Label> labels) {
        mLabels = new ArrayList<>();
        mChecked = new ArrayList<>();
        for(int i = 0; i < labels.size(); ++i){
            if(labels.get(i).getId() > 3){
                mLabels.add(labels.get(i));
            }
            mChecked.add(false);
        }
    }

    @Override
    public DialogLabelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_labels, parent, false));
            }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mCheckBox.setText(mLabels.get(position).getName());
            holder.mCheckBox.setOnCheckedChangeListener(null);
            holder.mCheckBox.setChecked(mChecked.get(position));
            holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mChecked.set(holder.getAdapterPosition(), b);
                }
            });

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
            return mLabels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox mCheckBox;

        ViewHolder(View v) {
            super(v);
            mCheckBox = (CheckBox) v.findViewById(R.id.dialog_item_checkbox);
        }

    }
    public ArrayList<Label> getChecked(){
         ArrayList<Label> checked = new ArrayList<>();
         for(int i = 0; i < mChecked.size(); ++i){
             if(mChecked.get(i)){
                 checked.add(mLabels.get(i));
             }
         }
        return checked;
    }

}
