package com.thinkit.jozsa.remember.view.notes;

import android.Manifest;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.appwidget.AppWidgetManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.Html;
import android.text.Layout;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.compute.Compute;
import com.thinkit.jozsa.remember.controller.compute.ComputeTextViewListener;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;
import com.thinkit.jozsa.remember.models.other.Result;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.SplashActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.view.appearance.TooltipStringResponse;
import com.thinkit.jozsa.remember.view.appearance.TooltipWindow;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.home.adapters.LabelChipAdapter;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.images.FullScreenImageActivity;
import com.thinkit.jozsa.remember.view.notes.analyzer.NoteAnalyzer;
import com.thinkit.jozsa.remember.view.notes.imagelist.ImageAdapter;
import com.thinkit.jozsa.remember.view.notes.list.ListItemTouchHelperCallback;
import com.thinkit.jozsa.remember.view.notes.list.ListRecyclerViewOperator;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.notes.smart.SmartAdapter;
import com.thinkit.jozsa.remember.view.notes.smart.SmartLongClickListener;
import com.thinkit.jozsa.remember.view.notes.soundrecord.OnStartDragListener;
import com.thinkit.jozsa.remember.view.notes.soundrecord.SoundAdapter;
import com.thinkit.jozsa.remember.view.notes.soundrecord.ModifyListener;
import com.thinkit.jozsa.remember.view.paint.ColorRecyclerAdapter;
import com.thinkit.jozsa.remember.view.paint.PaintActivity;
import com.thinkit.jozsa.remember.view.notification.ReminderReceiver;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;
import com.thinkit.jozsa.remember.view.web.ConnectivityReceiver;
import com.thinkit.jozsa.remember.widget.note.NoteWidget;
import com.thinkit.jozsa.remember.widget.note.NoteWidgetConfigureActivity;
import com.thinkit.jozsa.remember.widget.UpdateWidgets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class NoteActivity extends AppCompatActivity {
    public static final String NOTE_GALLERY_POSITION = "GalleryPostion";
    private static final int CAMERA_REQUEST = 1888;
    public static final String RESULT_NOTE = "Result";
    public static final String RESULT_TYPE = "Type";
    public static final String RESULT_LABEL = "Label";
    public static final String RESULT_COPY = "COPY";
    private static final int RESULT_LOAD_IMAGE = 1999;
    public static final int RESULT_PAINT = 1666;
    public static final int RESULT_GALLERY = 1777;
    private static final int READ_REQUEST_CODE = 188;
    private static final int INTERNET_REQUEST_CODE = 198;
    private static final int RECORD_REQUEST_CODE = 178;

    private boolean mFirst = false;
    private boolean mLongClick = false;
    private int mCopyID = -1;
    private Note mNote;
    private Colors mColor;
    private EditText mEditTitle;
    private DatabaseManager mDb;
    private FileManager mFile;
    private CollapsingToolbarLayout mCollapsedToolbar;
    private Animations mAnimation;
    private ImageAdapter mAdapter;
    private LabelChipAdapter mLabelAdapter;
    private SmartAdapter mSmartAdapter;
    private TextViewTypeface mLastEdited;
    private Thread mAnalyzer;
    private int mResult = 0;
    private int mType = 0;
    private int mOriginalLabel = 0;
    private boolean mLocked;
    private boolean mSecured = false;
    private boolean mLabelChanged = false;
    private long mLastPassword = 0;
    private long mWaiting = 0;
    private SettingsPreferences mSettings;
    private TooltipWindow mTooltip;
    private ListRecyclerViewOperator mList;
    private SoundAdapter mSoundAdapter;
    private ToolbarImageClickListener mLastEditedListener = new ToolbarImageClickListener() {
        @Override
        public void onNoteClicked(int pos) {

            mLastEdited.setText(getString(R.string.last_edited, mNote.getLastModification()));
        }
    };
    private ArrayList<String> mSmartThings[];
    private ArrayList<Integer> mSmartThingsPos[];
    private Runnable mSmartAnalyzer = new Runnable() {
        @Override
        public void run() {
            mSmartThings = new ArrayList[6];
            mSmartThingsPos = new ArrayList[6];
            for (int i = 0; i < 6; ++i) {
                mSmartThings[i] = new ArrayList<>();
                mSmartThingsPos[i] = new ArrayList<>();
            }
            if (mNote != null) {
                if (mNote.getText().getActionText() != null && !mNote.getText().getActionText().isEmpty()) {
                    try {
                        analyzeString(-1, new JSONObject(mNote.getText().getActionText()));
                    } catch (JSONException e) {
                        Log.w("Note JSON parse error", e.toString());
                    }
                }
                if (mNote.getList() != null && !mNote.getList().isEmpty()) {
                    List<NoteChecked> list = mNote.getList();
                    int i = 0;
                    for (NoteChecked lb : list) {
                        if (lb.getActionText() != null && !lb.getActionText().isEmpty()) {
                            try {
                                analyzeString(i, new JSONObject(lb.getActionText()));
                            } catch (JSONException e) {
                                Log.w("Note JSON parse error", e.toString());
                            }
                        }
                        ++i;
                    }

                }
            }
            for (int i = 0; i < 6; ++i) {
                if (mSmartThings[i].size() > 0) {
                    findViewById(R.id.note_smart_things).setVisibility(VISIBLE);
                    break;
                }
            }
        }
    };

    private void analyzeString(int num, JSONObject json) throws JSONException {
        for (int i = 0; i < 6; ++i) {
            if (!json.isNull(i + "")) {
                JSONArray array = (JSONArray) json.get(i + "");
                for (int j = 0; j < array.length(); ++j) {
                    String value = array.get(j).toString();
                    if (value != null && !value.isEmpty()) {
                        mSmartThings[i].add(value);
                        mSmartThingsPos[i].add(num);
                    }
                }
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        mCollapsedToolbar = (CollapsingToolbarLayout) findViewById(R.id.note_collapsing);
        setSupportActionBar((Toolbar) findViewById(R.id.note_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDb = DatabaseManager.getInstance(this);

        mNote = mDb.getNote(getIntent().getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1));
        if (mNote == null) {
            saveAndFinish();
        }
        if (this.getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE) {
            findViewById(R.id.note_calendar).setVisibility(VISIBLE);
            findViewById(R.id.note_copy).setVisibility(VISIBLE);
            findViewById(R.id.note_share).setVisibility(VISIBLE);
            findViewById(R.id.note_more).setVisibility(GONE);
        }
        mSettings = new SettingsPreferences(NoteActivity.this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        mColor = new Colors(this);
        mEditTitle = (EditText) findViewById(R.id.note_title);
        mSoundAdapter = new SoundAdapter(mNote.getRecord(), this, mNote.getBgColor() > 0 ? mColor.getToolbarColor(mNote.getBgColor()) : mColor.getTransparentGrey(), mSettings);
        mSoundAdapter.setRecordClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                setLastEdited(3);
            }
        });
        mSoundAdapter.setModifyListener(new ModifyListener() {
            @Override
            public void refresh(int type) {
                if (type == 2) {
                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (ContextCompat.checkSelfPermission(NoteActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                            if (!ActivityCompat.shouldShowRequestPermissionRationale(NoteActivity.this, Manifest.permission.RECORD_AUDIO)) {
                                ActivityCompat.requestPermissions(NoteActivity.this, new String[]{Manifest.permission.RECORD_AUDIO},
                                        RECORD_REQUEST_CODE);
                            }
                        } else {
                            mSoundAdapter.addRecord();
                        }
                    } else {
                        mSoundAdapter.addRecord();
                    }
                } else {
                    mLastPassword = System.currentTimeMillis();
                }
            }
        });
        RecyclerView recordRecycler = (RecyclerView) findViewById(R.id.note_record_recycler);
        recordRecycler.setAdapter(mSoundAdapter);
        recordRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mLastEdited = (TextViewTypeface) findViewById(R.id.note_last_edited);
        mList = new ListRecyclerViewOperator((ViewGroup) findViewById(R.id.search_snack), (RecyclerView) findViewById(R.id.note_list_recycler_unchecked), (RecyclerView) findViewById(R.id.note_list_recycler_checked), this, mNote, mColor.getCardBackgroundColor(1), ((TextView) findViewById(R.id.note_recycler_show)), mLastEditedListener, mDb);
        mAdapter = new ImageAdapter(mNote.getImagesList(), this);
        mAdapter.setImageClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                Intent intent = new Intent(NoteActivity.this, FullScreenImageActivity.class);
                intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
                intent.putExtra(NOTE_GALLERY_POSITION, pos);
                //ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(NoteActivity.this, findViewById(R.id.recycle_picture), getString(R.string.picture_translation));
               // startActivityForResult(intent, NoteActivity.RESULT_GALLERY, options.toBundle());
                startActivityForResult(intent, NoteActivity.RESULT_GALLERY);
            }
        });
        if(mNote.getImagesList() == null || mNote.getImagesList().isEmpty()){
            findViewById(R.id.note_add_panel).setVisibility(GONE);
        }
        RecyclerView recyclerPictures = (RecyclerView) findViewById(R.id.toolbar_recycler_gallery);
        recyclerPictures.setAdapter(mAdapter);
        recyclerPictures.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerPictures.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState > 0 && findViewById(R.id.note_show_panel).getVisibility() != VISIBLE) {
                    mAnimation.showHidePanel(findViewById(R.id.note_add_panel), GONE);
                    findViewById(R.id.note_show_panel).startAnimation(AnimationUtils.loadAnimation(NoteActivity.this, R.anim.fade_in));
                    findViewById(R.id.note_show_panel).setVisibility(VISIBLE);
                }
            }
        });


        boolean checked = getIntent().getBooleanExtra(SplashActivity.NOTE_OPEN_SECURED, false);
        if (checked) {
            setLastPassword();
        }
        if (mNote.getSecured() && !checked) {
            final SettingsPreferences pass = new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
            String password = pass.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
            if (password != null && !password.isEmpty()) {
                mLocked = true;
                View passView = findViewById(R.id.password_view);
                ((ImageView) findViewById(R.id.note_lock)).setImageDrawable(getDrawable(R.drawable.ic_unlock_black_24dp));
                passView.setVisibility(VISIBLE);
                findViewById(R.id.search_layout).setVisibility(GONE);
                passView.setBackgroundColor(mColor.getToolbarColor(mNote.getBgColor()));
                mSecured = true;
                mEditTitle.setEnabled(false);
                final TextInputEditText psswrd = (TextInputEditText) findViewById(R.id.splash_edit_text_password);
                psswrd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            try {

                                if (!pass.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).equals(DataEncrypt.getMd5(psswrd.getText().toString()))) {
                                    ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError(getString(R.string.error_password, pass.getString("Reminder")));
                                } else {
                                    ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError("");
                                    View view = findViewById(R.id.password_view);
                                    new Animations().slideUpInvisible(view, NoteActivity.this);
                                    findViewById(R.id.search_layout).setVisibility(VISIBLE);
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                    mSecured = false;
                                    mNote.setNumberOfView(mNote.getNumberOfView() + 0.08f);
                                    mDb.updateNote(mNote, 1, 0);
                                    findViewById(R.id.note_toolbar).setVisibility(VISIBLE);
                                    mCollapsedToolbar.setActivated(true);
                                    mEditTitle.setEnabled(true);
                                    findViewById(R.id.search_layout).setVisibility(VISIBLE);
                                    settingsToolbar();
                                    postInitialize();
                                    invalidateOptionsMenu();
                                }

                            } catch (Exception e) {
                                Log.w("Note login", e.toString());
                            }
                            handled = true;
                        }
                        return handled;
                    }
                });
            } else {
                findViewById(R.id.note_lock).setVisibility(GONE);
            }
        } else {
            settingsToolbar();
        }
    }


    private void setLastPassword() {
        mLastPassword = System.currentTimeMillis();
        mSoundAdapter.setLastPassword(mLastPassword);
    }

    public void showPanel(View view) {
        view.setVisibility(GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));
        mAnimation.showHidePanel(findViewById(R.id.note_add_panel), VISIBLE);
    }

    @Override
    public void onEnterAnimationComplete() {
        mAnimation = new Animations();
        mFile = new FileManager(this);
        mTooltip = new TooltipWindow(this);
        mList.setLoaded(true);
        if (!mSecured && !mFirst) {
            mFirst = true;
            findViewById(R.id.search_layout).setVisibility(VISIBLE);
            new Animations().slideUpFast(findViewById(R.id.search_layout), this);
            mWaiting = mSettings.getInt(LoadingActivity.PREFERENCE_SECURE_WAITING);
            mSoundAdapter.setWaiting(mWaiting);
            mOriginalLabel = mNote.getLabel();
            mResult = mType = mNote.getTypeId();
            if (mType >= 10) {
                mResult = mType = 2;
            }
            postInitialize();
        } else if (!mSecured && findViewById(R.id.search_layout).getVisibility() != VISIBLE) {
            findViewById(R.id.search_layout).setVisibility(VISIBLE);
        }
        mAnalyzer = new Thread(mSmartAnalyzer);
        mAnalyzer.start();
        if(mNote.getImagesList() == null || mNote.getImagesList().isEmpty()){
            findViewById(R.id.note_add_panel).setVisibility(VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFirst && findViewById(R.id.search_layout).getVisibility() != VISIBLE) {
            findViewById(R.id.search_layout).setVisibility(VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == ORIENTATION_LANDSCAPE) {
            findViewById(R.id.note_calendar).setVisibility(VISIBLE);
            findViewById(R.id.note_copy).setVisibility(VISIBLE);
            findViewById(R.id.note_share).setVisibility(VISIBLE);
            findViewById(R.id.note_more).setVisibility(GONE);
        } else {
            findViewById(R.id.note_calendar).setVisibility(GONE);
            findViewById(R.id.note_copy).setVisibility(GONE);
            findViewById(R.id.note_share).setVisibility(GONE);
            findViewById(R.id.note_more).setVisibility(VISIBLE);
        }
        if (mFirst && findViewById(R.id.search_layout).getVisibility() != VISIBLE) {
            findViewById(R.id.search_layout).setVisibility(VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note, menu);
        menu.findItem(R.id.action_delete).getIcon().setColorFilter(new PorterDuffColorFilter(mColor.getTranslucent(), PorterDuff.Mode.MULTIPLY));
        if (mSecured) {
            menu.findItem(R.id.action_archive).setVisible(false);
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_unarchive).setVisible(false);
            menu.findItem(R.id.action_restore).setVisible(false);
            menu.findItem(R.id.action_delete_forever).setVisible(false);
        } else if (mNote.getTypeId() == 0) {
            menu.findItem(R.id.action_archive).setVisible(true);
            menu.findItem(R.id.action_delete).setVisible(true);
            menu.findItem(R.id.action_unarchive).setVisible(false);
            menu.findItem(R.id.action_restore).setVisible(false);
            menu.findItem(R.id.action_delete_forever).setVisible(false);
        } else if (mNote.getTypeId() == 1) {
            menu.findItem(R.id.action_unarchive).setVisible(true);
            menu.findItem(R.id.action_delete).setVisible(true);
            menu.findItem(R.id.action_archive).setVisible(false);
            menu.findItem(R.id.action_restore).setVisible(false);
            menu.findItem(R.id.action_delete_forever).setVisible(false);
        } else {
            menu.findItem(R.id.action_restore).setVisible(true);
            menu.findItem(R.id.action_delete_forever).setVisible(true);
            menu.findItem(R.id.action_archive).setVisible(false);
            menu.findItem(R.id.action_unarchive).setVisible(false);
            menu.findItem(R.id.action_delete).setVisible(false);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.note_clickable_panel).getVisibility() == VISIBLE) {
            hideSettings(true);
            findViewById(R.id.note_clickable_panel).setVisibility(GONE);

        } else {
            if (mSettings != null && mSettings.getBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE) && checkEmpty()) {
                mResult = 3;
                mDb.deleteNote(mNote, this);
            }
            saveAndFinish();
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if (id == android.R.id.home) {
            if (mSettings.getBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE) && checkEmpty()) {
                mResult = 3;
                mDb.deleteNote(mNote, this);
            }
            saveAndFinish();
            return true;
        } else {
            SettingsPreferences password = new SettingsPreferences(this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
            SettingsPreferences settings = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
            if (id == R.id.action_delete_forever) {
                if (settings.getBoolean("DeleteLock") || settings.getBoolean("ActionLock")) {
                    Dialogs.dialogCheckPassword(this, password, new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                Dialogs.dialogSimpleQuestion(NoteActivity.this, getString(R.string.note_delete_dialog_title), getString(R.string.note_delete_dialog_text), getString(R.string.dialog_button_cancel), getString(R.string.dialog_button_remove), new DialogSimpleResponse() {
                                    @Override
                                    public void dialogAnswer(boolean ok) {
                                        if (ok) {
                                            mDb.deleteNote(mNote, NoteActivity.this);
                                            mResult = 3;
                                            if (mNote.getWidgets().contains("|:")) {
                                                Intent intent = new Intent(NoteActivity.this, NoteWidget.class);
                                                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                                                String[] ids = mNote.getWidgets().split(":");
                                                ArrayList<Integer> wids = new ArrayList<>();
                                                if (ids.length > 0) {
                                                    for (int i = 1; i < ids.length - 1; ++i) {
                                                        if (!ids[i].equals("") && !ids[i].equals("|") && ids[i].length() > 1) {
                                                            int id = Integer.parseInt(ids[i].substring(0, ids[i].length() - 1));
                                                            wids.add(id);
                                                            NoteWidgetConfigureActivity.saveTitlePref(NoteActivity.this, id, mNote.getID());
                                                        }
                                                    }
                                                    int id = Integer.parseInt(ids[ids.length - 1]);
                                                    wids.add(id);
                                                    NoteWidgetConfigureActivity.saveTitlePref(NoteActivity.this, id, mNote.getID());
                                                }
                                                int list[] = new int[wids.size()];
                                                int j = 0;
                                                for (int i : wids) {
                                                    list[j] = i;
                                                    ++j;
                                                }
                                                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, list);
                                                sendBroadcast(intent);
                                            }
                                            saveAndFinish();
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    Dialogs.dialogSimpleQuestion(NoteActivity.this, getString(R.string.note_delete_dialog_title), getString(R.string.note_delete_dialog_text), getString(R.string.dialog_button_cancel), getString(R.string.dialog_button_remove), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                mDb.deleteNote(mNote, NoteActivity.this);
                                mResult = 3;
                                if (mNote.getWidgets().contains("|:")) {
                                    Intent intent = new Intent(NoteActivity.this, NoteWidget.class);
                                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                                    String[] ids = mNote.getWidgets().split(":");
                                    ArrayList<Integer> wids = new ArrayList<>();
                                    if (ids.length > 0) {
                                        for (int i = 1; i < ids.length - 1; ++i) {
                                            if (!ids[i].equals("") && !ids[i].equals("|") && ids[i].length() > 1) {
                                                int id = Integer.parseInt(ids[i].substring(0, ids[i].length() - 1));
                                                wids.add(id);
                                                NoteWidgetConfigureActivity.saveTitlePref(NoteActivity.this, id, mNote.getID());
                                            }
                                        }
                                        int id = Integer.parseInt(ids[ids.length - 1]);
                                        wids.add(id);
                                        NoteWidgetConfigureActivity.saveTitlePref(NoteActivity.this, id, mNote.getID());
                                    }
                                    int list[] = new int[wids.size()];
                                    int j = 0;
                                    for (int i : wids) {
                                        list[j] = i;
                                        ++j;
                                    }
                                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, list);
                                    sendBroadcast(intent);
                                }
                                saveAndFinish();
                            }
                        }
                    });
                    return true;
                }
            } else if (settings.getBoolean("ActionLock") && System.currentTimeMillis() - mLastPassword > mWaiting && !mNote.getSecured()) {
                Dialogs.dialogCheckPassword(this, password, new DialogSimpleResponse() {
                    @Override
                    public void dialogAnswer(boolean ok) {
                        if (ok) {
                            setLastPassword();
                            menuAction(id);
                        }
                    }
                });
            } else {
                setLastPassword();
                menuAction(id);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean menuAction(int id) {
        switch (id) {
            case R.id.action_archive: {
                mNote.setTypeId(1);
                mDb.updateNote(mNote, 1, 0);
                invalidateOptionsMenu();
                mResult = 1;
                return true;
            }
            case R.id.action_unarchive: {
                mNote.setTypeId(0);
                mDb.updateNote(mNote, 1, 0);
                invalidateOptionsMenu();
                mResult = 0;
                return true;
            }
            case R.id.action_restore: {
                if (mNote.getTypeId() >= 10) {
                    mResult = mNote.getTypeId() - 10;
                    mNote.setTypeId(mResult);
                    mDb.updateNote(mNote, 1, 0);
                }
                saveAndFinish();
                return true;
            }

            case R.id.action_delete: {
                if (mNote.getReminder().charAt(0) != 'D') {
                    if (mNote.getReminder().charAt(0) == 'T') {
                        Intent myIntent = new Intent(NoteActivity.this, ReminderReceiver.class);
                        ((AlarmManager) getSystemService(ALARM_SERVICE)).cancel(PendingIntent.getBroadcast(NoteActivity.this, mNote.getID(), myIntent, 0));
                    }
                    mNote.setReminder("D" + DatabaseManager.mFormat.format(new java.util.GregorianCalendar().getTime()));
                }
                mResult = 2;
                mNote.setTypeId(mType + 10);
                mDb.updateNote(mNote, 1, 0);
                saveAndFinish();

                return true;
            }
        }
        return false;
    }

    @Override
    public void onDestroy() {
        try {
            if (mAnalyzer != null) {
                mAnalyzer.join();
            }
        } catch (InterruptedException e) {
            Log.w("Note Analyzer error", e.toString());
        }
        mSoundAdapter.release();
        super.onDestroy();
    }

    @Override
    public void onPause() {

        if (mTooltip != null && mTooltip.isShown()) {
            mTooltip.dismiss();
        }
        mSoundAdapter.stopRecording();
        mSoundAdapter.stopPlaying();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST: {
                    mNote.getImagesList().add(new Picture(mFile.getPhotoPath(), 0));
                    update();
                    setLastEdited(2);
                    if (findViewById(R.id.toolbar_recycler_gallery).getVisibility() == INVISIBLE) {
                        showRecyclerGallery(View.VISIBLE);
                    }
                    break;

                }
                case RESULT_LOAD_IMAGE: {
                    if (data != null) {
                        String path = mFile.getPath(data.getData());
                        try {
                            if (path != null && !path.isEmpty()) {
                                File image = new File(path);
                                if (image.length() < image.getFreeSpace()) {
                                    mFile.copyFile(image, mFile.createImageFile());
                                    mNote.getImagesList().add(new Picture(mFile.getPhotoPath(), 0));
                                    setLastEdited(2);
                                    update();
                                    if (findViewById(R.id.toolbar_recycler_gallery).getVisibility() == INVISIBLE) {
                                        showRecyclerGallery(View.VISIBLE);
                                    }
                                } else {
                                    Toast.makeText(this, R.string.not_enough_space, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(this, R.string.image_chosen, Toast.LENGTH_LONG).show();
                    }
                    break;
                }
                case RESULT_GALLERY: {
                    int result = data.getIntExtra(FullScreenImageActivity.FULLSCREEN_RESULT_IMAGE, -1);
                    if (result == 1) {
                        mNote.setImagesList(mDb.getNote(mNote.getID()).getImagesList());
                        update();
                        setLastEdited(2);
                    }
                    break;
                }
                case RESULT_PAINT: {
                    String url = data.getStringExtra(PaintActivity.DRAW_IMAGE);
                    if (url != null) {
                        mNote.getImagesList().add(new Picture(url, 1));
                        update();
                        setLastEdited(2);
                        if (findViewById(R.id.toolbar_recycler_gallery).getVisibility() == INVISIBLE) {
                            showRecyclerGallery(View.VISIBLE);
                        }
                    }
                    break;
                }
            }
        }
    }

    public void onClickGallery(View view) {
        if (mNote.getImagesList().size() < 10) {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (ContextCompat.checkSelfPermission(NoteActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(NoteActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(NoteActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                READ_REQUEST_CODE);
                    }
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    if (mSoundAdapter != null) {
                        mSoundAdapter.stopRecording();
                        mSoundAdapter.stopPlaying();
                    }
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), RESULT_LOAD_IMAGE);
                }
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if (mSoundAdapter != null) {
                    mSoundAdapter.stopRecording();
                    mSoundAdapter.stopPlaying();
                }
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), RESULT_LOAD_IMAGE);
            }
        } else {
            Toast.makeText(NoteActivity.this, R.string.maximum_images, Toast.LENGTH_LONG).show();
        }
    }

    public void onClickWeb(View view) {
        if (mNote.getImagesList().size() < 10) {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (ContextCompat.checkSelfPermission(NoteActivity.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(NoteActivity.this, Manifest.permission.INTERNET)) {
                        ActivityCompat.requestPermissions(NoteActivity.this, new String[]{Manifest.permission.INTERNET}, INTERNET_REQUEST_CODE);
                    }
                } else {
                    if (ConnectivityReceiver.isConnected(NoteActivity.this)) {
                        if (mSoundAdapter != null) {
                            mSoundAdapter.stopRecording();
                            mSoundAdapter.stopPlaying();
                        }
                        Dialogs.showUrlDialog(NoteActivity.this, mNote, mDb, mAdapter);
                    } else {
                        Toast.makeText(NoteActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                if (ConnectivityReceiver.isConnected(NoteActivity.this)) {
                    if (mSoundAdapter != null) {
                        mSoundAdapter.stopRecording();
                        mSoundAdapter.stopPlaying();
                    }
                    Dialogs.showUrlDialog(NoteActivity.this, mNote, mDb, mAdapter);
                } else {
                    Toast.makeText(NoteActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(NoteActivity.this, R.string.maximum_images, Toast.LENGTH_LONG).show();
        }
    }

    public void onClickPhoto(View view) {
        if (mNote.getImagesList().size() < 10) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = mFile.createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(NoteActivity.this, "com.example.remember.fileprovider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    if (mSoundAdapter != null) {
                        mSoundAdapter.stopRecording();
                        mSoundAdapter.stopPlaying();
                    }
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST);
                }
            }
        } else {
            Toast.makeText(NoteActivity.this, R.string.maximum_images, Toast.LENGTH_LONG).show();
        }
    }

    public void onClickDraw(View view) {
        if (mNote.getImagesList().size() < 10) {
            Intent intent = new Intent(NoteActivity.this, PaintActivity.class);
            if (mSoundAdapter != null) {
                mSoundAdapter.stopRecording();
                mSoundAdapter.stopPlaying();
            }
            startActivityForResult(intent, NoteActivity.RESULT_PAINT);

        } else {
            Toast.makeText(NoteActivity.this, R.string.maximum_images, Toast.LENGTH_LONG).show();
        }
    }

    public void onClickShare(View view) {
        if (!mLongClick) {
            if (mSettings.getBoolean("ActionLock") && System.currentTimeMillis() - mLastPassword > mWaiting && !mNote.getSecured()) {
                Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                    @Override
                    public void dialogAnswer(boolean ok) {
                        if (ok) {
                            setLastPassword();
                            new CalendarAndShare(NoteActivity.this, mNote).share();
                        }
                    }
                });
            } else {
                setLastPassword();
                new CalendarAndShare(NoteActivity.this, mNote).share();
            }
        } else {
            mLongClick = false;
        }
    }

    public void onClickCopy(View view) {
        if (!mLongClick) {
            if (!mNote.getSecured() && mSettings.getBoolean("ActionLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                    @Override
                    public void dialogAnswer(boolean ok) {
                        if (ok) {
                            setLastPassword();
                            Note note = new Note();
                            try {
                                note = new Note(mNote);
                                note.setNumberOfView(0);
                                for (Picture pc : note.getImagesList()) {
                                    mFile.copyFile(new File(pc.getSrc().split(":")[1]), mFile.creteFileCopy(".jpg"));
                                    pc.setSrc(mFile.getPhotoPath());
                                }
                                if (!note.getRecord().isEmpty()) {
                                    for (String src : note.getRecord()) {
                                        mFile.copyFile(new File(src.split(":")[1]), mFile.creteFileCopy(".3gp"));
                                        note.getRecord().set(0, mFile.getPhotoPath());
                                    }
                                }

                                mCopyID = mDb.insertNote(note);
                                if (mCopyID > -1) {
                                    Toast.makeText(NoteActivity.this, R.string.copy_made, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(NoteActivity.this, R.string.copy_error, Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                if (note.getID() == 0) {
                                    mDb.deleteNote(note, NoteActivity.this);
                                }
                                Log.w("Note file copy error", e.toString());
                                Toast.makeText(NoteActivity.this, R.string.copy_file_error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            } else {
                setLastPassword();
                Note note = new Note();
                try {
                    note = new Note(mNote);
                    note.setNumberOfView(0);
                    for (Picture pc : note.getImagesList()) {
                        mFile.copyFile(new File(pc.getSrc().split(":")[1]), mFile.creteFileCopy(".jpg"));
                        pc.setSrc(mFile.getPhotoPath());
                    }
                    if (!note.getRecord().isEmpty()) {
                        mFile.copyFile(new File(note.getRecord().get(0).split(":")[1]), mFile.creteFileCopy(".jpg"));
                        note.getRecord().set(0, mFile.getPhotoPath());
                    }

                    mCopyID = mDb.insertNote(note);
                    if (mCopyID > -1) {
                        Toast.makeText(NoteActivity.this, R.string.copy_made, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NoteActivity.this, R.string.copy_error, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    if (note.getID() == 0) {
                        mDb.deleteNote(note, NoteActivity.this);
                    }
                    Log.w("Note file copy error", e.toString());
                    Toast.makeText(NoteActivity.this, R.string.copy_file_error, Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            mLongClick = false;
        }
    }


    public void onClickCalendar(View view) {
        if (!mLongClick) {
            new CalendarAndShare(NoteActivity.this, mNote).setCalendar();
        } else {
            mLongClick = false;
        }
    }

    private void postInitialize() {
        final RecyclerView bgColorLayout = (RecyclerView) findViewById(R.id.note_background_color_recycler);
        final ColorRecyclerAdapter adapterBackground = new ColorRecyclerAdapter(NoteActivity.this, mColor.getColorList(4));
        adapterBackground.setSelected(mNote.getBgColor());
        bgColorLayout.setAdapter(adapterBackground);
        bgColorLayout.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapterBackground.setOnClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                adapterBackground.setSelected(adapterBackground.getPosColor(pos));
                ((ImageView) findViewById(R.id.note_background_color)).setColorFilter(adapterBackground.getPosColor(pos));
                mAnimation.animateAppAndStatusBar(mColor.getToolbarColor(mNote.getBgColor()), mColor.getToolbarColor(pos), mCollapsedToolbar);
                mLabelAdapter.setColor(mColor.getStatusBarColor(pos));
                if (mSmartAdapter != null) {
                    mSmartAdapter.setColor(mColor.getStatusBarColor(pos));
                }
                mNote.setBgColor(pos);
                if (pos > 0) {
                    mSoundAdapter.setColor(mColor.getToolbarColor(pos));
                } else {
                    mSoundAdapter.setColor(mColor.getTransparentGrey());
                }
                setLastEdited(0);
            }
        });

        mLabelAdapter = new LabelChipAdapter(NoteActivity.this, mDb, mColor.getStatusBarColor(mNote.getBgColor()), mNote.getLabel(), false);
        RecyclerView recyclerLabel = (RecyclerView) findViewById(R.id.note_label_recycler_category);
        recyclerLabel.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerLabel.setAdapter(mLabelAdapter);
        recyclerLabel.setHasFixedSize(true);


        ListItemTouchHelperCallback callback = new ListItemTouchHelperCallback(mLabelAdapter);
        callback.setSaveLastItems(2);
        callback.setOrientation(true);
        final ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerLabel);
        mLabelAdapter.setOnStartDragListener(new OnStartDragListener() {
            @Override
            public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                touchHelper.startDrag(viewHolder);
                mLabelChanged = true;
            }
        });

        mLabelAdapter.setOnClickListener(new NoteClickListener() {

            boolean mLongClick = false;

            @Override
            public void onNoteClicked(int pos) {
                if (pos == mLabelAdapter.getItemCount() - 1) {

                    if (mLabelAdapter.getItemCount() > 5) {
                        if (mSettings.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                                @Override
                                public void dialogAnswer(boolean ok) {
                                    if (ok) {
                                        mLastPassword = System.currentTimeMillis();
                                        final Dialog dialogRemoveLabel = new Dialog(NoteActivity.this);
                                        dialogRemoveLabel.setContentView(R.layout.dialog_remove_label);
                                        final RecyclerView recyclerLabel = (RecyclerView) dialogRemoveLabel.findViewById(R.id.dialog_label_recycler);
                                        recyclerLabel.setAdapter(new DialogLabelAdapter(mLabelAdapter.getLabels()));
                                        recyclerLabel.setHasFixedSize(true);
                                        recyclerLabel.setLayoutManager(new LinearLayoutManager(NoteActivity.this));
                                        dialogRemoveLabel.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialogRemoveLabel.dismiss();
                                            }
                                        });
                                        dialogRemoveLabel.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ArrayList<Label> list = ((DialogLabelAdapter) recyclerLabel.getAdapter()).getChecked();
                                                mLabelChanged = true;
                                                for (Label label : list) {
                                                    mLabelAdapter.removeLabel(label);
                                                    if (label.getId() > 2) {
                                                        mDb.removeLabel(label.getName());
                                                    }
                                                }
                                                Toast.makeText(NoteActivity.this, R.string.label_moved, Toast.LENGTH_LONG).show();
                                                dialogRemoveLabel.dismiss();
                                            }
                                        });


                                        dialogRemoveLabel.show();
                                    }
                                }
                            });
                        } else {
                            final Dialog dialogRemoveLabel = new Dialog(NoteActivity.this);
                            dialogRemoveLabel.setContentView(R.layout.dialog_remove_label);
                            final RecyclerView recyclerLabel = (RecyclerView) dialogRemoveLabel.findViewById(R.id.dialog_label_recycler);
                            recyclerLabel.setAdapter(new DialogLabelAdapter(mLabelAdapter.getLabels()));
                            recyclerLabel.setHasFixedSize(true);
                            recyclerLabel.setLayoutManager(new LinearLayoutManager(NoteActivity.this));
                            dialogRemoveLabel.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogRemoveLabel.dismiss();
                                }
                            });
                            dialogRemoveLabel.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ArrayList<Label> list = ((DialogLabelAdapter) recyclerLabel.getAdapter()).getChecked();
                                    mLabelChanged = true;
                                    for (Label label : list) {
                                        mLabelAdapter.removeLabel(label);
                                        if (label.getId() > 2) {
                                            mDb.removeLabel(label.getName());
                                        }
                                    }
                                    Toast.makeText(NoteActivity.this, R.string.label_moved, Toast.LENGTH_LONG).show();
                                    dialogRemoveLabel.dismiss();
                                }
                            });


                            dialogRemoveLabel.show();
                        }

                    } else {
                        Toast.makeText(NoteActivity.this, R.string.label_not_found, Toast.LENGTH_SHORT).show();
                    }
                } else if (pos == mLabelAdapter.getItemCount() - 2) {
                    if (mSettings.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                        Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                            @Override
                            public void dialogAnswer(boolean ok) {
                                if (ok) {
                                    mLabelChanged = true;
                                    mLastPassword = System.currentTimeMillis();
                                    Dialogs.dialocEditLabel(mLabelAdapter, null, NoteActivity.this);
                                }
                            }
                        });
                    } else {
                        mLabelChanged = true;
                        Dialogs.dialocEditLabel(mLabelAdapter, null, NoteActivity.this);
                    }
                } else {
                    if (!mLongClick) {
                        mLabelAdapter.setSelected(pos);
                        mNote.setLabel(mLabelAdapter.getSelectedPos(pos));
                        setLastEdited(0);
                    } else {
                        mLongClick = false;
                    }
                }
            }

            @Override
            public boolean onNoteLongClicked(final int position) {

                mLongClick = true;
                if (position > 2 && position < mLabelAdapter.getItemCount() - 2) {
                    if (mSettings.getBoolean("LabelLock") && System.currentTimeMillis() - mLastPassword > mWaiting) {
                        Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                            @Override
                            public void dialogAnswer(boolean ok) {
                                if (ok) {
                                    mLastPassword = System.currentTimeMillis();
                                    Dialogs.dialocEditLabel(mLabelAdapter, mLabelAdapter.getPosLabel(position), NoteActivity.this);
                                }
                            }
                        });
                    } else {
                        mLastPassword = System.currentTimeMillis();
                        Dialogs.dialocEditLabel(mLabelAdapter, mLabelAdapter.getPosLabel(position), NoteActivity.this);
                    }
                }
                return false;
            }
        });
        recyclerLabel.scrollToPosition(mLabelAdapter.setSelected(mLabelAdapter.getSelectedPos(mNote.getLabel())));
        findViewById(R.id.note_lock).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mTooltip.show(v, getString(!mLocked ? R.string.lock : R.string.unlock));
                mTooltip.show(v, getString(!mLocked ? R.string.lock : R.string.unlock));
                mLongClick = true;
                return false;
            }
        });
        findViewById(R.id.note_calendar).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mTooltip.show(view, getString(R.string.add_to_calendar));
                mLongClick = true;
                return false;
            }
        });

        findViewById(R.id.note_copy).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mTooltip.show(view, getString(R.string.make_copy));
                mLongClick = true;
                return false;
            }
        });
        findViewById(R.id.note_share).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mTooltip.show(view, getString(R.string.action_share));
                mLongClick = true;
                return false;
            }
        });
    }

    private void settingsToolbar() {
        final ImageView toolbarBackgroundColor = (ImageView) findViewById(R.id.note_background_color);
        final ImageView toolbarLabel = (ImageView) findViewById(R.id.note_label);
        final ImageView toolbarNotification = (ImageView) findViewById(R.id.note_create_notification);

        toolbarBackgroundColor.setColorFilter(mColor.getToolbarColor(mNote.getBgColor()));
        toolbarNotification.setImageDrawable(getDrawable(mNote.getReminder() != null && !mNote.getReminder().equals("0") && mNote.getReminder().charAt(0) == 'T' ? R.drawable.ic_notifications_off_black_24dp : R.drawable.ic_notifications_black_24dp));

        if (mNote.getTypeId() > 1) {
            toolbarNotification.setVisibility(GONE);
        }
        toolbarNotification.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                char c = mNote.getReminder().charAt(0);
                if (mNote.getReminder().length() > 1) {
                    if (c == 'P') {
                        mTooltip.show(view, getString(R.string.reminder_location, mNote.getReminder().substring(1).split("---")[0]));
                    } else if (c == 'T' || c == 't') {

                        try {
                            String reminder = mNote.getReminder();
                            Calendar calendar = DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(reminder.substring(1, reminder.length())));
                            Calendar cal = new GregorianCalendar();
                            double millis = ((calendar.getTimeInMillis() - cal.getTimeInMillis()) / 86400000.0);
                            if (millis >= 2) {
                                mTooltip.show(view, getString(R.string.reminder_date, mNote.getReminder().substring(1)));
                            } else if (millis >= 1 || (cal.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH) && millis < 1 && millis >= 0)) {
                                mTooltip.show(view, getString(R.string.reminder_date_tomorrow, reminder.substring(reminder.length() - 5)));
                            } else if (millis >= 0) {
                                mTooltip.show(view, getString(R.string.reminder_date_today, reminder.substring(reminder.length() - 5)));
                            } else if (millis > -1 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis > -1 && millis < 0)) {
                                mTooltip.show(view, getString(R.string.last_reminder_date_today, reminder.substring(reminder.length() - 5)));
                            } else {
                                calendar.setTimeInMillis(calendar.getTimeInMillis() - 86400000);
                                if (millis >= -2 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis < -1 && millis >= -2)) {
                                    mTooltip.show(view, getString(R.string.last_reminder_date_yesterday, reminder.substring(reminder.length() - 5)));
                                } else {
                                    mTooltip.show(view, getString(R.string.last_reminder_date, reminder.substring(reminder.length() - 5)));
                                }
                            }
                        } catch (ParseException e) {
                            Log.w("Note reminder error", e.toString());
                        }
                    }
                } else {
                    mTooltip.show(view, getString(R.string.reminder_set));

                }
                mLongClick = true;
                return false;
            }
        });

        mLastEditedListener.onNoteClicked(0);

        mEditTitle.setText(mNote.getTitle());
        mEditTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mNote.setTitle(editable.toString());
                setLastEdited(0);
            }
        });
        final EditText editNote = (EditText) findViewById(R.id.note_note);
        if (!mNote.getText().isEmpty()) {
            editNote.setText(mNote.getText().getText());
            Linkify.addLinks(editNote, Linkify.ALL);

        }
        final TooltipStringResponse response = new TooltipStringResponse() {
            @Override
            public void onClickInsert(String result, EditText textView) {
                String text = mNote.getText().getText();
                int cursorPos = textView.getSelectionStart() - 1;
                boolean isNumber = result.replaceAll("[0-9]", "").length() != result.length();
                if (isNumber && text != null && text.charAt(cursorPos) == '=') {
                    mNote.setText(text.substring(0, cursorPos + 1) + result + text.substring(cursorPos + 1, text.length()));
                    textView.setText(mNote.getText().getText());
                    textView.setSelection(cursorPos + result.length() + 1);
                }
            }
        };

        editNote.addTextChangedListener(new TextWatcher() {
            Compute comp = new Compute(new ComputeTextViewListener() {
                @Override
                public void returnTextView(final EditText textView) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Result res = comp.getResult();
                            String s = " ";
                            if (res.ok.isEmpty()) {
                                s += res.result;
                            } else {
                                s += res.ok;
                            }
                            int pos = textView.getSelectionStart();
                            Layout layout = textView.getLayout();
                            int line = layout.getLineForOffset(pos);
                            int baseline = layout.getLineBaseline(line);
                            int ascent = layout.getLineAscent(line);
                            float x = layout.getPrimaryHorizontal(pos);
                            float y = baseline + ascent;
                            mTooltip.show(x, y, textView, s, response);
                        }
                    });

                }

            });

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                String text = editable.toString();
                int pos = editNote.getSelectionStart();
                if (pos > 0 && editable.charAt(pos - 1) == '=' && editable.toString().length() > mNote.getText().getText().length()) {
                    comp.getResult(editable.toString().substring(0, pos - 1), editNote);
                }
                mNote.setText(text);
                setLastEdited(0);
            }
        });
        mCollapsedToolbar.setContentScrimColor(mColor.getStatusBarColor(mNote.getBgColor()));
        mCollapsedToolbar.setStatusBarScrimColor(mColor.getStatusBarColor(mNote.getBgColor()));
        mCollapsedToolbar.setBackgroundColor(mColor.getStatusBarColor(mNote.getBgColor()));

        //RecyclerView

        if (mNote.getImagesList() == null || mNote.getImagesList().size() <= 0) {
            ((AppBarLayout) findViewById(R.id.note_appbar)).setExpanded(false);
            showRecyclerGallery(INVISIBLE);
        }
        toolbarLabel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mLongClick = true;
                Label lb = mDb.getLabel(mNote.getLabel());
                mTooltip.show(view, lb == null ? getString(R.string.unknow_label) : lb.getName());
                return false;
            }
        });


    }

    public void onClickClickablePanel(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            hideSettings(true);
        }

    }

    private void hideSettings(boolean hide) {
        final View backgroundColor = findViewById(R.id.note_background_color_box);
        final View label = findViewById(R.id.note_label_box);
        final View more = findViewById(R.id.note_more_box);
        final View smart = findViewById(R.id.note_smart_box);
        if (smart.getVisibility() == VISIBLE) {
            mAnimation.slideDown(smart, this);
            mAnimation.fadeOutSlow(findViewById(R.id.note_clickable_panel), this, hide);

        }
        if (backgroundColor.getVisibility() == VISIBLE) {
            mAnimation.slideDown(backgroundColor, this);
        }
        if (label.getVisibility() == VISIBLE) {
            mAnimation.slideDown(label, this);
        }
        if (more.getVisibility() == VISIBLE) {
            mAnimation.animateViews(more.getWidth() / 4 * 3, more.getHeight(), more.getHeight(), -Math.max(more.getMeasuredWidth(), more.getMeasuredHeight()), INVISIBLE, more);
        }
    }

    private boolean reminderDialog() {
        if (mNote.getTypeId() != 2) {
            final int RQS_R = mNote.getID();
            if (mNote.getReminder() == null || mNote.getReminder().charAt(0) != 'T') {
                final Dialog dialogDate = new Dialog(NoteActivity.this);
                dialogDate.setContentView(R.layout.dialog_reminder_time);
                dialogDate.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDate.dismiss();
                    }
                });
                final Calendar myCalendar = Calendar.getInstance();
                final TextInputEditText date = (TextInputEditText) dialogDate.findViewById(R.id.dialog_date);
                final TextInputEditText hour = (TextInputEditText) dialogDate.findViewById(R.id.dialog_hour);
                final TextInputLayout date_input = (TextInputLayout) dialogDate.findViewById(R.id.text_input_date);
                final TextInputLayout hour_input = (TextInputLayout) dialogDate.findViewById(R.id.text_input_hour);

                dialogDate.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (date.getText().length() > 0 && hour.getText().length() > 0) {
                            if (myCalendar.getTimeInMillis() > System.currentTimeMillis() + 1000) {
                                mNote.setReminder("T" + DatabaseManager.mFormat.format(myCalendar.getTime()));
                                ((ImageView) findViewById(R.id.note_create_notification)).setImageDrawable(getDrawable(R.drawable.ic_notifications_off_black_24dp));
                                setLastEdited(0);
                                Intent myIntent = new Intent(NoteActivity.this, ReminderReceiver.class);
                                myIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
                                ((AlarmManager) getSystemService(ALARM_SERVICE)).set(AlarmManager.RTC, myCalendar.getTimeInMillis(), PendingIntent.getBroadcast(NoteActivity.this, RQS_R, myIntent, 0));
                                dialogDate.dismiss();
                            } else {
                                date_input.setError(getString(R.string.dialog_past_date));
                                hour_input.setError(getString(R.string.dialog_past_date));
                            }
                        } else {
                            date_input.setError(date.getText().length() == 0 ? getString(R.string.dialog_complete) : null);
                            hour_input.setError(hour.getText().length() == 0 ? getString(R.string.dialog_complete) : null);
                        }
                    }
                });
                dialogDate.findViewById(R.id.dialog_date).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePickerDialog datePicker = new DatePickerDialog(NoteActivity.this, getPickerThem(mNote.getBgColor(), true), new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                String myFormat = "MM/dd/yy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                date.setText(sdf.format(myCalendar.getTime()));
                                date_input.setError(null);
                            }

                        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                        datePicker.show();
                    }
                });
                dialogDate.findViewById(R.id.dialog_hour).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TimePickerDialog mTimePicker = new TimePickerDialog(NoteActivity.this, getPickerThem(mNote.getBgColor(), false), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                myCalendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                                myCalendar.set(Calendar.MINUTE, selectedMinute);
                                hour.setText(selectedHour + ":" + (selectedMinute < 10 ? "0" + selectedMinute : selectedMinute));
                                hour_input.setError(null);
                            }
                        }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true);
                        mTimePicker.show();
                    }
                });
                dialogDate.show();
            } else {
                Dialogs.dialogSimpleQuestion(this, "", getString(R.string.dialog_delete_text), getString(R.string.dialog_button_cancel), getString(R.string.dialog_button_remove), new DialogSimpleResponse() {
                    @Override
                    public void dialogAnswer(boolean ok) {
                        if (ok) {
                            if (mNote.getReminder() != null) {
                                mNote.setReminder("0");
                            }
                            setLastEdited(0);
                            Intent myIntent = new Intent(NoteActivity.this, ReminderReceiver.class);
                            myIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
                            ((AlarmManager) getSystemService(ALARM_SERVICE)).cancel(PendingIntent.getBroadcast(NoteActivity.this, RQS_R, myIntent, 0));
                            ((ImageView) findViewById(R.id.note_create_notification)).setImageDrawable(getDrawable(R.drawable.ic_notifications_black_24dp));
                        }
                    }
                });
            }
        } else {
            Toast.makeText(this, R.string.deleted_note_reminder, Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void onClickLock(View view) {
        if (!mLongClick) {
            if (mLocked) {
                mNote.setSecured(!mNote.getSecured());
                mDb.updateNote(mNote, 1, 0);
                ((ImageView) findViewById(R.id.note_lock)).setImageDrawable(getDrawable(R.drawable.ic_lock_black_24dp));
            } else {
                Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                    @Override
                    public void dialogAnswer(boolean ok) {
                        if (ok) {
                            setLastPassword();
                            mNote.setSecured(true);
                            mNote.setNumberOfView(mNote.getNumberOfView() + 0.08f);
                            mDb.updateNote(mNote, 1, 0);
                            ((ImageView) findViewById(R.id.note_lock)).setImageDrawable(getDrawable(R.drawable.ic_unlock_black_24dp));
                        }
                    }
                });
            }
            mDb.updateNote(mNote, 1, 0);
            mLocked = false;
        } else {
            mLongClick = false;
        }
    }

    public int getPickerThem(int color, boolean datePicker) {
        if (datePicker) {
            switch (color) {
                case 0:
                    return R.style.DatePickerWhite;
                case 1:
                    return R.style.DatePickerGrey;
                case 2:
                    return R.style.DatePickerYellow;
                case 3:
                    return R.style.DatePickerOrange;
                case 4:
                    return R.style.DatePickerRed;
                case 5:
                    return R.style.DatePickerPurple;
                case 6:
                    return R.style.DatePickerBlue;
                default:
                    return R.style.DatePickerGreen;
            }
        } else {
            switch (color) {
                case 0:
                    return R.style.TimePickerWhite;
                case 1:
                    return R.style.TimePickerGrey;
                case 2:
                    return R.style.TimePickerYellow;
                case 3:
                    return R.style.TimePickerOrange;
                case 4:
                    return R.style.TimePickerRed;
                case 5:
                    return R.style.TimePickerPurple;
                case 6:
                    return R.style.TimePickerBlue;
                default:
                    return R.style.TimePickerGreen;
            }
        }
    }

    public void onClickSmart(View view) {
        View panel = findViewById(R.id.note_clickable_panel);
        View smartBox = findViewById(R.id.note_smart_box);
        if (smartBox.getVisibility() != VISIBLE) {
            if (mSmartAdapter == null) {
                initializeSmartPanel();
            }
            hideSettings(false);
            panel.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTransparent));
            panel.startAnimation(AnimationUtils.loadAnimation(NoteActivity.this, R.anim.fade_in));
            panel.setVisibility(VISIBLE);
            mAnimation.slideUp(smartBox, this);
        } else {
            mAnimation.slideDown(smartBox, this);
            mAnimation.fadeOutSlow(panel, this, true);
        }
    }

    private void initializeSmartPanel() {
        mSmartAdapter = new SmartAdapter(mSmartThings, mSmartThingsPos, this, mColor.getStatusBarColor(mNote.getBgColor()));
        mSmartAdapter.setSmartLongClickListener(new SmartLongClickListener() {
            @Override
            public void onLongClickSmartChip(int item, String text, View view) {
                if (item == -1) {
                    analyzeString(mNote.getText().getText().replaceAll("\\s{2,}", " "), text, view);
                } else {
                    List<NoteChecked> list = mNote.getList();
                    if (list == null || item >= list.size() || list.get(item) == null) {
                        Toast.makeText(NoteActivity.this, R.string.not_found_element, Toast.LENGTH_SHORT).show();
                    } else {
                        analyzeString(list.get(item).getLabel().replaceAll("\\s{2,}", " "), text, view);
                    }
                }
            }
        });
        RecyclerView smartRecycler = (RecyclerView) findViewById(R.id.note_smart_container_recycler);
        smartRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        smartRecycler.setAdapter(mSmartAdapter);
    }

    private void analyzeString(String text, String find, View view) {
        if (text == null || find == null || text.isEmpty() || find.isEmpty()) {
            Toast.makeText(NoteActivity.this, R.string.not_found_element, Toast.LENGTH_SHORT).show();
        } else {
            int startPos = text.indexOf(find);
            if (startPos == -1) {
                Toast.makeText(NoteActivity.this, R.string.not_found_element, Toast.LENGTH_SHORT).show();
            } else {
                String after;
                String before;
                int beforePos = startPos - 10;
                if (beforePos < 0) {
                    beforePos = 0;
                }

                int afterPos = startPos + find.length() + 10;
                if (afterPos >= text.length()) {
                    afterPos = text.length();
                }
                before = text.substring(beforePos, startPos);
                after = text.substring(startPos + find.length(), afterPos);
                mTooltip.show(view, Html.fromHtml(before + "<b>" + find + "</b>" + after));
            }

        }
    }

    public void onClickBgColor(View view) {
        View linear = findViewById(R.id.note_background_color_box);
        if (linear.getVisibility() == View.VISIBLE) {
            findViewById(R.id.note_clickable_panel).setVisibility(GONE);
            mAnimation.slideDown(linear, this);
        } else {
            hideSettings(false);
            findViewById(R.id.note_clickable_panel).setVisibility(VISIBLE);
            mAnimation.slideUp(linear, this);
        }
    }

    public void onClickTitle(View view) {
        hideSettings(true);
    }

    public void onClickReminder(View view) {
        if (!mLongClick) {
            reminderDialog();
        } else {
            mLongClick = false;
        }
    }

    public void onClickLabel(View view) {
        if (!mLongClick) {
            final View linear = findViewById(R.id.note_label_box);
            if (linear.getVisibility() == View.VISIBLE) {
                findViewById(R.id.note_clickable_panel).setVisibility(GONE);
                mAnimation.slideDown(linear, this);
            } else {
                if (mSettings.getBoolean("LabelLock") && !mNote.getSecured() && System.currentTimeMillis() - mLastPassword > mWaiting) {
                    Dialogs.dialogCheckPassword(NoteActivity.this, new SettingsPreferences(NoteActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD), new DialogSimpleResponse() {
                        @Override
                        public void dialogAnswer(boolean ok) {
                            if (ok) {
                                setLastPassword();
                                findViewById(R.id.note_clickable_panel).setVisibility(VISIBLE);
                                hideSettings(false);
                                mAnimation.slideUp(linear, NoteActivity.this);
                            }
                        }
                    });
                } else {
                    setLastPassword();
                    findViewById(R.id.note_clickable_panel).setVisibility(VISIBLE);
                    hideSettings(false);
                    mAnimation.slideUp(linear, this);
                }
            }
        }
        mLongClick = false;
    }

    public void onClickMore(View view) {
        LinearLayout linear = (LinearLayout) findViewById(R.id.note_more_box);
        if (linear.getVisibility() == View.VISIBLE) {
            findViewById(R.id.note_clickable_panel).setVisibility(GONE);
            mAnimation.animateViews(linear.getWidth(), linear.getHeight(), linear.getHeight(), -Math.max(linear.getMeasuredWidth(), linear.getMeasuredHeight()), INVISIBLE, linear);
        } else {
            hideSettings(false);
            findViewById(R.id.note_clickable_panel).setVisibility(VISIBLE);
            mAnimation.animateViews(linear.getWidth(), linear.getHeight(), 0, 2 * Math.max(linear.getMeasuredWidth(), linear.getMeasuredHeight()), View.VISIBLE, linear);
        }
    }

    public void saveAndFinish() {
        try {
            if (mAnalyzer != null) {
                mAnalyzer.join();
            }
        } catch (InterruptedException e) {
            Log.w("Analyzer join error", e.toString());
        }
        new UpdateWidgets(this).execute(mNote.getWidgets(), mOriginalLabel + "-" + mNote.getLabel(), mNote.getID() + "");
        Intent intent = new Intent();
        intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
        intent.putExtra(RESULT_NOTE, mResult);
        intent.putExtra(RESULT_TYPE, mType);
        intent.putExtra(RESULT_LABEL, mLabelChanged);
        if (mCopyID > -1) {
            intent.putExtra(RESULT_COPY, mCopyID);
        }
        mSoundAdapter.stopRecording();
        mSoundAdapter.stopPlaying();
        Intent analyzerIntent = new Intent(this, NoteAnalyzer.class);
        analyzerIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNote.getID());
        startService(analyzerIntent);
        setResult(RESULT_OK, intent);
        supportFinishAfterTransition();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_REQUEST_CODE: {
                // If request is cancelled, the mResult arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    if (mSoundAdapter != null) {
                        mSoundAdapter.stopRecording();
                        mSoundAdapter.stopPlaying();
                    }
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), RESULT_LOAD_IMAGE);
                }
                break;
            }
            case INTERNET_REQUEST_CODE: {
                // If request is cancelled, the mResult arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ConnectivityReceiver.isConnected(NoteActivity.this)) {
                        if (mSoundAdapter != null) {
                            mSoundAdapter.stopRecording();
                            mSoundAdapter.stopPlaying();
                        }
                        Dialogs.showUrlDialog(NoteActivity.this, mNote, mDb, mAdapter);
                    } else {
                        Toast.makeText(NoteActivity.this, getString(R.string.internet_connection), Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
            case RECORD_REQUEST_CODE: {
                mSoundAdapter.addRecord();
                break;
            }

        }
    }

    private void setLastEdited(int type) {
        mNote.setLastModification(DatabaseManager.mFormat.format(new GregorianCalendar().getTime()));
        mLastEditedListener.onNoteClicked(0);
        mDb.updateNote(mNote, 0, type);
    }

    private boolean checkEmpty() {

        mNote = mDb.getNote(mNote.getID());
        if (!mNote.getTitle().replaceAll("[ \n]", "").isEmpty()) {
            return false;
        } else if (!mNote.getText().getText().replaceAll("[ \n]", "").isEmpty()) {
            return false;
        } else if (!mNote.getList().isEmpty()) {
            return false;
        } else if (!mNote.getImagesList().isEmpty()) {
            return false;
        } else if (!mNote.getRecord().isEmpty()) {
            return false;
        } else if (mNote.getReminder().startsWith("T")) {
            mNote.setTitle(getString(R.string.reminder_text));
            mNote.setTitle(getString(R.string.reminder_text));
            mDb.updateNote(mNote, 1, 0);
            return false;
        }
        return true;
    }

    private void showRecyclerGallery(int visible) {
        if (visible == View.VISIBLE) {
            findViewById(R.id.toolbar_recycler_gallery).setVisibility(VISIBLE);
            findViewById(R.id.note_toolbar_gradient).setVisibility(VISIBLE);
        } else {
            findViewById(R.id.toolbar_recycler_gallery).setVisibility(INVISIBLE);
            findViewById(R.id.note_toolbar_gradient).setVisibility(INVISIBLE);
        }
    }

    public void update() {
        showRecyclerGallery(mNote.getImagesList().size() > 0 ? VISIBLE : INVISIBLE);
        mAdapter.update(mNote.getImagesList());
    }

    public void onClickSmartButton(View view) {
        if (mSmartAdapter != null) {
            List<String> selectedData = mSmartAdapter.getSelectedList();
            if (selectedData != null && selectedData.size() > 0) {
                switch (view.getId()) {
                    case R.id.note_smart_things_calendar:
                        Intent calIntent = new Intent(Intent.ACTION_INSERT);
                        calIntent.setType("vnd.android.cursor.item/event");
                        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, getText(selectedData));
                        String[] data = getCalendarData(selectedData);
                        if (data.length == 2) {
                            if (data[0] != null && data[0].length() > 0) {
                                calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, data[0]);
                            }
                            Matcher matcher = Pattern.compile("\\d{4}").matcher(data[1]);
                            Calendar date = new GregorianCalendar();
                            if (matcher.find()) {
                                date.set(Calendar.YEAR, Integer.parseInt(matcher.group()));
                            }
                            matcher = Pattern.compile("[a-zA-Z]{3,12}").matcher(data[1]);
                            if (matcher.find()) {
                                date.set(Calendar.MONTH, getMonth(matcher.group()));
                                matcher = Pattern.compile("[0-9]{1,2}").matcher(data[1]);
                                if (matcher.find()) {
                                    date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(matcher.group()));
                                }
                            } else {
                                matcher = Pattern.compile("[0-9]{1,2}").matcher(data[1]);
                                int day = -1;
                                int month = -1;
                                while (matcher.find()) {
                                    if (day == -1) {
                                        day = Integer.parseInt(matcher.group());
                                    } else {
                                        month = Integer.parseInt(matcher.group());
                                    }
                                }
                                if (day != -1 && month != -1) {
                                    int smaller = day < month ? day : month;
                                    if (smaller < 12) {
                                        date.set(Calendar.MONTH, smaller - 1);
                                        date.set(Calendar.DAY_OF_MONTH, smaller == day ? month : day);
                                    }
                                }
                            }
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, date.getTimeInMillis());

                        } else if (mNote.getReminder().startsWith("T")) {
                            try {
                                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(mNote.getReminder().substring(1))).getTimeInMillis());
                            } catch (Exception e) {
                                Log.e("Date format", e.toString());
                            }
                        }
                        startActivities(new Intent[]{calIntent});
                        break;
                    case R.id.note_smart_things_contact:
                        Intent contact = getContact(selectedData);
                        if (contact != null) {
                            startActivity(contact);
                        } else {
                            Toast.makeText(this, R.string.select_contact, Toast.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.note_smart_things_copy:
                        ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)).setPrimaryClip(ClipData.newPlainText("Remember app", getText(selectedData)));
                        break;
                    case R.id.note_smart_things_share:
                        String shareText = getText(selectedData);
                        ContextCompat.startActivities(this, new Intent[]{Intent.createChooser(
                                new Intent()
                                        .setAction(Intent.ACTION_SEND)
                                        .setType("text/plain")
                                        .putExtra(Intent.EXTRA_TEXT, shareText + "\n\nShared by Remember"),
                                "Share " + (selectedData.size() > 1 ? " data" : shareText.split(":")[0]))});
                        break;
                }
            } else {
                Toast.makeText(this, R.string.select_data, Toast.LENGTH_LONG).show();
            }
        }
    }

    private Intent getContact(List<String> data) {
        Intent contact = new Intent(ContactsContract.Intents.Insert.ACTION);
        contact.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        int phone = 0;
        int email = 0;
        for (String s : data) {
            String[] dates = s.split("\\.\\.");
            int id = Integer.parseInt(dates[0]);
            if (id == 0) {
                for (int i = 1; i < dates.length; ++i) {
                    phone++;
                    switch (phone) {
                        case 1:
                            contact.putExtra(ContactsContract.Intents.Insert.PHONE, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME);
                            break;
                        case 2:
                            contact.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
                            break;
                        case 3:
                            contact.putExtra(ContactsContract.Intents.Insert.TERTIARY_PHONE, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.TERTIARY_PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
                            break;
                    }
                }
            } else if (id == 1) {
                for (int i = 1; i < dates.length; ++i) {
                    email++;
                    switch (email) {
                        case 1:
                            contact.putExtra(ContactsContract.Intents.Insert.EMAIL, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_HOME);
                            break;
                        case 2:
                            contact.putExtra(ContactsContract.Intents.Insert.SECONDARY_EMAIL, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.SECONDARY_EMAIL, ContactsContract.CommonDataKinds.Email.TYPE_MOBILE);
                            break;
                        case 3:
                            contact.putExtra(ContactsContract.Intents.Insert.TERTIARY_EMAIL, dates[i]);
                            contact.putExtra(ContactsContract.Intents.Insert.TERTIARY_EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);
                            break;
                    }
                }
            }
        }
        if (phone > 0 || email > 0) {
            return contact;
        }
        return null;
    }

    private String[] getCalendarData(List<String> data) {
        String[] calendarData = new String[2];
        for (String s : data) {
            if (!s.isEmpty()) {
                String[] dates = s.split("\\.\\.");
                int id = Integer.parseInt(dates[0]);
                if (id == 4) {
                    if (dates.length == 2) {
                        calendarData[0] = dates[1];
                    }
                } else if (id == 5) {
                    if (dates.length == 2) {
                        calendarData[1] = dates[1];
                    } else {
                        return new String[3];
                    }
                }
            }
        }
        return calendarData;
    }

    public static int getMonth(String month) {
        return month.startsWith("ja") ? 0 :
                month.startsWith("f") ? 1 :
                        month.startsWith("mar") ? 2 :
                                month.startsWith("ap") ? 3 :
                                        month.startsWith("may") ? 4 :
                                                month.startsWith("jun") ? 5 :
                                                        month.startsWith("jul") ? 6 :
                                                                month.startsWith("au") ? 7 :
                                                                        month.startsWith("s") ? 8 :
                                                                                month.startsWith("o") ? 9 :
                                                                                        month.startsWith("n") ? 10 : 11;


    }

    private String getText(List<String> data) {
        String copyText = "";
        for (String s : data) {
            String[] dates = s.split("\\.\\.");
            copyText += getCategory(Integer.parseInt(dates[0]), dates.length - 2);
            for (int i = 1; i < dates.length; ++i) {
                copyText += dates[i] + (i < dates.length - 1 ? ", " : "\n");
            }
        }
        return copyText;
    }

    private String getCategory(int id, int length) {
        switch (id) {
            case 0:
                return "Phone number" + (length > 0 ? "s" : "") + ": ";
            case 1:
                return "Email address" + (length > 0 ? "es" : "") + ": ";
            case 2:
                return "Url" + (length > 0 ? "s" : "") + ": ";
            case 3:
                return "Credit card" + (length > 0 ? "s" : "") + ": ";
            case 4:
                return "Location" + (length > 0 ? "s" : "") + ": ";
            case 5:
                return "Date" + (length > 0 ? "s" : "") + ": ";
        }
        return "";
    }
}