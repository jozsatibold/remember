package com.thinkit.jozsa.remember.view.receiver;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.controller.filemanager.FileManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.analyzer.NoteAnalyzer;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class IntentReceiverActivity extends AppCompatActivity {
    private Intent mIntent;
    boolean mLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_receiver);
        SettingsPreferences settings = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        mLocked = settings.getBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION);
        mIntent = getIntent();
        if (mLocked) {
            findViewById(R.id.receiver_progress_bar).setVisibility(View.GONE);
            View lock = findViewById(R.id.password_view);
            lock.setVisibility(View.VISIBLE);
            final TextInputEditText mPassword = (TextInputEditText) findViewById(R.id.splash_edit_text_password);
            lock.setBackgroundColor(settings.getInt("ToolbarColor"));
            mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        try {
                            SettingsPreferences passwordSettings = new SettingsPreferences(IntentReceiverActivity.this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
                            if (!passwordSettings.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).equals(DataEncrypt.getMd5(mPassword.getText().toString()))) {
                                ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError(getString(R.string.error_password, passwordSettings.getString("Reminder")));
                            } else {
                                ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError("");
                                View viewpass = findViewById(R.id.password_view);
                                new Animations().slideUpInvisible(viewpass, IntentReceiverActivity.this);

                                InputMethodManager imm = (InputMethodManager) IntentReceiverActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                findViewById(R.id.receiver_progress_bar).setVisibility(View.VISIBLE);
                                save();
                            }

                        } catch (Exception e) {
                            Log.w("Settings login", e.toString());
                        }
                        handled = true;
                    }
                    return handled;
                }
            });
            if (lock.getVisibility() != View.VISIBLE) {
                lock.setVisibility(View.VISIBLE);
                mPassword.setText("");
            }


        } else {
            save();
        }

    }
    @Override
    public void onBackPressed(){
        if(mLocked){
            finish();
        }
    }
    public void save() {
        DatabaseManager db = DatabaseManager.getInstance(this);
        Note note = new Note();
        note.setID(db.insertNewNote(note));
        String action = mIntent.getAction();
        String type = mIntent.getType();
        String title = mIntent.getStringExtra(Intent.EXTRA_TITLE);
        if (title != null) {
            note.setTitle(title);
            db.updateNote(note, 1, 0);
        }
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String notetext = mIntent.getStringExtra(Intent.EXTRA_TEXT);
                if (notetext != null) {
                    String[] text = notetext.split("\n");
                    String nnote = "";
                    ArrayList<NoteChecked> nlist = new ArrayList<>();
                    for (String s : text) {
                        if (s.startsWith("[ ]")) {
                            nlist.add(new NoteChecked(s.substring(3), false));
                        } else if (s.startsWith("[x]")) {
                            nlist.add(new NoteChecked(s.substring(3), true));
                        } else if (s.startsWith("Title:") || s.startsWith("title:")) {
                            note.setTitle(s.substring(6));
                        } else {
                            nnote += s + "\n";
                        }
                    }
                    note.setList(nlist);
                    note.setText(nnote);
                    if (!nnote.isEmpty()) {
                        note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[0]);
                    }
                    if (!nlist.isEmpty()) {
                        note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[1]);
                    }
                    db.updateNote(note, 0, 0);
                    db.updateNote(note, 2, 1);
                }
            } else if (type.startsWith("image/")) {
                Uri imageUri = mIntent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (imageUri != null) {
                    try {
                        String path = FileManager.getPath(this, imageUri);
                        if (path != null) {
                            FileManager file = new FileManager(this);
                            file.copyFile(new File(path), file.createImageFile());
                            note.getImagesList().add(new Picture(file.getPhotoPath(), 0));
                            note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[2]);
                            db.updateNote(note, 0, 2);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (type.startsWith("audio/")) {
                Uri imageUri = mIntent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (imageUri != null) {
                    try {
                        String path = FileManager.getPath(this, imageUri);
                        if (path != null) {
                            FileManager file = new FileManager(this);
                            file.copyFile(new File(path), file.createImageFile());
                            ArrayList<String> record = new ArrayList<>();
                            record.add(file.getPhotoPath());
                            note.setRecord(record);
                            note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[4]);
                            db.updateNote(note, 0, 3);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                ArrayList<Uri> imageUris = mIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                if (imageUris != null) {
                    FileManager file = new FileManager(this);
                    for (Uri uri : imageUris) {
                        if (uri != null) {
                            try {
                                String path = FileManager.getPath(this, uri);
                                if (path != null) {
                                    file.copyFile(new File(path), file.createImageFile());
                                    note.getImagesList().add(new Picture(file.getPhotoPath(), 0));
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    note.setCategory(note.getCategory() + NoteAnalyzer.mCategorys[2]);
                    db.updateNote(note, 0, 2);
                }
            }
        }
        Intent analyzerIntent = new Intent(this, NoteAnalyzer.class);
        analyzerIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, note.getID());
        startService(analyzerIntent);
        this.finishAffinity();
        Intent resultIntent = new Intent(this, NoteActivity.class);
        resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, note.getID());
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(NoteActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        startActivities(stackBuilder.getIntents());
        overridePendingTransition(R.anim.mainfadin, R.anim.splashfadeout);
    }
}