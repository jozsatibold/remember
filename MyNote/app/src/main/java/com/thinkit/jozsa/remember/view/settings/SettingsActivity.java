package com.thinkit.jozsa.remember.view.settings;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.appearance.DialogSimpleResponse;
import com.thinkit.jozsa.remember.view.appearance.Dialogs;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.controller.filemanager.ExportData;
import com.thinkit.jozsa.remember.controller.filemanager.ImportData;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.paint.ColorRecyclerAdapter;

import java.io.File;

import static android.view.View.GONE;


public class SettingsActivity extends AppCompatActivity {
    public static String SETTINGS_CHANGED = "changed";
    public static final String PREFERENCE_FILENAME_PASSWORD = "Password";
    public static final int WRITE_REQUEST_CODE = 1234;
    public static final int READ_REQUEST_CODE = 2345;
    public static final int FILE_MANAGER_REQUEST_CODE = 3456;

    private SettingsPreferences mSettings;
    private SettingsPreferences mPassword;
    private View mReveal;
    private View mRevealBackground;
    private int mStatusBarColor;
    private int mBackgroundColor;
    private int mToolbarColor;
    private int mWaiting = 0;
    private long mLastPassword = System.currentTimeMillis() - 5000;
    private ImageView mPrimary;
    private ImageView mSecondary;
    private boolean mChanged;
    private boolean mLocked;
    private boolean mSwitchActions[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.settings_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSwitchActions = new boolean[8];
        mSettings = new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        mPassword = new SettingsPreferences(this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
        mReveal = findViewById(R.id.reveal);
        mRevealBackground = findViewById(R.id.revealBackground);
        mPrimary = (ImageView) findViewById(R.id.settings_primary_color_view);
        mSecondary = (ImageView) findViewById(R.id.settings_secondary_color_view);


        final Switch column = (Switch) findViewById(R.id.settings_column_switch);
        final Switch settingsLock = (Switch) findViewById(R.id.settings_settings_switch);
        final Switch archiveLock = (Switch) findViewById(R.id.settings_archive_switch);
        final Switch labelLock = (Switch) findViewById(R.id.settings_label_switch);
        final Switch actionLock = (Switch) findViewById(R.id.settings_action_switch);
        final Switch deleteLock = (Switch) findViewById(R.id.settings_action_delete_switch);
        final Switch waitingLock = (Switch) findViewById(R.id.settings_waiting_switch);
        final Switch applicationLock = (Switch) findViewById(R.id.settings_application_switch);
        final Switch removeLock = (Switch) findViewById(R.id.settings_note_deleting_switch);

        mStatusBarColor = mSettings.getInt(LoadingActivity.PREFERENCE_STATUS_BAR);
        mBackgroundColor = mSettings.getInt(LoadingActivity.PREFERENCE_BACKGROUND);
        mToolbarColor = mSettings.getInt(LoadingActivity.PREFERENCE_TOOLBAR);

        int headerColor = new Colors(this).getHeaderColor(mStatusBarColor);
        ((TextViewTypeface) findViewById(R.id.settings_header_appearance)).setTextColor(headerColor);
        ((TextViewTypeface) findViewById(R.id.settings_header_security)).setTextColor(headerColor);
        ((TextViewTypeface) findViewById(R.id.settings_header_backup)).setTextColor(headerColor);

        mLocked = mSettings.getBoolean("SettingsLock");
        mWaiting = mSettings.getInt("WaitingLock");
        archiveLock.setChecked(mSettings.getBoolean("ArchiveLock"));
        labelLock.setChecked(mSettings.getBoolean("LabelLock"));
        actionLock.setChecked(mSettings.getBoolean("ActionLock"));
        deleteLock.setChecked(mSettings.getBoolean("DeleteLock"));
        removeLock.setChecked(mSettings.getBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE));
        waitingLock.setChecked(mWaiting > 0);
        applicationLock.setChecked(mSettings.getBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION));
        if (actionLock.isChecked()) {
            deleteLock.setEnabled(false);
            findViewById(R.id.settings_action_delete).setEnabled(false);
        }
        settingsLock.setChecked(mLocked);

        if (mLocked) {
            findViewById(R.id.password_view).setVisibility(View.VISIBLE);
            final TextInputEditText password = (TextInputEditText) findViewById(R.id.splash_edit_text_password);
            password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        try {

                            if (!mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).equals(DataEncrypt.getMd5(password.getText().toString()))) {
                                ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError("Password don't match\n" +
                                        "Reminder: " + mPassword.getString("Reminder"));
                            } else {
                                ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError("");
                                View view = findViewById(R.id.password_view);
                                new Animations().slideUpInvisible(view, SettingsActivity.this);//animateViews(view.getWidth() / 2, 0, view.getWidth() / 2, 0, GONE, view);
                                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }

                        } catch (Exception e) {
                            Log.w("Settings login", e.toString());
                        }
                        handled = true;
                    }
                    return handled;
                }
            });

        }

        try {
            ((TextViewTypeface) findViewById(R.id.version_number)).setText(getString(R.string.settings_settings_version, getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("Version error", e.toString());
        }
        column.setChecked(mSettings.getBoolean(LoadingActivity.PREFERENCE_COLUMNS));
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(mStatusBarColor);
        mPrimary.setColorFilter(mToolbarColor);
        mReveal.setBackgroundColor(mToolbarColor);
        findViewById(R.id.password_view).setBackgroundColor(mToolbarColor);
        mRevealBackground.setBackgroundColor(mToolbarColor);

        mSecondary.setColorFilter(mBackgroundColor);
        findViewById(R.id.setting_layout).setBackgroundColor(mBackgroundColor);

        column.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mSettings.setBoolean(LoadingActivity.PREFERENCE_COLUMNS, b);
                mChanged = true;
            }
        });
        findViewById(R.id.settings_columns).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                column.setChecked(!column.isChecked());

            }
        });
        if (mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD) == null || mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).isEmpty()) {
            settingsLock.setEnabled(false);
            archiveLock.setEnabled(false);
            labelLock.setEnabled(false);
            actionLock.setEnabled(false);
            deleteLock.setEnabled(false);
            waitingLock.setEnabled(false);
            applicationLock.setEnabled(false);
            removeLock.setEnabled(false);
        }
        settingsLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[1]) {
                    mSwitchActions[1] = false;
                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            settingsLock.setChecked(!b);
                            if (!mSwitchActions[1]) {
                                mSwitchActions[1] = true;
                            }
                            checkPasswordSwitch(settingsLock, b, 1);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean("SettingsLock", b);
                            mLocked = b;
                        }
                    }
                }
            }
        });
        applicationLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[6]) {
                    mSwitchActions[6] = false;
                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            applicationLock.setChecked(!b);
                            if (!mSwitchActions[6]) {
                                mSwitchActions[6] = true;
                            }
                            checkPasswordSwitch(applicationLock, b, 6);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION, b);
                        }
                    }
                }
            }
        });
        labelLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[2]) {
                    mSwitchActions[2] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            labelLock.setChecked(!b);
                            if (!mSwitchActions[2]) {
                                mSwitchActions[2] = true;
                            }
                            checkPasswordSwitch(labelLock, b, 2);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean("LabelLock", b);
                        }
                    }
                }
            }
        });
        actionLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[3]) {
                    mSwitchActions[3] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            actionLock.setChecked(!b);
                            if (!mSwitchActions[3]) {
                                mSwitchActions[3] = true;
                            }
                            checkPasswordSwitch(actionLock, b, 3);
                        } else {
                            mLastPassword = System.currentTimeMillis();
                            deleteLock.setEnabled(!b);
                            findViewById(R.id.settings_action_delete).setEnabled(!b);
                            mSettings.setBoolean("ActionLock", b);
                        }
                    }
                }
            }
        });
        deleteLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[4]) {
                    mSwitchActions[4] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            deleteLock.setChecked(!b);
                            if (!mSwitchActions[4]) {
                                mSwitchActions[4] = true;
                            }
                            checkPasswordSwitch(deleteLock, b, 4);
                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean("DeleteLock", b);
                        }
                    }
                }
            }
        });
        archiveLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[0]) {
                    mSwitchActions[0] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            archiveLock.setChecked(!b);
                            if (!mSwitchActions[0]) {
                                mSwitchActions[0] = true;
                            }
                            checkPasswordSwitch(archiveLock, b, 0);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean("ArchiveLock", b);
                            mChanged = true;
                        }
                    }
                }
            }
        });

        removeLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[7]) {
                    mSwitchActions[7] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            removeLock.setChecked(!b);
                            if (!mSwitchActions[7]) {
                                mSwitchActions[7] = true;
                            }
                            checkPasswordSwitch(removeLock, b, 0);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mSettings.setBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE, b);
                        }
                    }
                }
            }
        });
        waitingLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (mSwitchActions[5]) {
                    mSwitchActions[5] = false;

                } else {
                    if (isPassword()) {
                        if (!mLocked && System.currentTimeMillis() - mLastPassword > mWaiting) {
                            waitingLock.setChecked(!b);
                            if (!mSwitchActions[5]) {
                                mSwitchActions[5] = true;
                            }
                            checkPasswordSwitch(waitingLock, b, 5);

                        } else {
                            mLastPassword = System.currentTimeMillis();
                            mWaiting = b ? 5000 : 0;
                            mSettings.setInt(LoadingActivity.PREFERENCE_SECURE_WAITING, mWaiting);
                        }
                    }
                }
            }
        });
        findViewById(R.id.settings_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPassword()) {
                    settingsLock.setChecked(!settingsLock.isChecked());
                }

            }
        });
        findViewById(R.id.settings_label).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPassword()) {
                    labelLock.setChecked(!labelLock.isChecked());
                }

            }
        });
        findViewById(R.id.settings_archive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPassword()) {
                    archiveLock.setChecked(!archiveLock.isChecked());
                }
            }
        });
        findViewById(R.id.settings_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPassword()) {
                    actionLock.setChecked(!actionLock.isChecked());
                    findViewById(R.id.settings_action_delete).setEnabled(!actionLock.isChecked());
                }
            }
        });
        findViewById(R.id.settings_action_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPassword()) {
                    deleteLock.setChecked(!deleteLock.isChecked());
                }
            }
        });
        findViewById(R.id.settings_waiting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPassword()) {
                    waitingLock.setChecked(!waitingLock.isChecked());
                }
            }
        });
        findViewById(R.id.settings_application).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPassword()) {
                    applicationLock.setChecked(!applicationLock.isChecked());
                }
            }
        });
        findViewById(R.id.settings_note_deleting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPassword()) {
                    removeLock.setChecked(!removeLock.isChecked());
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == FILE_MANAGER_REQUEST_CODE) {
            String path = data.getData().toString();
            if (!path.endsWith(".json")) {
                Toast.makeText(SettingsActivity.this, "The only supported extension is '.json'!", Toast.LENGTH_LONG).show();
            } else {
                (findViewById(R.id.loading_view)).setVisibility(View.VISIBLE);
                final ImportData importDb = new ImportData(this, new File(path.replace("file://", "")));
                if (importDb.isLocked()) {

                    final Dialog dialogCheckPasswd = new Dialog(SettingsActivity.this);
                    dialogCheckPasswd.setCanceledOnTouchOutside(false);
                    dialogCheckPasswd.setContentView(R.layout.dialog_import_password);
                    dialogCheckPasswd.findViewById(R.id.dialog_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {


                                if (!importDb.isOk(((TextInputEditText) dialogCheckPasswd.findViewById(R.id.dialog_password)).getText().toString())) {
                                    ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("Password don't match!\n");
                                } else {
                                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                    ((TextInputLayout) dialogCheckPasswd.findViewById(R.id.password_text_input_layout)).setError("");
                                    importDb.execute();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            while (!importDb.isFinshed() && !importDb.getError()) ;

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    (findViewById(R.id.loading_view)).setVisibility(GONE);
                                                    if (importDb.isFinshed()) {
                                                        Toast.makeText(SettingsActivity.this, R.string.settings_backup_loaded, Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(SettingsActivity.this, R.string.settings_error_file_load, Toast.LENGTH_LONG).show();
                                                    }

                                                }
                                            });

                                        }
                                    }).start();
                                    dialogCheckPasswd.dismiss();
                                }
                            } catch (Exception e) {
                                Log.w("Lock login import", e.toString());
                            }
                        }
                    });

                    dialogCheckPasswd.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogCheckPasswd.dismiss();
                        }
                    });
                    dialogCheckPasswd.show();

                } else {

                    importDb.execute();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (!importDb.isFinshed() && !importDb.getError()) ;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SettingsActivity.this, importDb.isFinshed() ? R.string.settings_backup_loaded : R.string.settings_error_file_load, Toast.LENGTH_LONG).show();
                                    (findViewById(R.id.loading_view)).setVisibility(View.GONE);
                                }
                            });

                        }
                    }).start();
                }
            }
        }
    }

    private boolean isPassword() {
        if (mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD) == null || mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).isEmpty()) {

            Toast.makeText(this, R.string.settings_set_password, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void onClickImport(View v) {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            READ_REQUEST_CODE);
                }
            } else {
                importData();
            }
        } else {
            importData();
        }

    }

    public void onClickExport(View v) {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            WRITE_REQUEST_CODE);
                }
            } else {
                exportData();
            }
        } else {
            exportData();
        }
    }

    private void exportData() {
        if (!mLocked) {
            checkPasswordData(true);
        } else {
            exportDataNext();
        }
    }

    private void exportDataNext() {
        final Dialog dialogEncryptKey = new Dialog(SettingsActivity.this);
        dialogEncryptKey.setCanceledOnTouchOutside(false);
        dialogEncryptKey.setContentView(R.layout.dialog_export_password);
        dialogEncryptKey.findViewById(R.id.dialog_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ExportData export = new ExportData(SettingsActivity.this, ((TextInputEditText) dialogEncryptKey.findViewById(R.id.dialog_password)).getText().toString());
                findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
                export.execute();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (export.getState() < export.getAll() && !export.getError()) ;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.loading_view).setVisibility(GONE);
                                Toast.makeText(SettingsActivity.this, export.getError() ? getString(R.string.settings_error_file_save) : getString(R.string.settings_file_saved, Environment.getExternalStorageDirectory().getPath()), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }).start();
                dialogEncryptKey.dismiss();
            }
        });

        dialogEncryptKey.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEncryptKey.dismiss();
            }
        });
        dialogEncryptKey.show();
    }

    private void importData() {
        if (!mLocked) {
            checkPasswordData(false);
        } else {
            importDataNext();
        }
    }

    private void importDataNext() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, FILE_MANAGER_REQUEST_CODE);
    }

    public void onClickPassword(View v) {

        final Dialog dialogPassword = new Dialog(this);
        dialogPassword.setContentView(R.layout.dialog_password);
        final String current = mPassword.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD);


        dialogPassword.findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPassword.dismiss();
            }
        });

        final TextInputEditText currentp = (TextInputEditText) dialogPassword.findViewById(R.id.dialog_current_password);
        final TextInputEditText newp = (TextInputEditText) dialogPassword.findViewById(R.id.dialog_new_password);
        final TextInputEditText newr = (TextInputEditText) dialogPassword.findViewById(R.id.dialog_reminder);
        final Switch removep = (Switch) dialogPassword.findViewById(R.id.settings_remove_password_switch);
        if (current == null || current.isEmpty()) {
            dialogPassword.findViewById(R.id.label_current_text_input_layout).setVisibility(GONE);
            dialogPassword.findViewById(R.id.settings_remove).setVisibility(GONE);
        }
        removep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                newp.setEnabled(!b);
                newr.setEnabled(!b);
            }
        });
        dialogPassword.findViewById(R.id.dialog_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ok1 = false, ok2, ok3;
                if (current != null && !current.isEmpty()) {
                    String curr = currentp.getText().toString();
                    if (curr.isEmpty()) {
                        ok1 = false;
                        ((TextInputLayout) dialogPassword.findViewById(R.id.label_current_text_input_layout)).setError(getString(R.string.settings_error_current_password));
                    } else {
                        try {
                            if (!current.equals(DataEncrypt.getMd5(curr))) {
                                ((TextInputLayout) dialogPassword.findViewById(R.id.label_current_text_input_layout)).setError(getString(R.string.settings_password_reminder, mPassword.getString("Reminder")));
                                ok1 = false;
                            } else {
                                currentp.setError("");
                                ok1 = true;
                            }
                        } catch (Exception e) {
                            Log.w("Current password", e.toString());
                        }
                    }
                } else {
                    ok1 = true;
                }
                boolean remove = removep.isChecked();
                if (!remove) {
                    String newpas = newp.getText().toString();
                    if (newpas.isEmpty()) {
                        ok2 = false;
                        ((TextInputLayout) dialogPassword.findViewById(R.id.label_new_text_input_layout)).setError(getString(R.string.settings_error_new_password));
                    } else if (newpas.length() < 8) {
                        ((TextInputLayout) dialogPassword.findViewById(R.id.label_new_text_input_layout)).setError(getString(R.string.settings_least_character_long));
                        ok2 = false;
                    } else {
                        ok2 = true;
                    }

                    String re = newr.getText().toString();
                    if (re.isEmpty()) {
                        ok3 = false;
                        ((TextInputLayout) dialogPassword.findViewById(R.id.label_reminder_text_input_layout)).setError(getString(R.string.settings_error_reminder));
                    } else if (newpas.length() < 5) {
                        ((TextInputLayout) dialogPassword.findViewById(R.id.label_reminder_text_input_layout)).setError(getString(R.string.settings_least_character));
                        ok3 = false;
                    } else {
                        ok3 = true;
                    }
                    if (ok1 == ok2 == ok3) {
                        try {
                            findViewById(R.id.settings_settings_switch).setEnabled(true);
                            findViewById(R.id.settings_archive_switch).setEnabled(true);
                            findViewById(R.id.settings_label_switch).setEnabled(true);
                            findViewById(R.id.settings_archive_switch).setEnabled(true);
                            findViewById(R.id.settings_settings_switch).setEnabled(true);
                            findViewById(R.id.settings_label_switch).setEnabled(true);
                            findViewById(R.id.settings_note_deleting_switch).setEnabled(true);
                            mPassword.setString("Password", DataEncrypt.getMd5(newpas));
                            mPassword.setString("Reminder", re);
                        } catch (Exception e) {
                            Log.w("New password", e.toString());
                        }

                        dialogPassword.dismiss();
                    }
                } else if (ok1) {
                    ((Switch) findViewById(R.id.settings_archive_switch)).setChecked(false);
                    ((Switch) findViewById(R.id.settings_settings_switch)).setChecked(false);
                    ((Switch) findViewById(R.id.settings_label_switch)).setChecked(false);
                    findViewById(R.id.settings_archive_switch).setEnabled(false);
                    findViewById(R.id.settings_settings_switch).setEnabled(false);
                    findViewById(R.id.settings_label_switch).setEnabled(false);
                    findViewById(R.id.settings_note_deleting_switch).setEnabled(false);
                    mLocked = false;
                    mSettings.setBoolean("SettingsLock", false);
                    mSettings.setBoolean("ArchiveLock", false);
                    mSettings.setBoolean("LabelLock", false);
                    mPassword.setString("Password", "");
                    mPassword.setString("Reminder", "");
                    mPassword.setBoolean("ActionLock", false);
                    mPassword.setBoolean("DeleteLock", false);
                    mPassword.setInt(LoadingActivity.PREFERENCE_SECURE_WAITING, 5000);
                    mPassword.setBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION, false);
                    mPassword.setBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE, false);
                    dialogPassword.dismiss();
                }
            }
        });
        dialogPassword.show();
    }

    public void onClickPrimaryColor(View v) {
        dialogColor(true);
    }

    public void onClickSecondaryColor(View v) {
        dialogColor(false);
    }

    private void dialogColor(final boolean primary) {
        final Dialog dialogColor = new Dialog(this);
        dialogColor.setContentView(R.layout.dialog_color);
        final Colors color = new Colors(this);
        final RecyclerView colors = (RecyclerView) dialogColor.findViewById(R.id.dialog_color_filter);
        colors.setLayoutManager(new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false));
        final ColorRecyclerAdapter colorAdapter = new ColorRecyclerAdapter(this, color.getColorList(primary ? 4 : 2));
        colors.setAdapter(colorAdapter);
        colorAdapter.setSelectedColor(primary ? mSettings.getInt(LoadingActivity.PREFERENCE_TOOLBAR) : mSettings.getInt(LoadingActivity.PREFERENCE_BACKGROUND));
        colorAdapter.setOnClickListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int position) {
                if (primary) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(color.getStatusBarColor(position));
                    mPrimary.setColorFilter(color.getToolbarColor(position));
                    new Animations().animateAppAndStatusBar(mReveal, mRevealBackground, color.getToolbarColor(mStatusBarColor), color.getToolbarColor(position));
                    mStatusBarColor = color.getStatusBarColor(position);
                    mToolbarColor = color.getToolbarColor(position);
                    mSettings.setInt(LoadingActivity.PREFERENCE_STATUS_BAR,mStatusBarColor);
                    mSettings.setInt(LoadingActivity.PREFERENCE_TOOLBAR, mToolbarColor);
                    int headerColor = color.getHeaderColor(mStatusBarColor);
                    ((TextViewTypeface) findViewById(R.id.settings_header_appearance)).setTextColor(headerColor);
                    ((TextViewTypeface) findViewById(R.id.settings_header_security)).setTextColor(headerColor);
                    ((TextViewTypeface) findViewById(R.id.settings_header_backup)).setTextColor(headerColor);
                } else {
                    mSettings.setInt(LoadingActivity.PREFERENCE_BACKGROUND, color.getBackgroundColor(position));
                    mSecondary.setColorFilter(mSettings.getInt(LoadingActivity.PREFERENCE_BACKGROUND));
                    new Animations().animateViewColor(color.getBackgroundColor(mBackgroundColor), color.getBackgroundColor(position), findViewById(R.id.setting_layout));
                    mBackgroundColor = mSettings.getInt(LoadingActivity.PREFERENCE_BACKGROUND);
                }
                colorAdapter.setSelected(position);
                mChanged = true;
                dialogColor.dismiss();
            }
        });
        dialogColor.show();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(SETTINGS_CHANGED, mChanged);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra(SETTINGS_CHANGED, mChanged);
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_REQUEST_CODE: {
                exportData();
                break;
            }
            case READ_REQUEST_CODE: {
                importData();
                break;
            }

        }
    }

    public void checkPasswordData(final boolean export) {

        Dialogs.dialogCheckPassword(this, mPassword, new DialogSimpleResponse() {
            @Override
            public void dialogAnswer(boolean ok) {
                if (ok) {
                    if (export) {
                        exportDataNext();
                    } else {
                        importDataNext();
                    }
                }
            }
        });
    }

    public void checkPasswordSwitch(final Switch swtch, final boolean b, final int number) {
        Dialogs.dialogCheckPassword(this, mPassword, new DialogSimpleResponse() {
            @Override
            public void dialogAnswer(boolean ok) {
                if (ok) {
                    mLastPassword = System.currentTimeMillis();
                    swtch.setChecked(b);
                    switch (number) {
                        case 0:
                            mChanged = true;
                            mSettings.setBoolean("ArchiveLock", b);
                            break;
                        case 1:
                            mLocked = b;
                            mSettings.setBoolean("SettingsLock", b);
                            break;
                        case 2:
                            mSettings.setBoolean("LabelLock", b);
                            break;
                        case 3:
                            mSettings.setBoolean("ActionLock", b);
                            findViewById(R.id.settings_action_delete_switch).setEnabled(!b);
                            findViewById(R.id.settings_action_delete).setEnabled(!b);
                            break;
                        case 4:
                            mSettings.setBoolean("DeleteLock", b);
                            break;
                        case 5:
                            mWaiting = b ? 5000 : 0;
                            mSettings.setInt(LoadingActivity.PREFERENCE_SECURE_WAITING, mWaiting);
                            break;
                        case 6:
                            mSettings.setBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION, b);
                            break;
                        case 7:
                            mSettings.setBoolean(LoadingActivity.PREFERENCE_SECURE_AUTO_REMOVE, b);
                            break;
                    }
                } else if (mSwitchActions[number]) {
                    mSwitchActions[number] = false;
                }

            }
        });
    }
}
