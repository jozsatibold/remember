package com.thinkit.jozsa.remember.view;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

public class SplashActivity extends Activity {

    public static final String NOTE_OPEN_SECURED ="SPLASH_NOTE_OPEN_SECURED_912";

    private boolean mWhich;
    private boolean mLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mWhich = false;
        SettingsPreferences settings = new SettingsPreferences(SplashActivity.this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        int toolColor = settings.getInt(LoadingActivity.PREFERENCE_STATUS_BAR);
        mLock = settings.getBoolean(LoadingActivity.PREFERENCE_SECURE_APPLICATION);
        setContentView(!mLock ? R.layout.activity_splash : R.layout.activity_splash_password);
        if (toolColor == -1) {
            mWhich = true;
            toolColor = ContextCompat.getColor(this, R.color.status_bar_blue);
        } else {
            findViewById(R.id.splash_background).setBackgroundColor(toolColor);
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(toolColor);
        final int id = getIntent().getIntExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, -1);
        if (!mLock) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, mWhich ? LoadingActivity.class : MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    overridePendingTransition(R.anim.mainfadin, R.anim.splashfadeout);
                }
            }, 200);
        } else {
            SettingsPreferences passwoPreferences = new SettingsPreferences(this, SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
            final String passwordtxt = passwoPreferences.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
            final String remindertxt = passwoPreferences.getString("Reminder");
            final TextInputEditText password = (TextInputEditText) findViewById(R.id.splash_edit_text_password);
            password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        try {
                            if (!passwordtxt.equals(DataEncrypt.getMd5(password.getText().toString()))) {
                                ((TextInputLayout) findViewById(R.id.splash_text_input_layout_password)).setError(getString(R.string.settings_password_reminder, remindertxt));
                            } else {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                if (id != -1) {
                                    startNoteActivity(id);
                                } else {
                                    startApplication();
                                }
                                SplashActivity.this.finish();
                                overridePendingTransition(R.anim.mainfadin, R.anim.splashfadeout);
                            }

                        } catch (Exception e) {
                            Log.w("Dialog Password login", e.toString());
                        }
                        handled = true;
                    }
                    return handled;
                }
            });
        }
    }

    @Override
    public void onBackPressed(){
        if(mLock){
            finish();
        }
    }

    public void startApplication() {
        startActivity(new Intent(SplashActivity.this, mWhich ? LoadingActivity.class : MainActivity.class));
    }

    public void startNoteActivity(int id) {
        Intent resultIntent = new Intent(this, NoteActivity.class);
        resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, id != -2 ? id : DatabaseManager.getInstance(this).insertNewNote(new Note()));
        resultIntent.putExtra(NOTE_OPEN_SECURED, true);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(NoteActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        startActivities(stackBuilder.getIntents());
    }

}
