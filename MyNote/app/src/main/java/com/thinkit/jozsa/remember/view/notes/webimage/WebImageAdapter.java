package com.thinkit.jozsa.remember.view.notes.webimage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;

import java.util.ArrayList;


public class WebImageAdapter extends RecyclerView.Adapter<WebImageAdapter.ViewHolder> {

    private ArrayList<String> mPictures;
    private ArrayList<Boolean> mChecked;
    private Context mContext;
    public WebImageAdapter(ArrayList<String> pictures, Context context) {
        mContext = context;
        mPictures = new ArrayList<>(pictures);
        mChecked = new ArrayList<>();
        for(int i = 0; i < mPictures.size(); ++i){
            mChecked.add(false);
        }
    }

    @Override
    public WebImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_pictures, parent, false));
            }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
            String name[] = mPictures.get(position).split("/");
            holder.mTextView.setText(name[name.length - 1]);
            Glide.with(mContext).load(mPictures.get(position)).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(holder.mImage);
            holder.mCheckBox.setOnCheckedChangeListener(null);
            holder.mCheckBox.setChecked(mChecked.get(position));
            holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mChecked.set(holder.getAdapterPosition(), b);
                }
            });

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
            return mPictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mTextView;
        CheckBox mCheckBox;
        ImageView mImage;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextViewTypeface) v.findViewById(R.id.dialog_item_name);
            mCheckBox = (CheckBox) v.findViewById(R.id.dialog_item_checkbox);
            mImage = (ImageView) v.findViewById(R.id.dialog_item_picture);
        }

    }
    public ArrayList<String> getChecked(){
         ArrayList<String> checked = new ArrayList<>();
         for(int i = 0; i < mChecked.size(); ++i){
             if(mChecked.get(i)){
                 checked.add(mPictures.get(i));
             }
         }
        return checked;
    }
    public void update(ArrayList<String> list){
        mChecked.clear();
        mPictures.clear();
        mPictures.addAll(list);
        for(int i = 0; i < mPictures.size(); ++i){
            mChecked.add(false);
        }
        this.notifyDataSetChanged();
    }

}
