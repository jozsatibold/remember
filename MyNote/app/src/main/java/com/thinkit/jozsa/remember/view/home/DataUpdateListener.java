package com.thinkit.jozsa.remember.view.home;



import com.thinkit.jozsa.remember.view.home.card.SelectionListener;

import java.util.List;

public interface DataUpdateListener {

    int getPageNumber();

    void update(int noteId, int to, int from);

    void addNote(int noteID);

    void makeCopy();

    List<Integer> getSelectedList(int type, boolean delete);

    void removeSelection();

    void showPlaceholder();

    void refresh();

    void refreshView();

    List<Integer> getUndoList();

    void remove(int id);

    void setSelection(SelectionListener selection);
}