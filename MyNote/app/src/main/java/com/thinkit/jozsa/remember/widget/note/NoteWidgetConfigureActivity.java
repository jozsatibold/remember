package com.thinkit.jozsa.remember.widget.note;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter;

import java.util.ArrayList;

/**
 * The configuration screen for the {@link NoteWidget NoteWidget} AppWidget.
 */
public class NoteWidgetConfigureActivity extends Activity {

    private static final String PREFS_NAME = "com.thinkit.jozsa.remember.widget.note.NoteWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    NotesAdapter mAdapter;


    public NoteWidgetConfigureActivity() {
        super();
    }

    // Write the prefix to the SharedPreferences object for this widget
    public static void saveTitlePref(Context context, int appWidgetId, int id) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putInt(PREF_PREFIX_KEY + appWidgetId, id);
        prefs.apply();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    static int loadTitlePref(Context context, int appWidgetId) {
        return context.getSharedPreferences(PREFS_NAME, 0).getInt(PREF_PREFIX_KEY + appWidgetId, -1);
    }


    public static void deleteTitlePref(Context context, int appWidgetId) {
        DatabaseManager db = DatabaseManager.getInstance(context);
        ArrayList<NoteLite> notes = db.getSortedCardNotes(0, 0, -1);
        for (NoteLite nt : notes) {
            Note cnt = db.getNote(nt.getID());
            if (cnt.getWidgets().contains(":" + appWidgetId)) {
                cnt.setWidgets(cnt.getWidgets().replaceAll("|:" + appWidgetId, ""));
                DatabaseManager.getInstance(context).updateNote(cnt,1, 0);
                break;
            }
        }

        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setResult(RESULT_CANCELED);

        setContentView(R.layout.note_widget_configure);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.note_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mAdapter = new NotesAdapter(true, 0, -1, this, 0);
        mAdapter.setNoteClickListener(new NoteClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                final Context context = NoteWidgetConfigureActivity.this;

                // When the button is clicked, store the string locally

                saveTitlePref(context, mAppWidgetId, mAdapter.getItemAtPosition(pos).getID());

                // It is the responsibility of the configuration activity to update the app widget
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                NoteWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

                // Make sure we pass back the original appWidgetId
                Intent resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                setResult(RESULT_OK, resultValue);

                finish();
            }

            @Override
            public boolean onNoteLongClicked(int position) {
                return false;
            }
        });
        recyclerView.setAdapter(mAdapter);
        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        mAppWidgetId = 0;
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
    }
}

