package com.thinkit.jozsa.remember.view.notes;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.Note;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;

import java.util.ArrayList;
import java.util.List;


class CalendarAndShare {
    private Context mContext;
    private Note mNote;

    CalendarAndShare(Context context, Note note) {
        this.mContext = context;
        this.mNote = note;
    }

    void setCalendar() {
        Intent calIntent = new Intent(Intent.ACTION_INSERT);
        calIntent.setType("vnd.android.cursor.item/event");
        calIntent.putExtra(CalendarContract.Events.TITLE, mNote.getTitle());
        String description = "";
        if (!mNote.getText().isEmpty()) {
            description += mNote.getText().getText() + "\n";
        }
        List<NoteChecked> list = mNote.getList();
        if (list != null) {
            for (NoteChecked ch : list) {
                description += "\n" + (ch.getChecked() ? "[x]" : "[ ]") + ch.getLabel();
            }
        }
        if (!description.isEmpty()) {
            calIntent.putExtra(CalendarContract.Events.DESCRIPTION, description);
        }

        if (mNote.getReminder().startsWith("T")) {
            try {
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(mNote.getReminder().substring(1))).getTimeInMillis());
            } catch (Exception e) {
                Log.e("Date format", e.toString());
            }
        }

        ContextCompat.startActivities(mContext, new Intent[]{calIntent});
    }

    public void share() {
        String title = "text";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, mNote.getTitle());
        sendIntent.setType("text/plain");
        String text = "";
        if (mNote.getText() != null & mNote.getText().getText().length() > 0) {
            text += mNote.getText().getText() + "\n";

        }
        if (mNote.getList() != null && !mNote.getList().isEmpty()) {
            String description = "";
            List<NoteChecked> list = mNote.getList();
            for (NoteChecked listElement : list) {
                description += (listElement.getChecked() ? "[x] " : "[ ] ") + listElement.getLabel() + "\n";
            }
            text += "\n" + description;
        }
        if (!mNote.getTitle().equals("")) {
            title = mNote.getTitle();
        }
        sendIntent.putExtra(Intent.EXTRA_TEXT, text + "\n\nShared by Remember");
        ContextCompat.startActivities(mContext, new Intent[]{Intent.createChooser(sendIntent, "Share " + title)});
    }

}
