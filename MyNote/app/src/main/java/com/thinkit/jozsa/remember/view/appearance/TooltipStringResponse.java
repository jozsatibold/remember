package com.thinkit.jozsa.remember.view.appearance;


import android.widget.EditText;

public interface TooltipStringResponse {
    void onClickInsert(String result, EditText textView);
}
