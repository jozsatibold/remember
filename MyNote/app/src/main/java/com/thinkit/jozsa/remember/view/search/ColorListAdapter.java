package com.thinkit.jozsa.remember.view.search;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;

import java.util.ArrayList;
import java.util.List;

public class ColorListAdapter extends ArrayAdapter<Integer> {
    private LayoutInflater mLayoutinflater;
    private List<Integer> mColors;
    private int mSelected = 0;
    private Drawable mBlack;
    private Drawable mTransparent;

    public ColorListAdapter(Context context, ArrayList<Integer> customizedListView) {
        super(context, R.layout.item_color, customizedListView);
        mBlack = context.getDrawable(R.drawable.ic_cycle_background);
        mTransparent = context.getDrawable(R.drawable.ic_cycle_background2);
        mLayoutinflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mColors = customizedListView;
    }

    @Override
    public int getCount() {
        return mColors.size();
    }

    public void setSelected(int pos) {
        mSelected = pos;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView view;
        if (convertView == null) {
            convertView = mLayoutinflater.inflate(R.layout.item_color, parent, false);
            view = (ImageView) convertView.findViewById(R.id.color_item);
            convertView.setTag(view);
        } else {
            view = (ImageView) convertView.getTag();
        }

        view.setColorFilter(mColors.get(position));

        if (mSelected == position) {
            view.setBackground(mBlack);
        } else {
            view.setBackground(mTransparent);
        }
        return convertView;
    }
}
