package com.thinkit.jozsa.remember.widget.floating;

import android.animation.ValueAnimator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.widget.WidgetClickActivity;


public class FloatingViewService extends Service {
    private WindowManager mWindowManager;
    private View mFloatingView;
    private View mFloatingRemoveView;
    private boolean mLongClick = false;
    private Point mScreenSize;
    private boolean mScaled = false;
    private View mRemoveView;
    private float mTripleContentPadding;

    public FloatingViewService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Inflate the floating view layout we created
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.widget_floating, null);
        mFloatingRemoveView = LayoutInflater.from(this).inflate(R.layout.widget_floating_remove, null);
        mTripleContentPadding = getResources().getDimension(R.dimen.triple_content_padding);
        mScreenSize = new Point();
        ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(mScreenSize);
        //Add the view to the window.
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        //Specify the view position


        final WindowManager.LayoutParams paramsCancel = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        paramsCancel.gravity = Gravity.BOTTOM | Gravity.CENTER;        //Initially view will be added to bottom corner
        paramsCancel.alpha = 0.0f;

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        if (!(mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_0 || mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_180)) {
            mScreenSize = new Point(mScreenSize.y, mScreenSize.x);
        }
        params.gravity = Gravity.TOP | Gravity.START;        //Initially view will be added to top-left corner
        params.x = 0;
        params.y = !(mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_0 || mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_180) ? mScreenSize.y / 3 : mScreenSize.x / 3;

        mWindowManager.addView(mFloatingView, params);
        mWindowManager.addView(mFloatingRemoveView, paramsCancel);


        View addButton =  mFloatingView.findViewById(R.id.floating_button);
        mRemoveView = mFloatingRemoveView.findViewById(R.id.floating_cancel_remove);

        addButton.setBackgroundTintList(ColorStateList.valueOf(new SettingsPreferences(this, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getInt("StatusBarColor")));

        //Drag and move floating view using user's touch action.
        addButton.setOnTouchListener(new View.OnTouchListener() {

            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            private long time = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;
                        //get the touch location
                        time = System.currentTimeMillis();
                        initialTouchX = event.getRawX() - mFloatingView.getWidth() / 2;
                        initialTouchY = event.getRawY() - mFloatingView.getHeight() / 2;

                        return true;
                    case MotionEvent.ACTION_UP:
                        if (System.currentTimeMillis() - time < 200 && (params.x < 10 || (params.x + +mFloatingView.getWidth()) > (!(mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_0 || mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_180) ? mScreenSize.y : mScreenSize.x) - 30)) {
                            Intent resultIntent = new Intent(FloatingViewService.this, WidgetClickActivity.class);
                            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            //resultIntent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, DatabaseManager.getInstance(FloatingViewService.this).insertNewNote(new Note()));
                            //resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                            //TaskStackBuilder stackBuilder = TaskStackBuilder.create(FloatingViewService.this);
                            //stackBuilder.addParentStack(NoteActivity.class);
                            //stackBuilder.addNextIntent(resultIntent);
                            //startActivities(stackBuilder.getIntents());
                            startActivity(resultIntent);
                            time = 0;
                        }
                        if (mScreenSize != null) {
                            if (mScaled) {
                                mFloatingRemoveView.setScaleX(0.95f);
                                mFloatingRemoveView.setScaleY(0.95f);
                                mScaled = false;
                                paramsCancel.alpha = 0.0f;
                                paramsCancel.height = 0;

                                mWindowManager.updateViewLayout(mFloatingRemoveView, paramsCancel);
                                mLongClick = false;
                                stopSelf();
                            } else {
                                mFloatingView.setScaleX(0.95f);
                                mFloatingView.setScaleY(0.95f);
                                mFloatingView.invalidate();
                                boolean landscape = !(mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_0 || mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_180);
                                if (params.x < (landscape ? mScreenSize.y : mScreenSize.x) - mFloatingView.getWidth() && params.x > 0) {
                                    int screensize = !landscape ? mScreenSize.x : mScreenSize.y;
                                    final int x = params.x + mFloatingView.getWidth() / 2 > screensize / 2 ? screensize - mFloatingView.getWidth() : 0;
                                    ValueAnimator valueAnimator = ValueAnimator.ofFloat((params.x - mFloatingView.getWidth()) > 0 ? params.x : params.x, x);
                                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                        @Override
                                        public void onAnimationUpdate(ValueAnimator animation) {
                                            params.x = Math.round((float) animation.getAnimatedValue());
                                            mWindowManager.updateViewLayout(mFloatingView, params);
                                        }
                                    });

                                    valueAnimator.setInterpolator(new DecelerateInterpolator());
                                    valueAnimator.setDuration(300);
                                    valueAnimator.start();


                                }
                                //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                                //So that is click event.
                            }
                            paramsCancel.alpha = 0.0f;
                            paramsCancel.height = 0;
                            mWindowManager.updateViewLayout(mFloatingView, params);
                            mWindowManager.updateViewLayout(mFloatingRemoveView, paramsCancel);
                            mLongClick = false;
                        }

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!mLongClick) {
                            paramsCancel.alpha = 1.0f;
                            paramsCancel.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            mWindowManager.updateViewLayout(mFloatingRemoveView, paramsCancel);
                            mLongClick = true;
                        }
                        boolean landscape = !(mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_0 || mWindowManager.getDefaultDisplay().getRotation() == Surface.ROTATION_180);
                        int pointx = (landscape ? mScreenSize.y : mScreenSize.x) / 2 - mRemoveView.getWidth() / 2;
                        int pointy = (landscape ? mScreenSize.x : mScreenSize.y) - (mFloatingRemoveView.getHeight() + Math.round(mTripleContentPadding * 0.5f));
                        if ((params.x > pointx && pointx + mRemoveView.getWidth() / 2 > params.x) && (params.y > pointy && params.y < pointy + mRemoveView.getHeight())) {
                            if (!mScaled) {
                                ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
                                mFloatingRemoveView.setScaleX(1.1f);
                                mFloatingRemoveView.setScaleY(1.1f);
                                mScaled = true;
                            }
                        } else {
                            if (mScaled) {
                                mFloatingRemoveView.setScaleX(0.95f);
                                mFloatingRemoveView.setScaleY(0.95f);
                                mScaled = false;
                            }
                        }
                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) (event.getRawX() - initialTouchX) - mFloatingView.getWidth() / 2;
                        params.y = initialY + (int) (event.getRawY() - initialTouchY) - mFloatingView.getHeight() / 2;

                        //Update the layout with new X & Y coordinate
                        mWindowManager.updateViewLayout(mFloatingView, params);
                        mWindowManager.updateViewLayout(mFloatingRemoveView, paramsCancel);
                        return true;
                }

                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
    }
}
