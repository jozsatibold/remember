package com.thinkit.jozsa.remember.view.appearance;

import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;

import java.util.ArrayList;


public interface LabelRecyclerDialogHelper {
    void addLabel(String label, int drawable);
    void update(Label label);
    ArrayList<Label> getLabels();
}
