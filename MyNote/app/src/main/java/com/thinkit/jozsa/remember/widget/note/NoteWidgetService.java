package com.thinkit.jozsa.remember.widget.note;

import android.content.Intent;
import android.widget.RemoteViewsService;


public class NoteWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new NoteWidgetFactory(this.getApplicationContext(), intent);
    }
}
