package com.thinkit.jozsa.remember.models.note;

import android.os.Parcel;
import android.os.Parcelable;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.noteTypes.NoteNote;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteChecked;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Note implements Parcelable {
    private int mId;
    private int mTypeId;
    private String mTitle;
    private NoteNote mText;
    private List<NoteChecked> mList;
    private List<Picture>  mImages;
    private List<String> mRecord;
    private Calendar mCreateDate;
    private String mLastModification;
    private String mRemainder = "0";
    private long mCategory;
    private int mBgColor;
    private String mWidgets;
    private float mStaticPosition;
    private float mNumberOfView;
    private int mLabel;
    private boolean mSecured;

    public Note() {
        mId = 0;
        mTypeId = 0;
        mTitle = "";
        mText = new NoteNote();
        mList = new ArrayList<>();
        mImages = new ArrayList<>();
        mRecord = new ArrayList<>();
        mCreateDate = new GregorianCalendar();
        mLastModification = DatabaseManager.mFormat.format(new GregorianCalendar().getTime());
        mRemainder = "0";
        mCategory = 0;
        mBgColor = 0;
        mStaticPosition = -1;
        mNumberOfView = 0;
        mLabel = 1;
        mWidgets = "";
        mSecured = false;
    }



    public Note(Note note) {
        mId = note.getID();
        mTitle = note.getTitle();
        mCreateDate = note.getCreateDate();
        mRemainder = note.getReminder();
        mCategory = note.getCategory();
        mText = new NoteNote(note.getText().getText());
        mList = new ArrayList<>(note.getList());
        mImages = new ArrayList<>(note.getImagesList());
        mRecord = new ArrayList<>(note.getRecord());
        mBgColor = note.getBgColor();
        mStaticPosition = note.getStaticPosition();
        mNumberOfView = note.getNumberOfView();
        mLabel = note.getLabel();
        mTypeId = getTypeId();
        mWidgets = note.getWidgets();
        mSecured = note.getSecured();
        mLastModification = note.getLastModification();

    }

    public Note(int noteID, String title, Calendar creatingDate, String lastModification, String reminder, int bgColor, boolean secured, float numberOfView, int nlid, int typeId, float staticPosition, long category, String widgets) {
        this.mId = noteID;
        this.mTitle = title;
        this.mCreateDate = creatingDate;
        this.mLastModification = lastModification;
        this.mRemainder = reminder;
        this.mBgColor = bgColor;
        this.mSecured = secured;
        this.mNumberOfView = numberOfView;
        this.mStaticPosition = staticPosition;
        this.mLabel = nlid;
        this.mTypeId = typeId;
        this.mText = new NoteNote();
        this.mList = new ArrayList<>();
        this.mImages = new ArrayList<>();
        this.mRecord = new ArrayList<>();
        this.mCategory = category;
        this.mWidgets = widgets;
    }
    //staticPosition", "BgColor", "title", "numberOfView", "NLID", "creatingDate", "reminder", "TypeID", "secured", "lastModification", "category
    public Note(int id, int bgColor, String title, int numberOfView, int label, Calendar creatingDate, String reminder, int type, boolean secured, String lasModification, long category, String widgets){
        this.mId = id;
        this.mTitle = title;
        this.mCreateDate = creatingDate;
        this.mLastModification = lasModification;
        this.mRemainder = reminder;
        this.mBgColor = bgColor;
        this.mSecured = secured;
        this.mNumberOfView = numberOfView;
        this.mStaticPosition = 0;
        this.mLabel = label;
        this.mTypeId = type;
        this.mWidgets = widgets;
        this.mText = new NoteNote();
        this.mList = new ArrayList<>();
        this.mImages = new ArrayList<>();
        this.mRecord = new ArrayList<>();
        this.mCategory = category;
    }
    public Note(int id, float staticPosition, int bgColor, String title, float numberOfView, int label, Calendar creatingDate, String reminder, int type, boolean secured, String lastModification, long category, String widgets) {
        this.mId = id;
        this.mTitle = title;
        this.mCreateDate = creatingDate;
        this.mLastModification = lastModification;
        this.mRemainder = reminder;
        this.mBgColor = bgColor;
        this.mSecured = secured;
        this.mNumberOfView = numberOfView;
        this.mStaticPosition = staticPosition;
        this.mLabel = label;
        this.mTypeId = type;
        this.mWidgets = widgets;
        this.mText = new NoteNote();
        this.mList = new ArrayList<>();
        this.mImages = new ArrayList<>();
        this.mRecord = new ArrayList<>();
        this.mCategory = category;
    }

    public Note(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        mText = (NoteNote) in.readValue(NoteNote.class.getClassLoader());
        mList = (ArrayList<NoteChecked>) in.readArrayList(NoteChecked.class.getClassLoader());
        mCreateDate = (Calendar) in.readValue(Calendar.class.getClassLoader());
        mRemainder = in.readString();
        mCategory = in.readLong();
        mBgColor = in.readInt();
        mStaticPosition = in.readFloat();
        mNumberOfView = in.readFloat();
        mImages = (ArrayList<Picture>) in.readValue(Picture.class.getClassLoader());
        mLabel = in.readInt();
        mTypeId = in.readInt();
        mSecured = in.readByte() == 1;
        mRecord = (ArrayList<String>) in.readArrayList(String.class.getClassLoader());
        mLastModification = in.readString();
        mWidgets = in.readString();
    }

    public void setID(int id) {
        mId = id;
    }

    public int getID() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title.length() > 180 ? title.substring(0, 180) : title;

    }

    public String getWidgets() {
        return mWidgets == null ? "" : mWidgets;
    }

    public void setWidgets(String widgets) {
        this.mWidgets= widgets;
    }
    public NoteNote getText() {
        return mText;
    }

    public void setText(String text) {
        if (mText == null) {
            mText = new NoteNote(text);
        } else {
            if (text != null) {
                this.mText.setText(text);
            }
        }
    }

    public void setText(String text, String actionText, boolean scanned) {
        if (mText == null) {
            mText = new NoteNote(text, actionText, scanned);
        } else {
            this.mText.setText(text);
            this.mText.setActionText(actionText);
            this.mText.setScanned(scanned);
        }
    }

    public List<NoteChecked> getList() {
        return mList;
    }

    public void setList(List<NoteChecked> list) {
        if (mList == null) {
            mList = new ArrayList<>(list);
        } else {
            mList.clear();
            this.mList.addAll(list);
        }
    }

    public List<Picture> getImagesList() {
        return mImages;
    }

    public void setImagesList(List<Picture> list) {
        if (mImages == null) {
            mImages = new ArrayList<>(list);
        } else {
            mImages.clear();
            mImages.addAll(list);
        }
    }

    public List<String> getRecord() {
        return mRecord;
    }

    public void setRecord(List<String> record) {
        if(mRecord == null){
            mRecord = new ArrayList<>(record);
        } else {
            mRecord.clear();
            mRecord.addAll(record);
        }
    }

    public Calendar getCreateDate() {
        return mCreateDate;
    }

    public void setCreatingDate(Calendar creatingDate) {
        this.mCreateDate = creatingDate;
    }

    public String getReminder() {
        return mRemainder;
    }

    public void setReminder(String mRemainderDate) {
        this.mRemainder = mRemainderDate;
    }

    public long getCategory() {
        return mCategory;
    }

    public void setCategory(long category) {
        mCategory = category;
    }

    public int getBgColor() {
        return mBgColor;
    }

    public void setBgColor(int color) {
        this.mBgColor = color;
    }

    public float getStaticPosition() {
        return mStaticPosition;
    }

    public void setStaticPosition(float position) {
        this.mStaticPosition = position;
    }

    public int getLabel() {
        return mLabel;
    }

    public void setLabel(int label) {
        this.mLabel = label;
    }

    public int getTypeId() {
        return mTypeId;
    }

    public void setTypeId(int typeId) {
        this.mTypeId = typeId;
    }

    public boolean getSecured() {
        return mSecured;
    }

    public void setSecured(boolean secured) {
        this.mSecured = secured;
    }

    public float getNumberOfView() {
        return mNumberOfView;
    }

    public void setNumberOfView(float number) {
        this.mNumberOfView = number;
    }

    public String getLastModification() {
        return mLastModification;
    }

    public void setLastModification(String lastModification) {
        this.mLastModification = lastModification;
    }

    public List<String> getCardData(){
        List<String> data = new ArrayList<>();
        if(mImages != null && !mImages.isEmpty()) {
            for (int i = 0; i < mImages.size() && i < 5; ++i){
                data.add("1:" + mImages.get(i).getSrc());
            }
        }
        if(mRecord != null && !mRecord.isEmpty()){
            for (int i = 0; i < mRecord.size() && i < 5; ++i){
                data.add("2:" + mRecord.get(i));
            }
        }
        if(!mText.isEmpty()) {
            String tx = mText.getText();
            tx = tx.length() > 5000 ? tx.substring(0, 4995) + " ..." : tx;
            data.add("3:" + tx);
        }
        if(mList != null && !mList.isEmpty()){
            for (int i = 0; i < mList.size() && i < 100; ++i){
                String lb = mList.get(i).getLabel();
                lb = lb.length() > 500 ? lb.substring(0, 495) + " ..." : lb;
                data.add("4:" + lb +":" + (mList.get(i).getChecked()? 1 : 0));
            }
        }
        data.add("S:" + (mSecured ? 1 : 0));
        return data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeValue(mText);
        dest.writeValue(mList);
        dest.writeValue(mCreateDate);
        dest.writeString(mRemainder);
        dest.writeLong(mCategory);
        dest.writeInt(mBgColor);
        dest.writeFloat(mStaticPosition);
        dest.writeFloat(mNumberOfView);
        dest.writeValue(mImages);
        dest.writeInt(mLabel);
        dest.writeInt(mTypeId);
        dest.writeByte(mSecured? 1 : (byte)0);
        dest.writeValue(mRecord);
        dest.writeString(mLastModification);
        dest.writeString(mWidgets);

    }

    @Override
    public String toString() {
        return "Note{" +
                "mId=" + mId +
                ", mTypeId=" + mTypeId +
                ", mTitle='" + mTitle + '\'' +
                ", mText=" + mText +
                ", mList=" + mList +
                ", mImages=" + mImages +
                ", mRecord=" + mRecord +
                ", mCreateDate=" + mCreateDate +
                ", mLastModification='" + mLastModification + '\'' +
                ", mRemainder='" + mRemainder + '\'' +
                ", mCategory=" + mCategory +
                ", mBgColor=" + mBgColor +
                ", mWidgets='" + mWidgets + '\'' +
                ", mStaticPosition=" + mStaticPosition +
                ", mNumberOfView=" + mNumberOfView +
                ", mLabel=" + mLabel +
                ", mSecured='" + mSecured + '\'' +
                '}';
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

}
