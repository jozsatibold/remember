package com.thinkit.jozsa.remember.view.notes.imagelist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;

import java.util.ArrayList;
import java.util.List;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private ArrayList<Picture> mPictures;
    private Context mContext;
    private ToolbarImageClickListener mListener;

    public ImageAdapter(List<Picture> list, Context context) {
        mContext = context;
        mPictures = new ArrayList<>(list);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_picture, parent, false);

        return new ViewHolder(v, mListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(mContext).load(mPictures.get(position).getSrc()).error(R.drawable.ic_not_found).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(holder.mImageView);
    }
    public void setImageClickListener(ToolbarImageClickListener listener) {
        mListener = listener;
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPictures.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;

        public ViewHolder(View v, final ToolbarImageClickListener listener) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onNoteClicked(getAdapterPosition());
                    }
                }
            });
            mImageView = (ImageView) v.findViewById(R.id.recycle_picture);
        }
    }
    public void update(List<Picture> list){
        mPictures.clear();
        mPictures.addAll(list);
        this.notifyDataSetChanged();
    }
}
