package com.thinkit.jozsa.remember.view.appearance;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.thinkit.jozsa.remember.R;

import java.util.ArrayList;
import java.util.List;


public class Colors {
    private Context mContext;

    public Colors(Context context) {
        mContext = context;
    }

    public int getPrimaryTextColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.primary_text_grey);
            case 1:
                return ContextCompat.getColor(mContext, R.color.primary_text_yellow);
            case 2:
                return ContextCompat.getColor(mContext, R.color.primary_text_orange);
            case 3:
                return ContextCompat.getColor(mContext, R.color.primary_text_red);
            case 4:
                return ContextCompat.getColor(mContext, R.color.primary_text_purple);
            case 5:
                return ContextCompat.getColor(mContext, R.color.primary_text_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.primary_text_green);
        }
    }

    public int getSecondaryTextColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.secondary_text_grey);
            case 1:
                return ContextCompat.getColor(mContext, R.color.secondary_text_yellow);
            case 2:
                return ContextCompat.getColor(mContext, R.color.secondary_text_orange);
            case 3:
                return ContextCompat.getColor(mContext, R.color.secondary_text_red);
            case 4:
                return ContextCompat.getColor(mContext, R.color.secondary_text_purple);
            case 5:
                return ContextCompat.getColor(mContext, R.color.secondary_text_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.secondary_text_green);
        }
    }

    public int getBackgroundColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.background_white);
            case 1:
                return ContextCompat.getColor(mContext, R.color.background_grey);
            case 2:
                return ContextCompat.getColor(mContext, R.color.background_yellow);
            case 3:
                return ContextCompat.getColor(mContext, R.color.background_orange);
            case 4:
                return ContextCompat.getColor(mContext, R.color.background_red);
            case 5:
                return ContextCompat.getColor(mContext, R.color.background_purple);
            case 6:
                return ContextCompat.getColor(mContext, R.color.background_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.background_green);
        }
    }

    public int getCardBackgroundColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.card_background_white);
            case 1:
                return ContextCompat.getColor(mContext, R.color.card_background_grey);
            case 2:
                return ContextCompat.getColor(mContext, R.color.card_background_yellow);
            case 3:
                return ContextCompat.getColor(mContext, R.color.card_background_orange);
            case 4:
                return ContextCompat.getColor(mContext, R.color.card_background_red);
            case 5:
                return ContextCompat.getColor(mContext, R.color.card_background_purple);
            case 6:
                return ContextCompat.getColor(mContext, R.color.card_background_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.card_background_green);
        }
    }

    public int getToolbarColor(int col) {
        switch (col) {

            case 0:
                return ContextCompat.getColor(mContext, R.color.toolbar_white);
            case 1:
                return ContextCompat.getColor(mContext, R.color.toolbar_grey);
            case 2:
                return ContextCompat.getColor(mContext, R.color.toolbar_yellow);
            case 3:
                return ContextCompat.getColor(mContext, R.color.toolbar_orange);
            case 4:
                return ContextCompat.getColor(mContext, R.color.toolbar_red);
            case 5:
                return ContextCompat.getColor(mContext, R.color.toolbar_purple);
            case 6:
                return ContextCompat.getColor(mContext, R.color.toolbar_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.toolbar_green);
        }
    }

    public int getStatusBarColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.status_bar_white);
            case 1:
                return ContextCompat.getColor(mContext, R.color.status_bar_grey);
            case 2:
                return ContextCompat.getColor(mContext, R.color.status_bar_yellow);
            case 3:
                return ContextCompat.getColor(mContext, R.color.status_bar_orange);
            case 4:
                return ContextCompat.getColor(mContext, R.color.status_bar_red);
            case 5:
                return ContextCompat.getColor(mContext, R.color.status_bar_purple);
            case 6:
                return ContextCompat.getColor(mContext, R.color.status_bar_blue);
            default:
                return ContextCompat.getColor(mContext, R.color.status_bar_green);
        }
    }

    public int getHeaderColor(int col) {
        int color[] = getColorArray(5);
        for(int i = 0; i < color.length; ++i){
            if(col == color[i]){
                if(i > 0) {
                    return getSecondaryTextColor(i - 1);
                } else {
                    return ContextCompat.getColor(mContext, R.color.transparent_grey);
                }
            }
        }
        return 0;
    }

    public int getColorNumber(int type, int col) {
        int color[] = getColorArray(type);
        for(int i = 0; i < color.length; ++i){
            if(col == color[i]){
                    return i;
            }
        }
        return 0;
    }


    public int getDrawColor(int col) {
        switch (col) {
            case 0:
                return ContextCompat.getColor(mContext, R.color.draw_black);
            case 1:
                return ContextCompat.getColor(mContext, R.color.draw_white);
            case 2:
                return ContextCompat.getColor(mContext, R.color.draw_red);
            case 3:
                return ContextCompat.getColor(mContext, R.color.draw_pink);
            case 4:
                return ContextCompat.getColor(mContext, R.color.draw_purple);
            case 5:
                return ContextCompat.getColor(mContext, R.color.draw_deep_purple);
            case 6:
                return ContextCompat.getColor(mContext, R.color.draw_indigo);
            case 7:
                return ContextCompat.getColor(mContext, R.color.draw_blue);
            case 8:
                return ContextCompat.getColor(mContext, R.color.draw_light_blue);
            case 9:
                return ContextCompat.getColor(mContext, R.color.draw_cyan);
            case 10:
                return ContextCompat.getColor(mContext, R.color.draw_teal);
            case 11:
                return ContextCompat.getColor(mContext, R.color.draw_green);
            case 12:
                return ContextCompat.getColor(mContext, R.color.draw_light_green);
            case 13:
                return ContextCompat.getColor(mContext, R.color.draw_lime);
            case 14:
                return ContextCompat.getColor(mContext, R.color.draw_yellow);
            case 15:
                return ContextCompat.getColor(mContext, R.color.draw_amber);
            case 16:
                return ContextCompat.getColor(mContext, R.color.draw_orange);
            case 17:
                return ContextCompat.getColor(mContext, R.color.draw_deep_orange);
            case 18:
                return ContextCompat.getColor(mContext, R.color.draw_brown);
            case 19:
                return ContextCompat.getColor(mContext, R.color.draw_grey);
            default:
                return ContextCompat.getColor(mContext, R.color.draw_blue_grey);
        }
    }

    public int getTransparentColor() {
        return ContextCompat.getColor(mContext, android.R.color.transparent);
    }

    public int getTransparentGrey() {
        return ContextCompat.getColor(mContext, R.color.transparent_grey);
    }

    public int getTranslucent() {
        return ContextCompat.getColor(mContext, R.color.colorTransparent);
    }

    public List<Integer> getColorList(int value) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            if (value < 2 && i < 7) {
                switch (value) {
                    case 0:
                        list.add(getPrimaryTextColor(i));
                        break;
                    default:
                        list.add(getSecondaryTextColor(i));
                        break;

                }
            } else if (value > 1) {
                switch (value - 2) {
                    case 0:
                        list.add(getBackgroundColor(i));
                        break;
                    case 1:
                        list.add(getCardBackgroundColor(i));
                        break;
                    case 2:
                        list.add(getToolbarColor(i));
                        break;
                    default:
                        list.add(getStatusBarColor(i));
                        break;
                }
            }
        }
        return list;
    }

    public int[] getColorArray(int value) {
        int[] list = new int[value < 2 ? 7 : 8];
        for (int i = 0; i < 8; i++) {
            if (value < 2 && i < 7) {
                switch (value) {
                    case 0:
                        list[i] = getPrimaryTextColor(i);
                        break;
                    default:
                        list[i] = getSecondaryTextColor(i);
                        break;

                }
            } else if (value > 1) {
                switch (value - 2) {
                    case 0:
                        list[i] = getBackgroundColor(i);
                        break;
                    case 1:
                        list[i] = getCardBackgroundColor(i);
                        break;
                    case 2:
                        list[i] = getToolbarColor(i);
                        break;
                    default:
                        list[i] = getStatusBarColor(i);
                        break;
                }
            }
        }
        return list;
    }

    public ArrayList<Integer> getDrawColorList() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 21; i++) {
            list.add(getDrawColor(i));
        }
        return list;
    }
}
