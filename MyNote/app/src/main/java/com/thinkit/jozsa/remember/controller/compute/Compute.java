package com.thinkit.jozsa.remember.controller.compute;


import android.util.Log;
import android.widget.EditText;

import com.thinkit.jozsa.remember.models.other.Result;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Compute {
    private String [] mathFunctions = {"pi", "e", "abs", "acos", "asin", "atan", "ceil", "cos", "cosh", "exp", "floor", "log", "log2", "log10", "sin", "sinh", "sqrt", "tan", "tanh", "signum"};
    private ComputeTextViewListener mResponse;
    private EditText mEditText;
    private Runnable mCounting = new Runnable() {
        @Override
        public void run() {
            String st = "";
            if(!mExpression.isEmpty()) {
                mExpression = mExpression.replaceAll(" ", "");
                mExpression = mExpression.toLowerCase();
                mExpression = mExpression.replaceAll("pi", "3.14159265");


                char[] arr = mExpression.toCharArray();
                for (int i = 0; i < arr.length; ++i) {
                    if (i == 0) {
                        st += operation(arr[i]) ? (arr[i] + " ") : arr[i];
                    } else if (i == arr.length - 1) {
                        st += operation(arr[i]) ? (" " + arr[i]) : arr[i];
                    } else {
                        st += operation(arr[i]) ? (" " + arr[i] + " ") : arr[i];
                    }
                }
                st = getExpression(st);
            }
            if(!st.isEmpty()) {
                st = st.replace(" e ", " 2.71828182845904 ");
                if (st.startsWith("e ")) {
                    st = "2.71828182845904 " + st.substring(2);
                }

                if (st.endsWith(" e")) {
                    st = st.substring(0, st.length() - 2) + " 2.71828182845904";
                }
                try {
                    ExecutorService exec = Executors.newFixedThreadPool(1);
                    Expression e = new ExpressionBuilder(st).build();
                    Future<Double> future = e.evaluateAsync(exec);
                    mResult = new Result();
                    mResult.ok = "";
                    mResult.result = future.get();
                } catch (Exception e) {
                    Log.e("Expression resolver", e.toString());
                    mResult = new Result();
                    if (e.toString().startsWith("net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException: Unknown function or variable")) {
                        mResult.ok =  "Unknown function or variable!";
                    } else if (e.toString().startsWith("java.lang.IllegalArgumentException: Mismatched parentheses detected. Please check the expression")) {
                        mResult.ok = "Mismatched parentheses!";
                    } else {
                        mResult.ok = "Something wrong!";
                    }
                }
            } else {
                mResult = new Result();
                mResult.ok = "Not found expression!";
            }
            Log.d("Comput", "finished");
            mResponse.returnTextView(mEditText);
        }
    };
    private String mExpression;
    private Result mResult;
    public Compute(ComputeTextViewListener response){
        mResponse = response;
    }

    public Result getResult(){
        return mResult;
    }
    //Expression count
    public void getResult(String expression, EditText editView) {
        mExpression = expression;
        mEditText = editView;
        new Thread(mCounting).start();
    }
    private boolean isFunction(String func){
        for(String s: mathFunctions){
            if(s.equals(func)){
                return true;
            }
        }
        return false;
    }
    private String getExpression(String expr){
        String [] st = expr.split(" ");
        String resp = "";
        for(int i = st.length - 1; i >= 0; --i){
            if(st[i].matches("[0-9]{1,13}(\\.[0-9]*)?") || isFunction(st[i]) || (st[i].length() == 1 && operation(st[i].charAt(0)))){
                resp = st[i] + " " + resp;
            } else {
                String s = containNumber(st[i]);
                if (!s.isEmpty()) {
                    st[i] = s;
                    ++i;
                } else {
                    break;
                }
            }
        }
        return resp;
    }
    private String containNumber(String s){
        String st = "";
        boolean ok = true;
        for ( int i = s.length() - 1; i >= 0; --i){
            if(st.matches("[0-9]{1,13}(\\.[0-9]*)?") || st.isEmpty()){
                st = s.charAt(i) + st;
            } else {
                ok = false;
                break;
            }
        }
        if(!ok){
            return st.substring(1);
        }
        return st;
    }
    private boolean operation(char c) {
        char[] operations = {'+', '-', '/', '*', '(', ')', '^'};
        for (char ch : operations) {
            if (ch == c) {
                return true;
            }
        }
        return false;
    }





}
