package com.thinkit.jozsa.remember.view.images;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Picture;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class FullScreenImageAdapter extends PagerAdapter {


    private Activity mActivity;
    private ArrayList<ImageView> mImages;
    private ArrayList<Picture> mImagePath;
   // private LayoutInflater mInflater;
    private Context mContext;
    public FullScreenImageAdapter(Activity activity, List<Picture> imagePaths, Context context) {
        this.mActivity = activity;
        this.mImagePath = new ArrayList<>(imagePaths);
        this.mContext = context;
        mImages = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return this.mImagePath.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        TouchImageView img = new TouchImageView(container.getContext());
        img.setImageBitmap(null);
        if(mImages.size() <= position || mImages.get(position) == null){
            mImages.add(img);
        }
        if(new File(mImagePath.get(position).getSrc().split(":")[1]).exists()) {
            Glide.with(mContext).load(mImagePath.get(position).getSrc()).fitCenter().into(img);
        } else {
            Glide.with(mContext).load(R.drawable.ic_not_found).fitCenter().into(img);
        }
        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        return img;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
         container.removeView((View) object);

    }
    public void refreshItem(int pos){
        if(mImages.get(pos) != null){
            Glide.with(mContext).load(mImagePath.get(pos).getSrc()).fitCenter().into(mImages.get(pos));
        }
    }
    public int removeImage(ViewPager pager, int pos){
        int size = mImagePath.size();
        if(pos + 1 < size - 1){
            pager.setCurrentItem(pos + 1);
        } else  if(pos > 0 && size > 0){
            pager.setCurrentItem(pos - 1);
        }

        mImagePath.remove(pos);
        if(mImages.size() > pos) {
            mImages.get(pos).setVisibility(GONE);
            mImages.remove(pos);
        }
        notifyDataSetChanged();
        return pos;

    }
    public ArrayList<Picture> getList(){
        return mImagePath;
    }
}
