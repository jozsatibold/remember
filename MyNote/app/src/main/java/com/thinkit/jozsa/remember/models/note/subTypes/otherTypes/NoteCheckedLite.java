package com.thinkit.jozsa.remember.models.note.subTypes.otherTypes;


import java.io.Serializable;

public class NoteCheckedLite implements Serializable {
    private String mLabel;
    private Boolean mChecked;

    public NoteCheckedLite(NoteChecked listelement){
        this.mLabel = listelement.getLabel();
        this.mChecked = listelement.getChecked();
    }

    public NoteCheckedLite(String label, boolean checked) {
        this.mLabel = label.length() > 40 ? label.substring(0, 37) + "..." : label;
        this.mChecked = checked;
    }


    public String getLabel() {
        return mLabel.replaceAll("\\s{3,}", " ");
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public Boolean getChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }
}
