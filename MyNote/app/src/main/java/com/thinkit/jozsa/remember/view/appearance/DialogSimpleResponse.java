package com.thinkit.jozsa.remember.view.appearance;


public interface DialogSimpleResponse {
    void dialogAnswer(boolean ok);
}
