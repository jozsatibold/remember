package com.thinkit.jozsa.remember.view.home.tab_archive;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Animations;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.controller.encryption.DataEncrypt;
import com.thinkit.jozsa.remember.view.home.adapters.NoteItemTouchHelperCallback;
import com.thinkit.jozsa.remember.view.home.adapters.SwipeListener;
import com.thinkit.jozsa.remember.view.home.card.CardItemDecorationNormal;
import com.thinkit.jozsa.remember.view.home.DataUpdateListener;
import com.thinkit.jozsa.remember.view.home.MainActivity;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;
import com.thinkit.jozsa.remember.view.notes.NoteActivity;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;
import com.thinkit.jozsa.remember.view.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.List;


public class ArchiveFragment extends Fragment implements DataUpdateListener {
    private View mView;
    private boolean mSelectToDelete = false;
    private RecyclerView mNoteRecycler;
    private NotesAdapter mNoteAdapter;
    private View mPlaceholder;
    private SelectionListener mSelectionListener;
    private boolean mSingle = false;
    private SettingsPreferences mSettings;
    private CardItemDecorationNormal mCardDecoration;
    private SwipeListener mSwipeListener;

    public ArchiveFragment() {
        // Required empty public constructor
    }

    public void setSwipeListener(SwipeListener listener){
        mSwipeListener = listener;
        if(mNoteAdapter != null){
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
    }

    @Override
    public void setSelection(SelectionListener selectionListener) {
        mSelectionListener = selectionListener;
    }

    @Override
    public void removeSelection() {
        mSelectToDelete = false;
        mNoteAdapter.removeSelection();
    }

    @Override
    public List<Integer> getUndoList(){
        return mNoteAdapter.getUndoIds();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSettings = new SettingsPreferences(getContext(), LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        mNoteAdapter = new NotesAdapter(true, 0, -1, this.getContext(), 1);
        mNoteAdapter.setShowListener(new ToolbarImageClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                showPlaceholder();
            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity) context).registerDataUpdateListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) this.getActivity()).unregisterDataUpdateListener(this);
    }

    @Override
    public int getPageNumber() {
        return 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_archive, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;
        if(mSwipeListener != null){
            mNoteAdapter.setSwipeListener(mSwipeListener);
        }
        mNoteAdapter.setNoteClickListener(new NoteClickListener() {
            @Override
            public void onNoteClicked(int pos) {
                if (!mSelectToDelete && pos > -1) {
                    Intent intent = new Intent(getActivity(), NoteActivity.class);
                    intent.putExtra(MainActivity.ACTIVITY_NOTE_ON_CLICK, mNoteAdapter.getItemAtPosition(pos).getID());
                    getActivity().startActivityForResult(intent, MainActivity.ACTIVITY_RESULT_NOTE_CLICK);
                } else {
                    mNoteAdapter.setSelected(pos);
                    if (mNoteAdapter.getNumberSelected() == 0) {
                        mSelectToDelete = false;
                        mSelectionListener.setSelection(-1);
                    }
                }
            }

            @Override
            public boolean onNoteLongClicked(int position) {
                if (!mSelectToDelete) {
                    mSelectToDelete = true;
                    //setMenuItemVisibility(false);
                    mSelectionListener.setSelection(0);
                }
                mNoteAdapter.setSelected(position);
                return true;
            }
        });

        mNoteRecycler = (RecyclerView) view.findViewById(R.id.note_recycler);
        mSingle = mSettings.getBoolean("SingleColumns");
        mCardDecoration = new CardItemDecorationNormal(mSingle);
        mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
        mNoteRecycler.setAdapter(this.mNoteAdapter);
        mNoteRecycler.setAdapter(mNoteAdapter);
        mNoteRecycler.addItemDecoration(mCardDecoration);
        NoteItemTouchHelperCallback callback = new NoteItemTouchHelperCallback(mNoteAdapter);
        callback.setLongPress(false);
        callback.setLeftSwipe(true);
        callback.setRightSwipe(true);
        callback.setSwipe(true);
        new ItemTouchHelper(callback).attachToRecyclerView(mNoteRecycler);
        mNoteRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        mPlaceholder = view.findViewById(R.id.empty_placeholder);

        setVisible();
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void refreshView() {
        if (mSettings == null) {
            mSettings = new SettingsPreferences(this.getContext(), LoadingActivity.PREFERENCE_FILENAME_SETTINGS);
        }
        if (mSingle != mSettings.getBoolean("SingleColumns")) {
            mSingle = !mSingle;
            mCardDecoration.setMultiColumn(mSingle);
            mNoteRecycler.setLayoutManager(mSingle ? new StaggeredGridLayoutManager(1, 1) : new StaggeredGridLayoutManager(2, 1));
            mNoteRecycler.setAdapter(this.mNoteAdapter);
        }
    }

    @Override
    public void refresh() {
        mNoteAdapter.refresh();
    }

    public void setVisible() {
        if (mSettings != null) {
            View mLock = mView.findViewById(R.id.password_view);
            if (mSettings.getBoolean("ArchiveLock")) {
                final TextInputEditText mPassword = (TextInputEditText) mView.findViewById(R.id.splash_edit_text_password);
                mLock.setBackgroundColor(mSettings.getInt("ToolbarColor"));
                mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            try {
                                SettingsPreferences passwordSettings = new SettingsPreferences(ArchiveFragment.this.getContext(), SettingsActivity.PREFERENCE_FILENAME_PASSWORD);
                                if (!passwordSettings.getString(SettingsActivity.PREFERENCE_FILENAME_PASSWORD).equals(DataEncrypt.getMd5(mPassword.getText().toString()))) {
                                    ((TextInputLayout) mView.findViewById(R.id.splash_text_input_layout_password)).setError(getString(R.string.error_password, passwordSettings.getString("Reminder")));
                                } else {
                                    ((TextInputLayout) mView.findViewById(R.id.splash_text_input_layout_password)).setError("");
                                    View viewpass = mView.findViewById(R.id.password_view);
                                    new Animations().slideUpInvisible(viewpass, ArchiveFragment.this.getContext());

                                    InputMethodManager imm = (InputMethodManager) ArchiveFragment.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                }

                            } catch (Exception e) {
                                Log.w("Settings login", e.toString());
                            }
                            handled = true;
                        }
                        return handled;
                    }
                });
                if (mLock.getVisibility() != View.VISIBLE) {
                    mLock.setVisibility(View.VISIBLE);
                    mPassword.setText("");
                }
            } else {
                if (mLock.getVisibility() != View.GONE) {
                    mLock.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void update(int noteID, int to, int from) {
        if (mNoteAdapter != null) {
            if (to == from) {
                mNoteAdapter.updateNote(noteID, getContext());
            } else {
                mNoteAdapter.removeElement(noteID);
                showPlaceholder();
            }
        }
    }


    @Override
    public void makeCopy() {
        mNoteAdapter.makeCopy(getContext());
    }

    @Override
    public ArrayList<Integer> getSelectedList(int id, boolean delete) {
        mSelectToDelete = false;
        showPlaceholder();
        return mNoteAdapter.getSelectedList(id, delete);
    }

    @Override
    public void showPlaceholder() {
        if (mNoteAdapter.getItemCount() == 0) {
            mNoteRecycler.setVisibility(View.GONE);
            mPlaceholder.setVisibility(View.VISIBLE);
        } else {
            mNoteRecycler.setVisibility(View.VISIBLE);
            mPlaceholder.setVisibility(View.GONE);
        }
    }

    @Override
    public void addNote(int noteID) {
        if (mNoteAdapter != null) {
            mNoteAdapter.addNote(noteID);
            showPlaceholder();
        }
    }

    @Override
    public void remove(int id){
        mNoteAdapter.removeNote(id);
    }
}
