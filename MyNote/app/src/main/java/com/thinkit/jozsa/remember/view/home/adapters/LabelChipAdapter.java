package com.thinkit.jozsa.remember.view.home.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.view.appearance.LabelRecyclerDialogHelper;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.home.card.NoteClickListener;
import com.thinkit.jozsa.remember.view.notes.list.ItemTouchHelperAdapter;
import com.thinkit.jozsa.remember.view.notes.soundrecord.OnStartDragListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LabelChipAdapter extends RecyclerView.Adapter<LabelChipAdapter.ViewHolder> implements ItemTouchHelperAdapter, LabelRecyclerDialogHelper {


    private int mSelected;
    private ColorStateList mSelectedColor;
    private ColorStateList mNormalColor;
    private NoteClickListener mListener;
    private int[] mLabelDrawables;
    private ArrayList<Label> mLabels;
    private Context mContext;
    private DatabaseManager mDb;
    private Drawable mAdd, mRemove;
    private int mContentPadding;
    private int mWhite;
    private OnStartDragListener mDragListener;
    private boolean mFull;

    public LabelChipAdapter(Context context, DatabaseManager db, int primary, int label, boolean all) {
        mLabelDrawables = new MyDrawable(context).getAllLabelDrawable();
        mDb = db;
        mLabels = db.getLabels();
        if (all) {
            mLabels.add(0, new Label(-1, "All", 0));
        }
        mFull = all;
        mLabels.add(new Label());
        mLabels.add(new Label());
        mContext = context;
        mSelectedColor = ColorStateList.valueOf(primary);
        mSelected = label;
        mWhite = ContextCompat.getColor(mContext, R.color.draw_white);
        mNormalColor = ColorStateList.valueOf(mWhite);
        mContentPadding = (int) mContext.getResources().getDimension(R.dimen.content_padding);
        mAdd = mContext.getDrawable(R.drawable.ic_add_white_24dp).mutate();
        mRemove = mContext.getDrawable(R.drawable.ic_remove_white_24dp).mutate();
    }

    public void setOnClickListener(NoteClickListener listener) {
        mListener = listener;
    }

    public void setOnStartDragListener(OnStartDragListener dragListener) {
        mDragListener = dragListener;
    }

    @Override
    public LabelChipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chip, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        if (holder.getAdapterPosition() < this.getItemCount() - 2) {
            Label label = mLabels.get(holder.getAdapterPosition());
            holder.mText.setText(label.getName());
            holder.mText.setCompoundDrawablesWithIntrinsicBounds(mContext.getDrawable(mLabelDrawables[label.getDrawable()]).mutate(), null, null, null);
            holder.mText.getCompoundDrawables()[0].setColorFilter(mSelected != label.getId() ? mSelectedColor.getDefaultColor() : mWhite, PorterDuff.Mode.SRC_ATOP);
            holder.mText.setCompoundDrawablePadding(mContentPadding);
            holder.mText.setBackgroundTintList(mSelected == label.getId() ? mSelectedColor : mNormalColor);
            holder.mText.setTextColor(mSelected != label.getId() ? mSelectedColor : mNormalColor);
        } else {
            holder.mText.setText("");
            holder.mText.setCompoundDrawablesWithIntrinsicBounds(holder.getAdapterPosition() == getItemCount() - 1 ? mRemove : mAdd, null, null, null);
            holder.mText.setCompoundDrawablePadding(0);
            holder.mText.setBackgroundTintList(mSelectedColor);
        }
        holder.mBackground.setBackgroundTintList(mSelectedColor);


    }


    @Override
    public int getItemCount() {
        return mLabels.size();
    }

    public void update(Label label) {
        for (int i = mFull ? 1 : 0; i < mLabels.size() - 2; ++i) {
            if (mLabels.get(i).getId() == label.getId()) {
                mLabels.set(i, label);
                mDb.updateLabel(label);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public int setSelected(int pos) {
        if (pos >= 0 && pos < getItemCount() - 2 && mLabels.get(pos).getId() != mSelected) {
            int last = mSelected;
            mSelected = mLabels.get(pos).getId();
            for (int i = 0; i < mLabels.size(); ++i) {
                if (mLabels.get(i).getId() == last) {
                    notifyItemChanged(i);
                    break;
                }
            }
            notifyItemChanged(pos);
            return pos;
        } else if(pos < 0 && pos > getItemCount() - 2){
            for(int i = 0; i < mLabels.size(); ++i){
                if(mLabels.get(i).getId() == 3){
                    mSelected = i;
                    notifyItemChanged(mSelected);
                    if(mListener != null) {
                        mListener.onNoteClicked(i);
                    }
                }
            }
        }
        return 0;
    }

    public int getSelectedPos(int pos) {
        if (pos >= mLabels.size() || pos < 0) {
            return 0;
        }
        return mLabels.get(pos).getId();
    }

    public int getSelectedPos() {
        return mSelected;
    }

    public Label getPosLabel(int pos) {
        if(pos == -1){
            return  mLabels.get(0);
        }
        return mLabels.get(pos);
    }

    public void refresh() {
        List<Label> labels = mDb.getLabels();
        labels.add(0, mLabels.get(0));
        labels.add(new Label());
        labels.add(new Label());
        mLabels.clear();
        mLabels.addAll(labels);
        notifyDataSetChanged();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            if (toPosition < mLabels.size() - 2) {
                for (int i = fromPosition; i < toPosition; i++) {
                    switchElementID(i, i + 1);
                }
            }
        } else {
            if (fromPosition < getItemCount() - 2) {
                for (int i = fromPosition; i > toPosition; i--) {
                    switchElementID(i, i - 1);
                }
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    private void switchElementID(final int from, final int to) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int pos = mLabels.get(from).getPosition();
                mLabels.get(from).setPosition(mLabels.get(to).getPosition());
                mLabels.get(to).setPosition(pos);
                mDb.updateLabel(mLabels.get(from));
                mDb.updateLabel(mLabels.get(to));
                Collections.swap(mLabels, from, to);

            }
        }).start();
    }

    public ArrayList<Label> getLabels() {
        return mLabels;
    }

    public List<Label> getDatabaseLabels() {
        return mLabels.subList(mFull ? 1 : 0, mLabels.size() - 2);
    }

    public void addLabel(String name, int Drawable) {
        mLabels.add(mLabels.size() - 2, new Label(mDb.addLabel(name, Drawable), name, Drawable));
        notifyItemInserted(mLabels.size() - 3);

    }
    public int getOtherLocations(){
      for(int i = 0; i < mLabels.size(); ++i){
          if(mLabels.get(i).getId() == 3){
              return i;
          }
      }
      return -1;
    }
    public void removeLabel(Label labels) {
        for (int i = mFull ? 1 : 0; i < mLabels.size() - 2; ++i) {
            if (mLabels.get(i).getId() == labels.getId()) {
                mLabels.remove(i);
                notifyItemRemoved(i);
                if (mSelected == i) {
                    mSelected = 0;
                    notifyItemChanged(0);
                }
                break;
            }
        }
    }

    public void setColor(int primary) {
        mSelectedColor = ColorStateList.valueOf(primary);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mText;
        View mBackground;
        private boolean moving = false;
        private boolean longClicked = false;
        private long click = 0;

        public ViewHolder(View v) {
            super(v);
            mText = (TextViewTypeface) v.findViewById(R.id.smart_chip_text);
            mBackground = v.findViewById(R.id.smart_chip_background);

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClicked = true;
                    return false;
                }
            });
            v.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            if (getAdapterPosition() < getItemCount() - 2 && getAdapterPosition() > (mFull ? 0 : -1) && longClicked && mDragListener != null && !moving) {
                                moving = true;
                                mDragListener.onStartDrag(ViewHolder.this);
                            }
                            break;
                        case MotionEvent.ACTION_CANCEL:
                            moving = false;
                            longClicked = false;
                            break;

                        case MotionEvent.ACTION_UP:
                            if (mListener != null) {
                                if (getAdapterPosition() < getItemCount() - 2 && getAdapterPosition() > (mFull ? 0 : -1)) {
                                    if (!longClicked) {
                                        if (System.currentTimeMillis() - click < 500) {
                                            mListener.onNoteLongClicked(getAdapterPosition());
                                        }
                                        mListener.onNoteClicked(getAdapterPosition());
                                        click = System.currentTimeMillis();
                                    }
                                } else {
                                    mListener.onNoteClicked(getAdapterPosition());

                                }
                            }
                            moving = false;
                            longClicked = false;
                    }
                    return false;
                }
            });
        }

    }
}

