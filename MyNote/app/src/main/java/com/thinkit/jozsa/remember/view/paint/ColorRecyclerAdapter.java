package com.thinkit.jozsa.remember.view.paint;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.notes.imagelist.ToolbarImageClickListener;

import java.util.ArrayList;
import java.util.List;

public class ColorRecyclerAdapter extends RecyclerView.Adapter<ColorRecyclerAdapter.ViewHolder> {

    private List<Integer> mColors;
    private ToolbarImageClickListener mListener;
    private Context mContext;
    private Drawable mBackground;
    private Drawable mTransparent;
    private int mSelected = 0;

    ColorRecyclerAdapter(Context context) {
        mContext = context;
        mBackground = context.getDrawable(R.drawable.ic_cycle_background).mutate();
        mTransparent = context.getDrawable(R.drawable.ic_cycle_background2).mutate();
        mColors = new ArrayList<>(new Colors(context).getDrawColorList());
    }

    public ColorRecyclerAdapter(Context context, List<Integer> colors) {
        mContext = context;
        mBackground = context.getDrawable(R.drawable.ic_cycle_background).mutate();
        mTransparent = context.getDrawable(R.drawable.ic_cycle_background2).mutate();
        mColors = colors;
    }

    public void setOnClickListener(ToolbarImageClickListener listener) {
        mListener = listener;
    }

    @Override
    public ColorRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_color, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mImage.setBackground(position == mSelected ? mBackground : mTransparent);
        holder.mImage.setImageDrawable(mContext.getDrawable(R.drawable.ic_cycle).mutate());
        holder.mImage.setColorFilter(mColors.get(position));
    }

    public void setSelected(int pos) {
        int p = mSelected;
        mSelected = pos;
        notifyItemChanged(p);
        notifyItemChanged(pos);
    }

    public void setSelectedColor(int color) {
        int pos = 0;
        while (pos < mColors.size() && mColors.get(pos) != color) {
            ++pos;
        }
        if (pos < mColors.size()) {
            int p = mSelected;
            mSelected = pos;
            notifyItemChanged(p);
            notifyItemChanged(pos);
        }
    }


    @Override
    public int getItemCount() {
        return mColors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;

        public ViewHolder(View v, final ToolbarImageClickListener listener) {
            super(v);
            mImage = (ImageView) v.findViewById(R.id.color_item);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onNoteClicked(getAdapterPosition());
                        setSelected(getAdapterPosition());
                    }
                }
            });
        }

    }

    void addColor(int color) {
        mColors.add(0, color);
        this.notifyItemInserted(0);
        setSelected(0);
    }

    public int getPosColor(int i) {
        return mColors.get(i);
    }

}

