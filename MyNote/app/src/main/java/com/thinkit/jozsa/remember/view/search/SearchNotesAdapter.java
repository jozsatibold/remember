package com.thinkit.jozsa.remember.view.search;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.NoteLite;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.NoteCheckedLite;
import com.thinkit.jozsa.remember.view.LoadingActivity;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.home.card.NoteCardView;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter.TIME_FORMAT;
import static com.thinkit.jozsa.remember.view.home.adapters.NotesAdapter.notEmpty;

class SearchNotesAdapter extends RecyclerView.Adapter<SearchNoteViewHolder> {


    private List<NoteLite> mDisplayedNotes;
    private SelectionListener mListener;
    private DatabaseManager mDb;
    private Boolean mArchiveLock;
    private SimpleDateFormat mDateFormat;
    private int[] mColors;
    private Context mContext;
    private LayoutInflater mInflater;
    private int mPrimary, mSecondary;
    private List<Label> mLabels;
    private int mDrawables[];
    SearchNotesAdapter(Context context) {
        mContext = context;
        mDb = DatabaseManager.getInstance(context);
        mDisplayedNotes = mDb.getSortedCardNotes(4, -1, -1);
        mArchiveLock = new SettingsPreferences(context, LoadingActivity.PREFERENCE_FILENAME_SETTINGS).getBoolean("ArchiveLock");
        mColors = new Colors(context).getColorArray(3);
        mDateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        mPrimary = ContextCompat.getColor(context, R.color.primary_text_grey);
        mSecondary = ContextCompat.getColor(context, R.color.secondary_text_grey);
        mInflater = LayoutInflater.from(context);
        mLabels = mDb.getLabels();
        mDrawables = new MyDrawable(context).getAllLabelDrawable();
    }


    public void search(String sqlQuery, long category) {
        mDisplayedNotes.clear();
        mDisplayedNotes = mDb.getQueryNotes(sqlQuery, category);
        notifyDataSetChanged();
    }

    void setNoteClickListener(SelectionListener listener) {
        mListener = listener;
    }


    @Override
    public SearchNoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchNoteViewHolder(new NoteCardView(parent.getContext()), mListener);
    }

    @Override
    public void onBindViewHolder(SearchNoteViewHolder holder, int position) {

        NoteCardView current = (NoteCardView) holder.itemView;
        NoteLite note = mDisplayedNotes.get(holder.getAdapterPosition());
        float textLength = 0;
        boolean secured = note.getSecured();
        current.setCardBackgroundColor(mColors[note.getBgColor()]);


        switch (note.getTypeId()){
            case 0:
                current.mTitle.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, R.drawable.ic_notes_selected_24dp).mutate(), null, null, null);
                break;
            case 1:
                secured = (note.getTypeId() == 1 && mArchiveLock) || secured;
                current.mTitle.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, R.drawable.ic_archive_selected_24dp).mutate(), null, null, null);
                break;
            default:
                current.mTitle.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, R.drawable.ic_delete_black_24dp).mutate(), null, null, null);
                break;
        }
        current.mTitle.getCompoundDrawables()[0].setColorFilter(mPrimary, PorterDuff.Mode.SRC_ATOP);
        current.mTitle.setText(note.getTitle());

        if (!note.getText().isEmpty()) {
            current.mContent.setVisibility(VISIBLE);
            String text = note.getText();
            textLength += (20.0f - text.length() / 10.0f) / 2;
            current.mContent.setText(secured ? text.replaceAll(".", "*") : text);
        } else {
            current.mContent.setVisibility(GONE);
        }
        if (note.getList() != null && !note.getList().isEmpty()) {

            current.mList.setVisibility(VISIBLE);
            current.mList.removeAllViews();
            ArrayList<NoteCheckedLite> list = note.getList();
            int listSize = list.size();
            if (!list.isEmpty()) {
                if (textLength != 0) {
                    textLength += (10.0f - listSize) / 2 + 1;
                    textLength /= 2;
                } else {
                    textLength += (10.0f - listSize) / 2 + 1;
                }
                current.mDots.setVisibility(GONE);
            } else {
                textLength *= 1.3;
                current.mDots.setVisibility(VISIBLE);
                current.mDots.setTextSize(14.0f + textLength);

            }
            int n = 0;
            float listFontSize = 14.0f + textLength;
            for (int i = 0; i < listSize && n < 10; ++i) {
                NoteCheckedLite check = list.get(i);
                if (!check.getChecked()) {
                    TextView checkText = (TextView) mInflater.inflate(R.layout.item_card_list_element_unchecked, null, false);
                    checkText.setTextColor(mSecondary);
                    checkText.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, R.drawable.ic_check_box_unchecked_grey_16dp).mutate(), null, null, null);
                    checkText.getCompoundDrawables()[0].setColorFilter(mSecondary, PorterDuff.Mode.SRC_ATOP);
                    checkText.setText(secured ? check.getLabel().replaceAll(".", "*") : check.getLabel());
                    checkText.setTextSize(listFontSize);
                    current.mList.addView(checkText);
                    ++n;
                }
            }
            for (int i = 0; i < listSize && n < 10; ++i) {
                NoteCheckedLite check = list.get(i);
                if (check.getChecked()) {
                    TextView checkText = (TextView) mInflater.inflate(R.layout.item_card_list_element_unchecked, null, false);
                    checkText.setTextColor(mSecondary);
                    checkText.setPaintFlags(checkText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    checkText.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, R.drawable.ic_check_box_grey_16dp).mutate(), null, null, null);
                    checkText.getCompoundDrawables()[0].setColorFilter(mSecondary, PorterDuff.Mode.SRC_ATOP);
                    checkText.setText(secured ? check.getLabel().replaceAll(".", "*") : check.getLabel());
                    checkText.setTextSize(listFontSize);
                    current.mList.addView(checkText);
                    ++n;
                }
            }
        } else {
            current.mDots.setVisibility(GONE);
            current.mList.setVisibility(GONE);
        }
        current.mContent.setTextSize(14.0f + textLength);
        current.mTitle.setTextSize(16.0f + textLength);
        if (!note.getImage().isEmpty()) {
            current.mImage.setVisibility(VISIBLE);
            Glide.with(mContext).load(secured ? "" : note.getImage()).placeholder(R.drawable.ic_lock_black_96dp).error(R.drawable.ic_not_found).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop().into(current.mImage);
        } else {
            current.mImage.setVisibility(GONE);
        }

        if (!note.getRecord().isEmpty() && note.getRecord().contains("R")) {
            current.mRecord.setVisibility(VISIBLE);
            String recDate = note.getRecord().split("REC_")[1];
            current.mRecord.setText(recDate.substring(0, 4) + "." + recDate.substring(4, 6) + "." + recDate.substring(6, 8));
        } else {
            current.mRecord.setVisibility(GONE);
        }
        char c = note.getReminder().charAt(0);
        if (c == '0') {
            current.mTime.setVisibility(GONE);
        } else if (c == 'T' || c == 't') {
            current.mTime.setVisibility(VISIBLE);
            try {
                String reminder = note.getReminder();
                Calendar calendar = DatabaseManager.DateToCalendar(DatabaseManager.mFormat.parse(reminder.substring(1, reminder.length())));
                Calendar cal = new GregorianCalendar();
                double millis = ((calendar.getTimeInMillis() - cal.getTimeInMillis()) / 86400000.0);
                if (millis >= 2) {
                    current.mTime.setText(mDateFormat.format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));
                } else if (millis >= 1 || (cal.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH) && millis < 1 && millis >= 0)) {
                    current.mTime.setText(mContext.getString(R.string.card_tomorrow, reminder.substring(reminder.length() - 5)));
                } else if (millis >= 0.000000) {
                    current.mTime.setText(mContext.getString(R.string.card_today, reminder.substring(reminder.length() - 5)));
                } else {
                    if (millis > -1 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis > -1 && millis < 0)) {
                        current.mTime.setText(mContext.getString(R.string.card_today, reminder.substring(reminder.length() - 5)));
                    } else {
                        calendar.setTimeInMillis(calendar.getTimeInMillis() - 86400000);
                        if (millis >= -2 || (cal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && millis < -1 && millis >= -2)) {
                            current.mTime.setText(mContext.getString(R.string.card_yesterday, reminder.substring(reminder.length() - 5)));
                        } else {
                            current.mTime.setText(mDateFormat.format(DatabaseManager.mFormat.parse(note.getReminder().substring(1))));
                        }
                    }
                    current.mTime.setPaintFlags(current.mTime.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

            } catch (Exception e) {
                Log.e("Card time", e.toString());
            }
        }
        TextViewTypeface label = (TextViewTypeface)current.findViewById(R.id.note_label);
        label.setVisibility(VISIBLE);
        for(Label lb : mLabels){
            if(lb.getId() == note.getLabel()){
                label.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, mDrawables[lb.getDrawable()]).mutate(), null, null, null);
                label.getCompoundDrawables()[0].setColorFilter(mSecondary, PorterDuff.Mode.SRC_ATOP);
                label.setText(lb.getName());
                break;
            }
        }



    }


    //Getter methods
    @Override
    public int getItemCount() {
        return mDisplayedNotes.size();
    }

    NoteLite getItemAtPosition(int pos) {
        return mDisplayedNotes.get(pos);
    }

    public void updateNote(int id, Context context) {
        int i = 0;
        int size = mDisplayedNotes.size();
        while (size > i && mDisplayedNotes.get(i).getID() != id) {
            ++i;
        }
        NoteLite note = mDb.getCardNote(id, 11);
        if (i < size) {
            mDisplayedNotes.remove(i);
            if (notEmpty(note)) {
                int pos = getPosition(note);
                if (pos == i) {
                    mDisplayedNotes.add(i, note);
                    notifyItemChanged(i);
                } else {
                    notifyItemRemoved(i);
                    if (pos >= mDisplayedNotes.size()) {
                        mDisplayedNotes.add(note);
                        notifyItemInserted(size - 1);
                    } else {
                        mDisplayedNotes.add(pos, note);
                        notifyItemInserted(pos);
                    }
                }
            } else {
                mDb.deleteNote(mDb.getNote(note.getID()), context);
                notifyItemRemoved(i);
            }
        } else {
            if (notEmpty(note)) {
                int pos = getPosition(note);
                if (pos < size) {
                    mDisplayedNotes.add(pos, note);
                    notifyItemInserted(pos);
                } else {
                    mDisplayedNotes.add(note);
                    notifyItemInserted(size);

                }
            } else {
                mDb.deleteNote(mDb.getNote(note.getID()), context);
            }
        }
    }

    private int getPosition(NoteLite note) {
        int i;
        for (i = 0; i < mDisplayedNotes.size(); ++i) {
            int greater;
            double a = mDb.getValue(mDisplayedNotes.get(i));
            double b = mDb.getValue(note);
            if (a > b) {
                greater = -1;
            } else if (a < b) {
                greater = 1;
            } else {
                greater = 0;
            }

            if (greater >= 0) {
                break;
            }
        }
        return i;
    }
}