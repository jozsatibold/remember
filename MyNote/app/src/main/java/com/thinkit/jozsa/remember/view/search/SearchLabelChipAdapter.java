package com.thinkit.jozsa.remember.view.search;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.models.note.subTypes.otherTypes.Label;
import com.thinkit.jozsa.remember.view.appearance.MyDrawable;
import com.thinkit.jozsa.remember.view.appearance.TextViewTypeface;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;

import java.util.ArrayList;
import java.util.List;

class SearchLabelChipAdapter extends RecyclerView.Adapter<SearchLabelChipAdapter.ViewHolder> {


    private int mSelected;
    private int mNormalColor;
    private SelectionListener mListener;
    private int[] mLabelDrawables;
    private ArrayList<Label> mLabels;
    private Context mContext;

    private int mContentPadding;
    private int mWhite;

    SearchLabelChipAdapter(Context context, int normalColor, boolean nightMode) {
        mLabelDrawables = new MyDrawable(context).getAllLabelDrawable();
        mLabels = DatabaseManager.getInstance(context).getLabels();
        mLabels.add(0, new Label(-1, "All", 0));
        mContext = context;
        mNormalColor = normalColor;
        mSelected = 0;
        mWhite = !nightMode ? context.getResources().getColor(R.color.draw_white) : context.getResources().getColor(R.color.draw_black);
        mContentPadding = (int) mContext.getResources().getDimension(R.dimen.content_padding);

    }

    public void setOnClickListener(SelectionListener listener) {
        mListener = listener;
    }

    @Override
    public SearchLabelChipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chip, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mBackground.setBackgroundTintList(ColorStateList.valueOf(mNormalColor));
        holder.mText.setBackgroundTintList(mSelected == holder.getAdapterPosition() ? ColorStateList.valueOf(mNormalColor): ColorStateList.valueOf(mWhite));
        holder.mText.setText(mLabels.get(holder.getAdapterPosition()).getName());
        holder.mText.setCompoundDrawablesWithIntrinsicBounds(getDrawable(mLabels.get(holder.getAdapterPosition()).getDrawable()), null, null, null);
        holder.mText.getCompoundDrawables()[0].setColorFilter(mSelected != holder.getAdapterPosition() ? mNormalColor : mWhite, PorterDuff.Mode.SRC_ATOP);
        holder.mText.setTextColor(mSelected != holder.getAdapterPosition() ? mNormalColor : mWhite);
        holder.mText.setCompoundDrawablePadding(mContentPadding);
    }


    @Override
    public int getItemCount() {
        return mLabels.size();
    }

    public int setSelected(int pos) {
        if (pos != mSelected) {
            int last = mSelected;
            mSelected = pos;
            notifyItemChanged(last);
            notifyItemChanged(mSelected);
            return pos;
        }
        return 0;
    }

    public void refresh() {
        List<Label> labels = DatabaseManager.getInstance(mContext).getLabels();
        ArrayList<Integer> ids = new ArrayList<>();
        boolean mtch;
        for (Label lb : labels) {
            mtch = false;
            for (int i = 1; i < mLabels.size(); ++i) {
                if (mLabels.get(i).getName().equals(lb.getName())) {
                    mtch = true;
                    ids.add(lb.getId());
                    break;
                }
            }
            if (!mtch) {
                mLabels.add(lb);
                ids.add(lb.getId());
                notifyItemInserted(mLabels.size() - 1);
            }
        }
        for (int i = 1; i < mLabels.size(); ++i) {
            mtch = false;
            for (int j : ids) {
                if (mLabels.get(i).getId() == j) {
                    mtch = true;
                    break;
                }
            }
            if (!mtch) {
                if (mSelected == i) {
                    mSelected = 0;
                    notifyItemChanged(0);
                }
                mLabels.remove(i);
                notifyItemRemoved(i);
                --i;
                if (i < 1) {
                    i = 1;
                }
            }
        }
    }

    int getSelectedPos(int pos) {
        if (pos >= mLabels.size() || pos < 0) {
            return 0;
        }
        return mLabels.get(pos).getId();
    }

    int getSelectedPos() {
        return mSelected;
    }

    private Drawable getDrawable(int pos) {
        return mContext.getDrawable(mLabelDrawables[pos]).mutate();


    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextViewTypeface mText;
        View mBackground;

        public ViewHolder(View v, final SelectionListener listener) {
            super(v);
            mText = (TextViewTypeface) v.findViewById(R.id.smart_chip_text);
            mBackground = v.findViewById(R.id.smart_chip_background);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.setSelection(getAdapterPosition());
                        setSelected(getAdapterPosition());
                    }
                }
            });

        }

    }
}

