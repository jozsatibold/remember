package com.thinkit.jozsa.remember.view.home.card;

import android.support.v7.widget.RecyclerView;
import android.view.View;


public class NoteViewHolder extends RecyclerView.ViewHolder {



    public NoteViewHolder(final NoteCardView cardView, final NoteClickListener noteListener) {
        super(cardView);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (noteListener != null) {
                    noteListener.onNoteClicked(getAdapterPosition());
                }
            }
        });

        cardView.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v) {
                noteListener.onNoteLongClicked(getAdapterPosition());
                return true;
            }
        });

    }

}
