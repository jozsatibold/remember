package com.thinkit.jozsa.remember.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.thinkit.jozsa.remember.controller.storage.database.DatabaseManager;
import com.thinkit.jozsa.remember.controller.storage.preference.SettingsPreferences;
import com.thinkit.jozsa.remember.view.appearance.Colors;
import com.thinkit.jozsa.remember.view.home.MainActivity;


public class LoadingActivity extends AppCompatActivity {
    public static final String PREFERENCE_FILENAME_SETTINGS = "Settings";
    public static final String PREFERENCE_STATUS_BAR = "StatusBarColor";
    public static final String PREFERENCE_BACKGROUND = "BackgroundColor";
    public static final String PREFERENCE_COLUMNS = "SingleColumns";
    public static final String PREFERENCE_TOOLBAR = "ToolbarColor";
    public static final String PREFERENCE_LABEL = "SortLabel";

    public static final String PREFERENCE_SECURE_WAITING = "WaitingLock";
    public static final String PREFERENCE_SECURE_APPLICATION = "ApplicationLock";
    public static final String PREFERENCE_SECURE_AUTO_REMOVE = "AutomaticRemove";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsPreferences preference = new SettingsPreferences(this, PREFERENCE_FILENAME_SETTINGS);
        Colors color = new Colors(this);
        preference.setInt(PREFERENCE_BACKGROUND, color.getBackgroundColor(0));
        preference.setInt(PREFERENCE_STATUS_BAR, color.getStatusBarColor(6));
        preference.setInt(PREFERENCE_TOOLBAR, color.getToolbarColor(6));
        preference.setBoolean(PREFERENCE_COLUMNS, false);
        preference.setInt(PREFERENCE_SECURE_WAITING, 5000);
        preference.setBoolean(PREFERENCE_SECURE_APPLICATION, false);
        preference.setBoolean(PREFERENCE_SECURE_AUTO_REMOVE, false);
    }

    @Override
    public void onStart() {
        super.onStart();
       final DatabaseManager db = DatabaseManager.getInstance(LoadingActivity.this);
        /*for (int i = 0; i < 10; ++i) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (LoadingActivity.class) {
                        db.insertNote(RandomDataGenerator.getNote(new Random().nextInt(10)));
                    }
                }
            }).start();
        }*/
        startActivity(new Intent(LoadingActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }
}
