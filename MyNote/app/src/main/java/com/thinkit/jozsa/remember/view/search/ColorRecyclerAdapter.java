
package com.thinkit.jozsa.remember.view.search;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thinkit.jozsa.remember.R;
import com.thinkit.jozsa.remember.view.home.card.SelectionListener;

import java.util.List;

public class ColorRecyclerAdapter extends RecyclerView.Adapter<ColorRecyclerAdapter.ViewHolder> {


    private int mSelected = 0;
    private Drawable mBackground;
    private Drawable mTransparent;
    private Drawable mNothing;
    private SelectionListener mListener;
    private List<Integer> mColors;
    private Context mContext;

    public ColorRecyclerAdapter(Context context, List<Integer> colors) {
        mBackground = context.getDrawable(R.drawable.ic_cycle_background).mutate();
        mTransparent = context.getDrawable(R.drawable.ic_cycle_background2).mutate();
        mColors = colors;
        mColors.add(0, 0);
        mContext = context;
        mNothing = context.getDrawable(R.drawable.ic_cancel_black_25dp).mutate();
    }

    public void setOnClickListener(SelectionListener listener) {
        mListener = listener;
    }

    @Override
    public ColorRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_color, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mImage.setBackground(position == mSelected ? mBackground : mTransparent);

        if (position == 0) {
            holder.mImage.setImageDrawable(mNothing);
            holder.mImage.setColorFilter(0x55000000);
        } else {
            holder.mImage.setImageDrawable(mContext.getDrawable(R.drawable.ic_cycle).mutate());
            holder.mImage.setColorFilter(mColors.get(position));
        }
    }


    @Override
    public int getItemCount() {
        return mColors.size();
    }


    public int setSelected(int pos) {
        if (pos != mSelected) {
            int last = mSelected;
            mSelected = pos;
            notifyItemChanged(last);
            notifyItemChanged(mSelected);
            return pos;
        }
        return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;

        public ViewHolder(View v, final SelectionListener listener) {
            super(v);
            mImage = (ImageView) v.findViewById(R.id.color_item);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.setSelection(getAdapterPosition());
                }
            });
        }

    }
}

