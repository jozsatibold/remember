# README #

This is an Android smart note application.

Version: 0.69

### How do I get set up? ###

* Download the project and run in the Android Studio.
* Minimum Snapdragon 400 and 1 GB RAM
* Minimum 480*800 px display
* Database: SQLite

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Developer: Jozsa Tibold
* Email: jozsatibold@gmail.com or jozsatibold@yahoo.com